package com.product.travel.interfaces;

import com.product.travel.models.User;
import com.product.travel.params.ActivationParams;
import com.product.travel.params.ChangePasswordParams;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface UserRepository {

    void activation(ActivationParams params,  CallBack<User> callBack) throws Exception;

    void getUserInfo( CallBack<User> callBack) throws Exception;

    void updateUserInfo( User user,  CallBack<Boolean> callBack) throws Exception;

    void updateUserInfo( User user, boolean isFromZusam,  CallBack<Boolean> callBack) throws Exception;

    void createPassword( String password,  CallBack<Boolean> callBack) throws Exception;

    void resetPassword( String code,  CallBack<String> callBack) throws Exception;

    void changePassword(ChangePasswordParams params,  CallBack<Boolean> callBack) throws Exception;

    void deletePassword(String password,  CallBack<Boolean> callBack) throws Exception;

    void deleteUser( CallBack<Boolean> callBack) throws Exception;

    void needToUpdateNewBuild(CallBack<String> callBack) throws Exception;
}
