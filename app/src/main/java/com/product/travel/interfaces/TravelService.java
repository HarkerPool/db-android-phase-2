package com.product.travel.interfaces;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.models.Travel;
import com.product.travel.models.TravelStatus;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface TravelService {

    void setTravel(Travel travel);

    void setTravel(TravelDbItem travelDbItem);

    Travel getTravel();

    Travel restoreTravel(Context context);

    void resetTravel();

    void createTravel( Travel travel, CallBack<Travel> callBack);

    void uploadTravelImage(String imageName, String travelId, boolean isLast, CallBack<Boolean> callBack);

    void updateTravelStatus(CallBack<List<TravelStatus>> callBack);

    void getTravelById( String travelId, CallBack<Travel> callBack);

    void deleteTravel( String travelId, CallBack<Boolean> callBack);
    
    void deleteImage(String imageName, CallBack<Boolean> callBack);

    void getEmail(CallBack<ArrayList<NachrichtenItem>> callBack);

    void getEmailByTravel(String travelId, CallBack<NachrichtenItem> callBack);

    void deleteEmail(ArrayList<Integer> email_id, CallBack<Boolean> callBack);

    void updateEmail(int email_id, CallBack<Boolean> callBack);
}
