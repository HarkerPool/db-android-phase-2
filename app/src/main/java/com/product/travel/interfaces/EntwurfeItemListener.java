package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/13/16.
 */
public interface EntwurfeItemListener {
    void onReiseEditListener(int position);
    void onVorschussEditListener(int position);
    void onReiseDeleteListener(int position);
    void onVorschussDeleteListener(int position);
}
