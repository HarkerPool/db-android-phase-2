package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/14/16.
 */
public interface ZielItemListener {
    void onRemoveListener(int index);
    void onTextChangeListener(int index, String city, String street);
}
