package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface CallBack<T> {
    void onSuccess(T result);
    void onError(Throwable t);
}
