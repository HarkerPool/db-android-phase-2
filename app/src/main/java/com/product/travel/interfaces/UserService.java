package com.product.travel.interfaces;

import com.product.travel.params.ActivationParams;
import com.product.travel.params.ChangePasswordParams;
import com.product.travel.models.User;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface UserService {

    void updateUserInfo(User user);

    User getUserInfo();

    void activation(ActivationParams params,  CallBack<User> callBack);

    void getUserInfo( CallBack<User> callBack);

    void updateUserInfo( User user,  CallBack<Boolean> callBack);

    void updateUserInfo( User user, boolean isFromZusam,  CallBack<Boolean> callBack);

    void createPassword( String password,  CallBack<Boolean> callBack);

    void resetPassword( String code,  CallBack<String> callBack);

    void changePassword(ChangePasswordParams params,  CallBack<Boolean> callBack);

    void deletePassword(String password,  CallBack<Boolean> callBack);

    void deleteUser( CallBack<Boolean> callBack);

    void needToUpdateNewBuild(CallBack<String> callBack);
}
