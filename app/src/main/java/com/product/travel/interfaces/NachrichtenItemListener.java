package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/13/16.
 */
public interface NachrichtenItemListener {
    void onNachrichtenItemClickListener(int position);
    void onNachrichtenItemSelectListener(boolean isChecked, int position);
}
