package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/13/16.
 */
public interface BelegItemChangeListener {
    void onCheckboxClickListener(int position);
}
