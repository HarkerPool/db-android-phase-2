package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/14/16.
 */
public interface NebenkostenItemListener {

    void onRemoveListener(int index);
    void onBezeichnungChangeListener(int index, String bezeichnung);
    void onBetragChangeListener(int index, double betrag);
    void onCaptureListener(int index);
    void onReviewListener(int index);
}
