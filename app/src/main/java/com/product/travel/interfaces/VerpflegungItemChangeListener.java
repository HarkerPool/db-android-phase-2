package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/13/16.
 */
public interface VerpflegungItemChangeListener {
    void onItemChangeListener(Enum meal, boolean isChecked, int position);
}
