package com.product.travel.interfaces;

/**
 * Created by HarkerPool on 6/13/16.
 */
public interface AntrageItemListener {
    void onReiseViewInfoListener(int position);
    void onVorschussViewInfoListener(int position);
    void onReiseEditListener(int position);
    void onVorschussEditListener(int position);
    void onReiseDeleteListener(int position);
    void onVorschussDeleteListener(int position);
    void onReiseOpenPdfListener(int position);
    void onVorschussOpenPdfListener(int position);
}
