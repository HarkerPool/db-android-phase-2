package com.product.travel.interfaces;

import com.product.travel.services.CitiesServiceImpl;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface ServiceFactory {

    UserService getUserService();

    TravelService getTravelService();

    CitiesServiceImpl getCityServiceImpl();
}
