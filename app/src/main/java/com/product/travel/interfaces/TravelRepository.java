package com.product.travel.interfaces;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.models.NachrichtenItem;
import com.product.travel.models.Travel;
import com.product.travel.models.TravelStatus;

/**
 * Created by HarkerPool on 6/2/16.
 */
public interface TravelRepository {

    void createTravel( Travel travel, CallBack<Travel> callBack) throws Exception;

    void uploadTravelImage(String imageName, String travelId, boolean isLast, CallBack<Boolean> callBack) throws Exception;

    void updateTravelStatus(CallBack<List<TravelStatus>> callBack) throws Exception;

    void getTravelById( String travelId, CallBack<Travel> callBack) throws Exception;

    void deleteTravel( String travelId, CallBack<Boolean> callBack) throws Exception;

    void deleteImage( String imageName, CallBack<Boolean> callBack) throws Exception;

    void getEmail(CallBack<ArrayList<NachrichtenItem>> callBack) throws Exception;

    void getEmailByTravel(String travelId, CallBack<NachrichtenItem> callBack) throws Exception;

    void deleteEmail(ArrayList<Integer> email_id, CallBack<Boolean> callBack) throws Exception;

    void updateEmail(int email_id, CallBack<Boolean> callBack) throws Exception;
}
