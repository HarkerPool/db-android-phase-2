package com.product.travel.params;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class ActivationParams {
    private String activationCode;
    private String deviceToken;

    public ActivationParams(String activationCode, String deviceToken) {
        this.activationCode = activationCode;
        this.deviceToken = deviceToken;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
