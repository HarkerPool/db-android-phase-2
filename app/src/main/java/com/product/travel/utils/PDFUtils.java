package com.product.travel.utils;

import android.content.Context;
import android.text.TextUtils;

import com.product.travel.R;
import com.product.travel.commons.DBApplication;
import com.product.travel.models.Travel;
import com.product.travel.models.User;

/**
 * Created by HarkerPool on 6/17/16.
 */
public class PDFUtils {

    public static String createReisePDFName(Context context) {
        String fileName = "";
        String personalNo = "";
        String employeeStatus = "";

        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user != null) {
            personalNo = user.getPersonalNo();
            employeeStatus = user.getEmployeeStatus();
        }
        if (TextUtils.isEmpty(personalNo)) {
            personalNo = StorageUtils.getPersonalNo(context);
        }
        if (TextUtils.isEmpty(employeeStatus)) {
            employeeStatus = StorageUtils.getEmployeeStatus(context);
        }

        Travel travel = DBApplication.getServiceFactory().getTravelService().getTravel();

        if (TextUtils.isEmpty(personalNo) || TextUtils.isEmpty(employeeStatus) || travel == null) {
            return fileName;
        }

        int currentVersionNumber = 2;

        if (!TextUtils.isEmpty(travel.getPdfName())) {
            int indexOfSub = travel.getPdfName().lastIndexOf("-");
            int indexOfDot = travel.getPdfName().lastIndexOf(".");
            if (indexOfSub != -1 && indexOfDot != -1) { // has existed the old file name
                String lastSubName = travel.getPdfName().substring(indexOfSub + 1, indexOfDot);
                String oldVersionNumber = "";
                for (int i = 0; i < lastSubName.length(); i++) {
                    if (TextUtils.isDigitsOnly(lastSubName.charAt(i) + "")) {
                        oldVersionNumber += lastSubName.charAt(i);
                    }
                }
                if (!TextUtils.isEmpty(oldVersionNumber)) {
                    currentVersionNumber = Integer.parseInt(oldVersionNumber);
                }
            }
        }
        currentVersionNumber += 1;

        String pdfDate;
        if (TextUtils.isEmpty(travel.getFirstPdfDate())) {
            if (travel.getBeginDate() == null) {
                return fileName;
            }
            pdfDate = DateUtils.convertDateToStringForCreatePDF(travel.getBeginDate());
        } else {
            pdfDate = travel.getFirstPdfDate();
        }

        fileName = 254 + "-" + personalNo.trim() + "-" + pdfDate + "-" + System.currentTimeMillis() + "-";

        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[1])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = fileName + "1.pdf";
            } else {
                if (travel.getPdfName().contains("VS1")) {
                    fileName = "V-" + fileName + "AV1.pdf";
                } else if (travel.getPdfName().contains("AV") || travel.getPdfName().contains("V-")) {
                    fileName = "V-" + fileName + "AV" + currentVersionNumber + ".pdf";
                } else {
                    fileName = fileName + currentVersionNumber + ".pdf";
                }
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[2])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = fileName + "B1.pdf";
            } else {
                if (travel.getPdfName().contains("BVS1")) {
                    fileName = "V-" + fileName + "BAV1.pdf";
                } else if (travel.getPdfName().contains("BAV") || travel.getPdfName().contains("V-")) {
                    fileName = "V-" + fileName + "BAV" + currentVersionNumber + ".pdf";
                } else {
                    fileName = fileName + "B" + currentVersionNumber + ".pdf";
                }
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[3])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = fileName + "L1.pdf";
            } else {
                if (travel.getPdfName().contains("LVS1")) {
                    fileName = "V-" + fileName + "LAV1.pdf";
                } else if (travel.getPdfName().contains("LAV") || travel.getPdfName().contains("V-")) {
                    fileName = "V-" + fileName + "LAV" + currentVersionNumber + ".pdf";
                } else {
                    fileName = fileName + "L" + currentVersionNumber + ".pdf";
                }
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[4])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = fileName + "F1.pdf";
            } else {
                if (travel.getPdfName().contains("FVS1")) {
                    fileName = "V-" + fileName + "FAV1.pdf";
                } else if (travel.getPdfName().contains("FAV") || travel.getPdfName().contains("V-")) {
                    fileName = "V-" + fileName + "FAV" + currentVersionNumber + ".pdf";
                } else {
                    fileName = fileName + "F" + currentVersionNumber + ".pdf";
                }
            }
            return fileName;
        }

        return "";
    }

    public static String createVorschussPDFName(Context context) {
        String fileName = "";
        String personalNo = "";
        String employeeStatus = "";

        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user != null) {
            personalNo = user.getPersonalNo();
            employeeStatus = user.getEmployeeStatus();
        }
        if (TextUtils.isEmpty(personalNo)) {
            personalNo = StorageUtils.getPersonalNo(context);
        }
        if (TextUtils.isEmpty(employeeStatus)) {
            employeeStatus = StorageUtils.getEmployeeStatus(context);
        }

        Travel travel = DBApplication.getServiceFactory().getTravelService().getTravel();

        if (TextUtils.isEmpty(personalNo) || TextUtils.isEmpty(employeeStatus) || travel == null) {
            return fileName;
        }

        if (travel.getBeginDate() == null) {
            return fileName;
        }

        fileName = "-254-" + personalNo.trim() + "-" + DateUtils.convertDateToStringForCreatePDF(travel.getBeginDate()) + "-" + System.currentTimeMillis() + "-";

        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[1])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = "VS" + fileName + "VS1.pdf";
            } else {
                fileName = "V" + fileName + "AV1.pdf";
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[2])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = "VS" + fileName + "BVS1.pdf";
            } else {
                fileName = "V" + fileName + "BAV1.pdf";
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[3])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = "VS" + fileName + "LVS1.pdf";
            } else {
                fileName = "V" + fileName + "LAV1.pdf";
            }
            return fileName;
        }
        if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.contains(context.getResources().getStringArray(R.array.employee_status_array)[4])) {
            if (TextUtils.isEmpty(travel.getPdfName())) {
                fileName = "VS" + fileName + "FVS1.pdf";
            } else {
                fileName = "V" + fileName + "FAV1.pdf";
            }
            return fileName;
        }

        return "";
    }

    /*public static void openPDF(Context context, String travelId) {
        if (TextUtils.isEmpty(travelId)) {
            // show message
            return;
        }

        TravelDbItem item = TravelDbHelper.getTravelById(travelId);
        if (item == null || TextUtils.isEmpty(item.getPdfName())) {
            Toast.makeText(context, "Could not found pdf file.", Toast.LENGTH_LONG).show();
            return;
        }

        String path = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + AppConfigs.PDF_URL + item.getTravelId() + "/" + item.getPdfName();
        // Log.i("openPdf", "path: " + path);

//        String path = "https://docs.google.com/viewer?url=" + AppConfigs.PDF_URL + item.getTravelId() + "/" + item.getPdfName();
        Intent intent = new Intent(context, OpenInternalWebViewActivity.class);
        intent.putExtra(AppConfigs.TITLE_URL, "");
        intent.putExtra(AppConfigs.INTERNAL_URL_KEY, path);
        context.startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }*/
}
