package com.product.travel.utils;

import java.io.FileInputStream;
import java.security.MessageDigest;

/**
 * Created by HarkerPool on 7/20/16.
 */
public class SecurityUtils {

    private static char[] hexDigits = "0123456789abcdef".toCharArray();

    public static String getMd5(FileInputStream fis) {
        if (fis == null) {
            return "";
        }

        String md5 = "";

        try {
            int read;
            byte[] bytes = new byte[4096];
            MessageDigest digest = MessageDigest.getInstance("MD5");

            while ((read = fis.read(bytes)) != -1) {
                digest.update(bytes, 0, read);
            }

            byte[] messageDigest = digest.digest();
            StringBuilder sb = new StringBuilder(32);

            for (byte b : messageDigest) {
                sb.append(hexDigits[(b >> 4) & 0x0f]);
                sb.append(hexDigits[b & 0x0f]);
            }

            md5 = sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return md5;
    }
}
