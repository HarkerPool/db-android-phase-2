package com.product.travel.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.product.travel.R;

/**
 * Created by HarkerPool on 6/5/16.
 */
public class AlertUtils {

    public static void showMessageAlert(Context context, String message) {
        showMessageAlert(context, context.getString(R.string.notification), message, null, false);
    }

    public static void showMessageAlert(Context context, String message, boolean has2Button) {
        showMessageAlert(context, context.getString(R.string.notification), message, null, has2Button);
    }

    public static void showMessageAlert(Context context, String title, String message) {
        showMessageAlert(context, title, message, null, false);
    }

    public static void showMessageAlert(final Context context, String title, String message, String notice) {
        showMessageAlert(context, title, message, notice, true);
    }

    public static void showMessageAlert(final Context context, String title, String message, boolean has2Button) {
        showMessageAlert(context, title, message, null, has2Button);
    }

    public static void showMessageAlert(final Context context, String title, String message, final String notice, boolean has2Button) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(title);
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(message);
        dialog.show();

        if (has2Button) {
            dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

            dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (!TextUtils.isEmpty(notice)) {
                        ((Activity)context).finish();
                        ((Activity)context).overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
                    }
                }
            });

            dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (!TextUtils.isEmpty(notice)) {
                        Toast.makeText(context, notice, Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            dialog.findViewById(R.id.rel_2btn).setVisibility(View.GONE);

            dialog.findViewById(R.id.ibtn_ok).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }
}
