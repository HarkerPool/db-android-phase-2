package com.product.travel.utils;

import android.text.TextUtils;

import com.product.travel.commons.DBApplication;
import com.product.travel.models.HomeAddress;
import com.product.travel.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HarkerPool on 8/1/16.
 */
public class UserUtils {

    public static String convertUserToString() {
        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user == null) {
            return "";
        }
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("last_name", user.getLastName());
            jsonRequest.put("first_name", user.getFirstName());
            jsonRequest.put("personal_no", user.getPersonalNo());
            jsonRequest.put("email", user.getEmail());
            jsonRequest.put("phone", user.getPhoneNumber() == null ? "" : user.getPhoneNumber());
            jsonRequest.put("employee_status", user.getEmployeeStatus());
            jsonRequest.put("company_street", user.getCompanyStreet());
            jsonRequest.put("company_no", user.getCompanyNo());
            jsonRequest.put("company_zip", user.getCompanyZip());
            jsonRequest.put("company_city", user.getCompanyCity());
            jsonRequest.put("country_id", -1);

            if (user.getHomeAddressList() != null && user.getHomeAddressList().size() > 0) {
                JSONArray jArrHomeAddress = new JSONArray();
                JSONObject jHomeAddress1 = new JSONObject();
                jHomeAddress1.put("street", user.getHomeAddressList().get(0).getStreet());
                jHomeAddress1.put("no", user.getHomeAddressList().get(0).getHomeNumber());
                jHomeAddress1.put("zip", user.getHomeAddressList().get(0).getZip());
                jHomeAddress1.put("city", user.getHomeAddressList().get(0).getCity());
                jHomeAddress1.put("country_id", user.getHomeAddressList().get(0).getCountryId());
                jArrHomeAddress.put(jHomeAddress1);

                if (user.getHomeAddressList().size() > 1) {
                    JSONObject jHomeAddress2 = new JSONObject();
                    jHomeAddress2.put("street", user.getHomeAddressList().get(1).getStreet());
                    jHomeAddress2.put("no", user.getHomeAddressList().get(1).getHomeNumber());
                    jHomeAddress2.put("zip", user.getHomeAddressList().get(1).getZip());
                    jHomeAddress2.put("city", user.getHomeAddressList().get(1).getCity());
                    jHomeAddress2.put("country_id", user.getHomeAddressList().get(1).getCountryId());
                    jArrHomeAddress.put(jHomeAddress2);
                }

                jsonRequest.put("home_address", jArrHomeAddress);
            }

            return jsonRequest.toString();
        } catch (Exception ex) {
            ex.printStackTrace();

            return "";
        }
    }

    public static User convertStringToUser(String sUser) {
        if (TextUtils.isEmpty(sUser)) {
            return null;
        }

        try {
            JSONObject data = new JSONObject(sUser);

            User user = new User();

            user.setLastName(data.getString("last_name"));
            user.setFirstName(data.getString("first_name"));
            user.setPersonalNo(data.getString("personal_no"));
            user.setEmail(data.getString("email"));

            String phone = "";
            if (data.has("phone") && !data.getString("phone").equals("null")) {
                phone = data.getString("phone");
            }
            user.setPhoneNumber(phone);

            user.setEmployeeStatus(data.getString("employee_status"));
            user.setCompanyStreet(data.getString("company_street"));
            user.setCompanyNo(data.getString("company_no"));
            user.setCompanyZip(data.getString("company_zip"));
            user.setCompanyCity(data.getString("company_city"));
            user.setCountryId(-1);

            List<HomeAddress> homeAddressList = new ArrayList<>();
            if (data.has("home_address")) {
                JSONArray jArrHomeAddress = data.getJSONArray("home_address");

                JSONObject jsonObject;
                for (int i = 0; i < jArrHomeAddress.length(); i++) {
                    jsonObject = jArrHomeAddress.getJSONObject(i);

                    HomeAddress element = new HomeAddress(jsonObject.getString("street"), jsonObject.getString("no"),
                            jsonObject.getString("zip"), jsonObject.getString("city"), jsonObject.getInt("country_id"));

                    homeAddressList.add(element);
                }
            }
            user.setHomeAddressList(homeAddressList);

            return user;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }
}
