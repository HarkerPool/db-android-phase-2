package com.product.travel.utils;

import android.os.Environment;
import android.util.Log;

import com.product.travel.commons.AppConfigs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by HarkerPool on 7/25/16.
 */
public class DownloaderUtils {

    private static final int MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory) {
        try {
            Log.i("downloadFile", "fileUrl: " + fileUrl + ", directory: " + directory);

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadPdf(String travelId, String pdfName, String pdfMd5) {
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File folder = new File(path, "pdf");
            folder.mkdirs();

            File pdfFile = new File(folder, pdfName);

            if (!pdfFile.exists() || pdfFile.length() == 0) {

                pdfFile.createNewFile();

                String url = AppConfigs.PDF_URL + travelId + "/" + pdfName;
                downloadFile(url, pdfFile);
            } else {
                String pdfPath = folder + "/" + pdfName;

                FileInputStream fis = new FileInputStream(pdfPath);
                String md5 = SecurityUtils.getMd5(fis);
                if (!md5.equalsIgnoreCase(pdfMd5)) {
                    String url = AppConfigs.PDF_URL + travelId + "/" + pdfName;
                    downloadFile(url, pdfFile);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
