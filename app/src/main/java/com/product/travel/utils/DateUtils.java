package com.product.travel.utils;

import android.text.TextUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by HarkerPool on 6/9/16.
 */
public class DateUtils {

//    private static final long DAY = 24 * 60 * 60 *1000;

    public static int daysBetweenDate(Date start, Date end) {

        if (start == null || end == null) {
            return 0;
        }

        Calendar s = Calendar.getInstance();
        s.setTime(start);
        s.set(Calendar.HOUR_OF_DAY, 0);
        s.set(Calendar.MINUTE, 0);
        s.set(Calendar.SECOND, 0);
        s.set(Calendar.MILLISECOND, 0);

        Calendar e = Calendar.getInstance();
        e.setTime(end);
        e.set(Calendar.HOUR_OF_DAY, 0);
        e.set(Calendar.MINUTE, 0);
        e.set(Calendar.SECOND, 0);
        e.set(Calendar.MILLISECOND, 0);

        int count = 0;
        while (s.compareTo(e) < 0) {
            s.add(Calendar.DATE, 1);
            count++;
        }

        return count;

        /*long diff = end.getTime() - start.getTime();
        return (int) (diff / DAY);*/
    }

    public static String convertDateToString(Date date) {
        if (date == null) {
            return "";
        }

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
        return df.format(date);
    }

    public static String convertDateToStringForCreatePDF(Date date) {
        if (date == null) {
            return "";
        }

        DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.GERMAN);
        return df.format(date);
    }

    public static Date convertStringToDate(String date) {
        if (TextUtils.isEmpty(date)) {
            return null;
        }
//        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.GERMAN);
        DateFormat df1 = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMAN);
        DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
        DateFormat df3 = new SimpleDateFormat("ddMMyyyy", Locale.GERMAN);
        Date result;
        try {
            result = df1.parse(date);
        } catch (ParseException e1) {
            e1.printStackTrace();

            try {
                result = df2.parse(date);
            } catch (ParseException e2) {
                e2.printStackTrace();

                try {
                    result = df3.parse(date);
                } catch (ParseException e3) {
                    e3.printStackTrace();

                    result = null;
                }
            }
        }

        Log.i("convertStringToDate", result == null ? "" : result.toString());
        return result;
    }

    public static Date removeTime(Date date) {
        if (date == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static String getCurrentDateTime() {
        Calendar cal = Calendar.getInstance();
        return new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMAN).format(cal.getTime());
    }
}
