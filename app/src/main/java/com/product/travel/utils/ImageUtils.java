package com.product.travel.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.product.travel.commons.DBApplication;

/**
 * Created by HarkerPool on 6/9/16.
 */
public class ImageUtils {

    public static Uri getImageFileUri() {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DbReisekosten");
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
                Log.d("DbReisekosten", "failed to create directory");
            }
            // Return the file target for the photo based on filename
            return Uri.fromFile(new File(mediaStorageDir.getPath() + File.separator + "IMG_DbReisekosten.jpg"));
        }
        return null;
    }

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public static boolean storageBitmapImage(Bitmap bitmap, String path) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        try {
            File f = new File(path);
            f.getParentFile().mkdirs();
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getImagePath(Context context) {
//        Log.e("Image Path dataDir", context.getApplicationInfo().dataDir + "/images/");
//        Log.e("Image Path", "getAbsolutePath:" + Environment.getExternalStorageDirectory().getAbsolutePath());
        return context.getApplicationInfo().dataDir + "/images/";
    }

    public static Bitmap getBitmap(Context context, Uri uri) throws IOException {
        if (uri == null) {
            return null;
        }

        System.gc();
        Bitmap bitmap = null;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        try {
            if (bitmap == null || bitmap.getWidth() == 0 || bitmap.getHeight() == 0) {
                Thread.sleep(1000);
                ContentResolver cr = context.getContentResolver();
                InputStream in = cr.openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(in, null, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            if (bitmap == null || bitmap.getWidth() == 0 || bitmap.getHeight() == 0) {
                Thread.sleep(1000);
                ContentResolver cr = context.getContentResolver();
                InputStream in = cr.openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(in, null, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bitmap;
    }

    public static void scaleAndStorageCacheImage(Context context, String imageName) {
        try {
            if (!TextUtils.isEmpty(imageName)) {
                if (imageName.contains("uploads")) {
                    return;
                }
                Bitmap bitmap = BitmapFactory.decodeFile(ImageUtils.getImagePath(context) + imageName);
                bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);

                if (DBApplication.getBitmapCache().getBitmap(imageName) == null) {
                    DBApplication.getBitmapCache().putBitmap(imageName, bitmap);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static boolean hasImageExist(Context context, String imageName) {
        try {
            if (!TextUtils.isEmpty(imageName)) {
                File imageFile = new File(ImageUtils.getImagePath(context), imageName);

                return ! (!imageFile.exists() || imageFile.length() == 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }
}
