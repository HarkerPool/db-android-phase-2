package com.product.travel.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.product.travel.R;

/**
 * Created by HarkerPool on 6/7/16.
 */
public class ValidationUtils {

    public static boolean isValidEmail(String email) {
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return false;
        }

        return true;
    }

    public static boolean isValidPassword(String password) {
        if (TextUtils.isEmpty(password) || password.length() < 8) {
            return false;
        }
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(password);
        return m.find();
//        if (b)
//            System.out.println("There is a special character in my string ");
//        else
//            System.out.println("There is no special char.");

//        return true;
    }

    /**
     *
     * @param beginDate
     * @param endDate
     * @param isTravel = true if is Travel, false - Vorschuss
     * @return
     */
    public static boolean isValidDate(Context context, Date beginDate, Date endDate, boolean isTravel) {
        Date currentDate = new Date();
        if (isTravel) { // endDate <= currentDate
            /*if (endDate.compareTo(currentDate) > 0) {
                Toast.makeText(context, context.getString(R.string.date_travel_must_past_msg), Toast.LENGTH_LONG).show();
                return false;
            }*/
        } else { // beginDate >= currentDate
            if (beginDate.compareTo(currentDate) < 0) {
                Toast.makeText(context, context.getString(R.string.date_vorschuss_must_future_msg), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        int i = endDate.compareTo(beginDate);
        if (i < 0) {
            Toast.makeText(context, context.getString(R.string.begin_date_smaller_than_end_date_msg), Toast.LENGTH_LONG).show();
            return false;
        } else if (i == 0) {
            Toast.makeText(context, context.getString(R.string.date_past_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
