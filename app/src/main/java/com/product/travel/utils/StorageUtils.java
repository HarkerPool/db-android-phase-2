package com.product.travel.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import com.product.travel.models.Travel;

/**
 * Created by HarkerPool on 6/17/16.
 */
public class StorageUtils {

    private static final int SHARE_PREFERENCES_MODE = Context.MODE_PRIVATE;
    private static final String SHARE_PREFERENCES_TOKEN_KEY = "SHARE_PREFERENCES_TOKEN_KEY";

    private static String TOKEN_KEY = "TOKEN_KEY";
    private static String TRAVEL_ID_KEY = "TRAVEL_ID_KEY";
    private static String ZIEL_START_ORT_KEY = "ZIEL_START_ORT_KEY";
    private static String ZIEL_START_STREET_KEY = "ZIEL_START_STREET_KEY";
    private static String BEGIN_CITY_STAR_KEY = "BEGIN_CITY_STAR_KEY";
//    private static String END_CITY_STAR_KEY = "END_CITY_STAR_KEY";
    private static String ACTIVATION_CODE_KEY = "ACTIVATION_CODE_KEY";
    private static String PERSONAL_NO_KEY = "PERSONAL_NO_KEY";
    private static String EMPLOYEE_STATUS_KEY = "EMPLOYEE_STATUS_KEY";
    private static String PASSWORD_KEY = "PASSWORD_KEY";
    private static String MOBILE_STATUS_KEY = "MOBILE_STATUS_KEY";
    private static String DATA_SECURE_STATUS_KEY = "DATA_SECURE_STATUS_KEY";
    private static String UNREAD_EMAIL_KEY = "UNREAD_EMAIL_KEY";
    private static String USER_INFO_KEY = "USER_INFO_KEY";
    private static String VERSION_CODE_KEY = "VERSION_CODE_KEY";
    private static String DELETED_TRAVEL_ID_LIST_KEY = "DELETED_TRAVEL_ID_LIST_KEY";

    private static void storageString(Context context, String key, String value) {
        if (TextUtils.isEmpty(key)) {
            return;
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private static String getString(Context context, String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        return sharedPreferences.getString(key, "");
    }

    public static void storageBoolean(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        return sharedPreferences.getBoolean(key, false);
    }

    public static void storageInteger(Context context, String key, int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInteger(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        return sharedPreferences.getInt(key, 0);
    }

    public static void storageToken(Context context, String token) {
        storageString(context, TOKEN_KEY, token);
    }

    public static String getToken(Context context) {
        return getString(context, TOKEN_KEY);
    }

    public static void storageTravelId(Context context, String travel_id) {
        storageString(context, TRAVEL_ID_KEY, travel_id);
    }

    public static String getTravelId(Context context) {
        return getString(context, TRAVEL_ID_KEY);
    }

    /**
     *
     * @param context
     * @param travelId
     * @param isRemove : true - remove, false - add
     */
    public static void storageDeletedTravelIdList(Context context, String travelId, boolean isRemove) {
        if (TextUtils.isEmpty(travelId)) {
            return;
        }

        String valueList = getDeletedTravelIdList(context);
        if (isRemove) {
            if (!TextUtils.isEmpty(valueList)) {
                if (valueList.contains(travelId)) {
                    valueList = valueList.replaceAll(travelId, "");
                }
                if (valueList.contains(";;;;")) {
                    valueList = valueList.replaceAll(";;;;", ";;");
                }
                if (!TextUtils.isEmpty(valueList) && valueList.charAt(0) == ';') {
                    valueList = valueList.substring(2);
                }
                if (!TextUtils.isEmpty(valueList) && valueList.charAt(valueList.length() - 1) == ';') {
                    valueList = valueList.substring(0, valueList.length() - 2);
                }
                storageString(context, DELETED_TRAVEL_ID_LIST_KEY, valueList);
            }
        } else {
            if (TextUtils.isEmpty(valueList)) {
                storageString(context, DELETED_TRAVEL_ID_LIST_KEY, travelId);
            } else {
                if (!valueList.contains(travelId)) {
                    valueList += ";;" + travelId;
                    storageString(context, DELETED_TRAVEL_ID_LIST_KEY, valueList);
                }
            }
        }
    }

    public static String getDeletedTravelIdList(Context context) {
        return getString(context, DELETED_TRAVEL_ID_LIST_KEY);
    }

    public static void removeTravelId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARE_PREFERENCES_TOKEN_KEY, SHARE_PREFERENCES_MODE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(TRAVEL_ID_KEY);
        editor.apply();
    }

    private static void storageStarsValue(Context context, String key, String value) {
        if (TextUtils.isEmpty(key) || TextUtils.isEmpty(value)) {
            return;
        }

        String stored = getString(context, key);
        if (!TextUtils.isEmpty(stored)) {
            ArrayList<String> valueList = new ArrayList<>(Arrays.asList(value.split(";;")));
            ArrayList<String> storedList = new ArrayList<>(Arrays.asList(stored.split(";;")));
            for (String s : valueList) {
                if (!storedList.contains(s)) {
                    if (storedList.size() >= 10) {
                        storedList.remove(0);
                    }
                    storedList.add(s);
                }
            }
            value = "";
            for (String s : storedList) {
                value += s + ";;";
            }
            value = value.substring(0, value.length() - 2);
        }

        storageString(context, key, value);
    }

    public static void storageStarsValue(Context context, Travel travel) {

        storageStarsValue(context, BEGIN_CITY_STAR_KEY, travel.getBeginCity());
//        storageStarsValue(context, END_CITY_STAR_KEY, travel.getEndCity());

        if (!TextUtils.isEmpty(travel.getZiel())) {
            String city = "";
            String street = "";
            try {
                JSONObject jsonObject = new JSONObject(travel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    city += jsonItem.getString("city") + ";;";
                    street += jsonItem.getString("street") + ";;";
                }

                city = city.substring(0, city.length() - 2);
                street = street.substring(0, street.length() - 2);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            storageStarsValue(context, ZIEL_START_ORT_KEY, city);
            storageStarsValue(context, ZIEL_START_STREET_KEY, street);
        }
    }

    public static String getBeginCityStarsValue(Context context) {
        return getString(context, BEGIN_CITY_STAR_KEY);
    }

    /*public static String getEndCityStarsValue(Context context) {
        return getString(context, END_CITY_STAR_KEY);
    }*/

    public static String getZielCityStarsValue(Context context) {
        return getString(context, ZIEL_START_ORT_KEY);
    }

    public static String getZielStreetStarsValue(Context context) {
        return getString(context, ZIEL_START_STREET_KEY);
    }

    public static void storageActivationCode(Context context, String code) {
        storageString(context, ACTIVATION_CODE_KEY, code);
    }

    public static String getActivationCode(Context context) {
        return getString(context, ACTIVATION_CODE_KEY);
    }

    public static void storagePersonalNo(Context context, String code) {
        storageString(context, PERSONAL_NO_KEY, code);
    }

    public static String getPersonalNo(Context context) {
        return getString(context, PERSONAL_NO_KEY);
    }

    public static void storageEmployeeStatus(Context context, String code) {
        storageString(context, EMPLOYEE_STATUS_KEY, code);
    }

    public static String getEmployeeStatus(Context context) {
        return getString(context, EMPLOYEE_STATUS_KEY);
    }

    public static void storagePassword(Context context, String password) {
        storageString(context, PASSWORD_KEY, password);
    }

    public static String getPassword(Context context) {
        return getString(context, PASSWORD_KEY);
    }

    public static void storageMobileStatus(Context context, boolean status) {
        storageBoolean(context, MOBILE_STATUS_KEY, status);
    }

    public static boolean getMobileStatus(Context context) {
        return getBoolean(context, MOBILE_STATUS_KEY);
    }

    public static void storageDataSecureStatus(Context context, boolean status) {
        storageBoolean(context, DATA_SECURE_STATUS_KEY, status);
    }

    public static boolean getDataSecureStatus(Context context) {
        return getBoolean(context, DATA_SECURE_STATUS_KEY);
    }

    public static void storageUnreadEmail(Context context, int value) {
        storageInteger(context, UNREAD_EMAIL_KEY, value);
    }

    public static int getUnreadEmail(Context context) {
        return getInteger(context, UNREAD_EMAIL_KEY);
    }

    public static void storageUserInfo(Context context, String userInfo) {
        storageString(context, USER_INFO_KEY, userInfo);
    }

    public static String getUserInfo(Context context) {
        return getString(context, USER_INFO_KEY);
    }

    public static void storageVersionCode(Context context, int value) {
        storageInteger(context, VERSION_CODE_KEY, value);
    }

    public static int getVersionCode(Context context) {
        return getInteger(context, VERSION_CODE_KEY);
    }
}
