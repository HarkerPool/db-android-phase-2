package com.product.travel.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.CallBack;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.ValidationUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class SetPasswordActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    TextView mTvTitle;
    Button mBtnNext;
    ImageButton mIBtnBack, mIBtnMenu;
    RadioButton mRdBtnSetPassword, mRdBtnNonSetPassword;
    ImageView mIvLock;
    LinearLayout mLinPetPassword;
    EditText mEdtNewPassword, mEdtNewPasswordRepeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_set_password;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mRdBtnSetPassword = (RadioButton) findViewById(R.id.rbtn_set_password);
        mRdBtnNonSetPassword = (RadioButton) findViewById(R.id.rbtn_non_set_password);
        mIvLock = (ImageView) findViewById(R.id.iv_lock);
        mLinPetPassword = (LinearLayout) findViewById(R.id.lin_set_password);
        mEdtNewPassword = (EditText) findViewById(R.id.edt_password_new);
        mEdtNewPasswordRepeat = (EditText) findViewById(R.id.edt_password_new_repeat);

        mTvTitle.setText(getResources().getString(R.string.set_password_title));
        mBtnNext.setText(getResources().getString(R.string.save));
        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
    }

    private void initEvents() {
        mIBtnBack.setVisibility(View.GONE);
        mIBtnMenu.setVisibility(View.GONE);

        /* set default */
        mRdBtnSetPassword.setChecked(false);
        mRdBtnNonSetPassword.setChecked(false);
        mIvLock.setVisibility(View.VISIBLE);
        mLinPetPassword.setVisibility(View.GONE);

        mBtnNext.setOnClickListener(this);
        mRdBtnSetPassword.setOnClickListener(this);
        mRdBtnNonSetPassword.setOnClickListener(this);
    }

    private void initData() {
        mEdtNewPassword.addTextChangedListener(this);
        mEdtNewPasswordRepeat.addTextChangedListener(this);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed(); // Do nothing
//        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.rbtn_set_password:
                onSetPasswordRadioButtonClick();
                break;
            case R.id.rbtn_non_set_password:
                onNonSetPasswordRadioButtonClick();
                break;
            default:
                break;
        }
    }

    private void onSetPasswordRadioButtonClick() {
        mRdBtnSetPassword.setChecked(true);
        mRdBtnNonSetPassword.setChecked(false);
        mIvLock.setVisibility(View.GONE);
        mLinPetPassword.setVisibility(View.VISIBLE);
        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
    }

    private void onNonSetPasswordRadioButtonClick() {
        mRdBtnSetPassword.setChecked(false);
        mRdBtnNonSetPassword.setChecked(true);
        mIvLock.setVisibility(View.VISIBLE);
        mLinPetPassword.setVisibility(View.GONE);
        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent;
        if (mRdBtnSetPassword.isChecked()) {
            if (isValidate()) {
                new CreatePasswordTask().execute();
            }
        } else if (mRdBtnNonSetPassword.isChecked()) {
            intent = new Intent(SetPasswordActivity.this, Instruction1Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        }
    }

    private boolean isValidate() {
        if (!ValidationUtils.isValidPassword(mEdtNewPassword.getText().toString())) {
            mEdtNewPassword.setError(getResources().getString(R.string.notice_password_length));
            mEdtNewPassword.requestFocus();
            return false;
        }
        if (!ValidationUtils.isValidPassword(mEdtNewPasswordRepeat.getText().toString())) {
            mEdtNewPasswordRepeat.setError(getResources().getString(R.string.notice_password_length));
            mEdtNewPasswordRepeat.requestFocus();
            return false;
        }
        if (!TextUtils.equals(mEdtNewPassword.getText().toString(), mEdtNewPasswordRepeat.getText().toString())) {
            mEdtNewPasswordRepeat.setError(getResources().getString(R.string.notice_password_not_match));
            mEdtNewPasswordRepeat.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        EditText edt;
        if (mEdtNewPassword.isFocused()) {
            edt = mEdtNewPassword;
        } else {
            edt = mEdtNewPasswordRepeat;
        }

        if (s != null && ValidationUtils.isValidPassword(s.toString())) {
            edt.setTextColor(getResources().getColor(R.color.green));
        } else {
            edt.setTextColor(getResources().getColor(R.color.bg_active));
        }
    }

    private void createPassword() {
        updateToken();
        DBApplication.getServiceFactory().getUserService().createPassword(mEdtNewPassword.getText().toString(), new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                StorageUtils.storagePassword(SetPasswordActivity.this, mEdtNewPassword.getText().toString());
                showLoadingDialog(false);

                Intent intent = new Intent(SetPasswordActivity.this, Instruction1Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private class CreatePasswordTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            createPassword();

            return null;
        }
    }
}
