package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class Instruction1Activity extends BaseActivity {

    ImageView mIvInstruction;
    WebView mWvInstruction;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_instruction;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIvInstruction = (ImageView) findViewById(R.id.iv_instruction);
        mWvInstruction = (WebView) findViewById(R.id.wv_instruction);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mWvInstruction.setVerticalScrollBarEnabled(false);
        mWvInstruction.setHorizontalScrollBarEnabled(false);
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void initData() {
        String message = "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "color:red;mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:14.0pt;font-family:Arial;\n" +
                "color:red;mso-ansi-language:DE;mso-bidi-font-weight:bold'>Herzlich willkommen!</span><span\n" +
                "style='font-size:14.0pt;font-family:Arial;color:red'><o:p></o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:4.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'>Bevor es losgeht, hier einige <span\n" +
                "style='color:red'>wichtige Hinweise</span> zur Nutzung der Reisekosten-App:<o:p></o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span style='font-size:4.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<ul style='margin-top:0cm' type=square>\n" +
                " <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><span\n" +
                "     lang=DE style='font-size:11.0pt;font-family:Arial;mso-ansi-language:DE;\n" +
                "     mso-bidi-font-weight:bold'>Sie können Reisen <span style='color:red'>innerhalb\n" +
                "     Deutschlands</span> abrechnen, wenn Ihr <span style='color:red'>Wohnsitz\n" +
                "     in Deutschland</span> <span class=GramE>ist</span></span><span\n" +
                "     style='font-size:11.0pt;font-family:Arial'><o:p></o:p></span></li>\n" +
                " <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><span\n" +
                "     lang=DE style='font-size:11.0pt;font-family:Arial;mso-ansi-language:DE;\n" +
                "     mso-bidi-font-weight:bold'><span\n" +
                "     style='mso-spacerun:yes'>&nbsp;</span>Bitte bei der Belegerfassung\n" +
                "     unbedingt auf die <span style='color:red'>Scan-Qualität und\n" +
                "     Vollständigkeit</span> (Fußzeile bei Hotelrechnungen!) achten</span><span\n" +
                "     style='font-size:11.0pt;font-family:Arial'><o:p></o:p></span></li>\n" +
                " <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><span\n" +
                "     lang=DE style='font-size:11.0pt;font-family:Arial;mso-ansi-language:DE;\n" +
                "     mso-bidi-font-weight:bold'>Sie brauchen Unterstützung? <span\n" +
                "     style='color:red'>Kontakt-informationen</span> finden Sie im Menü unter <span\n" +
                "     style='color:red'>Hilfe</span></span><span style='font-size:11.0pt;\n" +
                "     font-family:Arial'><o:p></o:p></span></li>\n" +
                "</ul>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:4.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'>Viel Spaß mit der App!</span><span\n" +
                "style='font-size:11.0pt;font-family:Arial'><o:p></o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'>Ihr DB Personalservice</span>";

        mWvInstruction.loadDataWithBaseURL("file:///android_asset/", message, "text/html", "utf-8", null);
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent = new Intent(Instruction1Activity.this, Instruction2Activity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}
