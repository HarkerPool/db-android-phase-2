package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.adapters.VerpflegungAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.VerpflegungItemChangeListener;
import com.product.travel.models.Travel;
import com.product.travel.models.VerpflegungItem;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.DateUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class VerpflegungActivity extends BaseActivity implements View.OnClickListener, VerpflegungItemChangeListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnCancel, mBtnNext;
    RecyclerView mRecyclerViewMeal;
    VerpflegungAdapter mAdapter;
    private ArrayList<VerpflegungItem> mItemList, mCurrentItemList;
    private Travel mTravel;
    private boolean mIsFromZusam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_verpflegung;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mRecyclerViewMeal = (RecyclerView) findViewById(R.id.lv_meal);

        mTvTitle.setText(getResources().getString(R.string.verpflegung_title));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
    }

    private void initData() {
        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getResources().getString(R.string.next_from_zusam));
        }

        updateItemList();

        mAdapter = new VerpflegungAdapter(mItemList, this);
        mRecyclerViewMeal.setHasFixedSize(true);
        mRecyclerViewMeal.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewMeal.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private ArrayList<VerpflegungItem> getOldItemList() {
        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(VerpflegungActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(VerpflegungActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return null;
        }

        if (!TextUtils.isEmpty(mTravel.getVerpflegung())) {
            try {
                ArrayList<VerpflegungItem> oldItemList = new ArrayList<>();

                JSONObject jsonObject = new JSONObject(mTravel.getVerpflegung());
                JSONArray jsonArray = jsonObject.getJSONArray("verpflegung");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    VerpflegungItem item = new VerpflegungItem();
                    item.setDate(DateUtils.convertStringToDate(jsonItem.get("date").toString()));
                    item.setHasBreakfast(jsonItem.getBoolean("is_breakfast"));
                    item.setHasLunch(jsonItem.getBoolean("is_lunch"));
                    item.setHasDinner(jsonItem.getBoolean("is_dinner"));

                    oldItemList.add(item);
                }

                return oldItemList;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private ArrayList<VerpflegungItem> createNewItemList() {
        try {
            Date beginDate = mTravel.getBeginDate();
            Date endDate = mTravel.getEndDate();

            if (beginDate == null || endDate == null) {
                return null;
            }

            ArrayList<VerpflegungItem> newItemList = new ArrayList<>();

            while (beginDate.compareTo(endDate) <= 0) {
                VerpflegungItem item = new VerpflegungItem();
                item.setDate(beginDate);
                item.setHasBreakfast(false);
                item.setHasLunch(false);
                item.setHasDinner(false);

                newItemList.add(item);

                Calendar c = Calendar.getInstance();
                c.setTime(beginDate);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                c.add(Calendar.DATE, 1);
                beginDate = c.getTime();
            }

            return newItemList;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private void updateItemList() {
        ArrayList<VerpflegungItem> oldItemList = getOldItemList();
        ArrayList<VerpflegungItem> newItemList = createNewItemList();
        if (oldItemList == null || oldItemList.size() == 0) {
            mItemList = newItemList;
        } else {
            if (newItemList == null || newItemList.size() == 0) {
                mItemList = oldItemList;
                return;
            }
            Date oldBeginDate = DateUtils.removeTime(oldItemList.get(0).getDate());
            Date oldEndDate = DateUtils.removeTime(oldItemList.get(oldItemList.size() - 1).getDate());
            Date newBeginDate = DateUtils.removeTime(newItemList.get(0).getDate());
            Date newEndDate = DateUtils.removeTime(newItemList.get(newItemList.size() - 1).getDate());

            try {
                if (oldBeginDate.compareTo(newEndDate) > 0 || oldEndDate.compareTo(newBeginDate) < 0) {
                    mItemList = newItemList;
                } else {
                    int i = 0, j = 0, index;
                    for (index = 0; index < oldItemList.size() || index < newItemList.size(); index++) {
                        oldBeginDate = DateUtils.removeTime(oldItemList.get(i).getDate());
                        newBeginDate = DateUtils.removeTime(newItemList.get(j).getDate());
                        if (oldBeginDate.compareTo(newBeginDate) < 0) {
                            i++;
                        } else if (oldBeginDate.compareTo(newBeginDate) > 0) {
                            j++;
                        } else {
                            newItemList.get(j).setHasBreakfast(oldItemList.get(i).isHasBreakfast());
                            newItemList.get(j).setHasLunch(oldItemList.get(i).isHasLunch());
                            newItemList.get(j).setHasDinner(oldItemList.get(i).isHasDinner());
                            i++;
                            j++;
                        }
                    }
                    mItemList = newItemList;
                }
            } catch (Exception ex) {
                ex.printStackTrace();

                mItemList = newItemList;
            }
        }

        mCurrentItemList = new ArrayList<>();
        for (int i = 0; i < mItemList.size(); i++) {
            mCurrentItemList.add(new VerpflegungItem());
            mCurrentItemList.get(i).setHasBreakfast(mItemList.get(i).isHasBreakfast());
            mCurrentItemList.get(i).setHasLunch(mItemList.get(i).isHasLunch());
            mCurrentItemList.get(i).setHasDinner(mItemList.get(i).isHasDinner());
        }
    }

    private boolean hasModify() {
        for (int i = 0; i < mItemList.size(); i++) {
            if (mCurrentItemList.get(i).isHasBreakfast() != mItemList.get(i).isHasBreakfast()
                    || mCurrentItemList.get(i).isHasLunch() != mItemList.get(i).isHasLunch()
                    || mCurrentItemList.get(i).isHasDinner() != mItemList.get(i).isHasDinner()) {
                return true;
            }
        }

        return false;
    }

    private void getLatestTravelInfo() {
        mTravel.setRadioVerp(false);

        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).isHasBreakfast()
                    || mItemList.get(i).isHasLunch()
                    || mItemList.get(i).isHasDinner()) {
                mTravel.setRadioVerp(true);
                break;
            }
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);

        try {
            for (int index = 0; index < mItemList.size(); index++) {
                JSONObject item = new JSONObject();
                item.put("date", formatter.format(mItemList.get(index).getDate()));
                item.put("is_breakfast", mItemList.get(index).isHasBreakfast());
                item.put("is_lunch", mItemList.get(index).isHasLunch());
                item.put("is_dinner", mItemList.get(index).isHasDinner());

                jsonArray.put(item);
            }

            jsonObject.put("verpflegung", jsonArray);
            mTravel.setVerpflegung(jsonObject.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsFromZusam) {
            Intent intent = new Intent(VerpflegungActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else if (hasModify()) {
            AlertUtils.showMessageAlert(VerpflegungActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(VerpflegungActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(VerpflegungActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);
        }

        if (mIsFromZusam) {
            Intent intent = new Intent(VerpflegungActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            finish();
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
    }

    @Override
    public void onItemChangeListener(Enum meal, boolean isChecked, int position) {
        if (meal == AppConfigs.MEAL.BREAKFAST) {
            mItemList.get(position).setHasBreakfast(isChecked);
        } else if (meal == AppConfigs.MEAL.LUNCH) {
            mItemList.get(position).setHasLunch(isChecked);
        } else if (meal == AppConfigs.MEAL.DINNER) {
            mItemList.get(position).setHasDinner(isChecked);
        }
    }
}
