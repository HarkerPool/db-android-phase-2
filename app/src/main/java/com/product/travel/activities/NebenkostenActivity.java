package com.product.travel.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.NebenkostenItemListener;
import com.product.travel.models.NebenkostenItem;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.ImageUtils;
import com.product.travel.utils.MarshMallowPermission;
import com.product.travel.widget.LayoutNebenkostenItemExt;
import com.product.travel.widget.TouchImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NebenkostenActivity extends BaseActivity implements View.OnClickListener, NebenkostenItemListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnCancel, mBtnNext;

    LinearLayout mLinAddItem, mLinItems;
    ImageButton mIBtnAdd;

    Travel mTravel;
    ArrayList<NebenkostenItem> mItemList, mCurrentItemList;
    private int mCurrentIndex;
    private boolean mIsFromZusam;
    MarshMallowPermission mMarshMallowPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_nebenkosten;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mIBtnAdd = (ImageButton) findViewById(R.id.ibtn_add);
        mLinAddItem = (LinearLayout) findViewById(R.id.lin_add_item);
        mLinItems = (LinearLayout) findViewById(R.id.lin_neben_items);
//        mLinAddItem.removeAllViewsInLayout();

        mTvTitle.setText(getResources().getString(R.string.nebenkosten_sonstiges_title));
    }

    private void initEvents() {
        mLinAddItem.setVisibility(View.GONE);

        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mIBtnAdd.setOnClickListener(this);
    }

    private void initData() {
        mMarshMallowPermission = new MarshMallowPermission(this);

        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getString(R.string.next_from_zusam));
        }

        if (mItemList == null) {
            mItemList = new ArrayList<>();
        } else {
            mItemList.clear();
        }
        if (mCurrentItemList == null) {
            mCurrentItemList = new ArrayList<>();
        } else {
            mCurrentItemList.clear();
        }

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(NebenkostenActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(NebenkostenActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        if (!TextUtils.isEmpty(mTravel.getNeben())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getNeben());
                JSONArray jsonArray = jsonObject.getJSONArray("neben");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    NebenkostenItem item = new NebenkostenItem();
                    item.setBezeichnung(jsonItem.getString("bezeichnung"));
                    item.setBetrag(jsonItem.getDouble("betrag"));

                    if (jsonItem.getString("image").contains("uploads")) {
                        String[] parts = jsonItem.getString("image").split("/");
                        if (ImageUtils.hasImageExist(NebenkostenActivity.this, parts[parts.length - 1])) {
                            item.setImage((parts[parts.length - 1]));
                        } else {
                            item.setImage(jsonItem.getString("image"));
                        }
                    } else {
                        item.setImage(jsonItem.getString("image"));
                    }

                    mItemList.add(item);
                    mCurrentItemList.add(new NebenkostenItem());
                    mCurrentItemList.get(i).setBezeichnung(jsonItem.getString("bezeichnung"));
                    mCurrentItemList.get(i).setBetrag(jsonItem.getDouble("betrag"));
                    mCurrentItemList.get(i).setImage(jsonItem.getString("image"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (mItemList.size() > 0) {
                saveAllImagesToCache();
            }
        }

        /* show all items */
        reloadAllItems();
    }

    private void reloadAllItems() {
        mLinItems.removeAllViewsInLayout();

        /* Add first item if it's null */
        if (mItemList.size() == 0) {
            NebenkostenItem item = new NebenkostenItem();
            mItemList.add(item);
        }

        /* Add all items */
        LayoutNebenkostenItemExt linearItem = new LayoutNebenkostenItemExt(NebenkostenActivity.this, mItemList.get(0), 0, this);
        mLinItems.addView(linearItem);

        for (int i = 1; i < mItemList.size(); i++) {
            LayoutNebenkostenItemExt item = new LayoutNebenkostenItemExt(NebenkostenActivity.this, mItemList.get(i), i, this);
            mLinItems.addView(item);
        }
    }

    private void reviewImageZoom(final int index) {
        final Dialog reviewDialog = new Dialog(NebenkostenActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        reviewDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        reviewDialog.setContentView(R.layout.layout_image_zoom);
        reviewDialog.setCancelable(false);
        reviewDialog.setCanceledOnTouchOutside(false);

        TouchImageView imgZoom = (TouchImageView) reviewDialog.findViewById(R.id.img_zoom);

        final Bitmap bitmap = BitmapFactory.decodeFile(ImageUtils.getImagePath(NebenkostenActivity.this) + mItemList.get(index).getImage());
        if (bitmap != null) {
            imgZoom.setImageBitmap(bitmap);
        } else {
            Picasso.with(NebenkostenActivity.this)
                    .load(AppConfigs.SERVER_URL + mItemList.get(index).getImage())
                    .placeholder(R.drawable.loading_icon)
                    .error(R.drawable.error)
                    .into(imgZoom);
        }
        reviewDialog.show();

        reviewDialog.findViewById(R.id.btnCancel).setBackgroundResource(R.drawable.dustbin);
        reviewDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(NebenkostenActivity.this)
                        .setTitle(getResources().getString(R.string.alert_delete))
                        .setMessage(getResources().getString(R.string.alert_photo))
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

//                                ThreadInBackground.deleteImage(mItemList.get(index).getImage());

                                DBApplication.getBitmapCache().removeBitmap(mItemList.get(index).getImage());
                                mItemList.get(index).setImage(null);

                                /* reload all items */
                                reloadAllItems();

                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                                reviewDialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        });

        reviewDialog.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }

                reviewDialog.dismiss();
            }
        });
    }


    private void saveAllImagesToCache() {
        for (int index = 0; index < mItemList.size(); index++) {
            ImageUtils.scaleAndStorageCacheImage(NebenkostenActivity.this, mItemList.get(index).getImage());
        }
    }

    private void removeAllImagesFromCache() {
        for (int index = 0; index < mItemList.size(); index++) {
            DBApplication.getBitmapCache().removeBitmap(mItemList.get(index).getImage());
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == AppConfigs.REQUEST_CAMERA_CAPTURE) {
                    Intent intent = new Intent(NebenkostenActivity.this, ScanActivity.class);
                    startActivityForResult(intent, AppConfigs.IMAGE_RESULT);
                } else if (requestCode == AppConfigs.IMAGE_RESULT) {
                    String imagePath = data.getStringExtra(AppConfigs.SCANNED_RESULT);
                    if (!TextUtils.isEmpty(imagePath)) {
                        mItemList.get(mCurrentIndex).setImage(imagePath);

                        /* reload all views */
                        reloadAllItems();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        initData();
    }

    @Override
    public void onBackPressed() {
        removeAllImagesFromCache(); // TODO - must release memory, note that maybe it does not back
        if (mIsFromZusam) {
            Intent intent = new Intent(NebenkostenActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            if (hasModify()) {
                AlertUtils.showMessageAlert(NebenkostenActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
            } else {
                super.onBackPressed();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                removeAllImagesFromCache(); // TODO - must release memory, note that maybe it does not go to other page
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.ibtn_add:
                NebenkostenItem item = new NebenkostenItem();
                mItemList.add(item);
                LayoutNebenkostenItemExt linearItem = new LayoutNebenkostenItemExt(NebenkostenActivity.this, mItemList.get(mItemList.size() - 1), mItemList.size() - 1, this);
//                mLinItems.addView(linearItem, mItemList.size() - 1);
                mLinItems.addView(linearItem);
                break;
            default:
                break;
        }
    }

    private boolean isValidate() {
        if (mItemList != null) {
            for (int i = 0; i < mItemList.size(); i++) {
                if (TextUtils.isEmpty(mItemList.get(i).getBezeichnung())) {
                    Toast.makeText(NebenkostenActivity.this, getResources().getString(R.string.neben_bezeichnung_empty_msg), Toast.LENGTH_LONG).show();
                    return false;
                }
                if (mItemList.get(i).getBetrag() <= 0) {
                    Toast.makeText(NebenkostenActivity.this, getResources().getString(R.string.neben_betrag_empty_msg), Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }

        return true;
    }

    private boolean hasModify() {
        if (mCurrentItemList.size() != mItemList.size()) {
            if (mCurrentItemList.size() == 0 && mItemList.size() == 1) {
                if (TextUtils.isEmpty(mItemList.get(0).getBezeichnung())
                        && mItemList.get(0).getBetrag() == 0
                        && TextUtils.isEmpty(mItemList.get(0).getImage())) {
                    return false;
                }
            }
            return true;
        }

        for (int i = 0; i < mItemList.size(); i++) {
            if (!TextUtils.equals(mCurrentItemList.get(i).getBezeichnung(), mItemList.get(i).getBezeichnung())) {
                return true;
            }
            if (mCurrentItemList.get(i).getBetrag() != mItemList.get(i).getBetrag()) {
                return true;
            }
            if (!mCurrentItemList.get(i).getImage().contains(mItemList.get(i).getImage())) {
                return true;
            }
        }

        return false;
    }

    private void getLatestTravelInfo() {
        if (mItemList.size() > 0) {
            mTravel.setRadioBeleg(true); // always Ja (not Nein)

            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            try {
                int i, index;
                for (index = 0; index < mItemList.size(); index++) {
                    JSONObject item = new JSONObject();
                    item.put("bezeichnung", mItemList.get(index).getBezeichnung());
                    item.put("betrag", mItemList.get(index).getBetrag());

                    if (TextUtils.isEmpty(mItemList.get(index).getImage())) {
                        item.put("image", "");
                    } else {
                        for (i = 0; i < mCurrentItemList.size(); i++) {
                            if (mCurrentItemList.get(i).getImage().contains(mItemList.get(index).getImage())) {
                                item.put("image", mCurrentItemList.get(i).getImage());
                                break;
                            }
                        }
                        if (i == mCurrentItemList.size()) {
                            item.put("image", mItemList.get(index).getImage());
                        }
                    }

                    jsonArray.put(item);
                }

                jsonObject.put("neben", jsonArray);
                mTravel.setNeben(jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // TODO - if privater and taxi are null --> mTravel.setRadioBeleg(false);
        }
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(NebenkostenActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(NebenkostenActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }
    
    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            removeAllImagesFromCache();

            if (hasModify()) {
                mTravel.setModify(true);
                getLatestTravelInfo();
                DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                TravelDbHelper.save(mTravel);
            }

            if (mIsFromZusam) {
                Intent intent = new Intent(NebenkostenActivity.this, ZusamActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            } else {
                finish();
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        }
    }

    @Override
    public void onRemoveListener(int index) {
        if (mItemList != null && mItemList.size() > index) {
            mItemList.remove(index);
        }
        if (mLinItems != null) {
            try {
                mLinItems.removeViewAt(index);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onBezeichnungChangeListener(int index, String bezeichnung) {
        if (!TextUtils.isEmpty(bezeichnung) && mItemList.get(index).getBetrag() > 0) {
            mLinAddItem.setVisibility(View.VISIBLE);
        } else {
            mLinAddItem.setVisibility(View.GONE);
        }

        mItemList.get(index).setBezeichnung(bezeichnung);
    }

    @Override
    public void onBetragChangeListener(int index, double betrag) {
        if (!TextUtils.isEmpty(mItemList.get(index).getBezeichnung()) && betrag > 0) {
            mLinAddItem.setVisibility(View.VISIBLE);
        } else {
            mLinAddItem.setVisibility(View.GONE);
        }

        mItemList.get(index).setBetrag(betrag);
    }

    @Override
    public void onCaptureListener(int index) {
        mCurrentIndex = index;

        if (!mMarshMallowPermission.checkPermissionForCamera()) {
            mMarshMallowPermission.requestPermissionForCamera();
        } else {
            if (!mMarshMallowPermission.checkPermissionForExternalStorage()) {
                mMarshMallowPermission.requestPermissionForExternalStorage();
            } else {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getImageFileUri());
                startActivityForResult(cameraIntent, AppConfigs.REQUEST_CAMERA_CAPTURE);
            }
        }
    }

    @Override
    public void onReviewListener(int index) {
        reviewImageZoom(index);
    }
}
