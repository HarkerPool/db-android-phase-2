package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.models.Travel;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NoticeSaveTravelWithDraftActivity extends BaseActivity {
    TextView mTvContent, mTvContent2;
    ImageView mIvStatus;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_confirm;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvContent = (TextView) findViewById(R.id.tv_content);
        mTvContent2 = (TextView) findViewById(R.id.tv_content_2);
        mIvStatus = (ImageView) findViewById(R.id.iv_status);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mTvContent2.setVisibility(View.GONE);
        mIvStatus.setVisibility(View.VISIBLE);
        mIvStatus.setImageResource(R.drawable.warning2);
        if (DBApplication.getServiceFactory().getTravelService().getTravel() != null
                && DBApplication.getServiceFactory().getTravelService().getTravel().isTravel()) {
            mTvContent.setText(getResources().getString(R.string.notice_save_reise_in_draft));
        } else {
            mTvContent.setText(getResources().getString(R.string.notice_save_vorschuss_in_draft));
        }
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void initData() {
        Travel travel = DBApplication.getServiceFactory().getTravelService().getTravel();
        if (travel != null && !TextUtils.isEmpty(travel.getTravelId())) {
//            travel.setTravelFrom(1); // TODO - bug fixed: could not re-create pdf name
            travel.setDraft(true);
            TravelDbHelper.save(travel);
        }
        StorageUtils.removeTravelId(NoticeSaveTravelWithDraftActivity.this);
        DBApplication.getServiceFactory().getTravelService().resetTravel();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();
        
        Intent intent;
        
        switch ((AppConfigs.MENU_ITEM) getIntent().getSerializableExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY))) {
            case HOME:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case PERSONAL:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, PersonalActivity.class);
                intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, false);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                break;
            case HILFE:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, HifleActivity.class);
                break;
            case MAPS:
                /*Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                intent.setPackage("com.google.android.apps.maps");
                if (intent.resolveActivity(NoticeSaveTravelWithDraftActivity.this.getPackageManager()) != null) {
                    NoticeSaveTravelWithDraftActivity.this.startActivity(intent);
                }*/
                return;
            case REISEAUSKUNFT:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, OpenInternalWebViewActivity.class);
                intent.putExtra(AppConfigs.TITLE_URL, NoticeSaveTravelWithDraftActivity.this.getResources().getString(R.string.db_reiseauskunft));
                intent.putExtra(AppConfigs.INTERNAL_URL_KEY, AppConfigs.REISEAUSKUNFT_URL);
                break;
            case EINSTELLUNGEN:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, EinstellungenActivity.class);
                break;
            /*case EPOSTSELECT:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, OpenInternalWebViewActivity.class);
                intent.putExtra(AppConfigs.TITLE_URL, NoticeSaveTravelWithDraftActivity.this.getResources().getString(R.string.activation_epostselect));
                intent.putExtra(AppConfigs.INTERNAL_URL_KEY, AppConfigs.E_POST_SELECT_URL);
                break;*/
            case IMPRESSUM:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, ImpressumActivity.class);
                break;
            case ABBRUCH_BEENDEN:
                intent = new Intent(NoticeSaveTravelWithDraftActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            default:
                return;
        }

        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // nothing
    }
}
