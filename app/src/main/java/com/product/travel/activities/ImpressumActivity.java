package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 6/18/16.
 */
public class ImpressumActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnImpressum, mBtnDatens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_impressum;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnImpressum = (Button) findViewById(R.id.btn_impressum);
        mBtnDatens = (Button) findViewById(R.id.btn_datenschutzhinweise);

        mTvTitle.setText(getResources().getString(R.string.impressum_title));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnImpressum.setOnClickListener(this);
        mBtnDatens.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ImpressumActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_impressum:
                onImpressumButtonClick();
                break;
            case R.id.btn_datenschutzhinweise:
                onDatensButtonClick();
                break;
            default:
                break;
        }
    }

    private void onImpressumButtonClick() {
        Intent intent = new Intent(ImpressumActivity.this, ImpressumDetailActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onDatensButtonClick() {
        Intent intent = new Intent(ImpressumActivity.this, DatensActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
