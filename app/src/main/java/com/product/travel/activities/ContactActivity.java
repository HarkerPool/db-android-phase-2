package com.product.travel.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ContactActivity extends BaseActivity implements View.OnClickListener {
    ImageButton mIBtnBack, mIBtnMenu;
    TextView mTvTitle;
    WebView mWvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_internal_webview;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mWvContent = (WebView) findViewById(R.id.wv_content);

        mTvTitle.setText(getString(R.string.contact));

        String content = "<p>\n" +
                "<br>\n" +
                "Haben Sie Fragen zur Reisekosten-App? Unser Team von Personal direkt hilft Ihnen gerne weiter.\n" +
                "<br><br><br>\n" +
                "E-Mail: \n" +
                "<a href='mailto:scp-direkt@deutschebahn.com'>scp-direkt@deutschebahn.com</a>\n" +
                "<br><br>\n" +
                "Tel: " +
                "<a href='tel:069-265-1083'>069-265-1083</a>" +
                "</p>";

        mWvContent.setVerticalScrollBarEnabled(false);
        mWvContent.setHorizontalScrollBarEnabled(false);
        mWvContent.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "utf-8", null);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }
}
