package com.product.travel.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ImpressumDetailActivity extends BaseActivity implements View.OnClickListener {
    ImageButton mIBtnBack, mIBtnMenu;
    TextView mTvTitle;
    WebView mWvContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_internal_webview;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mWvContent = (WebView) findViewById(R.id.wv_content);

        mTvTitle.setText(getString(R.string.impressum));


        String content = "<p>" +
                "<br>" +
                "Betreiber dieser App" +
                "<br><br>" +
                "Swiss Post Solutions GmbH" +
                "<br>" +
                "Kronacher Straße 70 - 80" +
                "<br>" +
                "96052 Bamberg" +
                "<br><br>" +
                "Telefon: " +
                "<a href='tel:+49 (0)9168 200'>+49 (0)951 / 9168 200</a>" +
                "<br>" +
                "Fax: " +
                "<a href='fax:+49 (0)951 / 9426 - 3399'>+49 (0)951 / 9426 - 3399</a>" +
                "<br>" +
                "E-Mail: " +
                "<a href='mailto:sps.de@swisspost.com'>sps.de@swisspost.com</a>" +

                "<br><br><br><br>" +
                "Geschäftsführung" +
                "<br><br>" +
                "Michael Auerbach" +
                "<br>" +
                "Frank-Michael Pácser" +

                "<br><br><br><br>" +
                "Handelsregister" +
                "<br>" +
                "Registergericht: Bamberg HRB 110" +
                "<br>" +
                "USt-IdNr. DE13 22 68 861" +
                "</p>";

        mWvContent.setVerticalScrollBarEnabled(false);
        mWvContent.setHorizontalScrollBarEnabled(false);
        mWvContent.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "utf-8", null);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }
}
