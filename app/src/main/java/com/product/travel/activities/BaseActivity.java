package com.product.travel.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;

import com.product.travel.R;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.models.User;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;
import com.product.travel.widget.MenuDialog;

/**
 * Created by HarkerPool on 5/16/16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static AlertDialog mLoadingDialog;
    private MenuDialog mMenuDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(getLayoutResource());

        initActionBar();

        if (getParentView() != null) {
            hideSoftKeyboardWhenTouchOutside(getParentView());
        }

        initLoadingDialog();
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View customView = layoutInflater.inflate(R.layout.layout_actionbar, null);
        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    /**
     * initialize loading dialog using for all loading screens.
     */
    private void initLoadingDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.dialog_loading, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        mLoadingDialog = builder.create();
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        ImageView ivLoading = (ImageView) view.findViewById(R.id.iv_loading);
        AnimationDrawable drawable = (AnimationDrawable) ivLoading.getBackground();
        drawable.start();
    }

    /**
     * @param isShow is true - show dialog, otherwise - dismiss it.
     */
    protected void showLoadingDialog(boolean isShow) {
        try {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
            if (mLoadingDialog == null) {
                initLoadingDialog();
            }
            if (mLoadingDialog != null) {
                if (isShow) {
                    mLoadingDialog.show();
                } else {
                    mLoadingDialog.dismiss();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Showing Menu
     * @param isNeedToSave - need to save data into database before go to the next screen.
     * @param isFromEditScreen - need to show Abbruch or Beenden, true - Abbruch, false - Beenden
     */
    protected void showMenuDialog(boolean isNeedToSave, boolean isFromEditScreen) {
        if (mMenuDialog == null) {
            mMenuDialog = new MenuDialog(this, isNeedToSave, isFromEditScreen);
        }

        mMenuDialog.show();
    }

    protected boolean checkNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            if (StorageUtils.getMobileStatus(this)) { // ON - use all
                return true;
            }
            if (netInfo.getType() == ConnectivityManager.TYPE_WIFI) { // OFF - use only WLAN
                return true;
            }
        }

        return false;
    }

    protected void handleErrorException(Throwable error) {
        if (error instanceof TimeoutError) {
            AlertUtils.showMessageAlert(this, getString(R.string.error), getString(R.string.network_timeout), false);
        } else if (error instanceof NoConnectionError) {
            AlertUtils.showMessageAlert(this, getString(R.string.error), getString(R.string.notice_no_internet), false);
        } else if (error == null || TextUtils.isEmpty(error.getMessage())) {
            AlertUtils.showMessageAlert(this, getString(R.string.error), getString(R.string.server_error_msg), false);
        } else if (error.getMessage().contains("Session time out") || error.getMessage().contains("Missing token") || error.getMessage().contains("Missing token in headers")) {
//            AlertUtils.showMessageAlert(this, getString(R.string.warning), getString(R.string.notice_auto_login), false);

            ThreadInBackground.activation(this);
        } else if (error.getMessage().contains(getString(R.string.saving_personal_info_error_msg))) {
            Toast.makeText(this, getString(R.string.saving_personal_info_error_msg), Toast.LENGTH_LONG).show();
        } else if (error.getMessage().contains(getString(R.string.saving_travel_error_msg))) {
            Toast.makeText(this, getString(R.string.saving_travel_error_msg), Toast.LENGTH_LONG).show();
        } else {
            Log.e("handleErrorException", error.getMessage());
            if (error.getMessage().contains("org.json.JSONException")) {
                AlertUtils.showMessageAlert(this, getString(R.string.error), getString(R.string.server_error_msg), false);
            } else {
                AlertUtils.showMessageAlert(this, getString(R.string.error), error.getMessage(), false);
            }
        }
    }

    protected void updateToken() {
        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user == null) {
            user = UserUtils.convertStringToUser(StorageUtils.getUserInfo(this));
            if (user == null) {
                user = new User();
            }
            if (TextUtils.isEmpty(user.getToken())) {
                user.setToken(StorageUtils.getToken(this));
            }
            DBApplication.getServiceFactory().getUserService().updateUserInfo(user);
        } else if (TextUtils.isEmpty(user.getToken())) {
            user.setToken(StorageUtils.getToken(this));
            DBApplication.getServiceFactory().getUserService().updateUserInfo(user);
        }
    }

    @Override
    public void onBackPressed() {
        hideSoftKeyboard();
        super.onBackPressed();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mMenuDialog != null) {
            mMenuDialog.dismiss();
            mMenuDialog = null;
        }
    }

    protected void showSoftKeyboard() {
        if (this.getCurrentFocus() != null && this.getCurrentFocus().requestFocus()) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(this.getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        }
    }


    protected void hideSoftKeyboard() {
        if (this.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void hideSoftKeyboardWhenTouchOutside(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                hideSoftKeyboardWhenTouchOutside(innerView);
            }
        }
    }

    protected abstract int getLayoutResource();

    protected abstract View getParentView();
}
