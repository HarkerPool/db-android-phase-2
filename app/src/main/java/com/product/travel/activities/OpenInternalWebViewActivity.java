package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class OpenInternalWebViewActivity extends BaseActivity implements View.OnClickListener {
    ImageButton mIBtnBack, mIBtnMenu;
    TextView mTvTitle;
    WebView mWvContent;
    View mTitleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_internal_webview;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mWvContent = (WebView) findViewById(R.id.wv_content);
        mTitleView = findViewById(R.id.i_title);


        mTvTitle.setText(getIntent().getStringExtra(AppConfigs.TITLE_URL));

        showLoadingDialog(true);

        mWvContent.getSettings().setJavaScriptEnabled(true);
        mWvContent.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                showLoadingDialog(false);
            }
        });
        mWvContent.loadUrl(getIntent().getStringExtra(AppConfigs.INTERNAL_URL_KEY));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
    }

    private void initData() {
        if (TextUtils.isEmpty(getIntent().getStringExtra(AppConfigs.TITLE_URL))) {
            mIBtnMenu.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.equals(getIntent().getStringExtra(AppConfigs.INTERNAL_URL_KEY), AppConfigs.REISEAUSKUNFT_URL)) {
            Intent intent = new Intent(OpenInternalWebViewActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
        } else {
            super.onBackPressed();
        }

        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }
}
