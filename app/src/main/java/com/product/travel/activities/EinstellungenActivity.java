package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 6/18/16.
 */
public class EinstellungenActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnCreatePassword, mBtnChangePassword, mBtnDeletePassword, mBtnMobileWlanPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_einstellungen;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCreatePassword = (Button) findViewById(R.id.btn_create_password);
        mBtnChangePassword = (Button) findViewById(R.id.btn_change_password);
        mBtnDeletePassword = (Button) findViewById(R.id.btn_delete_password);
        mBtnMobileWlanPassword = (Button) findViewById(R.id.btn_mobile_wlan);

        mTvTitle.setText(getResources().getString(R.string.einstellungen));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCreatePassword.setOnClickListener(this);
        mBtnChangePassword.setOnClickListener(this);
        mBtnDeletePassword.setOnClickListener(this);
        mBtnMobileWlanPassword.setOnClickListener(this);
    }

    private void initData() {
        if (TextUtils.isEmpty(StorageUtils.getPassword(EinstellungenActivity.this))) {
            mBtnChangePassword.setVisibility(View.GONE);
            mBtnDeletePassword.setVisibility(View.GONE);
        } else {
            mBtnCreatePassword.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EinstellungenActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_create_password:
                onCreatePasswordButtonClick();
                break;
            case R.id.btn_change_password:
                onChangePasswordButtonClick();
                break;
            case R.id.btn_delete_password:
                onDeletePasswordButtonClick();
                break;
            case R.id.btn_mobile_wlan:
                onMobileWlanButtonClick();
                break;
            default:
                break;
        }
    }

    private void onCreatePasswordButtonClick() {
        Intent intent = new Intent(EinstellungenActivity.this, PasswordActivity.class);
        intent.putExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY), AppConfigs.PASSWORD_HANDLER.CREATE);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onChangePasswordButtonClick() {
        Intent intent = new Intent(EinstellungenActivity.this, PasswordActivity.class);
        intent.putExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY), AppConfigs.PASSWORD_HANDLER.CHANGE);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onDeletePasswordButtonClick() {
        Intent intent = new Intent(EinstellungenActivity.this, PasswordActivity.class);
        intent.putExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY), AppConfigs.PASSWORD_HANDLER.DELETE);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onMobileWlanButtonClick() {
        Intent intent = new Intent(EinstellungenActivity.this, MobileWlanActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
