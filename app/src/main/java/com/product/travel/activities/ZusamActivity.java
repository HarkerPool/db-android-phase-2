package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.CallBack;
import com.product.travel.interfaces.ZusamListener;
import com.product.travel.models.Travel;
import com.product.travel.models.User;
import com.product.travel.models.ZusamItem;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.ImageUtils;
import com.product.travel.utils.PDFUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;
import com.product.travel.widget.LayoutZusamItemExt;
import com.product.travel.widget.LayoutZusamTitleExt;
import com.squareup.picasso.Picasso;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ZusamActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle;
    private ImageButton mIBtnBack, mIBtnMenu;
    private ImageView mIvEditPersonal, mIvVorlagen;
    private Button mBtnCancel, mBtnNext;
    private EditText mEdtComment;
    private LinearLayout mLinItemsZusam;
    private View mViewItemTitle;

    private ArrayList<String> mUberImagesList, mTaxiImagesList, mFahrImagesList, mKraftImagesList, mMietImageList, mPakenImagesList, mTagungsImagesList, mNebenImagesList;
    private Double mBetragTotal;
    private Travel mTravel;

    private boolean mCanNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_zusam;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mIvEditPersonal = (ImageView) findViewById(R.id.iv_zusam_edit);
        mIvVorlagen = (ImageView) findViewById(R.id.iv_vorlagen);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mEdtComment = (EditText) findViewById(R.id.edt_comment);
        mLinItemsZusam = (LinearLayout) findViewById(R.id.lin_items_zusam);
        mViewItemTitle = findViewById(R.id.i_item_title);

        mTvTitle.setText(getResources().getString(R.string.zusam_reise_title));

        TextView itemTitle = (TextView) mViewItemTitle.findViewById(R.id.tv_zusam_title);
        itemTitle.setText(getString(R.string.comment));
        mViewItemTitle.findViewById(R.id.iv_zusam_edit).setVisibility(View.GONE);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mIvEditPersonal.setOnClickListener(this);
        mIvVorlagen.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
    }

    private void initData() {
        mBtnNext.setEnabled(true);

        if (getIntent().getBooleanExtra(AppConfigs.HAS_DATE_CHANGED, false)) {
            AlertUtils.showMessageAlert(ZusamActivity.this, getString(R.string.warning), getString(R.string.notice_change_date));
        }

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(ZusamActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(ZusamActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }
        if (!mTravel.isTravel()) { // Vorschuss
            mTvTitle.setText(getResources().getString(R.string.zusam_vorschuss_title));
            mBtnNext.setText(getResources().getString(R.string.next_vorschuss_zusam));
        }
        mEdtComment.setText(mTravel.getComment());

        Date currentDate = new Date();
        if (mTravel.isTravel() && mTravel.getEndDate().compareTo(currentDate) > 0) {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
            mCanNext = false;
        } else {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
            mCanNext = true;
        }

        // Max = 10
        if (TravelDbHelper.countVorlagen() >= 10) {
            mIvVorlagen.setSelected(false);
            mTravel.setFavorite(false);
        } else {
            mIvVorlagen.setSelected(mTravel.isFavorite());
        }

//        showLoadingDialog(true);
        showImagesOnUi();
//        showLoadingDialog(false);
//        new ShowImagesOnUiTask().execute();
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showImagesOnUi();
            }
        });*/
    }

    private void showImagesOnUi() {
        /* init all data */
        mLinItemsZusam.removeAllViewsInLayout();
        if (mTravel == null) {
            return;
        }
        initPersonalInfo();
        initReiseUnd();
        if (mTravel.isTravel()) {
            initUber();
            initVerp();
            initBeleg();
        }

        /* Betrag */
        if (mTravel.getAdvanceMoney() > 0) {
            ZusamItem item = new ZusamItem();
            item.setTitle(getResources().getString(R.string.zusam_hohe));
            item.setValue(String.format(Locale.GERMAN, "%.2f %s", mTravel.getAdvanceMoney(), getResources().getString(R.string.currency_ge)));
            item.setHasEdit(!mTravel.isTravel()); // can not edit if it's a travel.
            addItem(item, new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel(); // need to be updated, avoid out update data when goto other screen to edit then come back

                    Intent intent = new Intent(ZusamActivity.this, VorschussBeanActivity.class);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });
        }
    }

    private void initPersonalInfo() {
        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user == null) {
            user = UserUtils.convertStringToUser(StorageUtils.getUserInfo(ZusamActivity.this));
        }
        if (user != null) {
            String value = String.format("%s", user.getCompanyStreet() + " " + user.getCompanyNo() + ", " + user.getCompanyZip() + " " + user.getCompanyCity());
            ZusamItem item = new ZusamItem(getResources().getString(R.string.address), value, false);
            addItem(item, null);

            /* mViewAddressSub1 */
            if (user.getHomeAddressList() != null && user.getHomeAddressList().size() > 0) {
                value = String.format("%s", user.getHomeAddressList().get(0).getStreet() + " " + user.getHomeAddressList().get(0).getHomeNumber()
                        + ", " + user.getHomeAddressList().get(0).getZip() + " " + user.getHomeAddressList().get(0).getCity());

                item = new ZusamItem("1. Wohnsitz:", value, false);
                addItem(item, null);
            }

            /* mViewAddressSub2 */
            if (user.getHomeAddressList() != null && user.getHomeAddressList().size() > 1) {
                value = String.format("%s", user.getHomeAddressList().get(1).getStreet() + " " + user.getHomeAddressList().get(1).getHomeNumber()
                        + ", " + user.getHomeAddressList().get(1).getZip() + " " + user.getHomeAddressList().get(1).getCity());

                item = new ZusamItem("2. Wohnsitz:", value, false);
                addItem(item, null);
            }

            item = new ZusamItem("Mitarbeiterstatus:", user.getEmployeeStatus(), false);
            addItem(item, null);
        }
    }

    private void initReiseUnd() {
        addTitle(getResources().getString(R.string.reise_und_title), new ZusamListener() {
            @Override
            public void onEditListener() {
                updateTravel();

                Intent intent;
                if (mTravel.isTravel()) {
                    intent = new Intent(ZusamActivity.this, ReiseUndActivity.class);
                } else {
                    intent = new Intent(ZusamActivity.this, VorschussBeanActivity.class);
                }
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });

        // Reason
        ZusamItem item = new ZusamItem(getResources().getString(R.string.zusam_reisezweck), mTravel.getReason(), false);
        addItem(item, null);

        /* Add Ziel(e) */
        item.setTitle(null);
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getResources().getString(R.string.zusam_ziel));
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        if (!TextUtils.isEmpty(mTravel.getZiel())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    item.setTitle(getResources().getString(R.string.zusam_ort));
                    item.setValue(jsonItem.getString("city"));
                    item.setHasEdit(false);
                    addItem(item, null);

                    item.setTitle(getResources().getString(R.string.zusam_strabe));
                    item.setValue(jsonItem.getString("street"));
                    item.setHasEdit(false);
                    addItem(item, null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /* Begin */
        item.setTitle(null);
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getResources().getString(R.string.zusam_begin));
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getResources().getString(R.string.zusam_ort));
        item.setValue(mTravel.getBeginCity());
        item.setHasEdit(false);
        addItem(item, null);

        DateFormat[] formats = new DateFormat[] {
                DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.GERMAN),
//                    DateFormat.getDateTimeInstance(),
//                    DateFormat.getTimeInstance(),
        };
        item.setTitle(getResources().getString(R.string.zusam_datum));
        item.setValue(String.format("%s", formats[0].format(mTravel.getBeginDate())));
        item.setHasEdit(false);
        addItem(item, null);

        if (mTravel.isTravel()) {
            final Calendar calendar = Calendar.getInstance();
            if (mTravel != null && mTravel.getBeginDate() != null) {
                calendar.setTime(mTravel.getBeginDate());
            }
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            String hr = (hour > 9) ? ("" + hour) : ("0" + hour);
            String min = (minute > 9) ? ("" + minute) : ("0" + minute);
            item.setTitle(getResources().getString(R.string.zusam_uhrzeit));
            item.setValue(String.format(Locale.GERMAN, "%s:%s", hr, min));
            item.setHasEdit(false);
            addItem(item, null);
        }

        /* End */
        item.setTitle(null);
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getResources().getString(R.string.zusam_ende));
        item.setValue(null);
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getResources().getString(R.string.zusam_datum));
        item.setValue(String.format("%s", formats[0].format(mTravel.getEndDate())));
        item.setHasEdit(false);
        addItem(item, null);

        if (mTravel.isTravel()) {
            final Calendar calendar = Calendar.getInstance();
            if (mTravel != null && mTravel.getEndDate() != null) {
                calendar.setTime(mTravel.getEndDate());
            }
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            String hr = (hour > 9) ? ("" + hour) : ("0" + hour);
            String min = (minute > 9) ? ("" + minute) : ("0" + minute);
            item.setTitle(getResources().getString(R.string.zusam_uhrzeit));
            item.setValue(String.format(Locale.GERMAN, "%s:%s", hr, min));
            item.setHasEdit(false);
            addItem(item, null);
        }
    }

    private void initUber() {
        addTitle(getResources().getString(R.string.zusam_ubernachtung), new ZusamListener() {
            @Override
            public void onEditListener() {
                updateTravel();

                Intent intent = new Intent(ZusamActivity.this, UbernachtungActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });

        int sum = mTravel.getInklusiveFruhstuck() + mTravel.getOhneFruhstuck() + mTravel.getOhneRechnung() + mTravel.getUnentgeltlich();
        if (sum > 0) {
            ZusamItem item = new ZusamItem(getResources().getString(R.string.zusam_uber_total_night), sum + "", false);
            addItem(item, null);

            // add image
            showLoadingDialog(true);
            initUberImages();
            showLoadingDialog(false);
        }
    }

    private void initUberImages() {
        if (!TextUtils.isEmpty(mTravel.getUberImages())) {
            try {
                mUberImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getUberImages());
                JSONArray jsonArray = jsonObject.getJSONArray("uber_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mUberImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mUberImagesList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mUberImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mUberImagesList.get(index));
                }

                // show image on the UI
                for (int i = 0; i < mUberImagesList.size(); i++) {
                    addImage(mUberImagesList.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initVerp() {
        addTitle(getResources().getString(R.string.verpflegung_title), new ZusamListener() {
            @Override
            public void onEditListener() {
                updateTravel();

                Intent intent = new Intent(ZusamActivity.this, VerpflegungActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });

        int totalBreakfast = 0;
        int totalLunch = 0;
        int totalDinner = 0;
        if (!TextUtils.isEmpty(mTravel.getVerpflegung())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getVerpflegung());
                JSONArray jsonArray = jsonObject.getJSONArray("verpflegung");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    if (jsonItem.getBoolean("is_breakfast")) {
                        totalBreakfast++;
                    }
                    if (jsonItem.getBoolean("is_lunch")) {
                        totalLunch++;
                    }
                    if (jsonItem.getBoolean("is_dinner")) {
                        totalDinner++;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ZusamItem item = new ZusamItem(getString(R.string.fruhstuck) + ":", totalBreakfast + "", false);
        addItem(item, null);

        item.setTitle(getString(R.string.mittagessen) + ":");
        item.setValue(totalLunch + "");
        item.setHasEdit(false);
        addItem(item, null);

        item.setTitle(getString(R.string.abendessen) + ":");
        item.setValue(totalDinner + "");
        item.setHasEdit(false);
        addItem(item, null);
    }

    private void initBeleg() {
        addTitle(getResources().getString(R.string.zusam_beleg_title), new ZusamListener() {
            @Override
            public void onEditListener() {
                updateTravel();

                Intent intent = new Intent(ZusamActivity.this, BelegErfassenActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });

        int imagesCount = getTotalImages();
        if (imagesCount > 0) {
            ZusamItem item = new ZusamItem(getResources().getString(R.string.zusam_beleg_total_images), imagesCount + "", false);
            addItem(item, null);
        }

        // show 8 items
        showLoadingDialog(true);
        initTaxi();
        initFahr();
        initKraft();
        initMiet();
        initPrivater();
        initNeben();
        initParken();
        initTagungs();
        showLoadingDialog(false);
    }

    private int getTotalImages() {
        int totalImages = 0;

        if (!TextUtils.isEmpty(mTravel.getTaxiImages())) {
            try {
                mTaxiImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getTaxiImages());
                JSONArray jsonArray = jsonObject.getJSONArray("taxi_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mTaxiImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mTaxiImagesList.add(jsonArray.getString(i));
                }

                // save image to cache
                for (int index = 0; index < mTaxiImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mTaxiImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getFahrtkostenImages())) {
            try {
                mFahrImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getFahrtkostenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("fahrtkosten_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mFahrImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mFahrImagesList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mFahrImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mFahrImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getMietwagenImages())) {
            try {
                mMietImageList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getMietwagenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("mietwagen_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mMietImageList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mMietImageList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mMietImageList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mMietImageList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getKraftstoffImages())) {
            try {
                mKraftImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getKraftstoffImages());
                JSONArray jsonArray = jsonObject.getJSONArray("kraftstoff_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mKraftImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mKraftImagesList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mKraftImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mKraftImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getParkenImages())) {
            try {
                mPakenImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getParkenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("parken_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mPakenImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mPakenImagesList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mPakenImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mPakenImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getTagungsImages())) {
            try {
                mTagungsImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getTagungsImages());
                JSONArray jsonArray = jsonObject.getJSONArray("tagungs_images");

                totalImages += jsonArray.length();

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mTagungsImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mTagungsImagesList.add(jsonArray.getString(i));
                }

                // save images to cache
                for (int index = 0; index < mTagungsImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mTagungsImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(mTravel.getNeben())) {
            try {
                mBetragTotal = 0.0;
                mNebenImagesList = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(mTravel.getNeben());
                JSONArray jsonArray = jsonObject.getJSONArray("neben");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    mBetragTotal += jsonItem.getDouble("betrag");

                    if (!TextUtils.isEmpty(jsonItem.getString("image"))) {
                        totalImages++;
                    }

                    if (jsonItem.getString("image").contains("uploads")) {
                        String[] parts = jsonItem.getString("image").split("/");
                        if (ImageUtils.hasImageExist(ZusamActivity.this, parts[parts.length - 1])) {
                            mNebenImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mNebenImagesList.add(jsonItem.getString("image"));
                }

                // save images to cache
                for (int index = 0; index < mNebenImagesList.size(); index++) {
                    ImageUtils.scaleAndStorageCacheImage(ZusamActivity.this, mNebenImagesList.get(index));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return totalImages;
    }

    private void initTaxi() {
        if (mTaxiImagesList != null && mTaxiImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.beleg_taxi), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.TAXI);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mTaxiImagesList.size(); i++) {
                addImage(mTaxiImagesList.get(i));
            }
        }
    }

    private void initFahr() {
        if (mFahrImagesList != null && mFahrImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.beleg_fahrtkosten), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.FAHRTKOSTEN);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mFahrImagesList.size(); i++) {
                addImage(mFahrImagesList.get(i));
            }
        }
    }

    private void initKraft() {
        if (mKraftImagesList != null && mKraftImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.beleg_kraftstoff), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.KRAFTSTOFF);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mKraftImagesList.size(); i++) {
                addImage(mKraftImagesList.get(i));
            }
        }
    }

    private void initMiet() {
        if (mMietImageList != null && mMietImageList.size() > 0) {
            addTitle(getResources().getString(R.string.beleg_mietwagen), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.MIETWAGEN);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mMietImageList.size(); i++) {
                addImage(mMietImageList.get(i));
            }
        }
    }

    private void initNeben() {
        if (mNebenImagesList != null && mNebenImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.nebenkosten_sonstiges_title), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, NebenkostenActivity.class);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            String betragTotal = String.format(Locale.GERMAN, "%.2f", mBetragTotal);
            ZusamItem item = new ZusamItem(getResources().getString(R.string.nebenkosten_sonstiges_title) + ":", betragTotal + "", false);
            addItem(item, null);
            /*addItem(item, new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, NebenkostenActivity.class);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });*/

            // show image on the UI
            for (int i = 0; i < mNebenImagesList.size(); i++) {
                addImage(mNebenImagesList .get(i));
            }
        }
    }

    private void initParken() {
        if (mPakenImagesList != null && mPakenImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.beleg_parken), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.PARKEN);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mPakenImagesList.size(); i++) {
                addImage(mPakenImagesList.get(i));
            }
        }
    }

    private void initTagungs() {
        if (mTagungsImagesList != null && mTagungsImagesList.size() > 0) {
            addTitle(getResources().getString(R.string.tagungspauschale_title), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, TaxiActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.TAGUNGSPAUSCHALE);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            // show image on the UI
            for (int i = 0; i < mTagungsImagesList.size(); i++) {
                addImage(mTagungsImagesList .get(i));
            }
        }
    }

    private void initPrivater() {
        if (mTravel.getPrivater() > 0) {
            addTitle(getResources().getString(R.string.privater_title), new ZusamListener() {
                @Override
                public void onEditListener() {
                    updateTravel();

                    Intent intent = new Intent(ZusamActivity.this, PrivaterActivity.class);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            });

            ZusamItem item = new ZusamItem(getResources().getString(R.string.privater_hint) + ":", mTravel.getPrivater() + "", false);
            addItem(item, null);
        }
    }

    private void addTitle(String title, ZusamListener listener) {
        LayoutZusamTitleExt linTitle = new LayoutZusamTitleExt(ZusamActivity.this, title, listener);
        mLinItemsZusam.addView(linTitle);
    }

    private void addItem(ZusamItem item, ZusamListener listener) {
        LayoutZusamItemExt linItem = new LayoutZusamItemExt(ZusamActivity.this, item, listener);
        mLinItemsZusam.addView(linItem);
    }

    private void addImage(String path) {
        if (!TextUtils.isEmpty(path)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.topMargin = 10;
            layoutParams.bottomMargin = 10;
            layoutParams.gravity = Gravity.CENTER;
            ImageView iv = new ImageView(ZusamActivity.this);
            iv.setLayoutParams(layoutParams);

            Bitmap bitmap = DBApplication.getBitmapCache().getBitmap(path);
            if (bitmap != null) {
                iv.setImageBitmap(bitmap);
            } else {
                Picasso.with(ZusamActivity.this)
                        .load(AppConfigs.SERVER_URL + path)
                        .placeholder(R.drawable.loading_icon)
                        .error(R.drawable.error)
                        .resize(200, 150)
                        .centerCrop()
                        .into(iv);
            }

            mLinItemsZusam.addView(iv);
        }
    }

    private void removeAllImagesFromCache() {
        // Uber
        if (mUberImagesList != null) {
            for (int index = 0; index < mUberImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mUberImagesList.get(index));
            }
        }

        // Taxi
        if (mTaxiImagesList != null) {
            for (int index = 0; index < mTaxiImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mTaxiImagesList.get(index));
            }
        }

        // Fahr
        if (mFahrImagesList != null) {
            for (int index = 0; index < mFahrImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mFahrImagesList.get(index));
            }
        }

        // Kraft
        if (mKraftImagesList != null) {
            for (int index = 0; index < mKraftImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mKraftImagesList.get(index));
            }
        }

        // Miet
        if (mMietImageList != null) {
            for (int index = 0; index < mMietImageList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mMietImageList.get(index));
            }
        }

        // Neben
        if (mNebenImagesList != null) {
            for (int index = 0; index < mNebenImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mNebenImagesList.get(index));
            }
        }

        // Paken
        if (mPakenImagesList != null) {
            for (int index = 0; index < mPakenImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mPakenImagesList.get(index));
            }
        }

        // Tagungs
        if (mTagungsImagesList != null) {
            for (int index = 0; index < mTagungsImagesList.size(); index++) {
                DBApplication.getBitmapCache().removeBitmap(mTagungsImagesList.get(index));
            }
        }
    }

    private boolean hasModify() {
        String comment = mTravel.getComment() == null ? "" : mTravel.getComment();
        if (!TextUtils.equals(comment, mEdtComment.getText().toString().trim())) {
            return true;
        }

        return false;
    }

    private boolean hasImage() {
        if (mUberImagesList != null && mUberImagesList.size() > 0) {
            return true;
        }
        if (mTaxiImagesList != null && mTaxiImagesList.size() > 0) {
            return true;
        }
        if (mFahrImagesList != null && mFahrImagesList.size() > 0) {
            return true;
        }
        if (mKraftImagesList != null && mKraftImagesList.size() > 0) {
            return true;
        }
        if (mMietImageList != null && mMietImageList.size() > 0) {
            return true;
        }
        if (mPakenImagesList != null && mPakenImagesList.size() > 0) {
            return true;
        }
        if (mTagungsImagesList != null && mTagungsImagesList.size() > 0) {
            return true;
        }
        if (mNebenImagesList != null && mNebenImagesList.size() > 0) {
            return true;
        }

        return false;
    }

    private void updateTravel() {
        removeAllImagesFromCache();

        if (hasModify() && mTravel.isTravel()) {
            mTravel.setModify(true);
        }

        mTravel.setComment(mEdtComment.getText().toString().trim());
        mTravel.setFavorite(mIvVorlagen.isSelected());
        if (mTravel.isModify()) {
            mTravel.setStatus(0);
        }

        mTravel.setUploading(false);
        mTravel.setCreatePdf(true);

        if (!mTravel.isUploading()) {
            try {
                JSONObject jsonObject;
                JSONArray jsonTemp;
                JSONArray jsonArray = new JSONArray();

                if (!TextUtils.isEmpty(mTravel.getUberImages())) {
                    jsonObject = new JSONObject(mTravel.getUberImages());
                    jsonTemp = jsonObject.getJSONArray("uber_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getTaxiImages())) {
                    jsonObject = new JSONObject(mTravel.getTaxiImages());
                    jsonTemp = jsonObject.getJSONArray("taxi_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getFahrtkostenImages())) {
                    jsonObject = new JSONObject(mTravel.getFahrtkostenImages());
                    jsonTemp = jsonObject.getJSONArray("fahrtkosten_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getMietwagenImages())) {
                    jsonObject = new JSONObject(mTravel.getMietwagenImages());
                    jsonTemp = jsonObject.getJSONArray("mietwagen_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getKraftstoffImages())) {
                    jsonObject = new JSONObject(mTravel.getKraftstoffImages());
                    jsonTemp = jsonObject.getJSONArray("kraftstoff_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getParkenImages())) {
                    jsonObject = new JSONObject(mTravel.getParkenImages());
                    jsonTemp = jsonObject.getJSONArray("parken_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getTagungsImages())) {
                    jsonObject = new JSONObject(mTravel.getTagungsImages());
                    jsonTemp = jsonObject.getJSONArray("tagungs_images");
                    if (jsonTemp.length() > 0) {
                        jsonArray.put(jsonTemp);
                    }
                }
                if (!TextUtils.isEmpty(mTravel.getNeben())) {
                    jsonObject = new JSONObject(mTravel.getNeben());

                    jsonTemp = jsonObject.getJSONArray("neben");

                    for (int i = 0; i < jsonTemp.length(); i++) {
                        JSONObject jsonItem = jsonTemp.getJSONObject(i);

                        if (!TextUtils.isEmpty(jsonItem.getString("image"))) {
                            jsonArray.put(jsonItem.getString("image"));
                        }
                    }
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (!jsonArray.getString(i).contains("uploads")) {
                        mTravel.setUploading(true);
                        mTravel.setCreatePdf(false);
                        break;
                    }
                }
            } catch (JSONException jex) {
                jex.printStackTrace();
            }
        }

        /*if (!mTravel.isUploading() && mUberImagesList != null && mUberImagesList.size() > 0) {
            for (int i = 0; i < mUberImagesList.size(); i++) {
                if (!mUberImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mTaxiImagesList != null && mTaxiImagesList.size() > 0) {
            for (int i = 0; i < mTaxiImagesList.size(); i++) {
                if (!mTaxiImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mFahrImagesList != null && mFahrImagesList.size() > 0) {
            for (int i = 0; i < mFahrImagesList.size(); i++) {
                if (!mFahrImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mKraftImagesList != null && mKraftImagesList.size() > 0) {
            for (int i = 0; i < mKraftImagesList.size(); i++) {
                if (!mKraftImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mMietImageList != null && mMietImageList.size() > 0) {
            for (int i = 0; i < mMietImageList.size(); i++) {
                if (!mMietImageList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mPakenImagesList != null && mPakenImagesList.size() > 0) {
            for (int i = 0; i < mPakenImagesList.size(); i++) {
                if (!mPakenImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mTagungsImagesList != null && mTagungsImagesList.size() > 0) {
            for (int i = 0; i < mTagungsImagesList.size(); i++) {
                if (!mTagungsImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }
        if (!mTravel.isUploading() && mNebenImagesList != null && mNebenImagesList.size() > 0) {
            for (int i = 0; i < mNebenImagesList.size(); i++) {
                if (!mNebenImagesList.get(i).contains("uploads")) {
                    mTravel.setUploading(true);
                    mTravel.setCreatePdf(false);
                    break;
                }
            }
        }*/

        mTravel.setUploaded(false);
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        TravelDbHelper.save(mTravel);
    }

    @Override
    public void onResume() {
        super.onResume();

//        initData();
    }

    @Override
    public void onBackPressed() {
//        removeAllImagesFromCache(); // TODO - must release memory
        if (hasModify()) {
            AlertUtils.showMessageAlert(ZusamActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
//                removeAllImagesFromCache(); // TODO - must release memory
                boolean hasModify = hasModify();
                if (hasModify) {
                    if (mTravel.isTravel()) { // only set for Reise, not Vorschuss
                        mTravel.setModify(true);
                    }
                    updateTravel();
//                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.iv_zusam_edit:
                onEditPersonalButtonClick();
                break;
            case R.id.iv_vorlagen:
                onStarIconClick();
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onEditPersonalButtonClick() {
        updateTravel();

        Intent intent = new Intent(ZusamActivity.this, PersonalActivity.class);
        intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, false);
        intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    private void onStarIconClick() {
        mIvVorlagen.setSelected(!mIvVorlagen.isSelected());

        if (mIvVorlagen.isSelected() && TravelDbHelper.countVorlagen() >= 10) {
            deleteVorlagenConfirmation();
        }
    }

    private void onCancelButtonClick() {
        boolean hasModify = hasModify();
        if (hasModify) {
            if (mTravel.isTravel()) { // only set for Reise, not Vorschuss
                mTravel.setModify(true);
            }
            updateTravel();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (hasModify || mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(ZusamActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(ZusamActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }

    private void onNextButtonClick() {
        mBtnNext.setEnabled(false);

        hideSoftKeyboard();

        if (!mCanNext) {
            return;
        }
        if (mIvVorlagen.isSelected() && TravelDbHelper.countVorlagen() >= 10) {
            deleteVorlagenConfirmation();
        }

        if (!mTravel.isTravel() && mTravel.getTravelFrom() == 1 && TextUtils.isEmpty(mTravel.getPdfName())) {
            mTravel.setPdfName(PDFUtils.createVorschussPDFName(ZusamActivity.this));
        }
        if (hasModify() && mTravel.isTravel()) { // only set for Reise, not Vorschuss
            mTravel.setModify(true);
        }
        updateTravel();

        if (mTravel.isTravel()) {
            mBtnNext.setEnabled(true);

            Intent intent = new Intent(ZusamActivity.this, ErklarungActivity.class);
            intent.putExtra(AppConfigs.HAS_IMAGE, hasImage());
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            createVorschuss();
            /*if (!checkNetworkConnection()) {
                AlertUtils.showMessageAlert(ZusamActivity.this, getString(R.string.notice_no_internet));
            } else {
//                new CreateVorschussTask().execute();
            }*/
        }
    }

    private void createVorschuss() {
        showLoadingDialog(true);

        updateToken();
        DBApplication.getServiceFactory().getTravelService().createTravel(mTravel, new CallBack<Travel>() {
            @Override
            public void onSuccess(Travel result) {
//                DBApplication.getServiceFactory().getTravelService().resetTravel(); // Rem for download pdf

                /* >> Create vorlagen >> */
                if (mTravel.isFavorite()) {
                    Travel travelFavorite = new Travel();
                    travelFavorite.setTravelId("VF" + System.currentTimeMillis());
                    travelFavorite.setTravel(false);
                    travelFavorite.setReason(mTravel.getReason());
                    travelFavorite.setZiel(mTravel.getZiel());
                    travelFavorite.setBeginCity(mTravel.getBeginCity());
                    travelFavorite.setVorlagen(true); // set to vorlagen

                    TravelDbHelper.save(travelFavorite);
                }
                /* << Create vorlagen << */

                mTravel.setDraft(false);
                mTravel.setFavorite(false); // change it to false - as default, and avoid to duplicate sending Vorlagen.
                mTravel.setUploaded(true); // uploaded successful.
                mTravel.setTravelFrom(3);
                TravelDbHelper.save(mTravel);

                StorageUtils.storageStarsValue(ZusamActivity.this, mTravel);

                StorageUtils.removeTravelId(ZusamActivity.this);

                Intent intent = new Intent(ZusamActivity.this, NoticeSentSuccessActivity.class);
                intent.putExtra(AppConfigs.IS_SENT_REISE, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();

                showLoadingDialog(false);
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
//                handleErrorException(t);

                if (t != null && !TextUtils.isEmpty(t.getMessage()) && (t.getMessage().contains("Session time out") || t.getMessage().contains("Missing token") || t.getMessage().contains("Missing token in headers"))) {
                    ThreadInBackground.activation(ZusamActivity.this);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    createVorschuss();
                } else {
                    // Log.e("createVorschuss", t == null ? "" : t.getMessage());

                    DBApplication.getServiceFactory().getTravelService().resetTravel();

                    Intent intent = new Intent(ZusamActivity.this, NoticeSentFailedActivity.class);
                    intent.putExtra(AppConfigs.IS_SENT_REISE, false);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }
            }
        });
    }

    private void deleteVorlagenConfirmation() {
        final Dialog dialog = new Dialog(ZusamActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.delete));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_delete_vorlagen));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                updateTravel();

                Intent intent = new Intent(ZusamActivity.this, VorlagenActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, true);
                startActivity(intent);
                finish();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                mIvVorlagen.setSelected(false);
            }
        });
    }
}
