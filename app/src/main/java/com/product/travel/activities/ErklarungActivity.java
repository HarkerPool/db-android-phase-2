package com.product.travel.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.PDFUtils;
import com.product.travel.utils.StorageUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ErklarungActivity extends BaseActivity implements View.OnClickListener {
    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    CheckBox mCbNotice1, mCbNotice2;
    Button mBtnCancel, mBtnNext;
    Travel mTravel;
    ArrayList<String> mImagesList;
    int mCountImageSent;
    ProgressDialog mProgressDialog;
    AlertDialog mLoadingDialog;
    boolean mIsThreadStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
        initLoadingDialog();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_erklarung;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mCbNotice1 = (CheckBox) findViewById(R.id.cb_erklarung_1);
        mCbNotice2 = (CheckBox) findViewById(R.id.cb_erklarung_2);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mTvTitle.setText(getResources().getString(R.string.erklarung_title));
        mBtnNext.setText(getResources().getString(R.string.erklarung_send));
        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mCbNotice1.setOnClickListener(this);
        mCbNotice2.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
    }

    private void initData() {
        mBtnNext.setEnabled(true);

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore mTravel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(ErklarungActivity.this);
//            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel); // No need to set it.
        }
        if (mTravel == null) {
            Toast.makeText(ErklarungActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        mCbNotice2.setVisibility(View.GONE);
        if (getIntent().getBooleanExtra(AppConfigs.HAS_IMAGE, false)) {
            mCbNotice2.setVisibility(View.VISIBLE);
        }
    }

    private void initLoadingDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.dialog_loading, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        mLoadingDialog = builder.create();
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        ImageView ivLoading = (ImageView) view.findViewById(R.id.iv_loading);
        AnimationDrawable drawable = (AnimationDrawable) ivLoading.getBackground();
        drawable.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.cb_erklarung_1:
                onCheckboxClick();
                break;
            case R.id.cb_erklarung_2:
                onCheckboxClick();
                break;
            default:
                break;
        }
    }

    private boolean onCheckboxClick() {
        if (mCbNotice1.isChecked() && mCbNotice2.isChecked()) {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
            return true;
        }
        if (mCbNotice2.getVisibility() == View.GONE && mCbNotice1.isChecked()) {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
            return true;
        }

        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
        return false;
    }

    private boolean isValidate() {
        if (!onCheckboxClick()) {
            return false;
        }

        if (mTravel.getTravelFrom() != 1 && !mTravel.isModify()) {
            AlertUtils.showMessageAlert(ErklarungActivity.this, getString(R.string.notice_travel_no_change));
            return false;
        }

        return true;
    }

    private void onCancelButtonClick() {
        Intent intent = new Intent(ErklarungActivity.this, NoticeSaveTravelWithDraftActivity.class);
        intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    private void onNextButtonClick() {
        mBtnNext.setEnabled(false);

        hideSoftKeyboard();

        if (isValidate()) {
            mLoadingDialog.show();

            if (mTravel.getTravelFrom() == 1 || mTravel.getTravelFrom() == 3) {
                mTravel.setModify(false); // Create new or Convert Vorschuss to Reise --> not bold.
            }
            if ((mTravel.getTravelFrom() == 1 && TextUtils.isEmpty(mTravel.getPdfName()))
                    || mTravel.getTravelFrom() != 1) {
                mTravel.setPdfName(PDFUtils.createReisePDFName(ErklarungActivity.this));
            }
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);

            createReise();
            mBtnNext.setEnabled(false);
        } else {
            mBtnNext.setEnabled(true);
        }
    }

    private void createReise() {
        updateToken();
        DBApplication.getServiceFactory().getTravelService().createTravel(mTravel, new CallBack<Travel>() {
            @Override
            public void onSuccess(Travel result) {
//                DBApplication.getServiceFactory().getTravelService().resetTravel(); // Rem for download pdf

                /* >> Create vorlagen >> */
                if (mTravel.isFavorite()) {
                    Travel travelFavorite = new Travel();
                    travelFavorite.setTravelId("RF" + System.currentTimeMillis());
                    travelFavorite.setTravel(true);
                    travelFavorite.setReason(mTravel.getReason());
                    travelFavorite.setZiel(mTravel.getZiel());
                    travelFavorite.setBeginCity(mTravel.getBeginCity());
                    travelFavorite.setVorlagen(true); // set to vorlagen

                    TravelDbHelper.save(travelFavorite);
                }
                /* << Create vorlagen << */

                if (!mTravel.isUploading()) {
                    mTravel.setDraft(false); // TODO - set to false if has_image = false
                } else {
                    mTravel.setDraft(true);
                }
                mTravel.setFavorite(false); // change it to false - as default, and avoid to duplicate sending Vorlagen.
                mTravel.setModify(false); // uploaded successful.
                mTravel.setUploaded(true);
                mTravel.setTravelFrom(2);
                TravelDbHelper.save(mTravel);

                StorageUtils.storageStarsValue(ErklarungActivity.this, mTravel);

                StorageUtils.removeTravelId(ErklarungActivity.this);

                if (mTravel.isUploading()) {
                    mLoadingDialog.hide();
                    getImagesList();
                    uploadImagesHandler();
                } else {
                    Intent intent = new Intent(ErklarungActivity.this, NoticeSentSuccessActivity.class);
                    intent.putExtra(AppConfigs.IS_SENT_REISE, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();
                }

            }

            @Override
            public void onError(Throwable t) {
                if (t != null && !TextUtils.isEmpty(t.getMessage()) && (t.getMessage().contains("Session time out") || t.getMessage().contains("Missing token") || t.getMessage().contains("Missing token in headers"))) {
                    ThreadInBackground.activation(ErklarungActivity.this);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    createReise();
                } else {
                    goToFailedConfirmationScreen();
                }
            }
        });
    }

    private void uploadImagesHandler() {
        mProgressDialog = new ProgressDialog(ErklarungActivity.this);
        mProgressDialog.setTitle(getString(R.string.processing));
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setProgress(0);
        mProgressDialog.setMax(mImagesList.size());
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onUploadingCancelButtonClick();
            }
        });
        mProgressDialog.show();

        mCountImageSent = 0;
        mIsThreadStop = false;
        uploadAllImages();
    }

    private void onUploadingCancelButtonClick() {
        final Dialog dialog = new Dialog(ErklarungActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.warning));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText("\t\t\t\t" + getString(R.string.cancel_confirm) + "\t\t\t\t");
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mIsThreadStop = true;

                dialog.dismiss();
                mProgressDialog.dismiss();
                mLoadingDialog.show();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mProgressDialog.show();
            }
        });
    }

    private void goToFailedConfirmationScreen() {

        mTravel.setDraft(true);

//        mTravel.setTravelFrom(1); // TODO - bug fixed: could not re-create pdf name
        TravelDbHelper.save(mTravel);
        Intent intent = new Intent(ErklarungActivity.this, NoticeSentFailedActivity.class);
        intent.putExtra(AppConfigs.IS_SENT_REISE, true);
        intent.putExtra(AppConfigs.HAS_CANCELED, mIsThreadStop);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    private void uploadTravelImage(final String imageName) {
        final String travelId = mTravel.getTravelId();

        Log.i("uploadTravelImage", "imageName: " + imageName + "\ntravelId: " + travelId + "\nstarting...");

        DBApplication.getServiceFactory().getTravelService().uploadTravelImage(imageName, travelId, false, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Log.i("uploadTravelImage", "imageName: " + imageName + "\ntravelId: " + travelId + "\nsuccessful.");

                mCountImageSent++;
                mProgressDialog.setProgress(mCountImageSent);

                mImagesList.remove(0);
                if (mImagesList.size() == 0) {
                    mTravel.setDraft(false);
                    TravelDbHelper.save(mTravel);

                    Intent intent = new Intent(ErklarungActivity.this, NoticeSentSuccessActivity.class);
                    intent.putExtra(AppConfigs.IS_SENT_REISE, true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    finish();

                    mProgressDialog.dismiss();
                } else {
                    uploadAllImages();
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.i("uploadTravelImage", "imageName: " + imageName + "\ntravelId: " + travelId + "\nfailed: " + (t != null ? t.getMessage() : ""));

                goToFailedConfirmationScreen();

                mProgressDialog.dismiss();
            }
        });
    }

    private void uploadAllImages() {
        if (mImagesList != null && mImagesList.size() > 0) {
            if (mIsThreadStop) {
                goToFailedConfirmationScreen();
            } else {
                uploadTravelImage(mImagesList.get(0));
            }
        }
    }

    private void getImagesList() {

        mImagesList = null;

        if (mTravel != null) {
            mImagesList = new ArrayList<>();

            // Uber
            if (!TextUtils.isEmpty(mTravel.getUberImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getUberImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("uber_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Taxi
            if (!TextUtils.isEmpty(mTravel.getTaxiImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getTaxiImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("taxi_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Fahrtkosten
            if (!TextUtils.isEmpty(mTravel.getFahrtkostenImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getFahrtkostenImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("fahrtkosten_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Mietwagen
            if (!TextUtils.isEmpty(mTravel.getMietwagenImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getMietwagenImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("mietwagen_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Kraftstoff
            if (!TextUtils.isEmpty(mTravel.getKraftstoffImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getKraftstoffImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("kraftstoff_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Parken
            if (!TextUtils.isEmpty(mTravel.getParkenImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getParkenImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("parken_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Tagungs
            if (!TextUtils.isEmpty(mTravel.getTagungsImages())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getTagungsImages());
                    JSONArray jsonArray = jsonObject.getJSONArray("tagungs_images");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        mImagesList.add(jsonArray.getString(i));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Neben
            if (!TextUtils.isEmpty(mTravel.getNeben())) {
                try {
                    JSONObject jsonObject = new JSONObject(mTravel.getNeben());
                    JSONArray jsonArray = jsonObject.getJSONArray("neben");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonItem = jsonArray.getJSONObject(i);

                        mImagesList.add(jsonItem.getString("image"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        // Remove all images had sent
        if (mImagesList != null) {
            for (int i = 0; i < mImagesList.size();) {
                if (TextUtils.isEmpty(mImagesList.get(i)) || mImagesList.get(i).contains("uploads")) {
                    mImagesList.remove(i);
                } else {
                    i++;
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }
}
