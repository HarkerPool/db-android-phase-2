package com.product.travel.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.NachrichtenDbHelper;
import com.product.travel.data.dao.NachrichtenDbItem;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class HomeActivity extends BaseActivity implements View.OnClickListener {
    TextView mTvTitle, mTvNachrichtenNoticeNew;
    Button mBtnReise, mBtnVorschuss, mBtnAntrage, mBtnNachrichten, mBtnVorlagen, mBtnEntwurfe;
    ImageButton mIBtnBack, mIBtnMenu;
    boolean mDoubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();

        ThreadInBackground.updateTravelStatus(); // Synchronize data.
        ThreadInBackground.uploadAllVorlagen(); // re-send vorlagen
        ThreadInBackground.syncDeleteTravels(HomeActivity.this); // Sync all travel deleted
        ThreadInBackground.getUserInfo(HomeActivity.this);
        ThreadInBackground.downloadImagesHandler(HomeActivity.this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvNachrichtenNoticeNew = (TextView) findViewById(R.id.tv_nachrichten_notice_new);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnReise = (Button) findViewById(R.id.btn_reise_abrechnen);
        mBtnVorschuss = (Button) findViewById(R.id.btn_vorschuss_beantragen);
        mBtnAntrage = (Button) findViewById(R.id.btn_antrage);
        mBtnNachrichten = (Button) findViewById(R.id.btn_nachrichten);
        mBtnVorlagen = (Button) findViewById(R.id.btn_vorlagen);
        mBtnEntwurfe = (Button) findViewById(R.id.btn_entwurfe);

        mTvTitle.setText(getResources().getString(R.string.home_title));
    }

    private void initEvents() {
        mIBtnBack.setVisibility(View.GONE);

        mIBtnMenu.setOnClickListener(this);
        mBtnReise.setOnClickListener(this);
        mBtnVorschuss.setOnClickListener(this);
        mBtnAntrage.setOnClickListener(this);
        mBtnNachrichten.setOnClickListener(this);
        mBtnVorlagen.setOnClickListener(this);
        mBtnEntwurfe.setOnClickListener(this);
    }

    private void initData() {
        mDoubleBackToExitPressedOnce = false;

        DBApplication.getServiceFactory().getTravelService().resetTravel();

        new SyncEmailsTask().execute();

//        ThreadInBackground.activation(HomeActivity.this);
//        ThreadInBackground.updateTravelStatus(); // Synchronize data.
//        ThreadInBackground.uploadAllVorlagen(); // re-send vorlagen
//        ThreadInBackground.syncDeleteTravels(HomeActivity.this); // Sync all travel deleted
    }

    @Override
    public void onResume() {
        super.onResume();

        initData();
    }

    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce) {
//            super.onBackPressed();
            Intent intent = new Intent(HomeActivity.this, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(AppConfigs.EXIT_APP, true);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.fade_out);
            finish();

            return;
        }

        mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Nochmaliges Tippen beendet die App", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_reise_abrechnen:
                onReiseAbrechnenButtonClick();
                break;
            case R.id.btn_vorschuss_beantragen:
                onVorschussBeantragenButtonClick();
                break;
            case R.id.btn_antrage:
                onAntrageButtonClick();
                break;
            case R.id.btn_nachrichten:
                onNachrichtenButtonClick();
                break;
            case R.id.btn_vorlagen:
                onVorlagenButtonClick();
                break;
            case R.id.btn_entwurfe:
                onEntwurfeButtonClick();
                break;
            default:
                break;
        }
    }

    private void onReiseAbrechnenButtonClick() {
        startActivity(new Intent(HomeActivity.this, ReiseAbrechnenActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onVorschussBeantragenButtonClick() {
        startActivity(new Intent(HomeActivity.this, VorschussBeanActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onAntrageButtonClick() {
        startActivity(new Intent(HomeActivity.this, GesendeteAntrageActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onNachrichtenButtonClick() {
        startActivity(new Intent(HomeActivity.this, NachrichtenActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onVorlagenButtonClick() {
        Intent intent = new Intent(HomeActivity.this, VorlagenActivity.class);
        intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onEntwurfeButtonClick() {
        startActivity(new Intent(HomeActivity.this, EntwurfeActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void syncEmails() {
        List<NachrichtenDbItem> nachrichtenList = NachrichtenDbHelper.getAllNachrichten(true);
        if (nachrichtenList == null || nachrichtenList.size() == 0) {
            getAllUnreadEmail();
            return;
        }
        final ArrayList<Integer> emailIdList = new ArrayList<>();
        for(int i = 0; i < nachrichtenList.size(); i++) {
            emailIdList.add(nachrichtenList.get(i).getNachrichtenId());
        }

        if (emailIdList.size() == 0) {
            return;
        }

        DBApplication.getServiceFactory().getTravelService().deleteEmail(emailIdList, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                Log.i("deleteEmails", "Successful.");
                for(int i = 0; i < emailIdList.size(); i++) {
                    // Delete from database
                    NachrichtenDbHelper.deleteNachrichtenById(emailIdList.get(i));
                }
                getAllUnreadEmail();
            }

            @Override
            public void onError(Throwable t) {
                if (t != null && !TextUtils.isEmpty(t.getMessage())) {
                    Log.i("deleteEmails", "Error " + t.getMessage());
                }
                getAllUnreadEmail();
            }
        });
    }
    private void getAllUnreadEmail() {
        DBApplication.getServiceFactory().getTravelService().getEmail(new CallBack<ArrayList<NachrichtenItem>>() {
            @Override
            public void onSuccess(ArrayList<NachrichtenItem> result) {
                int unreadEmail = 0;
                for (NachrichtenItem item : result) {
                    if (!item.isRead()) {
                        unreadEmail++;
                    }
                }
                List<TravelDbItem> travelDbItemList = TravelDbHelper.getAllReiseCanSend();
                if (travelDbItemList != null) {
                    unreadEmail += travelDbItemList.size();
                }
                StorageUtils.storageUnreadEmail(HomeActivity.this, unreadEmail <= 0 ? 0 : unreadEmail);
            }

            @Override
            public void onError(Throwable t) {
                if (t != null && !TextUtils.isEmpty(t.getMessage())
                        && (t.getMessage().contains("Session time out") || t.getMessage().contains("Missing token") || t.getMessage().contains("Missing token in headers"))) {
                    ThreadInBackground.activation(HomeActivity.this);
                }
            }
        });
    }

    private class SyncEmailsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mTvNachrichtenNoticeNew.setVisibility(View.GONE);
//            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            syncEmails();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (StorageUtils.getUnreadEmail(HomeActivity.this) <= 0) {
                mTvNachrichtenNoticeNew.setVisibility(View.GONE);
            } else {
                mTvNachrichtenNoticeNew.setVisibility(View.VISIBLE);
                mTvNachrichtenNoticeNew.setText(String.format(Locale.GERMAN, "%s", StorageUtils.getUnreadEmail(HomeActivity.this) + ""));
            }

//            showLoadingDialog(false);
        }
    }
}
