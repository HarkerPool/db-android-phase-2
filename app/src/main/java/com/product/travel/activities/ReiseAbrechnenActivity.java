package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ReiseAbrechnenActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    Button mBtnReiseOhne, mBtnReiseMit;
    ImageButton mIBtnBack, mIBtnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reise_abrechnen;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnReiseOhne = (Button) findViewById(R.id.btn_reise_ohne_vorschuss);
        mBtnReiseMit = (Button) findViewById(R.id.btn_reise_mit_vorschuss);

        mTvTitle.setText(getResources().getString(R.string.reise_abrechnen_title));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnReiseOhne.setOnClickListener(this);
        mBtnReiseMit.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_reise_ohne_vorschuss:
                onReiseOhneButtonClick();
                break;
            case R.id.btn_reise_mit_vorschuss:
                onReiseMitButtonClick();
                break;
            default:
                break;
        }
    }

    private void onReiseOhneButtonClick() {
        startActivity(new Intent(ReiseAbrechnenActivity.this, ReiseUndActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onReiseMitButtonClick() {
        startActivity(new Intent(ReiseAbrechnenActivity.this, ReiseMitVorschussActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
