package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.product.travel.R;
import com.product.travel.adapters.AntrageAdapter;
import com.product.travel.adapters.StatusAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.AntrageItemListener;
import com.product.travel.models.StatusModel;
import com.product.travel.utils.DownloaderUtils;
import com.product.travel.utils.SecurityUtils;
import com.product.travel.widget.DividerRecyclerViewExt;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class AntrageActivity extends BaseActivity implements View.OnClickListener, AntrageItemListener {

    TextView mTvTitle;
    RecyclerView mRecyclerViewReise, mRecyclerViewVorschuss;
    ImageButton mIBtnBack, mIBtnMenu;
    List<TravelDbItem> mReiseList, mVorschussList;
    AntrageAdapter mReiseAdapter, mVorschussAdapter;
    Spinner mSpnTravelStatus;
    LinearLayout mLinReisen, mLinVorschuss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_antrage;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mRecyclerViewReise = (RecyclerView) findViewById(R.id.rv_reise);
        mRecyclerViewVorschuss = (RecyclerView) findViewById(R.id.rv_vorschuss);
        mSpnTravelStatus = (Spinner) findViewById(R.id.spn_travel_status);
        mLinReisen = (LinearLayout) findViewById(R.id.lin_reisen);
        mLinVorschuss = (LinearLayout) findViewById(R.id.lin_vorschuss);
    }

    private void initEvents() {
        mRecyclerViewReise.addItemDecoration(new DividerRecyclerViewExt(AntrageActivity.this, LinearLayoutManager.VERTICAL));
        mRecyclerViewVorschuss.addItemDecoration(new DividerRecyclerViewExt(AntrageActivity.this, LinearLayoutManager.VERTICAL));
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mSpnTravelStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // Alle - show all status
                        mReiseList = TravelDbHelper.getAntrage(-1, true);
                        mVorschussList = TravelDbHelper.getAntrage(-1, false);
                        break;
                    case 1: // Wird Gesendet - show status 0 & In Bearbeitung - show status 1
                        mReiseList = TravelDbHelper.getAntrage(0, true);
                        mVorschussList = TravelDbHelper.getAntrage(0, false);
                        break;
//                    case 2: // In Bearbeitung - show status 1
//                        mReiseList = TravelDbHelper.getAntrage(1, true);
//                        mVorschussList = TravelDbHelper.getAntrage(1, false);
//                        break;
                    case 2: // Abgerechnet - show status 2
                        mReiseList = TravelDbHelper.getAntrage(2, true);
                        mVorschussList = TravelDbHelper.getAntrage(2, false);
                        break;
                    case 3: // Anteilig abgerechnet - show status 3
                        mReiseList = TravelDbHelper.getAntrage(3, true);
                        mVorschussList = TravelDbHelper.getAntrage(3, false);
                        break;
                    case 4: // Nicht abrechenbar - show status 4
                        mReiseList = TravelDbHelper.getAntrage(4, true);
                        mVorschussList = TravelDbHelper.getAntrage(4, false);
                        break;
                    default:
                        break;
                }

                mReiseAdapter = new AntrageAdapter(true, mReiseList, AntrageActivity.this);
                mVorschussAdapter = new AntrageAdapter(true, mVorschussList, AntrageActivity.this);
                mRecyclerViewReise.setAdapter(mReiseAdapter);
                mRecyclerViewVorschuss.setAdapter(mVorschussAdapter);
                mReiseAdapter.notifyDataSetChanged();
                mVorschussAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initData() {
        boolean isReisen = getIntent().getBooleanExtra(AppConfigs.IS_SENT_REISE, false);

        if (isReisen) {
            mTvTitle.setText(getResources().getString(R.string.mein_reisen));
            mLinReisen.setVisibility(View.VISIBLE);
            mLinVorschuss.setVisibility(View.GONE);

            mReiseList = TravelDbHelper.getAntrage(-1, true);

            mReiseAdapter = new AntrageAdapter(true, mReiseList, this);
            mRecyclerViewReise.setHasFixedSize(true);
            mRecyclerViewReise.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerViewReise.setAdapter(mReiseAdapter);
            mReiseAdapter.notifyDataSetChanged();
        } else {
            mTvTitle.setText(getResources().getString(R.string.mein_vorschuss));
            mLinReisen.setVisibility(View.GONE);
            mLinVorschuss.setVisibility(View.VISIBLE);

            mVorschussList = TravelDbHelper.getAntrage(-1, false);

            mVorschussAdapter = new AntrageAdapter(true, mVorschussList, this);
            mRecyclerViewVorschuss.setHasFixedSize(true);
            mRecyclerViewVorschuss.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerViewVorschuss.setAdapter(mVorschussAdapter);
            mVorschussAdapter.notifyDataSetChanged();
        }

        ArrayList<StatusModel> customListValue = new ArrayList<>();

        List<String> itemList = Arrays.asList(getResources().getStringArray(R.array.travel_status_array));
        StatusModel statusModel0 = new StatusModel(itemList.get(0), -1);
        customListValue.add(statusModel0);

//        StatusModel statusModel1 = new StatusModel(itemList.get(1), R.drawable.status0);
//        customListValue.add(statusModel1);

        StatusModel statusModel2 = new StatusModel(itemList.get(2), R.drawable.status1);
        customListValue.add(statusModel2);

        StatusModel statusModel3 = new StatusModel(itemList.get(3), R.drawable.status2);
        customListValue.add(statusModel3);

        StatusModel statusModel4 = new StatusModel(itemList.get(4), R.drawable.status3);
        customListValue.add(statusModel4);

        StatusModel statusModel5 = new StatusModel(itemList.get(5), R.drawable.status4);
        customListValue.add(statusModel5);

        StatusAdapter adapter = new StatusAdapter(this, R.layout.spinner_image_item, customListValue);
        mSpnTravelStatus.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onReiseViewInfoListener(int position) {
        if (mReiseList.get(position).getStatus() == 0) {
            Toast.makeText(this, getString(R.string.empty_message), Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(AntrageActivity.this, NachrichtenDetailActivity.class);
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID, mReiseList.get(position).getTravelId());
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY, "");
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    @Override
    public void onVorschussViewInfoListener(int position) {
        if (mVorschussList.get(position).getStatus() == 0) {
            Toast.makeText(this, getString(R.string.empty_message), Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(AntrageActivity.this, NachrichtenDetailActivity.class);
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID, mVorschussList.get(position).getTravelId());
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY, "");
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    @Override
    public void onReiseEditListener(int position) {
            Intent intent = new Intent(AntrageActivity.this, ReiseUndActivity.class);
            intent.putExtra(AppConfigs.TRAVEL_ID_KEY, mReiseList.get(position).getTravelId());
            intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//        }
    }

    @Override
    public void onVorschussEditListener(int position) {
        Intent intent = new Intent(AntrageActivity.this, ReiseMitVorschussActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onReiseDeleteListener(int position) {
        /*TravelDbHelper.deleteTravelById(mReiseList.get(position).getTravelId());
        mReiseList.remove(position);
        mReiseAdapter.notifyDataSetChanged();*/
        deleteConfirmationDialog(true, position);
    }

    @Override
    public void onVorschussDeleteListener(int position) {
        /*TravelDbHelper.deleteTravelById(mVorschussList.get(position).getTravelId());
        mVorschussList.remove(position);
        mVorschussAdapter.notifyDataSetChanged();*/
        deleteConfirmationDialog(false, position);
    }

    @Override
    public void onReiseOpenPdfListener(int position) {
//        PDFUtils.openPDF(AntrageActivity.this, mReiseList.get(position).getTravelId());

//        if (!checkNetworkConnection()) {
//            Toast.makeText(AntrageActivity.this, "Error when downloading PDF. " + getString(R.string.notice_no_internet), Toast.LENGTH_LONG).show();
//        }
        new DownloadPdf().execute(mReiseList.get(position).getTravelId(), mReiseList.get(position).getPdfName(), mReiseList.get(position).getPdfMd5());
    }

    @Override
    public void onVorschussOpenPdfListener(int position) {
//        PDFUtils.openPDF(AntrageActivity.this, mVorschussList.get(position).getTravelId());
//        if (!checkNetworkConnection()) {
//            Toast.makeText(AntrageActivity.this, "Error when downloading PDF. " + getString(R.string.notice_no_internet), Toast.LENGTH_LONG).show();
//        }
        new DownloadPdf().execute(mVorschussList.get(position).getTravelId(), mVorschussList.get(position).getPdfName(), mVorschussList.get(position).getPdfMd5());
    }

    private void deleteConfirmationDialog(final boolean isReise, final int position) {
        final Dialog dialog = new Dialog(AntrageActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.delete));
        if (isReise) {
            ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_reise_delete));
        } else {
            ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_vorschuss_delete));
        }
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                updateToken();

                if (isReise) {
                    ThreadInBackground.deleteTravel(AntrageActivity.this, mReiseList.get(position).getTravelId());
                    mReiseList.remove(position);
                    mReiseAdapter.notifyDataSetChanged();
                } else {
                    ThreadInBackground.deleteTravel(AntrageActivity.this, mVorschussList.get(position).getTravelId());
                    mVorschussList.remove(position);
                    mVorschussAdapter.notifyDataSetChanged();
                }
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void downloadFile(String travelId, String pdfName, File pdfFile) {
        String url = AppConfigs.PDF_URL + travelId + "/" + pdfName;
        DownloaderUtils.downloadFile(url, pdfFile);
    }

    private void downloadFile(String travelId, String pdfName, String pdfMd5) {
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File folder = new File(path, "pdf");
            folder.mkdirs();

            File pdfFile = new File(folder, pdfName);

            if (!pdfFile.exists() || pdfFile.length() == 0) {

                pdfFile.createNewFile();
                downloadFile(travelId, pdfName, pdfFile);
            } else {
                String pdfPath = folder + "/" + pdfName;

                FileInputStream fis = new FileInputStream(pdfPath);
                String md5 = SecurityUtils.getMd5(fis);
                if (!md5.equalsIgnoreCase(pdfMd5)) {
                    downloadFile(travelId, pdfName, pdfFile);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

//            Toast.makeText(AntrageActivity.this, "Error when downloading. PDF could not saved.", Toast.LENGTH_LONG).show();
        }
    }

    private class DownloadPdf extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoadingDialog(true);
        }

        @Override
        protected String doInBackground(String... params) {
            downloadFile(params[0], params[1], params[2]);

            return params[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            showLoadingDialog(false);

            try {
                File pdfFile = new File(Environment.getExternalStorageDirectory() + "/pdf/" + result);
                Uri path = Uri.fromFile(pdfFile);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();

                Toast.makeText(AntrageActivity.this, "PDF could not found / No application available to view PDF.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
