package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.adapters.BelegAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.BelegItemChangeListener;
import com.product.travel.models.BelegItem;
import com.product.travel.models.Travel;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class BelegErfassenActivity extends BaseActivity implements View.OnClickListener, BelegItemChangeListener {
    private TextView mTvTitle;
    private ImageButton mIBtnBack, mIBtnMenu;
    private Button mBtnCancel, mBtnNext;
    private GridView mGvImageList;

    private ArrayList<BelegItem> mItemList;
    private BelegAdapter mAdapter;
    private boolean mIsFromZusam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_beleg;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mGvImageList = (GridView) findViewById(R.id.grid_view_image);

        mTvTitle.setText(getResources().getString(R.string.beleg_title));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mGvImageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                handleOnItemClick(position);
            }
        });
    }

    private void initData() {
        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getString(R.string.next_from_zusam));
        }

        boolean isChecked = false;
        Travel travel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (travel == null) {
            travel = DBApplication.getServiceFactory().getTravelService().restoreTravel(BelegErfassenActivity.this);
        }

        if (travel == null) {
            Toast.makeText(BelegErfassenActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
        }

        mItemList = new ArrayList<>();

        if (travel != null && travel.getPrivater() > 0) {
            isChecked = true;
        }
        BelegItem privaterItem = new BelegItem(R.drawable.icon_privater, getResources().getString(R.string.beleg_privat_pkw), isChecked);
        mItemList.add(privaterItem);

        // Taxi
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getTaxiImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getTaxiImages());
                JSONArray jsonArray = jsonObject.getJSONArray("taxi_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem taxiItem = new BelegItem(R.drawable.icon_taxi, getResources().getString(R.string.beleg_taxi), isChecked);
        mItemList.add(taxiItem);

        // Fahrtkosten
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getFahrtkostenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getFahrtkostenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("fahrtkosten_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem fahrtkostenItem = new BelegItem(R.drawable.icon_fahrtkosten, getResources().getString(R.string.beleg_fahrtkosten), isChecked);
        mItemList.add(fahrtkostenItem);

        // Mietwagen
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getMietwagenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getMietwagenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("mietwagen_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem mietwagenItem = new BelegItem(R.drawable.icon_mietwagen, getResources().getString(R.string.beleg_mietwagen), isChecked);
        mItemList.add(mietwagenItem);

        // Kraftstoff
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getKraftstoffImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getKraftstoffImages());
                JSONArray jsonArray = jsonObject.getJSONArray("kraftstoff_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem kraftstoffItem = new BelegItem(R.drawable.icon_kraftstoff, getResources().getString(R.string.beleg_kraftstoff), isChecked);
        mItemList.add(kraftstoffItem);

        // Parken
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getParkenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getParkenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("parken_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem parkenItem = new BelegItem(R.drawable.icon_parkkosten, getResources().getString(R.string.beleg_parken), isChecked);
        mItemList.add(parkenItem);

        // Tagungs
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getTagungsImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getTagungsImages());
                JSONArray jsonArray = jsonObject.getJSONArray("tagungs_images");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem tagungsItem = new BelegItem(R.drawable.icon_tagung, getResources().getString(R.string.beleg_tagungspauschale), isChecked);
        mItemList.add(tagungsItem);

        // Nebenkosten
        isChecked = false;
        if (travel != null && !TextUtils.isEmpty(travel.getNeben())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getNeben());
                JSONArray jsonArray = jsonObject.getJSONArray("neben");

                if (jsonArray.length() > 0) {
                    isChecked = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        BelegItem nebenItem = new BelegItem(R.drawable.icon_neben, getResources().getString(R.string.beleg_nebenkosten_sonstiges), isChecked);
        mItemList.add(nebenItem);

        mAdapter = new BelegAdapter(mItemList, this);
        mGvImageList.setAdapter(mAdapter);
    }

    private void handleOnItemClick(int position) {
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(BelegErfassenActivity.this, PrivaterActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 1:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.TAXI);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 2:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.FAHRTKOSTEN);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 3:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.MIETWAGEN);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 4:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.KRAFTSTOFF);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 5:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.PARKEN);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 6:
                intent = new Intent(BelegErfassenActivity.this, TaxiActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE), AppConfigs.BELEG_ITEM.TAGUNGSPAUSCHALE);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            case 7:
                intent = new Intent(BelegErfassenActivity.this, NebenkostenActivity.class);
                intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        initData();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (mIsFromZusam) {
            Intent intent = new Intent(BelegErfassenActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void onCancelButtonClick() {
        Intent intent = new Intent(BelegErfassenActivity.this, NoticeSaveTravelWithDraftActivity.class);
        intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onCheckboxClickListener(int position) {
        handleOnItemClick(position);
    }
}
