package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class GesendeteAntrageActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    Button mBtnReisen, mBtnVorschuss;
    ImageButton mIBtnBack, mIBtnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reise_abrechnen;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnReisen = (Button) findViewById(R.id.btn_reise_ohne_vorschuss);
        mBtnVorschuss = (Button) findViewById(R.id.btn_reise_mit_vorschuss);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnReisen.setOnClickListener(this);
        mBtnVorschuss.setOnClickListener(this);
    }

    private void initData() {
        mTvTitle.setText(getResources().getString(R.string.antrage_title));
        mBtnReisen.setText(getResources().getString(R.string.mein_reisen));
        mBtnVorschuss.setText(getResources().getString(R.string.mein_vorschuss));

        mBtnReisen.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_next, 0);
        mBtnVorschuss.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_next, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_reise_ohne_vorschuss:
                onReisenButtonClick();
                break;
            case R.id.btn_reise_mit_vorschuss:
                onVorschussButtonClick();
                break;
            default:
                break;
        }
    }

    private void onReisenButtonClick() {
        Intent intent = new Intent(GesendeteAntrageActivity.this, AntrageActivity.class);
        intent.putExtra(AppConfigs.IS_SENT_REISE, true);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onVorschussButtonClick() {
        Intent intent = new Intent(GesendeteAntrageActivity.this, AntrageActivity.class);
        intent.putExtra(AppConfigs.IS_SENT_REISE, false);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
