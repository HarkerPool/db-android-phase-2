package com.product.travel.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.ZielItemListener;
import com.product.travel.models.Travel;
import com.product.travel.models.User;
import com.product.travel.models.ZielItem;
import com.product.travel.utils.DateUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;
import com.product.travel.utils.ValidationUtils;
import com.product.travel.widget.EditTextOrtExt;
import com.product.travel.widget.LayoutZielItemExt;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ReiseUndActivity extends BaseActivity implements View.OnClickListener, ZielItemListener {

    TextView mTvTitle, mTvAddZiel, mTvDatumBegin, mTvUhrBegin, mTvDatumEnd, mTvUhrEnd;
    ImageButton mIBtnBack, mIBtnMenu, mIBtnAddZiel, mIBtnStarBegin;
    Button mBtnCancel, mBtnNext;
    EditText mEdtReason, mEdtBetrag;
    EditTextOrtExt mEdtOrtBeginExt;
    Spinner mSpnStarBegin;
    RadioButton mRdBtnBeamterYes, mRdBtnBeamterNo;

    LinearLayout mLinBeamterStatus, mLinZielItems, mLinAdvanceMoney;

    private int mYearBegin, mMonthBegin, mDayBegin, mHourBegin, mMinuteBegin;
    private int mYearEnd, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd;
    private boolean mIsBeginDateSelected, mIsBeginTimeSelected;

    private boolean mIsFromZusam;
    private boolean mIsBeginOrtStarClick;

    private User mUser;
    Travel mTravel;
    ArrayList<ZielItem> mZielItemList;

    private DateFormat[] formats = new DateFormat[] {
            DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.GERMAN),
//                    DateFormat.getDateTimeInstance(),
//                    DateFormat.getTimeInstance(),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reise_und;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mEdtReason = (EditText) findViewById(R.id.edt_reason);

        mIBtnAddZiel = (ImageButton) findViewById(R.id.ibtn_add_ziel);
        mTvAddZiel = (TextView) findViewById(R.id.tv_add_ziel);

        /* Beamter */
        mRdBtnBeamterYes = (RadioButton) findViewById(R.id.rbtn_beamter_yes);
        mRdBtnBeamterNo = (RadioButton) findViewById(R.id.rbtn_beamter_no);
        mLinBeamterStatus = (LinearLayout) findViewById(R.id.lin_beamter_status);

        /* Beginn */
        mEdtOrtBeginExt = (EditTextOrtExt) findViewById(R.id.edt_ort_begin);
        mIBtnStarBegin = (ImageButton) findViewById(R.id.ibtn_star_begin);
        mSpnStarBegin = (Spinner) findViewById(R.id.spn_star_begin);
        mTvDatumBegin = (TextView) findViewById(R.id.tv_datum_begin);
        mTvUhrBegin = (TextView) findViewById(R.id.tv_uhr_begin);

        /* End */
//        mEdtOrtEndExt = (EditTextOrtExt) findViewById(R.id.edt_ort_end);
//        mIBtnStarEnd = (ImageButton) findViewById(R.id.ibtn_star_end);
//        mSpnStarEnd = (Spinner) findViewById(R.id.spn_star_end);
        mTvDatumEnd = (TextView) findViewById(R.id.tv_datum_end);
        mTvUhrEnd = (TextView) findViewById(R.id.tv_uhr_end);

        mLinZielItems = (LinearLayout) findViewById(R.id.lin_ziel_items);
        mLinAdvanceMoney = (LinearLayout) findViewById(R.id.lin_advance_money);
        mEdtBetrag = (EditText) findViewById(R.id.edt_betrag);

        mTvTitle.setText(getResources().getString(R.string.reise_und_title));
    }

    private void initEvents() {
        mLinBeamterStatus.setVisibility(View.GONE);
        try {
            mUser = DBApplication.getServiceFactory().getUserService().getUserInfo();
            if (mUser == null) {
                mUser = UserUtils.convertStringToUser(StorageUtils.getUserInfo(ReiseUndActivity.this));
            }
            if (mUser != null) {
                String employeeStatus = mUser.getEmployeeStatus();
                if (!TextUtils.isEmpty(employeeStatus) && employeeStatus.equals("Zugewiesener Beamter")) {
                    mLinBeamterStatus.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mRdBtnBeamterYes.setOnClickListener(this);
        mRdBtnBeamterNo.setOnClickListener(this);
        mIBtnAddZiel.setOnClickListener(this);

        /* Begin */
        mIBtnStarBegin.setOnClickListener(this);
        mTvDatumBegin.setOnClickListener(this);
        mTvUhrBegin.setOnClickListener(this);

        mSpnStarBegin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsBeginOrtStarClick) {
                    mEdtOrtBeginExt.setText(mSpnStarBegin.getSelectedItem().toString());
                } else {
                    mIsBeginOrtStarClick = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* End */
//        mIBtnStarEnd.setOnClickListener(this);
        mTvDatumEnd.setOnClickListener(this);
        mTvUhrEnd.setOnClickListener(this);
        /*mSpnStarEnd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsEndOrtStarClick) {
                    mEdtOrtEndExt.setText(mSpnStarEnd.getSelectedItem().toString());
                } else {
                    mIsEndOrtStarClick = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mEdtBetrag.setEnabled(false); // always set false
    }

    private void initData() {
        if (mZielItemList == null) {
            mZielItemList = new ArrayList<>();
        } else {
            mZielItemList.clear();
        }

        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        String travelId = getIntent().getStringExtra(AppConfigs.TRAVEL_ID_KEY);
        if (mIsFromZusam) { // Edit from zusam
            mBtnNext.setText(getResources().getString(R.string.next_from_zusam));

            mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();
        } else if (!TextUtils.isEmpty(travelId)) { // Edit from Antrage / Entwurfe / Vorlagen
            DBApplication.getServiceFactory().getTravelService().resetTravel();

            mTravel = new Travel();
            TravelDbItem travelDbItem = TravelDbHelper.getTravelById(travelId);
            mTravel.setTravel(travelDbItem);

            if (travelDbItem != null && travelDbItem.isVorlagen()) { // Edit from Vorlagen
                mTravel.setTravelId("R" + System.currentTimeMillis());
                mTravel.setFavorite(false);
                mTravel.setTravelFrom(1);
            }
            if (mTravel.getTravelFrom() != 1) { // not edit from Entwurfe
                mTravel.setFavorite(false);
            }
        } else { // back from previous screen or create a new one
            mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();
            if (mTravel == null) { // Create a new one
                DBApplication.getServiceFactory().getTravelService().resetTravel();

                mTravel = new Travel();
                mTravel.setTravel(true);
                mTravel.setTravelId("R" + System.currentTimeMillis());
                mTravel.setTravelFrom(1);
            }
        }

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(ReiseUndActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(ReiseUndActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        StorageUtils.storageTravelId(ReiseUndActivity.this, mTravel.getTravelId());

        if (mLinBeamterStatus.getVisibility() != View.GONE) {
            if (mTravel.isEdit()) {
                if (mTravel.isBeamter()) {
                    mRdBtnBeamterYes.setChecked(true);
                    mRdBtnBeamterNo.setChecked(false);
                } else {
                    mRdBtnBeamterYes.setChecked(false);
                    mRdBtnBeamterNo.setChecked(true);
                }
            } else {
                mRdBtnBeamterYes.setChecked(false);
                mRdBtnBeamterNo.setChecked(false);
            }
        }
        mEdtReason.setText(mTravel.getReason());

        mUser = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (mUser == null) {
            mUser = UserUtils.convertStringToUser(StorageUtils.getUserInfo(ReiseUndActivity.this));
        }
        if (TextUtils.isEmpty(mTravel.getBeginCity())
                && mUser != null
                && mUser.getHomeAddressList() != null
                && mUser.getHomeAddressList().size() > 0) {
            mEdtOrtBeginExt.setText(mUser.getHomeAddressList().get(0).getCity());
            mTravel.setBeginCity(mUser.getHomeAddressList().get(0).getCity());
        } else {
            mEdtOrtBeginExt.setText(mTravel.getBeginCity());
        }
        /*if (TextUtils.isEmpty(mTravel.getEndCity())
                && mUser != null
                && mUser.getHomeAddressList() != null
                && mUser.getHomeAddressList().size() > 0) {
            mEdtOrtEndExt.setText(mUser.getHomeAddressList().get(0).getCity());
        } else {
            mEdtOrtEndExt.setText(mTravel.getEndCity());
        }*/

        mLinAdvanceMoney.setVisibility(View.GONE);
        if (mTravel.getAdvanceMoney() > 0) {
            mLinAdvanceMoney.setVisibility(View.VISIBLE);
            mEdtBetrag.setText(String.format(Locale.GERMAN, "%.2f", mTravel.getAdvanceMoney()));
        }

        /* Ziel(e) */
        if (!TextUtils.isEmpty(mTravel.getZiel())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    ZielItem item = new ZielItem();
                    item.setCity(jsonItem.getString("city"));
                    item.setStreet(jsonItem.getString("street"));

                    mZielItemList.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /* show all ziel tems */
        reloadAllItems();

        /* init Spinner value */
        String beginCity = StorageUtils.getBeginCityStarsValue(ReiseUndActivity.this);
        List<String> beginCityList = new ArrayList<>(Arrays.asList(beginCity.split(";;")));
        beginCityList.add(0, "");
        ArrayAdapter<String> beginCityAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, beginCityList);
        beginCityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpnStarBegin.setAdapter(beginCityAdapter);
        mIsBeginOrtStarClick = false;

        /*String endCity = StorageUtils.getEndCityStarsValue(ReiseUndActivity.this);
        List<String> endCityList = Arrays.asList(endCity.split(";;"));
        ArrayAdapter<String> endCityAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, endCityList);
        endCityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpnStarEnd.setAdapter(endCityAdapter);
        mIsEndOrtStarClick = false;*/

        final Calendar calendar = Calendar.getInstance();
        if (mTravel != null && mTravel.getBeginDate() != null) {
            calendar.setTime(mTravel.getBeginDate());
        }
        mYearBegin = calendar.get(Calendar.YEAR);
        mMonthBegin = calendar.get(Calendar.MONTH);
        mDayBegin = calendar.get(Calendar.DAY_OF_MONTH);
        mHourBegin = calendar.get(Calendar.HOUR_OF_DAY);
        mMinuteBegin = calendar.get(Calendar.MINUTE);
        if (mTravel != null && mTravel.getBeginDate() != null) {
            mTvDatumBegin.setText(String.format("%s", formats[0].format(new Date(mYearBegin - 1900, mMonthBegin, mDayBegin))));
            if (mTravel.isTravel()) { // Vorschuss has no time
                String hour = (mHourBegin > 9) ? ("" + mHourBegin) : ("0" + mHourBegin);
                String min = (mMinuteBegin > 9) ? ("" + mMinuteBegin) : ("0" + mMinuteBegin);
                mTvUhrBegin.setText(String.format(Locale.GERMAN, "%s:%s", hour, min));
            }
        }

        if (mTravel != null && mTravel.getEndDate() != null) {
            calendar.setTime(mTravel.getEndDate());
        }
        mYearEnd = calendar.get(Calendar.YEAR);
        mMonthEnd = calendar.get(Calendar.MONTH);
        mDayEnd = calendar.get(Calendar.DAY_OF_MONTH);
        mHourEnd = calendar.get(Calendar.HOUR_OF_DAY);
        mMinuteEnd = calendar.get(Calendar.MINUTE);
        if (mTravel != null && mTravel.getEndDate() != null) {
            mTvDatumEnd.setText(String.format("%s", formats[0].format(new Date(mYearEnd - 1900, mMonthEnd, mDayEnd))));
            if (mTravel.isTravel()) {
                String hour = (mHourEnd > 9) ? ("" + mHourEnd) : ("0" + mHourEnd);
                String min = (mMinuteEnd > 9) ? ("" + mMinuteEnd) : ("0" + mMinuteEnd);
                mTvUhrEnd.setText(String.format(Locale.GERMAN, "%s:%s", hour, min));
            }
        }
    }

    private void reloadAllItems() {
        mLinZielItems.removeAllViewsInLayout();

        /* Add first item if it's null */
        if (mZielItemList.size() == 0) {
            ZielItem item = new ZielItem();
            mZielItemList.add(item);
        }

        /* Add all items */
        LayoutZielItemExt linearItem = new LayoutZielItemExt(ReiseUndActivity.this, mZielItemList.get(0).getCity(), mZielItemList.get(0).getStreet(), 0, this);
        mLinZielItems.addView(linearItem);

        for (int i = 1; i < mZielItemList.size(); i++) {
            LayoutZielItemExt item = new LayoutZielItemExt(ReiseUndActivity.this, mZielItemList.get(i).getCity(), mZielItemList.get(i).getStreet(), i, this);
            mLinZielItems.addView(item);
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (mIsBeginDateSelected) {
                mYearBegin = year;
                mMonthBegin = monthOfYear;
                mDayBegin = dayOfMonth;
                mTvDatumBegin.setText(String.format("%s", formats[0].format(new Date(year - 1900, monthOfYear, dayOfMonth))));
            } else {
                mYearEnd = year;
                mMonthEnd = monthOfYear;
                mDayEnd = dayOfMonth;
                mTvDatumEnd.setText(String.format("%s", formats[0].format(new Date(year - 1900, monthOfYear, dayOfMonth))));
            }
        }
    };

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if (mIsBeginTimeSelected) {
                mHourBegin = hourOfDay;
                mMinuteBegin = minute;
                String hour = (mHourBegin > 9) ? ("" + mHourBegin) : ("0" + mHourBegin);
                String min = (mMinuteBegin > 9) ? ("" + mMinuteBegin) : ("0" + mMinuteBegin);
                mTvUhrBegin.setText(String.format(Locale.GERMAN, "%s:%s", hour, min));
            } else {
                mHourEnd = hourOfDay;
                mMinuteEnd = minute;
                String hour = (mHourEnd > 9) ? ("" + mHourEnd) : ("0" + mHourEnd);
                String min = (mMinuteEnd > 9) ? ("" + mMinuteEnd) : ("0" + mMinuteEnd);
                mTvUhrEnd.setText(String.format(Locale.GERMAN, "%s:%s", hour, min));
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                /*Calendar minBeginDate = Calendar.getInstance();
                minBeginDate.add(Calendar.MONTH, -6);

                Calendar maxBeginDate = Calendar.getInstance();

                DatePickerDialog beginDatePickerDialog = new DatePickerDialog(this, dateSetListener, mYearBegin, mMonthBegin, mDayBegin);
                beginDatePickerDialog.getDatePicker().setMinDate(minBeginDate.getTimeInMillis());
                beginDatePickerDialog.getDatePicker().setMaxDate(maxBeginDate.getTimeInMillis());

                return beginDatePickerDialog;*/
            case 1:
                Calendar minEndDate = Calendar.getInstance();
                minEndDate.add(Calendar.MONTH, -6);

                Calendar maxEndDate = Calendar.getInstance();
                maxEndDate.set(maxEndDate.get(Calendar.YEAR), maxEndDate.get(Calendar.MONTH), maxEndDate.get(Calendar.DAY_OF_MONTH), 23, 59, 59);

                DatePickerDialog endDatePickerDialog = new DatePickerDialog(this, dateSetListener, mYearBegin, mMonthBegin, mDayBegin);
                endDatePickerDialog.getDatePicker().setMinDate(minEndDate.getTimeInMillis());
//                endDatePickerDialog.getDatePicker().setMaxDate(maxEndDate.getTimeInMillis());

                return endDatePickerDialog;
            case 2:
                return new TimePickerDialog(this, timeSetListener, mHourBegin, mMinuteBegin, true);
            case 3:
                return new TimePickerDialog(this, timeSetListener, mHourEnd, mMinuteEnd, true);
        }
        return null;
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            mEdtReason.setError(getResources().getString(R.string.empty_reason_reise_msg));
            mEdtReason.requestFocus();
            return false;
        }

        /* Beamter */
        if (mLinBeamterStatus.getVisibility() != View.GONE && !mRdBtnBeamterYes.isChecked() && !mRdBtnBeamterNo.isChecked()) {
            Toast.makeText(ReiseUndActivity.this, getString(R.string.beamter_not_check_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* Ziel */
        for (int i = 0; i < mZielItemList.size(); i++) {
            if (TextUtils.isEmpty(mZielItemList.get(i).getCity())) {
                Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_ort_ziel_msg), Toast.LENGTH_LONG).show();
                return false;
            }
            if (TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_strabe_ziel_msg), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        /* Begin */
        if (TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            mEdtOrtBeginExt.setError(getResources().getString(R.string.empty_ort_begin_msg));
            mEdtOrtBeginExt.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim())) {
            Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_datum_begin_msg), Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(mTvUhrBegin.getText().toString().trim())) {
            Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_uhr_begin_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* End */
        /*if (TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            mEdtOrtEndExt.setError(getResources().getString(R.string.empty_ort_end_msg));
            mEdtOrtEndExt.requestFocus();
            return false;
        }*/
        if (TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim())) {
            Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_datum_end_msg), Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(mTvUhrEnd.getText().toString().trim())) {
            Toast.makeText(ReiseUndActivity.this, getResources().getString(R.string.empty_uhr_end_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* Validate Date */
        Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, mHourBegin, mMinuteBegin);
        Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd);
        if (!ValidationUtils.isValidDate(ReiseUndActivity.this, beginDate, endDate, true)) {
            return false;
        }

        /* Check has overlap date? */
        if (TravelDbHelper.hasOverlapDate(beginDate, endDate, mTravel.getTravelId())) {
            Toast.makeText(ReiseUndActivity.this, getString(R.string.notice_reise_date_between_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private boolean checkHasDateChanged(Date beginDate, Date endDate) {
        if ((mTravel.getRadioVerp() != null && mTravel.getRadioVerp())
                ||(mTravel.getRadioBeleg() != null && mTravel.getRadioBeleg())) {
            if (mTravel.getBeginDate() != null && mTravel.getEndDate() != null
                    && beginDate != null && endDate != null) {
                if ((mTravel.getBeginDate().compareTo(beginDate) != 0)
                        || mTravel.getEndDate().compareTo(endDate) != 0) {

                    return true;
                }
            }
        }

        return false;
    }

    private void getLatestTravelInfo() {
        mTravel.setTravel(true);
        mTravel.setDraft(true);
        mTravel.setVorlagen(false);

        if (TextUtils.isEmpty(mTravel.getTravelId())) {
            mTravel.setTravelId("R" + System.currentTimeMillis());
        }
//        mTravel.setFavorite(false); // should be set at Zusam screen

        if (TextUtils.isEmpty(mTravel.getCreateTime())) {
            mTravel.setCreateTime(DateUtils.getCurrentDateTime()); // update it when getting pdf file from the server
        }
        mTravel.setReason(mEdtReason.getText().toString().trim());

        if (mLinBeamterStatus.getVisibility() != View.GONE) {

            mTravel.setEdit(true);

            if (mRdBtnBeamterYes.isChecked()) {
                mTravel.setBeamter(true);
            } else if (mRdBtnBeamterNo.isChecked()) {
                mTravel.setBeamter(false);
            }
        } else {
            mTravel.setEdit(false);
            mTravel.setBeamter(false);
        }

        /* Ziel(e) */
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < mZielItemList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("city", mZielItemList.get(i).getCity());
                jsonObject.put("street", mZielItemList.get(i).getStreet());
                jsonArray.put(jsonObject);
            }

            JSONObject ziel = new JSONObject();
            ziel.put("ziel", jsonArray);
            mTravel.setZiel(ziel.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /* Begin */
        mTravel.setBeginCity(mEdtOrtBeginExt.getText().toString().trim());

        if (!TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim())) {
            Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, mHourBegin, mMinuteBegin);
            mTravel.setBeginDate(beginDate);
        }

        /* End */
//        mTravel.setEndCity(mEdtOrtEndExt.getText().toString().trim());

        if (!TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim())) {
            Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd);
            mTravel.setEndDate(endDate);
        }

        /* 1st pdf date */
        if (mTravel.getFirstPdfDate() == null) {
            mTravel.setFirstPdfDate(DateUtils.convertDateToStringForCreatePDF(mTravel.getBeginDate()));
        }
    }

    private boolean hasModify() {
        /* Reason */
        if (TextUtils.isEmpty(mTravel.getReason()) && !TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getReason()) && TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getReason()) && !TextUtils.equals(mTravel.getReason(), mEdtReason.getText().toString().trim())) {
            return true;
        }

        /* Beamter */
        if (mLinBeamterStatus.getVisibility() != View.GONE) {
            if (!mTravel.isEdit()) {
                if (mRdBtnBeamterYes.isChecked() || mRdBtnBeamterNo.isChecked()) {
                    return true;
                }
            } else {
                if (mTravel.isBeamter() && !mRdBtnBeamterYes.isChecked() && mRdBtnBeamterNo.isChecked()) {
                    return true;
                }
                if (!mTravel.isBeamter() && mRdBtnBeamterYes.isChecked() && !mRdBtnBeamterNo.isChecked()) {
                    return true;
                }
            }
        }

        /* Ziel(e) */
        if (TextUtils.isEmpty(mTravel.getZiel())) {
            for (int i = 0; i < mZielItemList.size(); i++) {
                if (!TextUtils.isEmpty(mZielItemList.get(i).getCity()) || !TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                    return true;
                }
            }
        } else {
            ArrayList<ZielItem> zielItemList = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    ZielItem item = new ZielItem();
                    item.setCity(jsonItem.getString("city"));
                    item.setStreet(jsonItem.getString("street"));

                    zielItemList.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < mZielItemList.size() && i < zielItemList.size(); i++) {
                if (!TextUtils.equals(mZielItemList.get(i).getCity(), zielItemList.get(i).getCity())) {
                    return true;
                }
                if (!TextUtils.equals(mZielItemList.get(i).getStreet(), zielItemList.get(i).getStreet())) {
                    return true;
                }
            }
            if (mZielItemList.size() < zielItemList.size()) {
                for (int i = mZielItemList.size(); i < zielItemList.size(); i++) {
                    if (!TextUtils.isEmpty(zielItemList.get(i).getCity()) || !TextUtils.isEmpty(zielItemList.get(i).getStreet())) {
                        return true;
                    }
                }
            } else {
                for (int i = zielItemList.size(); i < mZielItemList.size(); i++) {
                    if (!TextUtils.isEmpty(mZielItemList.get(i).getCity()) || !TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                        return true;
                    }
                }
            }
        }

        /* Begin city */
        if (TextUtils.isEmpty(mTravel.getBeginCity()) && !TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getBeginCity()) && TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getBeginCity()) && !TextUtils.equals(mTravel.getBeginCity(), mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }

        /* End city */
        /*if (TextUtils.isEmpty(mTravel.getEndCity()) && !TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getEndCity()) && TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getEndCity()) && !TextUtils.equals(mTravel.getEndCity(), mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }*/

        /* Begin date */
        if ((mTravel.getBeginDate() == null && !TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim()))
                || (mTravel.getBeginDate() != null && TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim()))) {
            return true; // TODO - need to check mTvUhrBegin field
        }
        Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, mHourBegin, mMinuteBegin);
        if (mTravel.getBeginDate() != null && mTravel.getBeginDate().compareTo(beginDate) != 0) {
            return true;
        }

        /* End date */
        if ((mTravel.getEndDate() == null && !TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim()))
                || (mTravel.getEndDate() != null && TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim()))) {
            return true; // TODO - need to check mTvUhrEnd field
        }
        Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd);
        if (mTravel.getEndDate() != null && mTravel.getEndDate().compareTo(endDate) != 0) {
            return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        initData();
    }

    @Override
    public void onBackPressed() {

        Intent intent;
        if (mIsFromZusam) {
            intent = new Intent(ReiseUndActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            if (hasModify()) {
                mTravel.setModify(true);
                getLatestTravelInfo();
            }
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

            if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
                intent = new Intent(ReiseUndActivity.this, NoticeSaveTravelWithDraftActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            } else {
                intent = new Intent(ReiseUndActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        }

        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /* Beamter */
            case R.id.rbtn_beamter_yes:
                onBeamterYesRadioButtonClick();
                break;
            case R.id.rbtn_beamter_no:
                onBeamterNoRadioButtonClick();
                break;

            /* Ziel */
            case R.id.ibtn_add_ziel:
                onAddZielButtonClick();
                break;

            /* Begin */
            case R.id.ibtn_star_begin:
                onStarBeginIconClick();
                break;
            case R.id.tv_datum_begin:
                onSetBeginDateClick();
                break;
            case R.id.tv_uhr_begin:
                onSetBeginTimeClick();
                break;

            /* End */
            case R.id.ibtn_star_end:
//                onStarEndIconClick();
                break;
            case R.id.tv_datum_end:
                onSetEndDateClick();
                break;
            case R.id.tv_uhr_end:
                onSetEndTimeClick();
                break;

            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                boolean hasModify = hasModify();
                if (hasModify) {
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onBeamterYesRadioButtonClick() {
        mRdBtnBeamterYes.setChecked(true);
        mRdBtnBeamterNo.setChecked(false);
    }

    private void onBeamterNoRadioButtonClick() {
        mRdBtnBeamterYes.setChecked(false);
        mRdBtnBeamterNo.setChecked(true);
    }

    private void onAddZielButtonClick() {
        if (mZielItemList.size() < 5) { // max = 5
            ZielItem item = new ZielItem();
            mZielItemList.add(item);
            LayoutZielItemExt linearItem = new LayoutZielItemExt(ReiseUndActivity.this, "", "", mZielItemList.size() - 1, this);
//            mLinZielItems.addView(linearItem, mZielItemList.size() - 1);
            mLinZielItems.addView(linearItem);
        }
        if (mZielItemList.size() == 5) {
            mIBtnAddZiel.setVisibility(View.GONE);
            mTvAddZiel.setVisibility(View.GONE);
        }
    }

    private void onStarBeginIconClick() {
        mSpnStarBegin.performClick();
    }

    private void onSetBeginDateClick() {
        mIsBeginDateSelected = true;
        showDialog(1);
    }

    private void onSetBeginTimeClick() {
        mIsBeginTimeSelected = true;
        showDialog(2);
    }

    /*private void onStarEndIconClick() {
        mSpnStarEnd.performClick();
    }*/

    private void onSetEndDateClick() {
        mIsBeginDateSelected = false;
        showDialog(1);
    }

    private void onSetEndTimeClick() {
        mIsBeginTimeSelected = false;
        showDialog(3);
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(ReiseUndActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(ReiseUndActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            /* Check has date changed */
            Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, mHourBegin, mMinuteBegin);
            Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd);
            boolean hasDateChanged = checkHasDateChanged(beginDate, endDate);

            if (hasModify()) {
                mTravel.setModify(true);
                getLatestTravelInfo();
            }
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);

            Date currentDate = new Date();
            if (endDate.compareTo(currentDate) > 0) {
                theDateInFutureNotification(hasDateChanged);
            } else {
                goToNextScreen(hasDateChanged);
            }
        }
    }

    public void theDateInFutureNotification(final boolean hasDateChanged) {
        final Dialog dialog = new Dialog(ReiseUndActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.notification));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_reise_end_date_in_future_msg));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                goToNextScreen(hasDateChanged);
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void goToNextScreen(boolean hasDateChanged) {
        Intent intent;
        if (mIsFromZusam) {
            intent = new Intent(ReiseUndActivity.this, ZusamActivity.class);
            finish();
        } else {
            intent = new Intent(ReiseUndActivity.this, ReisekostenActivity.class);
        }
        intent.putExtra(AppConfigs.HAS_DATE_CHANGED, hasDateChanged);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onRemoveListener(int index) {
        if (mZielItemList != null && mZielItemList.size() > index) {
            mZielItemList.remove(index);
        }
        if (mLinZielItems != null) {
            try {
                mLinZielItems.removeViewAt(index);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        mIBtnAddZiel.setVisibility(View.VISIBLE);
        mTvAddZiel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTextChangeListener(int index, String city, String street) {
        try {
            mZielItemList.get(index).setCity(city);
            mZielItemList.get(index).setStreet(street);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
