package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import com.product.travel.R;
import com.product.travel.adapters.AntrageAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.AntrageItemListener;
import com.product.travel.utils.DownloaderUtils;
import com.product.travel.utils.SecurityUtils;
import com.product.travel.widget.DividerRecyclerViewExt;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ReiseMitVorschussActivity extends BaseActivity implements View.OnClickListener, AntrageItemListener {

    TextView mTvTitle;
    RecyclerView mRecyclerView;
    ImageButton mIBtnBack, mIBtnMenu;
    List<TravelDbItem> mVorschussList;
    AntrageAdapter mVorschussAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reise_mit_vorschuss;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_reise_mit_vorschuss);

        mTvTitle.setText(getResources().getString(R.string.reise_mit_item));
    }

    private void initEvents() {
        mRecyclerView.addItemDecoration(new DividerRecyclerViewExt(ReiseMitVorschussActivity.this, LinearLayoutManager.VERTICAL));
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
    }

    private void initData() {
        mVorschussList = TravelDbHelper.getAntrage(-1, false);

        mVorschussAdapter = new AntrageAdapter(false, mVorschussList, this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mVorschussAdapter);
        mVorschussAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onReiseViewInfoListener(int position) {

    }

    @Override
    public void onVorschussViewInfoListener(int position) {
        if (mVorschussList.get(position).getStatus() == 0) {
            Toast.makeText(this, getString(R.string.empty_message), Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(ReiseMitVorschussActivity.this, NachrichtenDetailActivity.class);
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID, mVorschussList.get(position).getTravelId());
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY, "");
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    @Override
    public void onReiseEditListener(int position) {

    }

    @Override
    public void onVorschussEditListener(int position) {
        if (mVorschussList.get(position).getStatus() == 0) {
            Toast.makeText(ReiseMitVorschussActivity.this, getString(R.string.pdf_is_sending), Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(ReiseMitVorschussActivity.this, ReiseUndActivity.class);
            intent.putExtra(AppConfigs.TRAVEL_ID_KEY, mVorschussList.get(position).getTravelId());
            intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    @Override
    public void onReiseDeleteListener(int position) {

    }

    @Override
    public void onVorschussDeleteListener(int position) {
        /*TravelDbHelper.deleteTravelById(mVorschussList.get(position).getTravelId());
        mVorschussList.remove(position);
        mVorschussAdapter.notifyDataSetChanged();*/
        deleteConfirmationDialog(position);
    }

    @Override
    public void onReiseOpenPdfListener(int position) {

    }

    @Override
    public void onVorschussOpenPdfListener(int position) {
//        PDFUtils.openPDF(ReiseMitVorschussActivity.this, mVorschussList.get(position).getTravelId());
//        if (!checkNetworkConnection()) {
//            Toast.makeText(ReiseMitVorschussActivity.this, "Error when downloading PDF. " + getString(R.string.notice_no_internet), Toast.LENGTH_LONG).show();
//        }
        new DownloadPdf().execute(mVorschussList.get(position).getTravelId(), mVorschussList.get(position).getPdfName(), mVorschussList.get(position).getPdfMd5());
    }

    private void deleteConfirmationDialog(final int position) {
        final Dialog dialog = new Dialog(ReiseMitVorschussActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.delete));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_vorschuss_delete));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                updateToken();

                ThreadInBackground.deleteTravel(ReiseMitVorschussActivity.this, mVorschussList.get(position).getTravelId());
                mVorschussList.remove(position);
                mVorschussAdapter.notifyDataSetChanged();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void downloadFile(String travelId, String pdfName, File pdfFile) {
        String url = AppConfigs.PDF_URL + travelId + "/" + pdfName;
        DownloaderUtils.downloadFile(url, pdfFile);
    }

    private void downloadFile(String travelId, String pdfName, String pdfMd5) {
        try {
            String path = Environment.getExternalStorageDirectory().toString();
            File folder = new File(path, "pdf");
            folder.mkdirs();

            File pdfFile = new File(folder, pdfName);

            if (!pdfFile.exists() || pdfFile.length() == 0) {

                pdfFile.createNewFile();
                downloadFile(travelId, pdfName, pdfFile);
            } else {
                String pdfPath = folder + "/" + pdfName;

                FileInputStream fis = new FileInputStream(pdfPath);
                String md5 = SecurityUtils.getMd5(fis);
                if (!md5.equalsIgnoreCase(pdfMd5)) {
                    downloadFile(travelId, pdfName, pdfFile);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

//            Toast.makeText(ReiseMitVorschussActivity.this, "Error when downloading. PDF could not saved.", Toast.LENGTH_LONG).show();
        }
    }

    private class DownloadPdf extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoadingDialog(true);
        }

        @Override
        protected String doInBackground(String... params) {
            downloadFile(params[0], params[1], params[2]);

            return params[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            showLoadingDialog(false);

            try {
                File pdfFile = new File(Environment.getExternalStorageDirectory() + "/pdf/" + result);
                Uri path = Uri.fromFile(pdfFile);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();

                Toast.makeText(ReiseMitVorschussActivity.this, "PDF could not found / No application available to view PDF.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
