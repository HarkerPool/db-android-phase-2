package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 6/18/16.
 */
public class HifleActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnVersion, mBtnFaq, mBtnContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_hilfe;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnVersion = (Button) findViewById(R.id.btn_version);
        mBtnFaq = (Button) findViewById(R.id.btn_faq);
        mBtnContact = (Button) findViewById(R.id.btn_contact);

        mTvTitle.setText(getResources().getString(R.string.hilfe));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnVersion.setOnClickListener(this);
        mBtnFaq.setOnClickListener(this);
        mBtnContact.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HifleActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_version:
                onVersionButtonClick();
                break;
            case R.id.btn_faq:
                onFaqButtonClick();
                break;
            case R.id.btn_contact:
                onContactButtonClick();
                break;
            default:
                break;
        }
    }

    private void onVersionButtonClick() {
        Intent intent = new Intent(HifleActivity.this, VersionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onFaqButtonClick() {
        Intent intent = new Intent(HifleActivity.this, OpenInternalWebViewActivity.class);
        intent.putExtra(AppConfigs.TITLE_URL, getString(R.string.faq));
        intent.putExtra(AppConfigs.INTERNAL_URL_KEY, AppConfigs.FAQ_URL);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void onContactButtonClick() {
        Intent intent = new Intent(HifleActivity.this, ContactActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
}
