package com.product.travel.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class PrivaterActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnCancel, mBtnNext;
    EditText mEdtKm;
    ImageView mIvMap;
    private Travel mTravel;
    private boolean mIsFromZusam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_privater_pkw;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mEdtKm = (EditText) findViewById(R.id.edt_km);
        mIvMap = (ImageView) findViewById(R.id.iv_map);

        mTvTitle.setText(getResources().getString(R.string.privater_title));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mIvMap.setOnClickListener(this);
        mEdtKm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String oriText = s.toString();
                String comma = "";
                if (!TextUtils.isEmpty(oriText) && oriText.charAt(oriText.length() - 1) == ',') {
                    comma = ",";
                }
                String text = getCommaSeparatedString(s);
                mEdtKm.removeTextChangedListener(this);
                s.clear();
                s.append(text);
                s.append(comma);
                mEdtKm.addTextChangedListener(this);
            }
        });
    }

    private void initData() {
        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getString(R.string.next_from_zusam));
        }

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(PrivaterActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }
        if (mTravel == null) {
            Toast.makeText(PrivaterActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        if (mTravel.getPrivater() > 0) {
            mEdtKm.setText(String.format(Locale.GERMAN, "%d", mTravel.getPrivater()));
        }
    }

    private String getCommaSeparatedString(Editable s) {
        String newText = s.toString();
        if (!TextUtils.isEmpty(newText) && newText.trim().charAt(0) == '.') {
            newText = "";
        }
        int iComTmp = newText.indexOf(",");
        String decimal = "";

        // integer
        String integer = newText.substring(0, iComTmp > 0 ? iComTmp : newText.length());
        integer = integer.replace(".", "");

        // decimal
        if (iComTmp > 0) {
            if (iComTmp != newText.length() - 1) {
                decimal = newText.substring(iComTmp, iComTmp + 3 > newText.length() ? newText.length() : iComTmp + 3);
            } else {
                decimal = "";
            }
            decimal = decimal.replace(",", ".");
        }

        String format = newText;
        if (!TextUtils.isEmpty(format)) {
            decimal = decimal.replace("..", "");
            Locale locale = Locale.GERMAN;
            NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
            numberFormat.setMaximumFractionDigits(1);
            numberFormat.setMaximumIntegerDigits(9);
            try {
                integer += decimal;
                if (!TextUtils.isEmpty(integer)) {
                    format = numberFormat.format(Double.parseDouble(integer));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return format;
    }

    @Override
    protected void onResume() {
        super.onResume();

        initData();
    }

    @Override
    public void onBackPressed() {
        if (mIsFromZusam) {
            Intent intent = new Intent(PrivaterActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            if (hasModify()) {
                AlertUtils.showMessageAlert(PrivaterActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
            } else {
                super.onBackPressed();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    if (mTravel.getPrivater() > 0) {
                        mTravel.setRadioBeleg(true);
                    } else {
                        // TODO - if taxi and neben are null --> mTravel.setRadioBeleg(false);
                    }
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.iv_map:
                onMapIconClick();
                break;
            default:
                break;
        }
    }

    private boolean hasModify() {
        if (!TextUtils.isEmpty(mEdtKm.getText().toString().trim())) {
            String km = mEdtKm.getText().toString().trim().replace(".", "");
            try {
                int privater = Integer.parseInt(km);
                if (mTravel.getPrivater() != privater) {
                    return true;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else { // if it's empty --> its value is 0
            if (mTravel.getPrivater() != 0) {
                return true;
            }
        }

        return false;
    }

    /*private boolean isValidate() {
        if (TextUtils.isEmpty(mEdtKm.getText().toString().trim())) {
            return false;
        }
        return true;
    }*/

    private void getLatestTravelInfo() {
        mTravel.setPrivater(0);

        if (!TextUtils.isEmpty(mEdtKm.getText().toString().trim())) {
            String km = mEdtKm.getText().toString().trim().replace(".", "");
            try {
                mTravel.setPrivater(Integer.parseInt(km));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void onMapIconClick() {
        Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(PrivaterActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(PrivaterActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (hasModify()) {
            mTravel.setModify(true);

            getLatestTravelInfo();
            if (mTravel.getPrivater() > 0) {
                mTravel.setRadioBeleg(true);
            } else {
                // TODO - if taxi and neben are null --> mTravel.setRadioBeleg(false);
            }

            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);
        }

        if (mIsFromZusam) {
            Intent intent = new Intent(PrivaterActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            finish();
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
    }
}
