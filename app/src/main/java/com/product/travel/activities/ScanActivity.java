package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.utils.ImageUtils;
import com.product.travel.widget.PolygonView;
import com.product.travel.widget.TouchImageView;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ScanActivity extends BaseActivity implements View.OnClickListener {
    private Button mBtnOriginalMode, mBtnGrauMode, mBtnSWMode, mBtnDone;
    private ImageView mIvImageScanned;
    private ImageButton mIBtnRotate;
    private FrameLayout mFrmImage;
    private PolygonView mPolygonView;

    private Bitmap mOriginalBitmap, mScannedBitmap;
    private int mRotateNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_scan;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mBtnOriginalMode = (Button) findViewById(R.id.btn_original_mode);
        mBtnGrauMode = (Button) findViewById(R.id.btn_grau_mode);
        mBtnSWMode = (Button) findViewById(R.id.btn_sw_mode);
        mBtnDone = (Button) findViewById(R.id.btn_next);
        mIvImageScanned = (ImageView) findViewById(R.id.iv_image_scanned);
        mIBtnRotate = (ImageButton) findViewById(R.id.ibtn_rotate);
        mFrmImage = (FrameLayout) findViewById(R.id.frame_image);
        mPolygonView = (PolygonView) findViewById(R.id.polygonView);
    }

    private void initEvents() {
        mBtnOriginalMode.setOnClickListener(this);
        mBtnGrauMode.setOnClickListener(this);
        mBtnSWMode.setOnClickListener(this);
        mBtnDone.setOnClickListener(this);
        mIBtnRotate.setOnClickListener(this);
        mIvImageScanned.setOnClickListener(this);
    }

    private void initData() {
        mBtnDone.setText(getString(R.string.done));

        mRotateNum = 0;

        new GetBitmapTask().execute();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_original_mode:
                onOriginalButtonClick();
                break;
            case R.id.btn_grau_mode:
                onGrauButtonClick();
                break;
            case R.id.btn_sw_mode:
                onSWButtonClick();
                break;
            case R.id.btn_next:
                onDoneButtonClick();
                break;
            case R.id.ibtn_rotate:
                onRotateButtonClick();
                break;
            case R.id.iv_image_scanned:
                reviewImageZoom();
                break;
            default:
                break;
        }
    }

    private void onOriginalButtonClick() {
        mScannedBitmap = mOriginalBitmap;
        new RotateBitmapTask().execute(mRotateNum);
    }

    private void onGrauButtonClick() {
        try {
            mScannedBitmap = getGrauBitmap(mOriginalBitmap);
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }
        new RotateBitmapTask().execute(mRotateNum);
    }

    private void onSWButtonClick() {
        try {
            mScannedBitmap = getSWBitmap(mOriginalBitmap);
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }
        new RotateBitmapTask().execute(mRotateNum);
    }

    private void onRotateButtonClick() {
        mRotateNum++;
        if (mRotateNum == 4) {
            mRotateNum = 0;
        }
        new RotateBitmapTask().execute(1);
    }

    private void onDoneButtonClick() {
        try {
            Map<Integer, PointF> points = mPolygonView.getPoints();
            if (points.size() == 4) {
                new ScannedBitmapTask(points).execute();
            } else {
                noticeWhenGettingPictureError();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            System.gc();
        }
    }

    private void noticeWhenGettingPictureError() {
        showLoadingDialog(false);
        Toast.makeText(ScanActivity.this, getString(R.string.can_not_crop), Toast.LENGTH_LONG).show();
        finish();
    }

    private void cacheOriginalBitmap(Bitmap imageBitmap) {
        int newWidth, newHeight;

        if (imageBitmap.getHeight() > imageBitmap.getWidth()) {
            newHeight = 2048;
            newWidth = newHeight * imageBitmap.getWidth() / imageBitmap.getHeight();
        } else {
            newWidth = 2048;
            newHeight = newWidth * imageBitmap.getHeight() / imageBitmap.getWidth();
        }

        mOriginalBitmap = Bitmap.createScaledBitmap(imageBitmap, newWidth, newHeight, true);

        // Rotate image
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        mOriginalBitmap = Bitmap.createBitmap(mOriginalBitmap, 0, 0, mOriginalBitmap.getWidth(), mOriginalBitmap.getHeight(), matrix, true);

        try {
            System.gc();
            mScannedBitmap = mOriginalBitmap;
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();

            mScannedBitmap = mOriginalBitmap;
            System.gc();
        }

        showLoadingDialog(false);
    }

    private void rotateBitmap(int rotateNum) {
        try {
            System.gc();
            Matrix matrix = new Matrix();
            matrix.postRotate(rotateNum * 90);
            mScannedBitmap = Bitmap.createBitmap(mScannedBitmap, 0, 0, mScannedBitmap.getWidth(), mScannedBitmap.getHeight(), matrix, true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showScannedBitmap() {
        int newWidth, newHeight;

        if (mScannedBitmap.getHeight() > mScannedBitmap.getWidth()) {
            newHeight = mFrmImage.getHeight();
            newWidth = newHeight * mScannedBitmap.getWidth() / mScannedBitmap.getHeight();
        } else {
            newWidth = mFrmImage.getWidth();
            newHeight = newWidth * mScannedBitmap.getHeight() / mScannedBitmap.getWidth();
        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mScannedBitmap, newWidth, newHeight, true);
        mIvImageScanned.setImageBitmap(scaledBitmap);

        Map<Integer, PointF> pointFs = getEdgePoints(scaledBitmap);
        mPolygonView.setPoints(pointFs);
        mPolygonView.setVisibility(View.VISIBLE);

        int padding = (int) getResources().getDimension(R.dimen.scan_padding);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(scaledBitmap.getWidth() + 2 * padding, scaledBitmap.getHeight() + 2 * padding);
        layoutParams.gravity = Gravity.CENTER;
        mPolygonView.setLayoutParams(layoutParams);

        long downTime = SystemClock.uptimeMillis();
        long eventTime = downTime + 1;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_UP, 0.0f, 0.0f, 0);
        mPolygonView.dispatchTouchEvent(motionEvent);
    }

    private Map<Integer, PointF> getEdgePoints(Bitmap bitmap) {
        List<PointF> pointFs = getContourEdgePoints(bitmap);

        return orderedValidEdgePoints(bitmap, pointFs);
    }

    private List<PointF> getContourEdgePoints(Bitmap bitmap) {
        float[] points = getPoints(bitmap);
        float x1 = points[0];
        float x2 = points[1];
        float x3 = points[2];
        float x4 = points[3];
        if (x1 > x2) { // get min
            x1 = x2;
        } else {
            x2 = x1;
        }
        if (x3 > x4) { // get max
            x4 = x3;
        } else {
            x3 = x4;
        }


        float y1 = points[4];
        float y2 = points[5];
        float y3 = points[6];
        float y4 = points[7];
        if (y1 > y4) { // get min
            y1 = y4;
        } else {
            y4 = y1;
        }
        if (y2 > y3) { // get max
            y3 = y2;
        } else {
            y2 = y3;
        }

        List<PointF> pointFs = new ArrayList<>();
        pointFs.add(new PointF(x1, y1));
        pointFs.add(new PointF(x2, y2));
        pointFs.add(new PointF(x3, y3));
        pointFs.add(new PointF(x4, y4));

        return pointFs;
    }

    private Map<Integer, PointF> orderedValidEdgePoints(Bitmap bitmap, List<PointF> pointFs) {
        Map<Integer, PointF> orderedPoints = mPolygonView.getOrderedPoints(pointFs);

        if (!mPolygonView.isValidShape(orderedPoints)) {
            orderedPoints = getOutlinePoints(bitmap);
        }

        return orderedPoints;
    }

    private Map<Integer, PointF> getOutlinePoints(Bitmap bitmap) {
        Map<Integer, PointF> outlinePoints = new HashMap<>();

        outlinePoints.put(0, new PointF(0, 0));
        outlinePoints.put(1, new PointF(bitmap.getWidth(), 0));
        outlinePoints.put(2, new PointF(0, bitmap.getHeight()));
        outlinePoints.put(3, new PointF(bitmap.getWidth(), bitmap.getHeight()));

        return outlinePoints;
    }

    private String getScannedBitmap(Bitmap bitmap, Map<Integer, PointF> points) {
        System.gc();

        Bitmap result = null;

        try {
            result = getBitmapPoints(bitmap, points);
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
        }

        if (result == null) {
            int realWidth = bitmap.getWidth();
            int realHeight = bitmap.getHeight();
            float ratio = (float) (realHeight * 1.0) / realWidth;
            int imageHeight = realHeight - 20;
            int imageWidth = (int) (imageHeight / ratio);
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, imageWidth, imageHeight);
            result = getBitmapPoints(bitmap, points);
        }

//        String imagePath = ImageUtils.getImagePath(ScanActivity.this) + "Image" + System.currentTimeMillis() + ".jpg";
        String imageName = "Image" + System.currentTimeMillis() + ".jpg";
        if(ImageUtils.storageBitmapImage(result, ImageUtils.getImagePath(ScanActivity.this) + imageName)) {
            ImageUtils.scaleAndStorageCacheImage(ScanActivity.this, imageName);
        }

        if (!result.isRecycled()) {
            result.recycle();
            result = null;
        }

        return imageName;
    }

    private Bitmap getBitmapPoints(Bitmap bitmap, Map<Integer, PointF> points) {
        System.gc();

        float xRatio = (float) bitmap.getWidth() / mIvImageScanned.getWidth();
        float yRatio = (float) bitmap.getHeight() / mIvImageScanned.getHeight();

        float x1 = (points.get(0).x) * xRatio;
        float x2 = (points.get(1).x) * xRatio;
        float x3 = (points.get(2).x) * xRatio;
        float x4 = (points.get(3).x) * xRatio;

        float y1 = (points.get(0).y) * yRatio;
        float y2 = (points.get(1).y) * yRatio;
        float y3 = (points.get(2).y) * yRatio;
        float y4 = (points.get(3).y) * yRatio;

        return getScannedBitmap(bitmap, x1, y1, x2, y2, x3, y3, x4, y4);
    }

    private void reviewImageZoom() {
        final Dialog dialog = new Dialog(ScanActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_image_zoom);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        if (mOriginalBitmap == null) {
            Toast.makeText(ScanActivity.this, "Could not get image", Toast.LENGTH_SHORT).show();
        } else {
            TouchImageView imgZoom = (TouchImageView) dialog.findViewById(R.id.img_zoom);
            imgZoom.setImageBitmap(mOriginalBitmap);
        }

        dialog.show();

        dialog.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (!mOriginalBitmap.isRecycled()) {
                    mOriginalBitmap.recycle();
                    mOriginalBitmap = null;
                }
                if (!mScannedBitmap.isRecycled()) {
                    mScannedBitmap.recycle();
                    mScannedBitmap = null;
                }

                finish();
            }
        });
    }

    private class GetBitmapTask extends AsyncTask<Void, Void, Void> {

        private Bitmap mImageBitmap;
        private Uri mImageUri;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingDialog(true);

            try {
                System.gc();
                mImageUri = ImageUtils.getImageFileUri();
                if(mImageUri == null) {
                    noticeWhenGettingPictureError();
                    return;
                }

                mImageBitmap = ImageUtils.getBitmap(ScanActivity.this, mImageUri);
                if (mImageBitmap == null) {
                    noticeWhenGettingPictureError();
                }
            } catch (IOException e) {
                e.printStackTrace();

                noticeWhenGettingPictureError();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            cacheOriginalBitmap(mImageBitmap);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            showScannedBitmap();

            try {
                getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.Images.ImageColumns.DATA + "=?", new String[]{mImageUri.getPath()});
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (!mImageBitmap.isRecycled()) {
                mImageBitmap.recycle();
                mImageBitmap = null;
            }

            showLoadingDialog(false);
        }
    }

    private class ScannedBitmapTask extends AsyncTask<Void, Void, String> {

        private Map<Integer, PointF> mPoints;

        ScannedBitmapTask(Map<Integer, PointF> points) {
            mPoints = points;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingDialog(true);
        }

        @Override
        protected String doInBackground(Void... params) {
            return getScannedBitmap(mScannedBitmap, mPoints);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            showLoadingDialog(false);

            Intent data = new Intent();
            data.putExtra(AppConfigs.SCANNED_RESULT, result);
            setResult(RESULT_OK, data);

            if (!mOriginalBitmap.isRecycled()) {
                mOriginalBitmap.recycle();
                mOriginalBitmap = null;
            }
            if (!mScannedBitmap.isRecycled()) {
                mScannedBitmap.recycle();
                mScannedBitmap = null;
            }

            System.gc();

            finish();
        }
    }

    private class RotateBitmapTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Integer... params) {
            rotateBitmap(params[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            showScannedBitmap();

            showLoadingDialog(false);
        }
    }

    public native Bitmap getScannedBitmap(Bitmap bitmap, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

    public native Bitmap getGrauBitmap(Bitmap bitmap);

    public native Bitmap getSWBitmap(Bitmap bitmap);

    public native float[] getPoints(Bitmap bitmap);

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("Scanner");
    }
}
