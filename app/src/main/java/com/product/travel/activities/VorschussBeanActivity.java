package com.product.travel.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.ZielItemListener;
import com.product.travel.models.Travel;
import com.product.travel.models.User;
import com.product.travel.models.ZielItem;
import com.product.travel.utils.DateUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;
import com.product.travel.utils.ValidationUtils;
import com.product.travel.widget.EditTextOrtExt;
import com.product.travel.widget.LayoutZielItemExt;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class VorschussBeanActivity extends BaseActivity implements View.OnClickListener, ZielItemListener {

    TextView mTvTitle, mTvAddZiel, mTvDatumBegin, mTvUhrBegin, mTvDatumEnd, mTvUhrEnd;
    ImageButton mIBtnBack, mIBtnMenu, mIBtnAddZiel, mIBtnStarBegin;
    Button mBtnCancel, mBtnNext;
    EditText mEdtReason, mEdtBetrag;
    EditTextOrtExt mEdtOrtBeginExt;
    Spinner mSpnStarBegin;

    LinearLayout mLinBeamterStatus, mLinZielItems, mLinAdvanceMoney;

    private int mYearBegin, mMonthBegin, mDayBegin;
    private int mYearEnd, mMonthEnd, mDayEnd;
    private boolean mIsBeginDateSelected;

    private boolean mIsFromZusam;
    private boolean mIsBeginOrtStarClick;

    private double mBetragValue;
    Travel mTravel;

    ArrayList<ZielItem> mZielItemList;
    private DateFormat[] formats = new DateFormat[] {
            DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.GERMAN),
//                    DateFormat.getDateTimeInstance(),
//                    DateFormat.getTimeInstance(),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reise_und;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mEdtReason = (EditText) findViewById(R.id.edt_reason);

        mIBtnAddZiel = (ImageButton) findViewById(R.id.ibtn_add_ziel);
        mTvAddZiel = (TextView) findViewById(R.id.tv_add_ziel);

        /* Beginn */
        mEdtOrtBeginExt = (EditTextOrtExt) findViewById(R.id.edt_ort_begin);
        mIBtnStarBegin = (ImageButton) findViewById(R.id.ibtn_star_begin);
        mSpnStarBegin = (Spinner) findViewById(R.id.spn_star_begin);
        mTvDatumBegin = (TextView) findViewById(R.id.tv_datum_begin);
        mTvUhrBegin = (TextView) findViewById(R.id.tv_uhr_begin);

        /* End */
//        mEdtOrtEndExt = (EditTextOrtExt) findViewById(R.id.edt_ort_end);
//        mIBtnStarEnd = (ImageButton) findViewById(R.id.ibtn_star_end);
//        mSpnStarEnd = (Spinner) findViewById(R.id.spn_star_end);
        mTvDatumEnd = (TextView) findViewById(R.id.tv_datum_end);
        mTvUhrEnd = (TextView) findViewById(R.id.tv_uhr_end);

        mLinBeamterStatus = (LinearLayout) findViewById(R.id.lin_beamter_status);
        mLinZielItems = (LinearLayout) findViewById(R.id.lin_ziel_items);
        mLinAdvanceMoney = (LinearLayout) findViewById(R.id.lin_advance_money);
        mEdtBetrag = (EditText) findViewById(R.id.edt_betrag);

        mTvTitle.setText(getResources().getString(R.string.vorschuss_beantragen_item));
        mTvAddZiel.setText(getString(R.string.weiteres_vorschuss));
    }

    private void initEvents() {
        mLinBeamterStatus.setVisibility(View.GONE);
        mTvUhrBegin.setVisibility(View.GONE);
        mTvUhrEnd.setVisibility(View.GONE);
        mLinAdvanceMoney.setVisibility(View.VISIBLE);

        mIBtnAddZiel.setOnClickListener(this);

        /* Begin */
        mIBtnStarBegin.setOnClickListener(this);
        mTvDatumBegin.setOnClickListener(this);

        mSpnStarBegin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsBeginOrtStarClick) {
                    mEdtOrtBeginExt.setText(mSpnStarBegin.getSelectedItem().toString());
                } else {
                    mIsBeginOrtStarClick = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* End */
//        mIBtnStarEnd.setOnClickListener(this);
        mTvDatumEnd.setOnClickListener(this);
        /*mSpnStarEnd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsEndOrtStarClick) {
                    mEdtOrtEndExt.setText(mSpnStarEnd.getSelectedItem().toString());
                } else {
                    mIsEndOrtStarClick = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mEdtBetrag.setEnabled(true);

        mEdtBetrag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String oriText = s.toString();
                String comma = "";
                if (!TextUtils.isEmpty(oriText)) {
                    if (oriText.charAt(oriText.length() - 1) == ',') {
                        comma = ",";
                    } else if (oriText.length() >= 3
                            && oriText.charAt(oriText.length() - 2) == ','
                            && oriText.charAt(oriText.length() - 1) == '0') {
                        comma = ",0";
                    }
                }
                String text = getCommaSeparatedString(s);
                mEdtBetrag.removeTextChangedListener(this);
                s.clear();
                s.append(text);
                s.append(comma);
                mEdtBetrag.addTextChangedListener(this);
            }
        });
    }

    private void initData() {
        if (mZielItemList == null) {
            mZielItemList = new ArrayList<>();
        } else {
            mZielItemList.clear();
        }

        /* init Spinner value */
        String beginCity = StorageUtils.getBeginCityStarsValue(VorschussBeanActivity.this);
        List<String> beginCityList = new ArrayList<>(Arrays.asList(beginCity.split(";;")));
        beginCityList.add(0, "");
        ArrayAdapter<String> beginCityAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, beginCityList);
        beginCityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpnStarBegin.setAdapter(beginCityAdapter);
        mIsBeginOrtStarClick = false;

        /*String endCity = StorageUtils.getEndCityStarsValue(VorschussBeanActivity.this);
        List<String> endCityList = Arrays.asList(endCity.split(";;"));
        ArrayAdapter<String> endCityAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, endCityList);
        endCityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpnStarEnd.setAdapter(endCityAdapter);
        mIsEndOrtStarClick = false;*/

        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        String travelId = getIntent().getStringExtra(AppConfigs.TRAVEL_ID_KEY);
        if (mIsFromZusam) { // Edit from zusam
            mBtnNext.setText(getResources().getString(R.string.next_from_zusam));
            mTvTitle.setText(getResources().getString(R.string.vorschuss_beantragen_title));
            mTvAddZiel.setText(getString(R.string.weiteres_reise));

            mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();
        } else if (!TextUtils.isEmpty(travelId)) { // Edit from Entwurfe / Vorlagen
            DBApplication.getServiceFactory().getTravelService().resetTravel();

            mTravel = new Travel();
            TravelDbItem travelDbItem = TravelDbHelper.getTravelById(travelId);
            mTravel.setTravel(travelDbItem);

            if (travelDbItem != null && travelDbItem.isVorlagen()) { // Edit from Vorlagen
                mTravel.setTravelId("VS" + System.currentTimeMillis());
                mTravel.setFavorite(false);
                mTravel.setTravelFrom(1);
            }
        } else { // back from previous screen or create a new one
            mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();
            if (mTravel == null) { // Create a new one
                DBApplication.getServiceFactory().getTravelService().resetTravel();
                mTravel = new Travel();
                mTravel.setTravel(false);
                mTravel.setTravelId("VS" + System.currentTimeMillis());
                mTravel.setTravelFrom(1);
            }
        }

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(VorschussBeanActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(VorschussBeanActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        StorageUtils.storageTravelId(VorschussBeanActivity.this, mTravel.getTravelId());

        mTravel.setEdit(false); // default does not edit
        mEdtReason.setText(mTravel.getReason());

        User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (user == null) {
            user = UserUtils.convertStringToUser(StorageUtils.getUserInfo(VorschussBeanActivity.this));
        }
        if (TextUtils.isEmpty(mTravel.getBeginCity())
                && user != null
                && user.getHomeAddressList() != null
                && user.getHomeAddressList().size() > 0) {
            mEdtOrtBeginExt.setText(user.getHomeAddressList().get(0).getCity());
            mTravel.setBeginCity(user.getHomeAddressList().get(0).getCity());
        } else {
            mEdtOrtBeginExt.setText(mTravel.getBeginCity());
        }
        /*if (TextUtils.isEmpty(mTravel.getEndCity())
                && user != null
                && user.getHomeAddressList() != null
                && user.getHomeAddressList().size() > 0) {
            mEdtOrtEndExt.setText(user.getHomeAddressList().get(0).getCity());
        } else {
            mEdtOrtEndExt.setText(mTravel.getEndCity());
        }*/

        if (mTravel.getAdvanceMoney() > 0) {
            mEdtBetrag.setText(String.format(Locale.GERMAN, "%f", mTravel.getAdvanceMoney()));
        }

        /* Ziel(e) */
        if (!TextUtils.isEmpty(mTravel.getZiel())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    ZielItem item = new ZielItem();
                    item.setCity(jsonItem.getString("city"));
                    item.setStreet(jsonItem.getString("street"));

                    mZielItemList.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    /* show all ziel tems */
        reloadAllItems();

        final Calendar calendar = Calendar.getInstance();
        if (mTravel != null && mTravel.getBeginDate() != null) {
            calendar.setTime(mTravel.getBeginDate());
        }
        mYearBegin = calendar.get(Calendar.YEAR);
        mMonthBegin = calendar.get(Calendar.MONTH);
        mDayBegin = calendar.get(Calendar.DAY_OF_MONTH);
        if (mTravel != null && mTravel.getBeginDate() != null) {
            mTvDatumBegin.setText(String.format("%s", formats[0].format(new Date(mYearBegin - 1900, mMonthBegin, mDayBegin))));
        }

        if (mTravel != null && mTravel.getEndDate() != null) {
            calendar.setTime(mTravel.getEndDate());
        }
        mYearEnd = calendar.get(Calendar.YEAR);
        mMonthEnd = calendar.get(Calendar.MONTH);
        mDayEnd = calendar.get(Calendar.DAY_OF_MONTH);
        if (mTravel != null && mTravel.getEndDate() != null) {
            mTvDatumEnd.setText(String.format("%s", formats[0].format(new Date(mYearEnd - 1900, mMonthEnd, mDayEnd))));
        }
    }

    private String getCommaSeparatedString(Editable editable) {

        String newText = editable.toString();
        int iComTmp = newText.indexOf(",");
        String decimal = "";

        // integer
        String integer = newText.substring(0, iComTmp > 0 ? iComTmp : newText.length());
        integer = integer.replace(".", "");

        // decimal
        if (iComTmp > 0) {
            if (iComTmp != newText.length() - 1) {
                decimal = newText.substring(iComTmp, iComTmp + 3 > newText
                        .length() ? newText.length() : iComTmp + 3);
            } else {
                decimal = "";
            }
            decimal = decimal.replace(",", ".");
        }

        String format = newText;
        if (!TextUtils.isEmpty(format)) {
            decimal = decimal.replace("..", "");
            if (integer.equals(",") && decimal.equals("")) {
                integer = "0";
            }
            Locale locale = Locale.GERMAN;
            NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
            numberFormat.setMaximumFractionDigits(2);
            numberFormat.setMaximumIntegerDigits(9);

            mBetragValue = Double.parseDouble(integer + decimal);
            format = numberFormat.format(mBetragValue);
        }

        return format;
    }

    private void reloadAllItems() {
        mLinZielItems.removeAllViewsInLayout();

        /* Add first item if it's null */
        if (mZielItemList.size() == 0) {
            ZielItem item = new ZielItem();
            mZielItemList.add(item);
        }

        /* Add all items */
        LayoutZielItemExt linearItem = new LayoutZielItemExt(VorschussBeanActivity.this, mZielItemList.get(0).getCity(), mZielItemList.get(0).getStreet(), 0, this);
        mLinZielItems.addView(linearItem);

        for (int i = 1; i < mZielItemList.size(); i++) {
            LayoutZielItemExt item = new LayoutZielItemExt(VorschussBeanActivity.this, mZielItemList.get(i).getCity(), mZielItemList.get(i).getStreet(), i, this);
            mLinZielItems.addView(item);
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (mIsBeginDateSelected) {
                mYearBegin = year;
                mMonthBegin = monthOfYear;
                mDayBegin = dayOfMonth;
                mTvDatumBegin.setText(String.format("%s", formats[0].format(new Date(year - 1900, monthOfYear, dayOfMonth))));
            } else {
                mYearEnd = year;
                mMonthEnd = monthOfYear;
                mDayEnd = dayOfMonth;
                mTvDatumEnd.setText(String.format("%s", formats[0].format(new Date(year - 1900, monthOfYear, dayOfMonth))));
            }
        }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
            Calendar minEndDate = Calendar.getInstance();

            Calendar maxEndDate = Calendar.getInstance();
            maxEndDate.add(Calendar.MONTH, 6);

            DatePickerDialog endDatePickerDialog = new DatePickerDialog(this, dateSetListener, mYearBegin, mMonthBegin, mDayBegin);
            endDatePickerDialog.getDatePicker().setMinDate(minEndDate.getTimeInMillis());
            endDatePickerDialog.getDatePicker().setMaxDate(maxEndDate.getTimeInMillis());

            return endDatePickerDialog;
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            mEdtReason.setError(getResources().getString(R.string.empty_reason_vorschuss_msg));
            mEdtReason.requestFocus();
            return false;
        }

        /* Ziel */
        for (int i = 0; i < mZielItemList.size(); i++) {
            if (TextUtils.isEmpty(mZielItemList.get(i).getCity())) {
                Toast.makeText(VorschussBeanActivity.this, getResources().getString(R.string.empty_ort_ziel_msg), Toast.LENGTH_LONG).show();
                return false;
            }
            if (TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                Toast.makeText(VorschussBeanActivity.this, getResources().getString(R.string.empty_strabe_ziel_msg), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        /* Begin */
        if (TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            mEdtOrtBeginExt.setError(getResources().getString(R.string.empty_ort_begin_msg));
            mEdtOrtBeginExt.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim())) {
            Toast.makeText(VorschussBeanActivity.this, getResources().getString(R.string.empty_datum_begin_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* End */
        /*if (TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            mEdtOrtEndExt.setError(getResources().getString(R.string.empty_ort_end_msg));
            mEdtOrtEndExt.requestFocus();
            return false;
        }*/
        if (TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim())) {
            Toast.makeText(VorschussBeanActivity.this, getResources().getString(R.string.empty_datum_end_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* Validate Date */
        Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, 23, 59, 58);
        Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, 23, 59, 59);
        if (!ValidationUtils.isValidDate(VorschussBeanActivity.this, beginDate, endDate, false)) {
            return false;
        }

        /* Check has overlap date? */
        if (TravelDbHelper.hasOverlapDate(beginDate, endDate, mTravel.getTravelId())) {
            Toast.makeText(VorschussBeanActivity.this, getString(R.string.notice_vorschuss_date_between_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        if (TextUtils.isEmpty(mEdtBetrag.getText().toString().trim()) || mBetragValue <= 0) {
            Toast.makeText(VorschussBeanActivity.this, getResources().getString(R.string.empty_betrag_msg), Toast.LENGTH_LONG).show();
            mEdtBetrag.requestFocus();
            return false;
        }

        return true;
    }

    private void getLatestTravelInfo() {
        mTravel.setTravel(false);
        mTravel.setDraft(true);
        mTravel.setBeamter(false);
        mTravel.setVorlagen(false);
        mTravel.setModify(false);

        if (TextUtils.isEmpty(mTravel.getTravelId())) {
            mTravel.setTravelId("VS" + System.currentTimeMillis());
        }

        if (TextUtils.isEmpty(mTravel.getCreateTime())) {
            mTravel.setCreateTime(DateUtils.getCurrentDateTime()); // update it when getting pdf file from the server
        }
        mTravel.setReason(mEdtReason.getText().toString().trim());

        /* Ziel(e) */
        try {
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < mZielItemList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("city", mZielItemList.get(i).getCity());
                jsonObject.put("street", mZielItemList.get(i).getStreet());
                jsonArray.put(jsonObject);
            }

            JSONObject ziel = new JSONObject();
            ziel.put("ziel", jsonArray);
            mTravel.setZiel(ziel.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /* Begin */
        mTravel.setBeginCity(mEdtOrtBeginExt.getText().toString().trim());

        if (!TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim())) {
            Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, 0, 0);
            mTravel.setBeginDate(beginDate);
        }

        /* End */
//        mTravel.setEndCity(mEdtOrtEndExt.getText().toString().trim());

        if (!TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim())) {
            Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, 0, 0);
            mTravel.setEndDate(endDate);
        }

        /* 1st pdf date */
        if (mTravel.getFirstPdfDate() == null) {
            mTravel.setFirstPdfDate(DateUtils.convertDateToStringForCreatePDF(mTravel.getBeginDate()));
        }

        mTravel.setAdvanceMoney(mBetragValue);
    }

    private boolean hasModify() {
        /* Reason */
        if (TextUtils.isEmpty(mTravel.getReason()) && !TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getReason()) && TextUtils.isEmpty(mEdtReason.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getReason()) && !TextUtils.equals(mTravel.getReason(), mEdtReason.getText().toString().trim())) {
            return true;
        }

        /* Ziel(e) */
        if (TextUtils.isEmpty(mTravel.getZiel())) {
            for (int i = 0; i < mZielItemList.size(); i++) {
                if (!TextUtils.isEmpty(mZielItemList.get(i).getCity()) || !TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                    return true;
                }
            }
        } else {
            ArrayList<ZielItem> zielItemList = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getZiel());
                JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    ZielItem item = new ZielItem();
                    item.setCity(jsonItem.getString("city"));
                    item.setStreet(jsonItem.getString("street"));

                    zielItemList.add(item);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < mZielItemList.size() && i < zielItemList.size(); i++) {
                if (!TextUtils.equals(mZielItemList.get(i).getCity(), zielItemList.get(i).getCity())) {
                    return true;
                }
                if (!TextUtils.equals(mZielItemList.get(i).getStreet(), zielItemList.get(i).getStreet())) {
                    return true;
                }
            }
            if (mZielItemList.size() < zielItemList.size()) {
                for (int i = mZielItemList.size(); i < zielItemList.size(); i++) {
                    if (!TextUtils.isEmpty(zielItemList.get(i).getCity()) || !TextUtils.isEmpty(zielItemList.get(i).getStreet())) {
                        return true;
                    }
                }
            } else {
                for (int i = zielItemList.size(); i < mZielItemList.size(); i++) {
                    if (!TextUtils.isEmpty(mZielItemList.get(i).getCity()) || !TextUtils.isEmpty(mZielItemList.get(i).getStreet())) {
                        return true;
                    }
                }
            }
        }

        /* Begin city */
        if (TextUtils.isEmpty(mTravel.getBeginCity()) && !TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getBeginCity()) && TextUtils.isEmpty(mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getBeginCity()) && !TextUtils.equals(mTravel.getBeginCity(), mEdtOrtBeginExt.getText().toString().trim())) {
            return true;
        }

        /* End city */
        /*if (TextUtils.isEmpty(mTravel.getEndCity()) && !TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getEndCity()) && TextUtils.isEmpty(mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.isEmpty(mTravel.getEndCity()) && !TextUtils.equals(mTravel.getEndCity(), mEdtOrtEndExt.getText().toString().trim())) {
            return true;
        }*/

        /* Begin date */
        if ((mTravel.getBeginDate() == null && !TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim()))
                || (mTravel.getBeginDate() != null && TextUtils.isEmpty(mTvDatumBegin.getText().toString().trim()))) {
            return true;
        }
        Date beginDate = new Date(mYearBegin - 1900, mMonthBegin, mDayBegin, 0, 0);
        if (mTravel.getBeginDate() != null && mTravel.getBeginDate().compareTo(beginDate) != 0) {
            return true;
        }

        /* End date */
        if ((mTravel.getEndDate() == null && !TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim()))
                || (mTravel.getEndDate() != null && TextUtils.isEmpty(mTvDatumEnd.getText().toString().trim()))) {
            return true;
        }
        Date endDate = new Date(mYearEnd - 1900, mMonthEnd, mDayEnd, 0, 0);
        if (mTravel.getEndDate() != null && mTravel.getEndDate().compareTo(endDate) != 0) {
            return true;
        }

        /* Betrag */
        if (mTravel.getAdvanceMoney() != mBetragValue) {
            return true;
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();

        initData();
    }

    @Override
    public void onBackPressed() {

        Intent intent;
        if (mIsFromZusam) {
            intent = new Intent(VorschussBeanActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            if (hasModify()) {
//                mTravel.setModify(true);
                getLatestTravelInfo();
                DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

                intent = new Intent(VorschussBeanActivity.this, NoticeSaveTravelWithDraftActivity.class);
                intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            } else {
                intent = new Intent(VorschussBeanActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        }

        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_add_ziel:
                onAddZielButtonClick();
                break;

            /* Begin */
            case R.id.ibtn_star_begin:
                onStarBeginIconClick();
                break;
            case R.id.tv_datum_begin:
                onSetBeginDateClick();
                break;

            /* End */
            case R.id.ibtn_star_end:
//                onStarEndIconClick();
                break;
            case R.id.tv_datum_end:
                onSetEndDateClick();
                break;

            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                boolean hasModify = hasModify();
                if (hasModify) {
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onAddZielButtonClick() {
        if (mZielItemList.size() < 5) { // max = 5
            ZielItem item = new ZielItem();
            mZielItemList.add(item);
            LayoutZielItemExt linearItem = new LayoutZielItemExt(VorschussBeanActivity.this, "", "", mZielItemList.size() - 1, this);
//            mLinZielItems.addView(linearItem, mZielItemList.size() - 1);
            mLinZielItems.addView(linearItem);
        }
        if (mZielItemList.size() == 5) {
            mIBtnAddZiel.setVisibility(View.GONE);
            mTvAddZiel.setVisibility(View.GONE);
        }
    }

    private void onStarBeginIconClick() {
        mSpnStarBegin.performClick();
    }

    private void onSetBeginDateClick() {
        mIsBeginDateSelected = true;
        showDialog(0);
    }

    /*private void onStarEndIconClick() {
        mSpnStarEnd.performClick();
    }*/

    private void onSetEndDateClick() {
        mIsBeginDateSelected = false;
        showDialog(1);
    }

    private void onCancelButtonClick() {
        Intent intent;

        if (hasModify()) {
            getLatestTravelInfo();
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

            intent = new Intent(VorschussBeanActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(VorschussBeanActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }

        finish();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
//            if (hasModify()) {
//                mTravel.setModify(true);
//            }
            getLatestTravelInfo();
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);

            Intent intent = new Intent(VorschussBeanActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            if (mIsFromZusam) {
                finish();
            }
        }
    }

    @Override
    public void onRemoveListener(int index) {
        if (mZielItemList != null && mZielItemList.size() > index) {
            mZielItemList.remove(index);
        }
        if (mLinZielItems != null) {
            try {
                mLinZielItems.removeViewAt(index);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        mIBtnAddZiel.setVisibility(View.VISIBLE);
        mTvAddZiel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTextChangeListener(int index, String city, String street) {
        try {
            mZielItemList.get(index).setCity(city);
            mZielItemList.get(index).setStreet(street);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
