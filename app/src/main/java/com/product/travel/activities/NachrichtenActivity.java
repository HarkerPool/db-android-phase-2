package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.adapters.NachrichtenAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.NachrichtenDbHelper;
import com.product.travel.data.dao.NachrichtenDbItem;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.CallBack;
import com.product.travel.interfaces.NachrichtenItemListener;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NachrichtenActivity extends BaseActivity implements View.OnClickListener, NachrichtenItemListener {

    private TextView mTvTitle, mTvNewTotal, mTvDeleteAll;
    private ImageButton mIBtnBack, mIBtnMenu;
    private CheckBox mCbSelectAll;
    private ListView mListView;

    private ArrayList<NachrichtenItem> mItemList;
    private NachrichtenAdapter mAdapter;
    private int mUnreadEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_nachrichten;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mTvNewTotal = (TextView) findViewById(R.id.tv_new_total);
        mTvDeleteAll = (TextView) findViewById(R.id.tv_delete_all);
        mCbSelectAll = (CheckBox) findViewById(R.id.cb_nachrichten_select_all);
        mListView = (ListView) findViewById(R.id.lv_nachrichten);

        mTvTitle.setText(getResources().getString(R.string.nachrichten_item));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mTvDeleteAll.setOnClickListener(this);
        mCbSelectAll.setOnClickListener(this);
    }

    private void initData() {
        mUnreadEmail = 0;

        if (!checkNetworkConnection()) {
            getAllEmail(getAllEmailFromDb());
        } else {
            showLoadingDialog(true);

            updateToken();

            DBApplication.getServiceFactory().getTravelService().getEmail(new CallBack<ArrayList<NachrichtenItem>>() {
                @Override
                public void onSuccess(ArrayList<NachrichtenItem> result) {
                    showLoadingDialog(false);

                    getAllEmail(result);
                }

                @Override
                public void onError(Throwable t) {
                    showLoadingDialog(false);

                    getAllEmail(getAllEmailFromDb());
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.tv_delete_all:
                if (mItemList != null && mItemList.size() != 0) {
                    confirmDeleteEmail();
                }
                break;
            case R.id.cb_nachrichten_select_all:
                onSelectAllCheckboxClick();
                break;
            default:
                break;
        }
    }

    private void onDeleteAllButtonClick() {
        int nachrichtenId;
        final ArrayList<Integer> emailIdList = new ArrayList<>();
        for(int i = 0; i < mItemList.size();) {
            if (mItemList.get(i).isDelete()) {
                nachrichtenId = mItemList.get(i).getId();
                NachrichtenDbHelper.setIsDelete(true, nachrichtenId);
                emailIdList.add(nachrichtenId);
                mItemList.remove(i);
            } else {
                i++;
            }
        }
        mAdapter.notifyDataSetChanged();

        mUnreadEmail = 0;
        for (int i = 0; i < mItemList.size(); i++) {
            if (!mItemList.get(i).isRead()) {
                mUnreadEmail++;
            }
        }
        updateUnreadEmail();

        updateToken();
        DBApplication.getServiceFactory().getTravelService().deleteEmail(emailIdList, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
//                showLoadingDialog(false);
                for (int i = 0; i < emailIdList.size(); i++) {
                    NachrichtenDbHelper.deleteNachrichtenById(emailIdList.get(i));
                }
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private void onSelectAllCheckboxClick() {
        if (mCbSelectAll.isChecked()) {
            for (int i = 0; i < mItemList.size(); i++) {
                mItemList.get(i).setDelete(true);
            }
        } else {
            for (int i = 0; i < mItemList.size(); i++) {
                mItemList.get(i).setDelete(false);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void updateUnreadEmail() {
        mTvNewTotal.setText(String.format(Locale.GERMAN, "%s", "Neu (" + mUnreadEmail + ")"));
        StorageUtils.storageUnreadEmail(NachrichtenActivity.this, mUnreadEmail <= 0 ? 0 : mUnreadEmail);
    }

    @Override
    public void onNachrichtenItemClickListener(int position) {
        if (!mItemList.get(position).isRead()) {
            mUnreadEmail--;

            if (mUnreadEmail < 0) {
                mUnreadEmail = 0;
            }
        }
        updateUnreadEmail();

        NachrichtenDbHelper.setIsRead(true, mItemList.get(position).getId());
        mItemList.get(position).setRead(true);
        mAdapter.notifyDataSetChanged();

        updateToken();
        ThreadInBackground.updateEmail(mItemList.get(position).getId());

        Intent intent = new Intent(NachrichtenActivity.this, NachrichtenDetailActivity.class);
        intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID, mItemList.get(position).getTravelId());
        if (mItemList.get(position).getId() == -1) {
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY, mItemList.get(position).getBody1());
        } else {
            intent.putExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY, "");
        }
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onNachrichtenItemSelectListener(boolean isChecked, int position) {
        mItemList.get(position).setDelete(isChecked);
    }

    private ArrayList<NachrichtenItem> getAllEmailFromDb() {
        List<NachrichtenDbItem> itemList = NachrichtenDbHelper.getAllNachrichten(false);

        ArrayList<NachrichtenItem> result = new ArrayList<>();
        if (itemList != null) {
            for (int i = 0; i < itemList.size(); i++) {
                NachrichtenItem item = new NachrichtenItem();
                item.setId(itemList.get(i).getNachrichtenId());
                item.setTravelId(itemList.get(i).getTravelId());
                item.setSubject(itemList.get(i).getSubject());
                item.setBody1(itemList.get(i).getBody1());
                item.setBody2(itemList.get(i).getBody2());
                item.setCreateDate(itemList.get(i).getCreateDate());
                item.setMissingTitle1(itemList.get(i).getMissingTitle1());
                item.setMissingTitle2(itemList.get(i).getMissingTitle2());
                item.setMissingTitle3(itemList.get(i).getMissingTitle3());
                item.setMissingDetail1(itemList.get(i).getMissingDetail1());
                item.setMissingDetail2(itemList.get(i).getMissingDetail2());
                item.setMissingDetail3(itemList.get(i).getMissingDetail3());
                item.setRead(itemList.get(i).isRead());

                result.add(item);
            }
        }

        return result;
    }

    private void getAllEmail(ArrayList<NachrichtenItem> result) {
        if (mItemList == null) {
            mItemList = new ArrayList<>();
        }
        mItemList = result;

        ArrayList<NachrichtenItem> nachrichtenItemList = getAllReiseCanSend();
        for (int i = 0; i < nachrichtenItemList.size(); i++) {
            mItemList.add(0, nachrichtenItemList.get(i));
        }

        mAdapter = new NachrichtenAdapter(NachrichtenActivity.this, R.layout.layout_nachrichten_item, mItemList, NachrichtenActivity.this);
        mListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mUnreadEmail = 0;
        for (NachrichtenItem item : result) {
            if (!item.isRead()) {
                mUnreadEmail++;
            }
        }
        updateUnreadEmail();
    }

    private ArrayList<NachrichtenItem> getAllReiseCanSend() {
        String body1 = "<p>Guten Tag,</p>\n" +
                "<p>Sie hatten einen Antrag zur Reisekostenabrechnung mit einem Reiseende in der Zukunft als Entwurf gespeichert. Diesen können sie jetzt final bearbeiten und versenden. Um den Antrag zu öffnen, klicken Sie einfach auf das Stift-Symbol rechts.</p>\n" +
                "<p>Herzliche Gr&uuml;&szlig;e</p>\n" +
                "<p>Ihr DB Personalservice</p>";

        List<TravelDbItem> travelDbItemList = TravelDbHelper.getAllReiseCanSend();

        ArrayList<NachrichtenItem> nachrichtenItemList = new ArrayList<>();

        if (travelDbItemList != null) {
            for (int i = 0; i < travelDbItemList.size(); i++) {
                NachrichtenItem item = new NachrichtenItem();
                item.setId(-1);
                item.setSubject(getString(R.string.notice_reise_can_send_msg) + " " + travelDbItemList.get(i).getReason() + " jetzt abrechnen"); //Sie können Ihre Reise [Reisezweck] jetzt abrechnen"
                item.setBody1(body1);
                item.setBody2(null);
                item.setCreateDate(travelDbItemList.get(i).getCreateTime());
                item.setTravelId(travelDbItemList.get(i).getTravelId());
                item.setMissingTitle1(null);
                item.setMissingTitle2(null);
                item.setMissingTitle3(null);
                item.setMissingDetail1(null);
                item.setMissingDetail2(null);
                item.setMissingDetail3(null);
                item.setRead(false);

                nachrichtenItemList.add(item);
            }
        }

        return nachrichtenItemList;
    }

    private class GetAllEmailTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
//            getAllEmail();

            return null;
        }
    }

    private void confirmDeleteEmail() {
        final Dialog dialog = new Dialog(NachrichtenActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.delete));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_delete_email));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                onDeleteAllButtonClick();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
