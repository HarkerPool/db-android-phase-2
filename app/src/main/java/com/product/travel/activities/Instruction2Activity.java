package com.product.travel.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class Instruction2Activity extends BaseActivity {

    ImageView mIvInstruction;
    WebView mWvInstruction;
    Button mBtnNext;
    AlertDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_instruction;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIvInstruction = (ImageView) findViewById(R.id.iv_instruction);
        mWvInstruction = (WebView) findViewById(R.id.wv_instruction);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mWvInstruction.setVerticalScrollBarEnabled(false);
        mWvInstruction.setHorizontalScrollBarEnabled(true);
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void initData() {
        /* >> Storage version code >> */
        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StorageUtils.storageVersionCode(Instruction2Activity.this, currentVersionCode);
        /* << Storage version code << */

        String message = "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "color:red;mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:14.0pt;font-family:Arial;\n" +
                "color:red;mso-ansi-language:DE;mso-bidi-font-weight:bold'>Es gibt Neuigkeiten!<o:p></o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:4.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'>Mit dieser Version bieten wir\n" +
                "Ihnen zwei neue Services:<o:p></o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span style='font-size:4.0pt;font-family:Arial'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<ul style='margin-top:0cm' type=square>\n" +
                " <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><span\n" +
                "     lang=DE style='font-size:11.0pt;font-family:Arial;mso-ansi-language:DE;\n" +
                "     mso-bidi-font-weight:bold'>Reisen können jetzt auch <span\n" +
                "     style='color:red'>für die Zukunft als Entwurf angelegt</span> werden.\n" +
                "     Senden ist aber erst nach Abschluss der Reise möglich. Eine Nachricht\n" +
                "     erinnert Sie daran, Ihren Antrag zu senden<o:p></o:p></span></li>\n" +
                " <li class=MsoNormal style='mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><span\n" +
                "     lang=DE style='font-size:11.0pt;font-family:Arial;mso-ansi-language:DE;\n" +
                "     mso-bidi-font-weight:bold'>Sie erhalten beim Status „nicht abrechenbar“ oder\n" +
                "     „teilabgerechnet“ über Ihre Nachrichten <span style='color:red'>Informationen\n" +
                "     zum Grund dafür und was zu tun ist</span><o:p></o:p></span></li>\n" +
                "</ul>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:4.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'><o:p>&nbsp;</o:p></span></p>\n" +
                "\n" +
                "<p class=MsoNormal><span lang=DE style='font-size:11.0pt;font-family:Arial;\n" +
                "mso-ansi-language:DE;mso-bidi-font-weight:bold'>Ihr DB Personalservice</span>";

        mWvInstruction.loadDataWithBaseURL("file:///android_asset/", message, "text/html", "utf-8", null);

        initLoadingDialog();

        mLoadingDialog.show();

        ThreadInBackground.updateTravelStatus(); // Synchronize data.
        ThreadInBackground.uploadAllVorlagen(); // re-send vorlagen
        ThreadInBackground.syncDeleteTravels(Instruction2Activity.this); // Sync all travel deleted
        ThreadInBackground.getUserInfo(Instruction2Activity.this);
        ThreadInBackground.downloadImagesHandler(Instruction2Activity.this);

        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mLoadingDialog.isShowing()) {
                    mLoadingDialog.dismiss();
                }
            }
        };

        mLoadingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 1000 * 30);
    }

    private void initLoadingDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.dialog_loading, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        mLoadingDialog = builder.create();
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        ImageView ivLoading = (ImageView) view.findViewById(R.id.iv_loading);
        AnimationDrawable drawable = (AnimationDrawable) ivLoading.getBackground();
        drawable.start();

        TextView tvMessage = (TextView) view.findViewById(R.id.tv_message);
        tvMessage.setText("Bitte warten Sie kurz");
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent = new Intent(Instruction2Activity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra(AppConfigs.EXIT_APP, false);
//        intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, true);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }
}
