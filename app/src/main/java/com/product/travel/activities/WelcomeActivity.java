package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.interfaces.CallBack;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class WelcomeActivity extends BaseActivity implements View.OnClickListener {

    private EditText mEdtPassword;
    private Button mBtnNext;
    private TextView mTvForgotPassword;
    private LinearLayout mLinPassword;
    private boolean mIsExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mIsExit = getIntent().getBooleanExtra(AppConfigs.EXIT_APP, false);
        if (mIsExit) {
            finish();
        }

//        deleteDatabase("rasia_db_travel.db");

        initObjects();
        initEvents();

//        new GetAllCitiesTask().execute();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_welcome;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        needToUpdateNewBuild();
    }

    private void initObjects() {
        mEdtPassword = (EditText) findViewById(R.id.edt_password);
        mTvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mLinPassword = (LinearLayout) findViewById(R.id.lin_password);
    }

    private void initEvents() {
        mEdtPassword.requestFocus();
        showSoftKeyboard();

        mTvForgotPassword.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
    }

    private void initData() {
        Intent intent;

        if (!StorageUtils.getDataSecureStatus(WelcomeActivity.this)) {
            intent = new Intent(WelcomeActivity.this, DatensActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
            return;
        }

        if (TextUtils.isEmpty(StorageUtils.getPassword(WelcomeActivity.this))) {
            hideSoftKeyboard();

            mLinPassword.setVisibility(View.GONE);
            mBtnNext.setVisibility(View.GONE);

            String token = StorageUtils.getToken(WelcomeActivity.this);
            if (TextUtils.isEmpty(token)) {
                intent = new Intent(WelcomeActivity.this, ActivationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            } else {
                updateToken();

                ThreadInBackground.activation(WelcomeActivity.this); // Re-active and Synchronize data when opening app.

                hasNewVersion();
            }
        } else {
            mLinPassword.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_forgot_password:
                onForgotPasswordButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onForgotPasswordButtonClick() {
        showingConfirmDialog(getString(R.string.forgot_password), getString(R.string.confirm_reset_password), 1);
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (mEdtPassword.getText() == null || !TextUtils.equals(StorageUtils.getPassword(WelcomeActivity.this), mEdtPassword.getText().toString())) {
            Toast.makeText(WelcomeActivity.this, getString(R.string.notice_password_wrong), Toast.LENGTH_LONG).show();
            return;
        }

        hasNewVersion();
    }

    private void needToUpdateNewBuild() {
        mBtnNext.setVisibility(View.GONE);

        DBApplication.getServiceFactory().getUserService().needToUpdateNewBuild(new CallBack<String>() {
            @Override
            public void onSuccess(String result) {
                mBtnNext.setVisibility(View.VISIBLE);

                try {
                    int latestVersion = Integer.parseInt(result);
                    int currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

                    if (currentVersion < latestVersion) {
                        showingConfirmDialog(getString(R.string.notification), getString(R.string.update_new_build), 2);
                    } else {
                        new GetAllCitiesTask().execute();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                    new GetAllCitiesTask().execute();
                }
            }

            @Override
            public void onError(Throwable t) {
                mBtnNext.setVisibility(View.VISIBLE);

                new GetAllCitiesTask().execute();

                if (t != null) {
                    Log.e("WelcomeActivity", "needToUpdateNewBuild: " + t.getMessage());
                }
            }
        });
    }

    private void hasNewVersion() {
        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (StorageUtils.getVersionCode(WelcomeActivity.this) != currentVersionCode) {
            Intent intent = new Intent(WelcomeActivity.this, Instruction2Activity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            Intent intent = new Intent(WelcomeActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        }
    }


    /**
     *
     * @param title
     * @param message
     * @param type 1 - reset password, 2 - update new build from the store
     */
    private void showingConfirmDialog(String title, String message, final int type) {
        final Dialog dialog = new Dialog(WelcomeActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(title);
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(message);
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                switch (type) {
                    case 1:
                        resetPassword();
                        break;
                    case 2:
                        String url = AppConfigs.APP_STORE_URL + getApplicationContext().getPackageName();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        startActivity(intent);

                        break;
                }
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (type) {
                    case 1:
                        dialog.dismiss();
                        break;
                    case 2:
                        mIsExit = true;
                        finish();
                        break;
                }
            }
        });
    }

    private void resetPassword() {
        showLoadingDialog(true);

        String code = StorageUtils.getActivationCode(WelcomeActivity.this);
        DBApplication.getServiceFactory().getUserService().resetPassword(code, new CallBack<String>() {
            @Override
            public void onSuccess(String result) {
                showLoadingDialog(false);

                StorageUtils.storagePassword(WelcomeActivity.this, result);
                AlertUtils.showMessageAlert(WelcomeActivity.this, getString(R.string.forgot_password), getString(R.string.notice_reset_password));
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private class GetAllCitiesTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<String> cityList = DBApplication.getServiceFactory().getCityServiceImpl().getAllCityDistinct();
            ArrayList<String> zipList = DBApplication.getServiceFactory().getCityServiceImpl().getAllZipDistinct();
            Log.i("WelcomeActivity", "city list:" + cityList.toString());
            Log.i("WelcomeActivity", "zip list:" + zipList.toString());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            showLoadingDialog(false);

            initData();
        }
    }

    @Override
    protected void onDestroy()
    {
        if (mIsExit) {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        }
        super.onDestroy();
    }
}
