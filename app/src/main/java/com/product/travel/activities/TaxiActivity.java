package com.product.travel.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.adapters.ImageAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.ImageUtils;
import com.product.travel.utils.MarshMallowPermission;
import com.product.travel.widget.TouchImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class TaxiActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle, mTvAddSuggestion;
    private ImageButton mIBtnBack, mIBtnMenu;
    private Button mBtnCancel, mBtnNext;

    private LinearLayout mLinCamera, mLinAddCamera;
    private ImageButton mIBtnCamera, mIBtnRemove, mIBtnAdd;
    private GridView mGvImageList;

    private ArrayList<String> mImagesList, mCurrentImageList;
    private ImageAdapter mImageAdapter;

    private Travel mTravel;
    private String sTitle, sErrorMessage, sAddSuggestion;
    private boolean mIsFromZusam;
    MarshMallowPermission mMarshMallowPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_taxi;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvAddSuggestion = (TextView) findViewById(R.id.tv_add_suggestion);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mIBtnAdd = (ImageButton) findViewById(R.id.ibtn_add);
        mIBtnRemove = (ImageButton) findViewById(R.id.ibtn_remove);
        mIBtnCamera = (ImageButton) findViewById(R.id.ibtn_camera);

        mLinCamera = (LinearLayout) findViewById(R.id.lin_camera);
        mLinAddCamera = (LinearLayout) findViewById(R.id.lin_add_camera);

        mGvImageList = (GridView) findViewById(R.id.grid_view_image);

        getBelegTypes();
        mTvTitle.setText(sTitle);
        mTvAddSuggestion.setText(sAddSuggestion);
    }

    private void initEvents() {
        mLinCamera.setVisibility(View.GONE);
        mLinAddCamera.setVisibility(View.GONE);

        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mIBtnAdd.setOnClickListener(this);
        mIBtnRemove.setOnClickListener(this);
        mIBtnCamera.setOnClickListener(this);

        mGvImageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reviewImageZoom(position);
            }
        });
    }

    private void initData() {
        mMarshMallowPermission = new MarshMallowPermission(this);

        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getString(R.string.next_from_zusam));
        }

        mImagesList = new ArrayList<>();
        mCurrentImageList = new ArrayList<>();

        mImageAdapter = new ImageAdapter(TaxiActivity.this, mImagesList);
        mGvImageList.setAdapter(mImageAdapter);

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(TaxiActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(TaxiActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        switch ((AppConfigs.BELEG_ITEM) getIntent().getSerializableExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE))) {
            case TAXI:
                if (!TextUtils.isEmpty(mTravel.getTaxiImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getTaxiImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("taxi_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case FAHRTKOSTEN:
                if (!TextUtils.isEmpty(mTravel.getFahrtkostenImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getFahrtkostenImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("fahrtkosten_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case MIETWAGEN:
                if (!TextUtils.isEmpty(mTravel.getMietwagenImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getMietwagenImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("mietwagen_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case KRAFTSTOFF:
                if (!TextUtils.isEmpty(mTravel.getKraftstoffImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getKraftstoffImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("kraftstoff_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case PARKEN:
                if (!TextUtils.isEmpty(mTravel.getParkenImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getParkenImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("parken_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case TAGUNGSPAUSCHALE:
                if (!TextUtils.isEmpty(mTravel.getTagungsImages())) {
                    try {
                        JSONObject jsonObject = new JSONObject(mTravel.getTagungsImages());
                        JSONArray jsonArray = jsonObject.getJSONArray("tagungs_images");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            mCurrentImageList.add(jsonArray.getString(i));

                            if (jsonArray.getString(i).contains("uploads")) {
                                String[] parts = jsonArray.getString(i).split("/");
                                if (ImageUtils.hasImageExist(TaxiActivity.this, parts[parts.length - 1])) {
                                    mImagesList.add(parts[parts.length - 1]);
//                                    mCurrentImageList.add(parts[parts.length - 1]);
                                    continue;
                                }
                            }
                            mImagesList.add(jsonArray.getString(i));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }

        mImageAdapter.notifyDataSetChanged();

        if (mImagesList.size() > 0) {
            saveAllImagesToCache();
            mLinCamera.setVisibility(View.GONE);
            mLinAddCamera.setVisibility(View.VISIBLE);
        } else {
            mLinCamera.setVisibility(View.VISIBLE);
            mLinAddCamera.setVisibility(View.GONE);
            mIBtnRemove.setVisibility(View.GONE);
        }
    }

    private void getBelegTypes() {
        sTitle = getResources().getString(R.string.beleg_taxi);
        switch ((AppConfigs.BELEG_ITEM) getIntent().getSerializableExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE))) {
            case TAXI:
                sTitle = getResources().getString(R.string.beleg_taxi);
                sErrorMessage = getResources().getString(R.string.taxi_error_msg);
                sAddSuggestion = getResources().getString(R.string.taxi_suggestion_msg);
                break;
            case FAHRTKOSTEN:
                sTitle = getResources().getString(R.string.beleg_fahrtkosten);
                sErrorMessage = getResources().getString(R.string.fahrtkosten_error_msg);
                sAddSuggestion = getResources().getString(R.string.fahrtkosten_suggestion_msg);
                break;
            case MIETWAGEN:
                sTitle = getResources().getString(R.string.beleg_mietwagen);
                sErrorMessage = getResources().getString(R.string.mietwagen_error_msg);
                sAddSuggestion = getResources().getString(R.string.mietwagen_suggestion_msg);
                break;
            case KRAFTSTOFF:
                sTitle = getResources().getString(R.string.beleg_kraftstoff);
                sErrorMessage = getResources().getString(R.string.krafstoff_error_msg);
                sAddSuggestion = getResources().getString(R.string.krafstoff_suggestion_msg);
                break;
            case PARKEN:
                sTitle = getResources().getString(R.string.beleg_parken);
                sErrorMessage = getResources().getString(R.string.parken_error_msg);
                sAddSuggestion = getResources().getString(R.string.parken_suggestion_msg);
                break;
            case TAGUNGSPAUSCHALE:
                sTitle = getResources().getString(R.string.tagungspauschale_title);
                sErrorMessage = getResources().getString(R.string.tagungs_error_msg);
                sAddSuggestion = getResources().getString(R.string.tagungs_suggestion_msg);
                break;
        }
    }

    private void reviewImageZoom(final int position) {
        final Dialog reviewDialog = new Dialog(TaxiActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        reviewDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        reviewDialog.setContentView(R.layout.layout_image_zoom);
        reviewDialog.setCancelable(false);
        reviewDialog.setCanceledOnTouchOutside(false);

        TouchImageView imgZoom = (TouchImageView) reviewDialog.findViewById(R.id.img_zoom);

        final Bitmap bitmap = BitmapFactory.decodeFile(ImageUtils.getImagePath(TaxiActivity.this) + mImagesList.get(position));
        if (bitmap != null) {
            imgZoom.setImageBitmap(bitmap);
        } else {
            Picasso.with(TaxiActivity.this)
                    .load(AppConfigs.SERVER_URL + mImagesList.get(position))
                    .placeholder(R.drawable.loading_icon)
                    .error(R.drawable.error)
                    .into(imgZoom);
        }
        reviewDialog.show();

        reviewDialog.findViewById(R.id.btnCancel).setBackgroundResource(R.drawable.dustbin);
        reviewDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(TaxiActivity.this)
                        .setTitle(getResources().getString(R.string.alert_delete))
                        .setMessage(getResources().getString(R.string.alert_photo))
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

//                                ThreadInBackground.deleteImage(mImagesList.get(position));

                                DBApplication.getBitmapCache().removeBitmap(mImagesList.get(position));
                                mImagesList.remove(position);
                                mImageAdapter.notifyDataSetChanged();

                                if (mImagesList.size() == 0) {
                                    mLinCamera.setVisibility(View.VISIBLE);
                                    mLinAddCamera.setVisibility(View.GONE);
                                    mIBtnRemove.setVisibility(View.GONE);
                                }

                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                                reviewDialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        });

        reviewDialog.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }

                reviewDialog.dismiss();
            }
        });
    }

    private void saveAllImagesToCache() {
        for (int index = 0; index < mImagesList.size(); index++) {
            ImageUtils.scaleAndStorageCacheImage(TaxiActivity.this, mImagesList.get(index));
        }
    }

    private void removeAllImagesFromCache() {
        for (int index = 0; index < mImagesList.size(); index++) {
            DBApplication.getBitmapCache().removeBitmap(mImagesList.get(index));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == AppConfigs.REQUEST_CAMERA_CAPTURE) {
                    Intent intent = new Intent(TaxiActivity.this, ScanActivity.class);
                    startActivityForResult(intent, AppConfigs.IMAGE_RESULT);
                } else if (requestCode == AppConfigs.IMAGE_RESULT) {
                    mIBtnRemove.performClick();

                    String imagePath = data.getStringExtra(AppConfigs.SCANNED_RESULT);
                    if (!TextUtils.isEmpty(imagePath)) {
                        mImagesList.add(imagePath);
                        mImageAdapter.notifyDataSetChanged();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        removeAllImagesFromCache(); // TODO - must release memory - note that, maybe it does not back
        if (mIsFromZusam) {
            Intent intent = new Intent(TaxiActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            if (hasModify()) {
                AlertUtils.showMessageAlert(TaxiActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
            } else {
                super.onBackPressed();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                removeAllImagesFromCache(); // TODO - must release memory - note that, maybe it does not go to other page
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.ibtn_add:
                mLinAddCamera.setVisibility(View.GONE);
                mLinCamera.setVisibility(View.VISIBLE);
                mIBtnRemove.setVisibility(View.VISIBLE);
                break;
            case R.id.ibtn_remove:
                mLinAddCamera.setVisibility(View.VISIBLE);
                mLinCamera.setVisibility(View.GONE);
                break;
            case R.id.ibtn_camera:
                if (!mMarshMallowPermission.checkPermissionForCamera()) {
                    mMarshMallowPermission.requestPermissionForCamera();
                } else {
                    if (!mMarshMallowPermission.checkPermissionForExternalStorage()) {
                        mMarshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getImageFileUri());
                        startActivityForResult(cameraIntent, AppConfigs.REQUEST_CAMERA_CAPTURE);
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean isValidate() {

        if (mLinCamera.getVisibility() == View.GONE) {
            return true;
        }

        if (hasModify() && mImagesList.size() == 0) {
            return true;
        }

        Toast.makeText(TaxiActivity.this, sErrorMessage, Toast.LENGTH_LONG).show();
        return false;
    }

    private boolean hasModify() {
        if (mCurrentImageList.size() != mImagesList.size()) {
            return true;
        }

        for (int i = 0; i < mImagesList.size(); i++) {
            if (!mCurrentImageList.get(i).contains(mImagesList.get(i))) {
                return true;
            }
        }

        return false;
    }

    private void getLatestTravelInfo() {
        if (mImagesList.size() > 0) {
            mTravel.setRadioBeleg(true); // has image --> must be Ja (not Nein)
        }
//    } else {
        // TODO - if privater and neben are null --> mTravel.setRadioBeleg(false);
//        }

        JSONArray jsonArray = new JSONArray();

        int i, j;
        for (i = 0; i < mImagesList.size(); i++) {
            for (j = 0; j < mCurrentImageList.size(); j++) {
                if (mCurrentImageList.get(j).contains(mImagesList.get(i))) {
                    jsonArray.put(mCurrentImageList.get(j));
                    break;
                }
            }
            if (j == mCurrentImageList.size()) {
                jsonArray.put(mImagesList.get(i));
            }
        }

        switch ((AppConfigs.BELEG_ITEM) getIntent().getSerializableExtra(String.valueOf(AppConfigs.BELEG_ITEM.TITLE))) {
            case TAXI:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("taxi_images", jsonArray);

                    mTravel.setTaxiImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case FAHRTKOSTEN:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("fahrtkosten_images", jsonArray);

                    mTravel.setFahrtkostenImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case MIETWAGEN:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("mietwagen_images", jsonArray);

                    mTravel.setMietwagenImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case KRAFTSTOFF:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("kraftstoff_images", jsonArray);

                    mTravel.setKraftstoffImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case PARKEN:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("parken_images", jsonArray);

                    mTravel.setParkenImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case TAGUNGSPAUSCHALE:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("tagungs_images", jsonArray);

                    mTravel.setTagungsImages(jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(TaxiActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(TaxiActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }
    
    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            removeAllImagesFromCache();

            if (hasModify()) {
                mTravel.setModify(true);
                if (mImagesList.size() > 0) {
                    mTravel.setRadioBeleg(true);
                } else {
                    // TODO - if privater and neben are null --> mTravel.setRadioBeleg(false);
                }
                getLatestTravelInfo();
                DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                TravelDbHelper.save(mTravel);
            }

            if (mIsFromZusam) {
                Intent intent = new Intent(TaxiActivity.this, ZusamActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            } else {
                finish();
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        }
    }
}
