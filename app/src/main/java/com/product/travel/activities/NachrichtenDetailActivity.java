package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.data.dao.NachrichtenDbHelper;
import com.product.travel.data.dao.NachrichtenDbItem;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.utils.AlertUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NachrichtenDetailActivity extends BaseActivity implements View.OnClickListener {

    private ImageButton mIBtnBack, mIBtnMenu, mIBtnEdit, mIBtnMissingData1, mIBtnMissingData2, mIBtnMissingData3;
    private TextView mTvTitle, mTvMissingData1, mTvMissingData2, mTvMissingData3;
    private WebView mWvBody1, mWvBody2;
    private boolean mIsTravel;
    private TravelDbItem mTravelDbItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showLoadingDialog(true);
        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_nachrichten_detail;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mIBtnEdit = (ImageButton) findViewById(R.id.ibtn_edit);
        mIBtnMissingData1 = (ImageButton) findViewById(R.id.ibtn_missing_data_1);
        mIBtnMissingData2 = (ImageButton) findViewById(R.id.ibtn_missing_data_2);
        mIBtnMissingData3 = (ImageButton) findViewById(R.id.ibtn_missing_data_3);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvMissingData1 = (TextView) findViewById(R.id.tv_missing_data_1);
        mTvMissingData2 = (TextView) findViewById(R.id.tv_missing_data_2);
        mTvMissingData3 = (TextView) findViewById(R.id.tv_missing_data_3);
        mWvBody1 = (WebView) findViewById(R.id.wv_body_1);
        mWvBody2 = (WebView) findViewById(R.id.wv_body_2);

        mTvTitle.setText(null);

        mWvBody1.setVerticalScrollBarEnabled(false);
        mWvBody1.setHorizontalScrollBarEnabled(false);

        mWvBody2.setVerticalScrollBarEnabled(false);
        mWvBody2.setHorizontalScrollBarEnabled(false);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnEdit.setOnClickListener(this);
        mIBtnMissingData1.setOnClickListener(this);
        mIBtnMissingData2.setOnClickListener(this);
        mIBtnMissingData3.setOnClickListener(this);
        mIBtnMenu.setVisibility(View.GONE);
        mIBtnEdit.setVisibility(View.GONE);
        mTvMissingData1.setVisibility(View.GONE);
        mTvMissingData2.setVisibility(View.GONE);
        mTvMissingData3.setVisibility(View.GONE);
        mIBtnMissingData1.setVisibility(View.GONE);
        mIBtnMissingData2.setVisibility(View.GONE);
        mIBtnMissingData3.setVisibility(View.GONE);
    }

    private void initData() {
        showLoadingDialog(false);

        String body1 = getIntent().getStringExtra(AppConfigs.NACHRICHTEN_DETAIL_BODY);
        if (TextUtils.isEmpty(body1)) {
            getNachrichtenInfoFromDb();
        } else {
            mWvBody1.loadDataWithBaseURL("file:///android_asset/", body1, "text/html", "utf-8", null);
        }

        String travelId = getIntent().getStringExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID);
        TravelDbItem travel = TravelDbHelper.getTravelById(travelId);

        if (travel != null) {
            if (travel.isDraft() && travel.isTravel()) {
                mIBtnEdit.setVisibility(View.VISIBLE);
            }

            if (travel.isTravel()) {
                mIsTravel = true;
            } else {
                mIsTravel = false;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_edit:
                onEditButtonClick();
                break;
            case R.id.ibtn_missing_data_1:
                if (mTravelDbItem != null) {
                    AlertUtils.showMessageAlert(NachrichtenDetailActivity.this, "", mTravelDbItem.getMissingDetail1().replace("<p>", "").replace("</p>", "\n").replace("<br>", "\n"));
                }
                break;
            case R.id.ibtn_missing_data_2:
                if (mTravelDbItem != null) {
                    AlertUtils.showMessageAlert(NachrichtenDetailActivity.this, "", mTravelDbItem.getMissingDetail2().replace("<p>", "").replace("</p>", "\n").replace("<br>", "\n"));
                }
                break;
            case R.id.ibtn_missing_data_3:
                if (mTravelDbItem != null) {
                    AlertUtils.showMessageAlert(NachrichtenDetailActivity.this, "", mTravelDbItem.getMissingDetail3().replace("<p>", "").replace("</p>", "\n").replace("<br>", "\n"));
                }
                break;
            default:
                break;
        }
    }

    private void onEditButtonClick() {
        String travelId = getIntent().getStringExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID);

        if (mIsTravel) {
            Intent intent = new Intent(NachrichtenDetailActivity.this, ReiseUndActivity.class);
            intent.putExtra(AppConfigs.TRAVEL_ID_KEY, travelId);
            intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            Intent intent = new Intent(NachrichtenDetailActivity.this, VorschussBeanActivity.class);
            intent.putExtra(AppConfigs.TRAVEL_ID_KEY, travelId);
            intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    private void getNachrichtenInfoFromDb() {
        String travelId = getIntent().getStringExtra(AppConfigs.NACHRICHTEN_DETAIL_TRAVEL_ID);

        mTravelDbItem = TravelDbHelper.getTravelById(travelId);
        if (mTravelDbItem == null) {
            NachrichtenDbItem nachrichtenDbItem = NachrichtenDbHelper.getNachrichtenByTravelId(travelId);
            if (nachrichtenDbItem != null) {
                mTravelDbItem = new TravelDbItem();

                mTravelDbItem.setBody1(nachrichtenDbItem.getBody1());
                mTravelDbItem.setBody2(nachrichtenDbItem.getBody2());
                mTravelDbItem.setMissingTitle1(nachrichtenDbItem.getMissingTitle1());
                mTravelDbItem.setMissingTitle2(nachrichtenDbItem.getMissingTitle2());
                mTravelDbItem.setMissingTitle3(nachrichtenDbItem.getMissingTitle3());
                mTravelDbItem.setMissingDetail1(nachrichtenDbItem.getMissingDetail1());
                mTravelDbItem.setMissingDetail2(nachrichtenDbItem.getMissingDetail2());
                mTravelDbItem.setMissingDetail3(nachrichtenDbItem.getMissingDetail3());
            }
        }

        if (mTravelDbItem != null) {
            // Body 1
            mWvBody1.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mTravelDbItem.getBody1())) {
                mWvBody1.setVisibility(View.VISIBLE);
                mWvBody1.loadDataWithBaseURL("file:///android_asset/", mTravelDbItem.getBody1(), "text/html", "utf-8", null);
            }

            // Missing data 1
            mTvMissingData1.setVisibility(View.GONE);
            mIBtnMissingData1.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mTravelDbItem.getMissingTitle1())) {
                mTvMissingData1.setVisibility(View.VISIBLE);
                mIBtnMissingData1.setVisibility(View.VISIBLE);
                mTvMissingData1.setText(mTravelDbItem.getMissingTitle1().replace("<p>", "").replace("</p>", "\n").replace("<br>", " "));
            }

            // Missing data 2
            mTvMissingData2.setVisibility(View.GONE);
            mIBtnMissingData2.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mTravelDbItem.getMissingTitle2())) {
                mTvMissingData2.setVisibility(View.VISIBLE);
                mIBtnMissingData2.setVisibility(View.VISIBLE);
                mTvMissingData2.setText(mTravelDbItem.getMissingTitle2().replace("<p>", "").replace("</p>", "\n").replace("<br>", " "));
            }

            // Missing data 3
            mTvMissingData3.setVisibility(View.GONE);
            mIBtnMissingData3.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mTravelDbItem.getMissingTitle3())) {
                mTvMissingData3.setVisibility(View.VISIBLE);
                mIBtnMissingData3.setVisibility(View.VISIBLE);
                mTvMissingData3.setText(mTravelDbItem.getMissingTitle3().replace("<p>", "").replace("</p>", "\n").replace("<br>", " "));
            }

            // Body 2
            mWvBody2.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mTravelDbItem.getBody2())) {
                mWvBody2.setVisibility(View.VISIBLE);
                mWvBody2.loadDataWithBaseURL("file:///android_asset/", mTravelDbItem.getBody2(), "text/html", "utf-8", null);
            }
        }
    }
}
