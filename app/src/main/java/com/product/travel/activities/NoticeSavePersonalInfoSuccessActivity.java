package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NoticeSavePersonalInfoSuccessActivity extends BaseActivity {
    TextView mTvContent, mTvContent2;
    ImageView mIvStatus;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_confirm;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvContent = (TextView) findViewById(R.id.tv_content);
        mTvContent2 = (TextView) findViewById(R.id.tv_content_2);
        mIvStatus = (ImageView) findViewById(R.id.iv_status);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mTvContent.setText(getResources().getString(R.string.notice_save_personal_success_msg_1));
        mIvStatus.setVisibility(View.VISIBLE);
        mTvContent2.setVisibility(View.VISIBLE);
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent;
        if (getIntent().getBooleanExtra(AppConfigs.IS_FIRST_ACTIVATE, false)) {
            intent = new Intent(NoticeSavePersonalInfoSuccessActivity.this, SetPasswordActivity.class);
        } else {
            if (getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false)) {
                intent = new Intent(NoticeSavePersonalInfoSuccessActivity.this, ZusamActivity.class);
            } else {
                intent = new Intent(NoticeSavePersonalInfoSuccessActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
        }
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // nothing
    }
}
