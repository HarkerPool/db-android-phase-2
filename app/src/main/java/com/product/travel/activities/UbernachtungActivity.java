package com.product.travel.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import com.product.travel.R;
import com.product.travel.adapters.ImageAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.DateUtils;
import com.product.travel.utils.ImageUtils;
import com.product.travel.utils.MarshMallowPermission;
import com.product.travel.widget.TouchImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class UbernachtungActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    Button mBtnCancel, mBtnNext;

    Spinner mInkSpinner, mOhneSpinner, mOhneRechnungSpinner, mUnentgelSpinner;
    ImageButton mIBtnCamera, mIBtnRemove, mIBtnAdd;
    LinearLayout mLinCamera, mLinAddCamera;
    GridView mGvImageList;

    private int mAllDateChoise, mAllDateInk, mAllDateOhne, mAllDateOhneRechnung, mAllDateUnentgelt;

    ArrayList<String> mImagesList, mCurrentImageList, mInkList, mOhneList, mOhneRechnungList, mUnentgeltList;
    ImageAdapter mImageAdapter;
    ArrayAdapter<String> mInkAdapter, mOhneAdapter, mOhneRechnungAdapter, mUnentgeltAdapter;

    Travel mTravel;
    private boolean mIsFromZusam;
    MarshMallowPermission mMarshMallowPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_ubernachtung;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mInkSpinner = (Spinner) findViewById(R.id.spn_ink_dropbox);
        mOhneSpinner = (Spinner) findViewById(R.id.spn_ohne_dropbox);
        mOhneRechnungSpinner = (Spinner) findViewById(R.id.spn_ohne_rechnung_dropbox);
        mUnentgelSpinner = (Spinner) findViewById(R.id.spn_unentgel_dropbox);
        mIBtnAdd = (ImageButton) findViewById(R.id.ibtn_add);
        mIBtnRemove = (ImageButton) findViewById(R.id.ibtn_remove);
        mIBtnCamera = (ImageButton) findViewById(R.id.ibtn_camera);

        mLinCamera = (LinearLayout) findViewById(R.id.lin_camera);
        mLinAddCamera = (LinearLayout) findViewById(R.id.lin_add_camera);

        mGvImageList = (GridView) findViewById(R.id.grid_view_image);

        mTvTitle.setText(getResources().getString(R.string.ubernachtung_title));
    }

    private void initEvents() {
        mLinCamera.setVisibility(View.GONE);
        mLinAddCamera.setVisibility(View.GONE);

        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mIBtnAdd.setOnClickListener(this);
        mIBtnRemove.setOnClickListener(this);
        mIBtnCamera.setOnClickListener(this);

        mInkSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAllDateInk = position;
                if (mAllDateInk > 0 || mAllDateOhne > 0) {
                    if (mLinCamera.getVisibility() == View.GONE && mLinAddCamera.getVisibility() == View.GONE) {
                        if (mImagesList.size() > 0) {
                            mLinCamera.setVisibility(View.GONE);
                            mLinAddCamera.setVisibility(View.VISIBLE);
                        } else {
                            mLinCamera.setVisibility(View.VISIBLE);
                            mLinAddCamera.setVisibility(View.GONE);
                            mIBtnRemove.setVisibility(View.GONE);
                        }
                    }
                } else {
                    mLinCamera.setVisibility(View.GONE);
                    mLinAddCamera.setVisibility(View.GONE);
                }
                updateSpinnersDataList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mOhneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAllDateOhne = position;
                if (mAllDateInk > 0 || mAllDateOhne > 0) {
                    if (mLinCamera.getVisibility() == View.GONE && mLinAddCamera.getVisibility() == View.GONE) {
                        if (mImagesList.size() > 0) {
                            mLinCamera.setVisibility(View.GONE);
                            mLinAddCamera.setVisibility(View.VISIBLE);
                        } else {
                            mLinCamera.setVisibility(View.VISIBLE);
                            mLinAddCamera.setVisibility(View.GONE);
                            mIBtnRemove.setVisibility(View.GONE);
                        }
                    }
                } else {
                    mLinCamera.setVisibility(View.GONE);
                    mLinAddCamera.setVisibility(View.GONE);
                }
                updateSpinnersDataList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mOhneRechnungSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAllDateOhneRechnung = position;
                updateSpinnersDataList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mUnentgelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAllDateUnentgelt = position;
                updateSpinnersDataList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mGvImageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reviewImageZoom(position);
            }
        });
    }

    private void initData() {
        mMarshMallowPermission = new MarshMallowPermission(this);

        mIsFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        if (mIsFromZusam) {
            mBtnNext.setText(getString(R.string.next_from_zusam));
        }

        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(UbernachtungActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(UbernachtungActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        /* images */
        mImagesList = new ArrayList<>();
        mCurrentImageList = new ArrayList<>();
        if (!TextUtils.isEmpty(mTravel.getUberImages())) {
            try {
                JSONObject jsonObject = new JSONObject(mTravel.getUberImages());
                JSONArray jsonArray = jsonObject.getJSONArray("uber_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    mCurrentImageList.add(jsonArray.getString(i));

                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (ImageUtils.hasImageExist(UbernachtungActivity.this, parts[parts.length - 1])) {
                            mImagesList.add(parts[parts.length - 1]);
                            continue;
                        }
                    }
                    mImagesList.add(jsonArray.getString(i));
                }

                if (mImagesList.size() > 0) {
                    saveAllImagesToCache();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mImageAdapter = new ImageAdapter(UbernachtungActivity.this, mImagesList);
        mGvImageList.setAdapter(mImageAdapter);

        /* Days */
        Date beginDate = mTravel.getBeginDate();
        Date endDate = mTravel.getEndDate();
        mAllDateChoise = DateUtils.daysBetweenDate(beginDate, endDate);

        mAllDateInk = mTravel.getInklusiveFruhstuck();
        mAllDateOhne = mTravel.getOhneFruhstuck();
        mAllDateOhneRechnung = mTravel.getOhneRechnung();
        mAllDateUnentgelt = mTravel.getUnentgeltlich();

        /* Show camera */
        if (mAllDateInk > 0 || mAllDateOhne > 0) {
            if (mImagesList.size() > 0) {
                mLinCamera.setVisibility(View.GONE);
                mLinAddCamera.setVisibility(View.VISIBLE);
            } else {
                mLinCamera.setVisibility(View.VISIBLE);
                mLinAddCamera.setVisibility(View.GONE);
                mIBtnRemove.setVisibility(View.GONE);
            }
        } else {
            mLinCamera.setVisibility(View.GONE);
            mLinAddCamera.setVisibility(View.GONE);
        }

        mInkList = new ArrayList<String>();
        mOhneList = new ArrayList<String>();
        mOhneRechnungList = new ArrayList<String>();
        mUnentgeltList = new ArrayList<String>();
        initSpinnerAdapter();
        updateSpinnersDataList();

        /* Set default selection */
        mInkSpinner.setSelection(mAllDateInk);
        mOhneSpinner.setSelection(mAllDateOhne);
        mOhneRechnungSpinner.setSelection(mAllDateOhneRechnung);
        mUnentgelSpinner.setSelection(mAllDateUnentgelt);
    }

    private void initSpinnerAdapter() {
        mInkAdapter = new ArrayAdapter<String>(UbernachtungActivity.this, R.layout.spinner_item, mInkList);
        mInkAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mInkSpinner.setAdapter(mInkAdapter);

        mOhneAdapter = new ArrayAdapter<String>(UbernachtungActivity.this, R.layout.spinner_item, mOhneList);
        mOhneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOhneSpinner.setAdapter(mOhneAdapter);

        mOhneRechnungAdapter = new ArrayAdapter<String>(UbernachtungActivity.this, R.layout.spinner_item, mOhneRechnungList);
        mOhneRechnungAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOhneRechnungSpinner.setAdapter(mOhneRechnungAdapter);

        mUnentgeltAdapter = new ArrayAdapter<String>(UbernachtungActivity.this, R.layout.spinner_item, mUnentgeltList);
        mUnentgeltAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mUnentgelSpinner.setAdapter(mUnentgeltAdapter);
    }

    private void updateSpinnersDataList() {
        mInkList.clear();
        mOhneList.clear();
        mOhneRechnungList.clear();
        mUnentgeltList.clear();

		/* Ink list */
        int max = mAllDateChoise - (mAllDateOhne + mAllDateOhneRechnung + mAllDateUnentgelt);
        for (int i = 0; i <= max; i++) {
            mInkList.add(i + "");
        }

		/* Ohne list */
        max = mAllDateChoise - (mAllDateInk + mAllDateOhneRechnung + mAllDateUnentgelt);
        for (int i = 0; i <= max; i++) {
            mOhneList.add(i + "");
        }

		/* Ohne Rechnung list */
        max = mAllDateChoise - (mAllDateInk + mAllDateOhne + mAllDateUnentgelt);
        for (int i = 0; i <= max; i++) {
            mOhneRechnungList.add(i + "");
        }

		/* Unentgelt list */
        max = mAllDateChoise - (mAllDateInk + mAllDateOhne + mAllDateOhneRechnung);
        for (int i = 0; i <= max; i++) {
            mUnentgeltList.add(i + "");
        }

        mInkAdapter.notifyDataSetChanged();
        mOhneAdapter.notifyDataSetChanged();
        mOhneRechnungAdapter.notifyDataSetChanged();
        mUnentgeltAdapter.notifyDataSetChanged();
    }

    private void reviewImageZoom(final int position) {
        final Dialog reviewDialog = new Dialog(UbernachtungActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        reviewDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        reviewDialog.setContentView(R.layout.layout_image_zoom);
        reviewDialog.setCancelable(false);
        reviewDialog.setCanceledOnTouchOutside(false);

        TouchImageView imgZoom = (TouchImageView) reviewDialog.findViewById(R.id.img_zoom);

        final Bitmap bitmap = BitmapFactory.decodeFile(ImageUtils.getImagePath(UbernachtungActivity.this) + mImagesList.get(position));
        if (bitmap != null) {
            imgZoom.setImageBitmap(bitmap);
        } else {
            Picasso.with(UbernachtungActivity.this)
                    .load(AppConfigs.SERVER_URL + mImagesList.get(position))
                    .placeholder(R.drawable.loading_icon)
                    .error(R.drawable.error)
                    .into(imgZoom);
        }
        reviewDialog.show();

        reviewDialog.findViewById(R.id.btnCancel).setBackgroundResource(R.drawable.dustbin);
        reviewDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(UbernachtungActivity.this)
                        .setTitle(getResources().getString(R.string.alert_delete))
                        .setMessage(getResources().getString(R.string.alert_photo))
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

//                                ThreadInBackground.deleteImage(mImagesList.get(position));

                                DBApplication.getBitmapCache().removeBitmap(mImagesList.get(position));
                                mImagesList.remove(position);
                                mImageAdapter.notifyDataSetChanged();

                                if (mImagesList.size() == 0) {
                                    mLinCamera.setVisibility(View.GONE);
                                    mLinAddCamera.setVisibility(View.GONE);
                                    mIBtnRemove.setVisibility(View.GONE);

                                    if (mAllDateInk > 0 || mAllDateOhne > 0) {
                                        mLinCamera.setVisibility(View.VISIBLE);
                                    }
                                }

                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                                reviewDialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (bitmap != null && !bitmap.isRecycled()) {
                                    bitmap.recycle();
                                }
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert).show();
            }
        });

        reviewDialog.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }

                reviewDialog.dismiss();
            }
        });
    }


    private void saveAllImagesToCache() {
        for (int index = 0; index < mImagesList.size(); index++) {
            ImageUtils.scaleAndStorageCacheImage(UbernachtungActivity.this, mImagesList.get(index));
        }
    }

    private void removeAllImagesFromCache() {
        for (int index = 0; index < mImagesList.size(); index++) {
            DBApplication.getBitmapCache().removeBitmap(mImagesList.get(index));
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == AppConfigs.REQUEST_CAMERA_CAPTURE) {
                    Intent intent = new Intent(UbernachtungActivity.this, ScanActivity.class);
                    startActivityForResult(intent, AppConfigs.IMAGE_RESULT);
                } else if (requestCode == AppConfigs.IMAGE_RESULT) {
                    mIBtnRemove.performClick();

                    String imagePath = data.getStringExtra(AppConfigs.SCANNED_RESULT);
                    if (!TextUtils.isEmpty(imagePath)) {
                        mImagesList.add(imagePath);
                        mImageAdapter.notifyDataSetChanged();

                        mLinCamera.setVisibility(View.GONE);
                        mLinAddCamera.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        removeAllImagesFromCache(); // TODO - must release memory, note that maybe it does not back
        if (mIsFromZusam) {
            Intent intent = new Intent(UbernachtungActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            if (hasModify()) {
                AlertUtils.showMessageAlert(UbernachtungActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
            } else {
                super.onBackPressed();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                removeAllImagesFromCache(); // TODO - must release memory, note that maybe it does not go to other page
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            case R.id.ibtn_add:
                mLinAddCamera.setVisibility(View.GONE);
                mLinCamera.setVisibility(View.VISIBLE);
                mIBtnRemove.setVisibility(View.VISIBLE);
                break;
            case R.id.ibtn_remove:
                mLinAddCamera.setVisibility(View.VISIBLE);
                mLinCamera.setVisibility(View.GONE);
                break;
            case R.id.ibtn_camera:
                if (!mMarshMallowPermission.checkPermissionForCamera()) {
                    mMarshMallowPermission.requestPermissionForCamera();
                } else {
                    if (!mMarshMallowPermission.checkPermissionForExternalStorage()) {
                        mMarshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getImageFileUri());
                        startActivityForResult(cameraIntent, AppConfigs.REQUEST_CAMERA_CAPTURE);
                    }
                }
                break;
            default:
                break;
        }
    }

    private boolean isValidate() {

        if (mLinCamera.getVisibility() != View.GONE) {
            if (!hasModify() || mImagesList.size() != 0) {
                Toast.makeText(UbernachtungActivity.this, getResources().getString(R.string.uber_not_scan_msg), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if ((mAllDateInk + mAllDateOhne + mAllDateOhneRechnung + mAllDateUnentgelt) == 0) {
            Toast.makeText(UbernachtungActivity.this, getResources().getString(R.string.uber_not_select_night_msg), Toast.LENGTH_SHORT).show();
            return false;
        }

        if ((mAllDateInk + mAllDateOhne) == 0 && mImagesList.size() > 0) {
            Toast.makeText(UbernachtungActivity.this, getResources().getString(R.string.uber_has_image_but_not_select_night_msg), Toast.LENGTH_SHORT).show();
            return false;
        }
        if ((mAllDateInk + mAllDateOhne) != 0 && mImagesList.size() == 0) {
            Toast.makeText(UbernachtungActivity.this, getResources().getString(R.string.uber_please_scann_msg), Toast.LENGTH_SHORT).show();
            return false;
        }

        if ((mAllDateInk + mAllDateOhne + mAllDateOhneRechnung + mAllDateUnentgelt) < mAllDateChoise) {
            Toast.makeText(UbernachtungActivity.this, getResources().getString(R.string.uber_night_chooser_not_max_msg), Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    private void getLatestTravelInfo() {
        mTravel.setInklusiveFruhstuck(mAllDateInk);
        mTravel.setOhneFruhstuck(mAllDateOhne);
        mTravel.setOhneRechnung(mAllDateOhneRechnung);
        mTravel.setUnentgeltlich(mAllDateUnentgelt);

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        int i, j;
        for (i = 0; i < mImagesList.size(); i++) {
            for (j = 0; j < mCurrentImageList.size(); j++) {
                if (mCurrentImageList.get(j).contains(mImagesList.get(i))) {
                    jsonArray.put(mCurrentImageList.get(j));
                    break;
                }
            }
            if (j == mCurrentImageList.size()) {
                jsonArray.put(mImagesList.get(i));
            }
        }

        try {
            jsonObject.put("uber_images", jsonArray);

            mTravel.setUberImages(jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mAllDateInk > 0 || mAllDateOhne > 0 || mAllDateOhneRechnung > 0 || mAllDateUnentgelt > 0) {
            mTravel.setRadioUber(true);
        } else {
            mTravel.setRadioUber(false);
        }
    }

    private boolean hasModify() {
        if (mTravel.getInklusiveFruhstuck() != mAllDateInk) {
            return true;
        }

        if ((mTravel.getOhneFruhstuck() != mAllDateOhne)) {
            return true;
        }

        if (mTravel.getOhneRechnung() != mAllDateOhneRechnung) {
            return true;
        }

        if (mTravel.getUnentgeltlich() != mAllDateUnentgelt) {
            return true;
        }

        if (TextUtils.isEmpty(mTravel.getUberImages())) {
            if (mImagesList.size() > 0) {
                return true;
            }
        } else {
            if (mCurrentImageList.size() != mImagesList.size()) {
                return true;
            }

            for (int i = 0; i < mCurrentImageList.size(); i++) {
                if (!mCurrentImageList.get(i).contains(mImagesList.get(i))) { // TODO - image link from server is difference with from local
                    return true;
                }
            }
        }

        return false;
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(UbernachtungActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(UbernachtungActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }
    
    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            removeAllImagesFromCache();

            if (hasModify()) {
                mTravel.setModify(true);
                getLatestTravelInfo();
                DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                TravelDbHelper.save(mTravel);
            }

            if (mIsFromZusam) {
                Intent intent = new Intent(UbernachtungActivity.this, ZusamActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            } else {
                finish();
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
        }
    }
}
