package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.models.Travel;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.DateUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ReisekostenActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle;
    ImageButton mIBtnBack, mIBtnMenu;
    RadioButton mRdBtnUberYes, mRdBtnUberNo, mRdBtnVerpYes, mRdBtnVerpNo, mRdBtnBelegYes, mRdBtnBelegNo;
    Button mBtnCancel, mBtnNext;
    Travel mTravel;
    Boolean mRadioUber, mRadioVerp, mRadioBeleg;

    private enum  REISEKOSTEN_TYPE {
        UBER, VERP, BELEG
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reisekosten;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mRdBtnUberYes = (RadioButton) findViewById(R.id.rbtn_uber_yes);
        mRdBtnUberNo = (RadioButton) findViewById(R.id.rbtn_uber_no);
        mRdBtnVerpYes = (RadioButton) findViewById(R.id.rbtn_verpflegung_yes);
        mRdBtnVerpNo = (RadioButton) findViewById(R.id.rbtn_verpflegung_no);
        mRdBtnBelegYes = (RadioButton) findViewById(R.id.rbtn_beleg_yes);
        mRdBtnBelegNo = (RadioButton) findViewById(R.id.rbtn_beleg_no);
        mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
        mTvTitle.setText(getResources().getString(R.string.reisekosten_title));

        if (getIntent().getBooleanExtra(AppConfigs.HAS_DATE_CHANGED, false)) {
            AlertUtils.showMessageAlert(ReisekostenActivity.this, getString(R.string.warning), getString(R.string.notice_change_date));
        }
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mRdBtnUberYes.setOnClickListener(this);
        mRdBtnUberNo.setOnClickListener(this);
        mRdBtnVerpYes.setOnClickListener(this);
        mRdBtnVerpNo.setOnClickListener(this);
        mRdBtnBelegYes.setOnClickListener(this);
        mRdBtnBelegNo.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
    }

    private void initData() {
        mTravel = DBApplication.getServiceFactory().getTravelService().getTravel();

        // Restore travel when app crashed
        if (mTravel == null) {
            mTravel = DBApplication.getServiceFactory().getTravelService().restoreTravel(ReisekostenActivity.this);
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
        }

        if (mTravel == null) {
            Toast.makeText(ReisekostenActivity.this, "Error when loading data. It's null.", Toast.LENGTH_LONG).show();
            return;
        }

        /* Uber */
        int totalDate = DateUtils.daysBetweenDate(mTravel.getBeginDate(), mTravel.getEndDate());
        if (totalDate == 0) {
            mRdBtnUberYes.setEnabled(false);
            mRdBtnUberNo.setChecked(true);
            mTravel.setUberImages(null); // delete all Uber's images
//            mTravel.setRadioUber(false); // avoid press back button then change date. has old data --> can not Nein
        } else {
            if (mTravel.getRadioUber() != null) {
                if (mTravel.getRadioUber()) {
                    mRdBtnUberYes.setChecked(true);
                    mRdBtnUberNo.setChecked(false);
                } else {
                    mRdBtnUberYes.setChecked(false);
                    mRdBtnUberNo.setChecked(true);
                }
            } else {
                mRdBtnUberYes.setChecked(false);
                mRdBtnUberNo.setChecked(false);
            }
        }

        /* Verpflegung */
        if (mTravel.getRadioVerp() != null) {
            if (mTravel.getRadioVerp()) {
                mRdBtnVerpYes.setChecked(true);
                mRdBtnVerpNo.setChecked(false);
            } else {
                mRdBtnVerpYes.setChecked(false);
                mRdBtnVerpNo.setChecked(true);
            }
        } else {
            mRdBtnVerpYes.setChecked(false);
            mRdBtnVerpNo.setChecked(false);
        }

        /* Beleg */
        if (mTravel.getRadioBeleg() != null) {
            if (mTravel.getRadioBeleg()) {
                mRdBtnBelegYes.setChecked(true);
                mRdBtnBelegNo.setChecked(false);
            } else {
                mRdBtnBelegYes.setChecked(false);
                mRdBtnBelegNo.setChecked(true);
            }
        } else {
            mRdBtnBelegYes.setChecked(false);
            mRdBtnBelegNo.setChecked(false);
        }

        mRadioUber = mTravel.getRadioUber();
        mRadioVerp = mTravel.getRadioVerp();
        mRadioBeleg = mTravel.getRadioBeleg();

        setActiveNextButton();
    }

    private boolean setActiveNextButton() {
        if ((mRdBtnUberYes.isChecked() || mRdBtnUberNo.isChecked())
                && (mRdBtnVerpYes.isChecked() || mRdBtnVerpNo.isChecked())
                && (mRdBtnBelegYes.isChecked() || mRdBtnBelegNo.isChecked())) {

            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
            return true;
        } else {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
            return false;
        }
    }

    private boolean hasModify() {
        if (mRadioUber == null && mTravel.getRadioUber() != null) {
            return true;
        } else if (mRadioUber != mTravel.getRadioUber()) {
            return true;
        }

        if (mRadioVerp == null && mTravel.getRadioVerp() != null) {
            return true;
        } else if (mRadioVerp != mTravel.getRadioVerp()) {
            return true;
        }

        if (mRadioBeleg == null && mTravel.getRadioBeleg() != null) {
            return true;
        } else if (mRadioBeleg != mTravel.getRadioBeleg()) {
            return true;
        }

        return false;
    }

    private void getLatestTravelInfo() {
        if (mRdBtnUberNo.isChecked()) {
            mTravel.setRadioUber(false);
        } else if (mRdBtnUberYes.isChecked()) {
            mTravel.setRadioUber(true);
        }

        if (mRdBtnVerpNo.isChecked()) {
            mTravel.setRadioVerp(false);
        } else if (mRdBtnVerpYes.isChecked()) {
            mTravel.setRadioVerp(true);
        }

        if (mRdBtnBelegNo.isChecked()) {
            mTravel.setRadioBeleg(false);
        } else if (mRdBtnBelegYes.isChecked()) {
            mTravel.setRadioBeleg(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        initData();
    }

    @Override
    public void onBackPressed() {
        if (hasModify()) {
            AlertUtils.showMessageAlert(ReisekostenActivity.this, getString(R.string.warning), getString(R.string.notice_when_back), getString(R.string.notice_cancel_when_back_pressed));
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                boolean hasModify = hasModify();
                if (hasModify) {
                    mTravel.setModify(true);
                    getLatestTravelInfo();
                    DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
                }
                showMenuDialog(hasModify, true);
                break;
            case R.id.rbtn_uber_yes:
                onUberYesRadioButtonClick();
                break;
            case R.id.rbtn_uber_no:
                onUberNoRadioButtonClick();
                break;
            case R.id.rbtn_verpflegung_yes:
                onVerpYesRadioButtonClick();
                break;
            case R.id.rbtn_verpflegung_no:
                onVerpNoRadioButtonClick();
                break;
            case R.id.rbtn_beleg_yes:
                onBelegYesRadioButtonClick();
                break;
            case R.id.rbtn_beleg_no:
                onBelegNoRadioButtonClick();
                break;
            case R.id.btn_cancel:
                onCancelButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onUberYesRadioButtonClick() {
//        mTravel.setRadioUber(true);
        mRdBtnUberNo.setChecked(false);
        mRdBtnUberYes.setChecked(true);
        startActivity(new Intent(ReisekostenActivity.this, UbernachtungActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        setActiveNextButton();
    }

    private void onUberNoRadioButtonClick() {
        if (mRdBtnUberYes.isChecked()) {
            showConfirmDialog(REISEKOSTEN_TYPE.UBER);
        } else {
            mTravel.setRadioUber(false);
            mRdBtnUberNo.setChecked(true);
            mRdBtnUberYes.setChecked(false);
        }
        setActiveNextButton();
    }

    private void onVerpYesRadioButtonClick() {
//        mTravel.setRadioVerp(true);
        mRdBtnVerpNo.setChecked(false);
        mRdBtnVerpYes.setChecked(true);
        startActivity(new Intent(ReisekostenActivity.this, VerpflegungActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        setActiveNextButton();
    }

    private void onVerpNoRadioButtonClick() {
        if (mRdBtnVerpYes.isChecked()) {
            showConfirmDialog(REISEKOSTEN_TYPE.VERP);
        } else {
            mTravel.setRadioVerp(false);
            mRdBtnVerpNo.setChecked(true);
            mRdBtnVerpYes.setChecked(false);
        }
        setActiveNextButton();
    }

    private void onBelegYesRadioButtonClick() {
//        mTravel.setRadioBeleg(true);
        mRdBtnBelegNo.setChecked(false);
        mRdBtnBelegYes.setChecked(true);
        startActivity(new Intent(ReisekostenActivity.this, BelegErfassenActivity.class));
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        setActiveNextButton();
    }

    private void onBelegNoRadioButtonClick() {
        if (mRdBtnBelegYes.isChecked()) {
            showConfirmDialog(REISEKOSTEN_TYPE.BELEG);
        } else {
            mTravel.setRadioBeleg(false);
            mRdBtnBelegNo.setChecked(true);
            mRdBtnBelegYes.setChecked(false);
        }
        setActiveNextButton();
    }

    private void onCancelButtonClick() {
        if (hasModify()) {
            mTravel.setModify(true);
            getLatestTravelInfo();
        }
        DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);

        Intent intent;
        if (mTravel.isModify()) { // maybe modified from another screen, not this screen.
            intent = new Intent(ReisekostenActivity.this, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            intent = new Intent(ReisekostenActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
        }
        finish();
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (setActiveNextButton()) {
            if (hasModify()) {
                mTravel.setModify(true);
            }
            getLatestTravelInfo();
            DBApplication.getServiceFactory().getTravelService().setTravel(mTravel);
            TravelDbHelper.save(mTravel);

            startActivity(new Intent(ReisekostenActivity.this, ZusamActivity.class));
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    private void showConfirmDialog(final Enum type) {
        final Dialog dialog = new Dialog(ReisekostenActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.alert_delete));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_reisekosten_delete));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch ((REISEKOSTEN_TYPE) type) {
                    case UBER:
                        mRdBtnUberYes.setChecked(false);
                        mRdBtnUberNo.setChecked(true);
                        mTravel.setRadioUber(false);

                        mTravel.setInklusiveFruhstuck(0);
                        mTravel.setOhneRechnung(0);
                        mTravel.setOhneFruhstuck(0);
                        mTravel.setUnentgeltlich(0);
                        mTravel.setUberImages(null);
                        break;

                    case VERP:
                        mRdBtnVerpYes.setChecked(false);
                        mRdBtnVerpNo.setChecked(true);
                        mTravel.setRadioVerp(false);

                        mTravel.setVerpflegung(null);
                        break;

                    case BELEG:
                        mRdBtnBelegYes.setChecked(false);
                        mRdBtnBelegNo.setChecked(true);
                        mTravel.setRadioBeleg(false);

                        mTravel.setPrivater(0);
                        mTravel.setTaxiImages(null);
                        mTravel.setFahrtkostenImages(null);
                        mTravel.setMietwagenImages(null);
                        mTravel.setKraftstoffImages(null);
                        mTravel.setParkenImages(null);
                        mTravel.setTagungsImages(null);
                        mTravel.setNeben(null);
                        break;
                }

                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch ((REISEKOSTEN_TYPE) type) {
                    case UBER:
                        mRdBtnUberYes.setChecked(true);
                        mRdBtnUberNo.setChecked(false);
                        break;

                    case VERP:
                        mRdBtnVerpYes.setChecked(true);
                        mRdBtnVerpNo.setChecked(false);
                        break;

                    case BELEG:
                        mRdBtnBelegYes.setChecked(true);
                        mRdBtnBelegNo.setChecked(false);
                        break;
                }

                dialog.dismiss();
            }
        });
    }
}
