package com.product.travel.activities;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.CallBack;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 6/18/16.
 */
public class DatensActivity extends BaseActivity implements View.OnClickListener {

    TextView mTvTitle, mTvBesondere, mTvDatens;
    ImageButton mIBtnBack, mIBtnMenu;
    CheckBox mCbDatens1, mCbDatens2;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_datens;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvBesondere = (TextView) findViewById(R.id.tv_besondere);
        mTvDatens = (TextView) findViewById(R.id.tv_datens);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mCbDatens1 = (CheckBox) findViewById(R.id.cb_datens_1);
        mCbDatens2 = (CheckBox) findViewById(R.id.cb_datens_2);
        mBtnNext = (Button) findViewById(R.id.btn_next);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);
        mTvBesondere.setOnClickListener(this);
        mTvDatens.setOnClickListener(this);
    }

    private void initData() {
        mTvTitle.setText(getResources().getString(R.string.impressum_datenschutzhinweise));

        String besondere = "<b style='mso-bidi-font-weight:normal'><span lang=DE>I.\n" +
                "Besondere Nutzungsbedingungen Reisekosten-App der Swiss Post Solutions GmbH (<u><font color=\"#0432FF\">hier bitte weiterlesen</font></u>).<o:p></o:p></span></b>";

        String datens = "<b style='mso-bidi-font-weight:normal'><span lang=DE>II.\n" +
                "Datenschutzerklärung (<u><font color=\"#0432FF\">hier bitte weiterlesen</font></u>).<o:p></o:p></span></b>";

        mTvBesondere.setText(Html.fromHtml(besondere));
        mTvDatens.setText(Html.fromHtml(datens));


        mIBtnBack.setVisibility(View.GONE);
        mIBtnMenu.setVisibility(View.GONE);
        mCbDatens1.setChecked(false);
        mCbDatens2.setChecked(false);

        if (StorageUtils.getDataSecureStatus(DatensActivity.this)) {
            mIBtnBack.setVisibility(View.VISIBLE);
            mIBtnMenu.setVisibility(View.VISIBLE);
            mCbDatens1.setChecked(true);
            mCbDatens2.setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_besondere:
                showDatensDialog(getString(R.string.datens_besondere_msg), content1);
                break;
            case R.id.tv_datens:
                showDatensDialog(getString(R.string.datens_datens_msg), content2);
                break;
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onNextButtonClick() {
        if (mCbDatens1.isChecked() && mCbDatens2.isChecked()) {
            StorageUtils.storageDataSecureStatus(DatensActivity.this, true);

            Intent intent;
            if (TextUtils.isEmpty(StorageUtils.getToken(DatensActivity.this))) {
                intent = new Intent(DatensActivity.this, ActivationActivity.class);
            } else {
                intent = new Intent(DatensActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }

            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        } else {
            StorageUtils.storageDataSecureStatus(DatensActivity.this, false);
            confirmUnUseApp();
        }
    }

    private boolean deleteFile(File file) {
        boolean deleteAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                for (String children : file.list()) {
                    deleteAll = deleteFile(new File(file, children)) && deleteAll;
                }
            } else {
                deleteAll = file.delete();
            }
        }

        return deleteAll;
    }

    private void clearAppData() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ((ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)).clearApplicationUserData();
        } else {
            File cachDir = getCacheDir();
            File appDir = new File(cachDir.getParent());
            if (appDir.exists()) {
                for (String fileName : appDir.list()) {
                    if (!fileName.equals("lib")) {
                        deleteFile(new File(appDir, fileName));
                    }
                }
            }
        }

        File dir = new File(Environment.getExternalStorageDirectory() + "/pdf");
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }

        deleteDatabase("DbReisekosten.db");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(DatensActivity.this);
        sharedPreferences.edit().clear().apply();
    }

    private void deleteUser() {
        showLoadingDialog(true);

        updateToken();
        DBApplication.getServiceFactory().getUserService().deleteUser(new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                showLoadingDialog(false);
                Log.i("deleteUser", "successful!");

                clearAppData();

                Intent intent = new Intent(DatensActivity.this, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(AppConfigs.EXIT_APP, true);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.fade_out);
                finish();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private void confirmUnUseApp() {
        final Dialog dialog = new Dialog(DatensActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.warning));
        ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.datens_confirm_msg));
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                deleteUser();
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void showDatensDialog(String title, String message) {
        final Dialog dialog = new Dialog(DatensActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_datens);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(title);
        WebView wvContent = ((WebView) dialog.findViewById(R.id.wv_message));
        wvContent.setVerticalScrollBarEnabled(false);
        wvContent.setHorizontalScrollBarEnabled(true);
        wvContent.loadDataWithBaseURL("file:///android_asset/", message, "text/html", "utf-8", null);

        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.hide();
                dialog.dismiss();
            }
        });
    }

    private String content1 = "<h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>1<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Geltungsbereich\n" +
            "</span><span lang=DE style='font-size:10.0pt;line-height:115%;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Die vorliegenden besonderen Nutzungsbedingungen regeln das Verhältnis\n" +
            "zwischen den Beschäftigten des Deutsche Bahn Konzerns (nachfolgend Nutzer\n" +
            "genannt) und dem Anbieter der Reisekosten App - der Swiss Post Solutions GmbH,\n" +
            "Kronacher Straße 70-80, 96052 Bamberg (siehe Rubrik „Impressum“ auf\n" +
            "www.swisspostsolutions.com, nachfolgend SPS genannt und Impressum innerhalb der\n" +
            "Reisekosten App) im Zusammenhang mit der Nutzung der Reisekosten App und den\n" +
            "darauf basierenden Dienstleistungen.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>2<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Teilnahme- und\n" +
            "Nutzungsvoraussetzungen </span><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext;mso-bidi-font-weight:\n" +
            "bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<h2 style='margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "28.9pt;margin-bottom:.0001pt;text-indent:-28.9pt;line-height:115%;mso-pagination:\n" +
            "widow-orphan lines-together;mso-list:l6 level2 lfo23;tab-stops:-148.85pt -134.7pt;\n" +
            "border:none;mso-padding-alt:0cm 0cm 0cm 0cm'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext'><span style='mso-list:Ignore'>2.1<span\n" +
            "style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;color:windowtext'>Authentifizierung\n" +
            "und Erhalt Registrierungs-ID <o:p></o:p></span></h2>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Der Nutzer hat sich für die Authentifizierung der Reisekosten\n" +
            "App über ein Papierformular anzumelden. Hierfür erhält er von seinem\n" +
            "Arbeitgeber oder dessen Auftragnehmer einen Brief, in dem er der\n" +
            "Datenschutzerklärung und den besonderen<span style='mso-spacerun:yes'>&nbsp;\n" +
            "</span>Nutzungsbedingungen zustimmt und mit seiner Unterschrift bestätigt. Dem\n" +
            "Nutzer wird von seinem Arbeitgeber oder dessen Auftragnehmer eine\n" +
            "Registrierungs-ID zugeschickt, um die Reisekosten App nutzen zu können.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h2 style='margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "28.9pt;margin-bottom:.0001pt;text-indent:-28.9pt;line-height:115%;mso-pagination:\n" +
            "widow-orphan lines-together;mso-list:l6 level2 lfo23;tab-stops:-148.85pt -134.7pt;\n" +
            "border:none;mso-padding-alt:0cm 0cm 0cm 0cm'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext'><span style='mso-list:Ignore'>2.2<span\n" +
            "style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;color:windowtext'>Belehrung\n" +
            "über das Widerrufsrecht , Ende der Nutzung<o:p></o:p></span></h2>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Der Nutzer hat jederzeit das Recht, sein Einverständnis zu\n" +
            "diesen besonderen Nutzungsbedingungen zu widerrufen. Sein Widerruf hat zur\n" +
            "Folge, dass der Nutzer die Reisekosten App dann nicht mehr nutzen kann. Der\n" +
            "Widerruf ist jederzeit möglich durch den Entzug der Einwilligungserklärung in\n" +
            "der Reisekosten App (Opt-out). Die Deinstallation ist jederzeit ohne weitere\n" +
            "Mitwirkung des App-Anbieters durch den Nutzer selbst möglich.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h1 style='margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>3<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Prozessbeschreibung\n" +
            "Erstellung Reisekostenantrag </span><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext;mso-bidi-font-weight:\n" +
            "bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "mso-bidi-font-family:\"Times New Roman\";color:windowtext'>Der Nutzer gibt die\n" +
            "Stammdaten zu seiner Person sowie die Reisedaten der abzurechnenden Dienstreise\n" +
            "in die App ein, scannt mit der Kamerafunktion des mobilen Endgerätes eventuell\n" +
            "zu berücksichtigende Originalbelege ein und schließt die Erfassung der\n" +
            "Reisedaten mit „Absenden“ ab. <o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "mso-bidi-font-family:\"Times New Roman\";color:windowtext'>Den Status der\n" +
            "Abrechnung seiner Reise kann der Nutzer innerhalb der App abrufen. <o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "mso-bidi-font-family:\"Times New Roman\";color:windowtext'>Die App übermittelt\n" +
            "die Reisedaten verschlüsselt an den Reisekostenserver der Swiss Post Solutions\n" +
            "GmbH bei der noris network AG zur Erstellung des Reisekostenantrages im PDF-Format\n" +
            "einschließlich der dazugehörigen Anlagen.<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "mso-bidi-font-family:\"Times New Roman\";color:windowtext'>Die Anträge werden auf\n" +
            "dem Server der Swiss Post Solutions GmbH für den Abruf durch den Arbeitgeber\n" +
            "des Nutzer oder dessen Auftragnehmer (Auftragsdatenverarbeitung gem. § 11 BDSG)\n" +
            "bereitgestellt. Der Abruf der Daten erfolgt auf der Basis eines\n" +
            "Datenübermittlungsvertrages zwischen dem Datenübermittler Swiss Post Solutions\n" +
            "GmbH und dem Arbeitgeber des Nutzers als Datenempfänger.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h1 style='margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>4<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Pflichten des\n" +
            "Nutzers </span><span lang=DE style='font-size:10.0pt;line-height:115%;\n" +
            "mso-bidi-font-family:Frutiger;color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Der Nutzer verpflichtet sich, bei der Nutzung der Reisekosten\n" +
            "App keine vertraglichen oder gesetzlichen Pflichten zu verletzen. <span\n" +
            "style='mso-bidi-font-weight:bold'>Der Nutzer ist für die sorgfältige\n" +
            "Verwahrung, die ordnungsgemäße Verwendung und die Qualität seines Passworts und\n" +
            "die sorgfältige Verwahrung der Registrierungs-ID verantwortlich. </span>Er ändert\n" +
            "sein Passwort unverzüglich, wenn er weiß oder den Verdacht hat, dass ein\n" +
            "Unbefugter davon Kenntnis bzw. Zugriff darauf haben könnte. Eine regelmäßige\n" +
            "Änderung des Passworts zumindest vierteljährlich wird empfohlen.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h1 style='margin-top:6.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>5<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Haftung</span><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-bidi-font-family:Frutiger;\n" +
            "color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<h1 style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:none'><span lang=DE\n" +
            "style='font-size:10.0pt;line-height:115%;mso-bidi-font-family:Frutiger;\n" +
            "color:windowtext;font-weight:normal'>Die Haftung der Swiss Post Solutions GmbH\n" +
            "richtet sich nach den Regelungen des Rahmenvertrages.</span><span lang=DE\n" +
            "style='font-size:10.0pt;line-height:115%;mso-bidi-font-family:Frutiger;\n" +
            "color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;line-height:115%;page-break-before:\n" +
            "auto;mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;line-height:115%;mso-fareast-font-family:\"DB Office\";\n" +
            "mso-bidi-font-family:\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span\n" +
            "style='mso-list:Ignore'>6<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "line-height:115%;mso-bidi-font-family:Frutiger;color:windowtext'>Anwendbares\n" +
            "Recht </span><span lang=DE style='font-size:10.0pt;line-height:115%;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Auf das Nutzungsverhältnis ist ausschließlich deutsches Recht\n" +
            "anwendbar.<o:p></o:p></span></p>\n" +
            "\n" +
            "<h1 style='margin-top:12.0pt;margin-right:0cm;margin-bottom:0cm;margin-left:\n" +
            "21.55pt;margin-bottom:.0001pt;text-indent:-21.55pt;page-break-before:auto;\n" +
            "mso-pagination:widow-orphan lines-together;mso-list:l6 level1 lfo23'><![if !supportLists]><span\n" +
            "lang=DE style='font-size:10.0pt;mso-fareast-font-family:\"DB Office\";mso-bidi-font-family:\n" +
            "\"DB Office\";color:windowtext;mso-bidi-font-weight:bold'><span style='mso-list:\n" +
            "Ignore'>7<span style='font:7.0pt \"Times New Roman\"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
            "</span></span></span><![endif]><span lang=DE style='font-size:10.0pt;\n" +
            "mso-bidi-font-family:Frutiger;color:windowtext'>Kontaktadresse Swiss Post\n" +
            "Solutions GmbH </span><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-bidi-font-weight:bold'><o:p></o:p></span></h1>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "color:windowtext'>Die Kontaktadresse der Swiss Post Solutions GmbH (siehe Punkt\n" +
            "1) ist dem Impressum innerhalb der Reisekosten App zu entnehmen.<o:p></o:p></span></p>";

    private String content2 = "<span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Die Swiss Post Solutions\n" +
            "GmbH stellt für Beschäftigte des DB-Konzerns eine App zur vereinfachten und\n" +
            "optimierten Erstellung von Reisekostenanträgen zur Verfügung. <o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Die bei der Swiss Post\n" +
            "Solutions GmbH für den Versand der Abrechnungsbescheinigungen gespeicherten\n" +
            "Daten (Name, Vorname, Personalnummer) werden für die Authentifizierung an der\n" +
            "Reisekosten App genutzt und verschlüsselt übertragen.<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><span\n" +
            "style='mso-spacerun:yes'>&nbsp;</span><o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Für die Erstellung des Reisekostenantrags\n" +
            "werden personenbezogene Daten (Name, Vorname, Personalnummer, Adressdaten,\n" +
            "Reisedaten, Scans der Reisebelege) der Nutzer durch die App verschlüsselt auf\n" +
            "einen von der Swiss Post Solutions administrierten Server bei der noris network\n" +
            "AG (Unterauftragnehmer der Swiss Post Solutions GmbH) übertragen und dort\n" +
            "gespeichert. Die Swiss Post Solutions GmbH verwendet die Daten des Nutzers\n" +
            "ausschließlich zu dem Zweck der Erstellung und Bereitstellung zum Abruf von\n" +
            "Reisekostenanträgen unter Beachtung der gesetzlichen und vertraglichen\n" +
            "Datenschutzbestimmungen.<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Die für die Erstellung der\n" +
            "Reisekostenanträge erforderlichen Stammdaten werden dauerhaft auf dem mobilen\n" +
            "Endgerät gespeichert bis der Nutzer sie ändert oder löscht. Wird die\n" +
            "Reisekosten App durch den Nutzer vom mobilen Endgerät deinstalliert, werden\n" +
            "sämtliche Daten die innerhalb der Reisekosten App gespeichert sind\n" +
            "datenschutzkonform gelöscht.<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Nach dem verschlüsselten\n" +
            "Versand der Anträge an den Server der Swiss Post Solutions GmbH werden die Antragsdaten\n" +
            "zum Zwecke der Qualitätssicherung, der Revision und um dem Mitarbeiter die\n" +
            "Möglichkeit zu geben, innerhalb dieser Frist seinen Reisekostenantrag ändern zu\n" +
            "können sieben Monate auf dem Server der Swiss Post Solutions GmbH gespeichert\n" +
            "und anschließend datenschutzkonform gelöscht.<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Für die Wahrung Ihrer\n" +
            "Betroffenenrechte wenden Sie sich bitte an folgende Adresse:<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Swiss Post Solutions GmbH<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Kronacher Straße 70 - 80<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>96052 Bamberg<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Rufen Sie uns an: +49 (0)\n" +
            "951 9168 200<o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=MsoNormal><span lang=DE style='font-size:10.0pt;mso-bidi-font-family:\n" +
            "Frutiger;color:windowtext;mso-font-kerning:14.0pt'>Schreiben Sie uns ein\n" +
            "E-Mail: sps.de@swisspost.com</span><span lang=DE style='font-size:10.0pt'><o:p></o:p></span></p>\n" +
            "\n" +
            "<p class=Default><span lang=DE style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";\n" +
            "mso-bidi-font-family:\"Times New Roman\";color:windowtext'><o:p>&nbsp;</o:p></span></p>\n" +
            "\n" +
            "<p class=Default align=right style='text-align:right'><span lang=DE\n" +
            "style='font-size:10.0pt;font-family:\"DB Office\",\"sans-serif\";color:windowtext'>Swiss\n" +
            "Post Solutions GmbH, Juni 2016<o:p></o:p></span></p>\n";
}
