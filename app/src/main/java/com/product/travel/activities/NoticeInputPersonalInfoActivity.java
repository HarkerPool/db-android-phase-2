package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NoticeInputPersonalInfoActivity extends BaseActivity {
    TextView mTvContent, mTvContent2;
    ImageView mIvStatus;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_confirm;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvContent = (TextView) findViewById(R.id.tv_content);
        mTvContent2 = (TextView) findViewById(R.id.tv_content_2);
        mIvStatus = (ImageView) findViewById(R.id.iv_status);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mTvContent.setText(getResources().getString(R.string.notice_input_personal_info_msg));
        mIvStatus.setVisibility(View.GONE);
        mTvContent2.setVisibility(View.GONE);
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent = new Intent(NoticeInputPersonalInfoActivity.this, PersonalActivity.class);
        intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, true);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}
