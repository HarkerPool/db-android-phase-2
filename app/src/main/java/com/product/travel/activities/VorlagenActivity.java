package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import com.product.travel.R;
import com.product.travel.adapters.VorlagenAdapter;
import com.product.travel.commons.AppConfigs;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.EntwurfeItemListener;
import com.product.travel.widget.DividerRecyclerViewExt;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class VorlagenActivity extends BaseActivity implements View.OnClickListener, EntwurfeItemListener {
    TextView mTvTitle;
    RecyclerView mRecyclerViewReise, mRecyclerViewVorschuss;
    ImageButton mIBtnBack, mIBtnMenu;
    List<TravelDbItem> mReiseList, mVorschussList;
    VorlagenAdapter mReiseAdapter, mVorschussAdapter;
    Spinner mSpnTravelStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_antrage;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mRecyclerViewReise = (RecyclerView) findViewById(R.id.rv_reise);
        mRecyclerViewVorschuss = (RecyclerView) findViewById(R.id.rv_vorschuss);
        mSpnTravelStatus = (Spinner) findViewById(R.id.spn_travel_status);

        mTvTitle.setText(getResources().getString(R.string.vorlagen_item));
    }

    private void initEvents() {
        mRecyclerViewReise.addItemDecoration(new DividerRecyclerViewExt(VorlagenActivity.this, LinearLayoutManager.VERTICAL));
        mRecyclerViewVorschuss.addItemDecoration(new DividerRecyclerViewExt(VorlagenActivity.this, LinearLayoutManager.VERTICAL));
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);

        mSpnTravelStatus.setVisibility(View.GONE);
    }

    private void initData() {
        mReiseList = TravelDbHelper.getVorlagen(true);
        mVorschussList = TravelDbHelper.getVorlagen(false);

        boolean isFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        mReiseAdapter = new VorlagenAdapter(mReiseList, !isFromZusam, this);
        mRecyclerViewReise.setHasFixedSize(true);
        mRecyclerViewReise.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewReise.setAdapter(mReiseAdapter);
        mReiseAdapter.notifyDataSetChanged();

        mVorschussAdapter = new VorlagenAdapter(mVorschussList, !isFromZusam, this);
        mRecyclerViewVorschuss.setHasFixedSize(true);
        mRecyclerViewVorschuss.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewVorschuss.setAdapter(mVorschussAdapter);
        mVorschussAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false)) {
            Intent intent = new Intent(VorlagenActivity.this, ZusamActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onReiseEditListener(int position) {
        Intent intent = new Intent(VorlagenActivity.this, ReiseUndActivity.class);
        intent.putExtra(AppConfigs.TRAVEL_ID_KEY, mReiseList.get(position).getTravelId());
        intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onVorschussEditListener(int position) {
        Intent intent = new Intent(VorlagenActivity.this, VorschussBeanActivity.class);
        intent.putExtra(AppConfigs.TRAVEL_ID_KEY, mVorschussList.get(position).getTravelId());
        intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    @Override
    public void onReiseDeleteListener(int position) {
        /*TravelDbHelper.deleteTravelById(mReiseList.get(position).getTravelId());
        mReiseList.remove(position);
        mReiseAdapter.notifyDataSetChanged();*/
        deleteConfirmationDialog(true, position);
    }

    @Override
    public void onVorschussDeleteListener(int position) {
        /*TravelDbHelper.deleteTravelById(mVorschussList.get(position).getTravelId());
        mVorschussList.remove(position);
        mVorschussAdapter.notifyDataSetChanged();*/
        deleteConfirmationDialog(false, position);
    }

    private void deleteConfirmationDialog(final boolean isReise, final int position) {
        final Dialog dialog = new Dialog(VorlagenActivity.this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_alert);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);

        ((TextView) dialog.findViewById(R.id.tv_title)).setText(getString(R.string.delete));
        if (isReise) {
            ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_reise_delete));
        } else {
            ((TextView) dialog.findViewById(R.id.tv_message)).setText(getString(R.string.notice_vorschuss_delete));
        }
        dialog.show();

        dialog.findViewById(R.id.ibtn_ok).setVisibility(View.GONE);

        dialog.findViewById(R.id.ibtn_yes).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                updateToken();

                if (isReise) {
                    ThreadInBackground.deleteTravel(VorlagenActivity.this, mReiseList.get(position).getTravelId());
                    mReiseList.remove(position);
                    mReiseAdapter.notifyDataSetChanged();
                } else {
                    ThreadInBackground.deleteTravel(VorlagenActivity.this, mVorschussList.get(position).getTravelId());
                    mVorschussList.remove(position);
                    mVorschussAdapter.notifyDataSetChanged();
                }
            }
        });

        dialog.findViewById(R.id.ibtn_no).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}
