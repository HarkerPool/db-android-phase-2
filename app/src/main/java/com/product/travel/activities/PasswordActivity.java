package com.product.travel.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.CallBack;
import com.product.travel.params.ChangePasswordParams;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.ValidationUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class PasswordActivity extends BaseActivity implements View.OnClickListener, TextWatcher {

    TextView mTvTitle, mTvOldPassword, mTvNewPassword, mTvNewPasswordRepeat, mTvSetPasswordNotice;
    Button mBtnNext;
    ImageButton mIBtnBack, mIBtnMenu;
    EditText mEdtOldPassword, mEdtNewPassword, mEdtNewPasswordRepeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_password;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvOldPassword = (TextView) findViewById(R.id.tv_password_old);
        mTvNewPassword = (TextView) findViewById(R.id.tv_password_new);
        mTvNewPasswordRepeat = (TextView) findViewById(R.id.tv_password_new_repeat);
        mTvSetPasswordNotice = (TextView) findViewById(R.id.tv_password_set);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mEdtOldPassword = (EditText) findViewById(R.id.edt_password_old);
        mEdtNewPassword = (EditText) findViewById(R.id.edt_password_new);
        mEdtNewPasswordRepeat = (EditText) findViewById(R.id.edt_password_new_repeat);
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        mEdtOldPassword.addTextChangedListener(this);
        mEdtNewPassword.addTextChangedListener(this);
        mEdtNewPasswordRepeat.addTextChangedListener(this);
    }

    private void initData() {
        switch ((AppConfigs.PASSWORD_HANDLER) getIntent().getSerializableExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY))) {
            case CREATE:
                mTvTitle.setText(getResources().getString(R.string.create_password));

                mTvOldPassword.setVisibility(View.GONE);
                mEdtOldPassword.setVisibility(View.GONE);
                mTvSetPasswordNotice.setVisibility(View.GONE);
                break;
            case CHANGE:
                mTvTitle.setText(getResources().getString(R.string.change_password));

                mTvSetPasswordNotice.setVisibility(View.GONE);
                break;
            case DELETE:
                mTvTitle.setText(getResources().getString(R.string.delete_password));

                mTvSetPasswordNotice.setVisibility(View.VISIBLE);

                mTvNewPassword.setVisibility(View.GONE);
                mEdtNewPassword.setVisibility(View.GONE);

                mTvNewPasswordRepeat.setVisibility(View.GONE);
                mEdtNewPasswordRepeat.setVisibility(View.GONE);
                break;
            default:
                break;
        }

        mBtnNext.setText(getResources().getString(R.string.save));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            passwordHandler();
//            new PasswordHandlerTask().execute();
        }
    }

    private boolean isValidate() {
        Enum type = (AppConfigs.PASSWORD_HANDLER) getIntent().getSerializableExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY));

        if (type == AppConfigs.PASSWORD_HANDLER.CHANGE) {
            if (!TextUtils.equals(StorageUtils.getPassword(PasswordActivity.this), mEdtOldPassword.getText().toString())) {
                mEdtOldPassword.setError(getString(R.string.notice_password_wrong));
                mEdtOldPassword.requestFocus();
                return false;
            }
            if (TextUtils.equals(mEdtOldPassword.getText().toString(), mEdtNewPassword.getText().toString())) {
                mEdtNewPassword.setError(getString(R.string.notice_same_password));
                mEdtNewPassword.requestFocus();
                return false;
            }
            /*if (!ValidationUtils.isValidPassword(mEdtOldPassword.getText().toString())) {
                mEdtOldPassword.setError(getResources().getString(R.string.notice_password_length));
                mEdtOldPassword.requestFocus();
                return false;
            }*/
        }

        if (type == AppConfigs.PASSWORD_HANDLER.CHANGE || type == AppConfigs.PASSWORD_HANDLER.CREATE) {
            if (!ValidationUtils.isValidPassword(mEdtNewPassword.getText().toString())) {
                mEdtNewPassword.setError(getResources().getString(R.string.notice_password_length));
                mEdtNewPassword.requestFocus();
                return false;
            }
            if (!ValidationUtils.isValidPassword(mEdtNewPasswordRepeat.getText().toString())) {
                mEdtNewPasswordRepeat.setError(getResources().getString(R.string.notice_password_length));
                mEdtNewPasswordRepeat.requestFocus();
                return false;
            }
            if (!TextUtils.equals(mEdtNewPassword.getText().toString(), mEdtNewPasswordRepeat.getText().toString())) {
                mEdtNewPasswordRepeat.setError(getResources().getString(R.string.notice_password_not_match));
                mEdtNewPasswordRepeat.requestFocus();
                return false;
            }
        }

        if (type == AppConfigs.PASSWORD_HANDLER.DELETE
                && !TextUtils.equals(StorageUtils.getPassword(PasswordActivity.this), mEdtOldPassword.getText().toString())) {
            mEdtOldPassword.setError(getResources().getString(R.string.notice_password_not_match));
            mEdtOldPassword.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        EditText edt;
        if (mEdtOldPassword.isFocused()) {
            edt = mEdtOldPassword;
        } else if (mEdtNewPassword.isFocused()) {
            edt = mEdtNewPassword;
        } else {
            edt = mEdtNewPasswordRepeat;
        }

        if (s != null && ValidationUtils.isValidPassword(s.toString())) {
            edt.setTextColor(getResources().getColor(R.color.green));
        } else {
            edt.setTextColor(getResources().getColor(R.color.bg_active));
        }
    }

    private void backToHome() {
        Intent intent = new Intent(PasswordActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    private void createPassword() {
        updateToken();
        DBApplication.getServiceFactory().getUserService().createPassword(mEdtNewPassword.getText().toString(), new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                StorageUtils.storagePassword(PasswordActivity.this, mEdtNewPassword.getText().toString());
                showLoadingDialog(false);
                backToHome();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private void changePassword() {
        updateToken();
        ChangePasswordParams params = new ChangePasswordParams(mEdtOldPassword.getText().toString(), mEdtNewPassword.getText().toString());
        DBApplication.getServiceFactory().getUserService().changePassword(params, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                StorageUtils.storagePassword(PasswordActivity.this, mEdtNewPassword.getText().toString());
                showLoadingDialog(false);
                backToHome();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private void deletePassword() {
        updateToken();
        DBApplication.getServiceFactory().getUserService().deletePassword(mEdtOldPassword.getText().toString(), new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                StorageUtils.storagePassword(PasswordActivity.this, "");
                showLoadingDialog(false);
                backToHome();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }

    private void passwordHandler() {
        showLoadingDialog(true);

        switch ((AppConfigs.PASSWORD_HANDLER) getIntent().getSerializableExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY))) {
            case CREATE:
                createPassword();
                break;
            case CHANGE:
                changePassword();
                break;
            case DELETE:
                deletePassword();
                break;
            default:
                break;
        }
    }

    private class PasswordHandlerTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showLoadingDialog(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            switch ((AppConfigs.PASSWORD_HANDLER) getIntent().getSerializableExtra(String.valueOf(AppConfigs.PASSWORD_HANDLER.KEY))) {
                case CREATE:
                    createPassword();
                    break;
                case CHANGE:
                    changePassword();
                    break;
                case DELETE:
                    deletePassword();
                    break;
                default:
                    break;
            }

            return null;
        }
    }
}
