package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class NoticeSentFailedActivity extends BaseActivity {
    TextView mTvContent, mTvContent2;
    ImageView mIvStatus;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showLoadingDialog(false);
        initObjects();
        initEvents();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.layout_confirm;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvContent = (TextView) findViewById(R.id.tv_content);
        mTvContent2 = (TextView) findViewById(R.id.tv_content_2);
        mIvStatus = (ImageView) findViewById(R.id.iv_status);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        if (getIntent().getBooleanExtra(AppConfigs.IS_SENT_REISE, false)) {
            if (getIntent().getBooleanExtra(AppConfigs.HAS_CANCELED, false)) {
                mTvContent.setText(getResources().getString(R.string.confirm_reise_send_cancel_msg));
            } else {
                if (checkNetworkConnection()) {
                    mTvContent.setText(getResources().getString(R.string.confirm_reise_send_fail_to_rk_msg));
                } else {
                    mTvContent.setText(getResources().getString(R.string.confirm_reise_send_fail_msg));
                }
            }
        } else {
            if (checkNetworkConnection()) {
                mTvContent.setText(getResources().getString(R.string.confirm_vorschuss_send_fail_to_rk_msg));
            } else {
                mTvContent.setText(getResources().getString(R.string.confirm_vorschuss_send_fail_msg));
            }
        }
        mTvContent2.setVisibility(View.GONE);
        mIvStatus.setVisibility(View.VISIBLE);
        mIvStatus.setImageResource(R.drawable.fail);
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextButtonClick();
            }
        });
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        Intent intent = new Intent(NoticeSentFailedActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    @Override
    public void onBackPressed() {
        // nothing
    }
}
