package com.product.travel.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.api.ThreadInBackground;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.User;
import com.product.travel.params.ActivationParams;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class ActivationActivity extends BaseActivity implements View.OnClickListener {
    EditText mEdtCodeBlock1, mEdtCodeBlock2, mEdtCodeBlock3, mEdtCodeBlock4;
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_activation;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mEdtCodeBlock1 = (EditText) findViewById(R.id.edt_code_block_1);
        mEdtCodeBlock2 = (EditText) findViewById(R.id.edt_code_block_2);
        mEdtCodeBlock3 = (EditText) findViewById(R.id.edt_code_block_3);
        mEdtCodeBlock4 = (EditText) findViewById(R.id.edt_code_block_4);
        mBtnNext = (Button) findViewById(R.id.btn_next);

        mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));

        mEdtCodeBlock1.requestFocus();
        showSoftKeyboard();
    }

    private int getCodeLength() {
        return mEdtCodeBlock1.getText().length() + mEdtCodeBlock2.getText().length() + mEdtCodeBlock3.getText().length() + mEdtCodeBlock4.getText().length();
    }

    private void setBtnNextColor() {
        if (getCodeLength() >= 16) {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_active));
        } else {
            mBtnNext.setBackgroundColor(getResources().getColor(R.color.bg_inactive));
        }
    }

    private void initEvents() {
        mBtnNext.setOnClickListener(this);

        mEdtCodeBlock1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 4) {
                    mEdtCodeBlock2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnNextColor();
            }
        });

        mEdtCodeBlock2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0 && mEdtCodeBlock1.getText().length() < 4) {
                    mEdtCodeBlock1.requestFocus();
                } else if (s.length() >= 4) {
                    mEdtCodeBlock3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnNextColor();
            }
        });

        mEdtCodeBlock3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0 && mEdtCodeBlock2.getText().length() < 4) {
                    mEdtCodeBlock2.requestFocus();
                } else if (s.length() >= 4) {
                    mEdtCodeBlock4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnNextColor();
            }
        });


        mEdtCodeBlock4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0 && mEdtCodeBlock3.getText().length() < 4) {
                    mEdtCodeBlock3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                setBtnNextColor();
            }
        });
    }

    private void initData() {
        Toast.makeText(ActivationActivity.this, getString(R.string.notice_activate), Toast.LENGTH_LONG).show();

        StorageUtils.storageMobileStatus(ActivationActivity.this, true); // default is ON - user can use all data network
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (getCodeLength() < 16) {
            return;
        }

        showLoadingDialog(true);

        if (!checkNetworkConnection()) {
            AlertUtils.showMessageAlert(ActivationActivity.this, getString(R.string.notice_no_internet));
            showLoadingDialog(false);
        } else {
            new StartActivationTask().execute();
        }
    }

    private void startActivation(final ActivationParams params) {

        DBApplication.getServiceFactory().getUserService().activation(params, new CallBack<User>() {
            @Override
            public void onSuccess(User result) {
                showLoadingDialog(false);

                StorageUtils.storageActivationCode(ActivationActivity.this, params.getActivationCode());
                StorageUtils.storageToken(ActivationActivity.this, result.getToken());

                startActivity(new Intent(ActivationActivity.this, NoticeInputPersonalInfoActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();

                ThreadInBackground.updateTravelStatus(); // Synchronize data when opening app.
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                if (t != null && !TextUtils.isEmpty(t.getMessage())) {
                    if (t.getMessage().contains("Ihre RegistrierungsID ist nicht korrekt") || t.getMessage().contains("User not found with code")) {
                        Toast.makeText(ActivationActivity.this, "Ihre RegistrierungsID ist nicht korrekt.", Toast.LENGTH_LONG).show();
                    } else {
                        handleErrorException(t);
                    }
                }
            }
        });
    }

    private class StartActivationTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String deviceToken;
            try {
                InstanceID instanceID = InstanceID.getInstance(ActivationActivity.this);
                deviceToken = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            } catch (Exception e) {
                e.printStackTrace();
                deviceToken = "CouldNotGetAToken";
            }
            Log.i("Activation", "Device token:" + deviceToken);

            return deviceToken;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String code = mEdtCodeBlock1.getText().toString() + "-";
            code += mEdtCodeBlock2.getText().toString() + "-";
            code += mEdtCodeBlock3.getText().toString() + "-";
            code += mEdtCodeBlock4.getText().toString();

            ActivationParams params = new ActivationParams(code, result);
            startActivation(params);
        }
    }
}
