package com.product.travel.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.product.travel.R;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class MobileWlanActivity extends BaseActivity implements View.OnClickListener {
    ImageButton mIBtnBack, mIBtnMenu;
    TextView mTvTitle;
    ToggleButton mToggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();
        initData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_mobile_wlan;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mToggleButton = (ToggleButton) findViewById(R.id.togbtn_mobile);

        mTvTitle.setText(getString(R.string.mobile_wlan));
    }

    private void initEvents() {
        mIBtnBack.setOnClickListener(this);
        mIBtnMenu.setOnClickListener(this);
        mToggleButton.setOnClickListener(this);
    }

    private void initData() {
        mToggleButton.setChecked(StorageUtils.getMobileStatus(MobileWlanActivity.this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.togbtn_mobile:
                toggleMobileButtonClick();
                break;
            default:
                break;
        }
    }

    private void toggleMobileButtonClick() {
        StorageUtils.storageMobileStatus(MobileWlanActivity.this, mToggleButton.isChecked());
    }
}
