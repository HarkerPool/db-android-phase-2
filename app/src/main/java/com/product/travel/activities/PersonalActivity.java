package com.product.travel.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.CityItem;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.HomeAddress;
import com.product.travel.models.User;
import com.product.travel.utils.AlertUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;
import com.product.travel.utils.ValidationUtils;
import com.product.travel.widget.EditTextOrtExt;
import com.product.travel.widget.EditTextPlzExt;
import com.product.travel.widget.ShowSubTextWatcher;

/**
 * Created by HarkerPool on 5/16/16.
 */
public class PersonalActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle, mTvAddMoreAddress, mTvPersonalNo, mTvEmail, mTvTel;
    private ImageButton mIBtnBack, mIBtnMenu, mIBtnAdd;
    private Button mBtnNext;
    private View mViewAddressSub1, mViewAddressSub2, mViewAddressMain;
    private EditText mEdtLastName, mEdtFirstName, mEdtEmail, mEdtTel, mEdtStreetSub1, mEdtHouseNumberSub1, mEdtStreetSub2, mEdtHouseNumberSub2, mEdtStreetMain, mEdtHouseNumberMain;
    private Spinner mSpnEmployeeStatus;

    private EditTextPlzExt mEdtPlzSub1Ext, mEdtPlzSub2Ext, mEdtPlzMainExt;
    private EditTextOrtExt mEdtOrtSub1Ext, mEdtOrtSub2Ext, mEdtOrtMainExt;

    private User mUser;

    private Enum mCurrentType;

    private enum AUTOCOMPLETETEXTVIEW_ITEM_TYPE {
        PLZ_MAIN, ORT_MAIN, PLZ_SUB_1, ORT_SUB_1, PLZ_SUB_2, ORT_SUB_2
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initObjects();
        initEvents();

        /*if (DBApplication.getServiceFactory().getUserService().getUserInfo() == null
                || getIntent().getBooleanExtra(AppConfigs.IS_FIRST_ACTIVATE, false)) {

            new GetUserInfoTask().execute();
        } else {
            initData();
        }*/

        getUserInfo();
        /*if (!checkNetworkConnection()) {
            AlertUtils.showMessageAlert(PersonalActivity.this, getString(R.string.notice_no_internet));
        } else {
            getUserInfo();
//            new GetUserInfoTask().execute();
        }*/
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_personal;
    }

    @Override
    protected View getParentView() {
        return findViewById(R.id.parent);
    }

    private void initObjects() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvAddMoreAddress = (TextView) findViewById(R.id.tv_add_more_address);
        mTvPersonalNo = (TextView) findViewById(R.id.tv_personal_no);
        mTvEmail = (TextView) findViewById(R.id.tv_email);
        mTvTel = (TextView) findViewById(R.id.tv_tel);
        mIBtnBack = (ImageButton) findViewById(R.id.ibtn_back);
        mIBtnMenu = (ImageButton) findViewById(R.id.ibtn_menu);
        mIBtnAdd = (ImageButton) findViewById(R.id.ibtn_add);
        mViewAddressSub1 = findViewById(R.id.i_address_sub1);
        mViewAddressSub2 = findViewById(R.id.i_address_sub2);
        mViewAddressMain = findViewById(R.id.i_address_main);
        mBtnNext = (Button) findViewById(R.id.btn_next);
        mEdtLastName = (EditText) findViewById(R.id.edt_name);
        mEdtFirstName = (EditText) findViewById(R.id.edt_first_name);
        mEdtEmail = (EditText) findViewById(R.id.edt_email);
        mEdtTel = (EditText) findViewById(R.id.edt_tel);
        mSpnEmployeeStatus = (Spinner) findViewById(R.id.spn_employee_status);

        /* init Spinner value */
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.employee_status_array));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpnEmployeeStatus.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.country_array));
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinnerSub1 = (Spinner) mViewAddressSub1.findViewById(R.id.spn_national);
        Spinner spinnerSub2 = (Spinner) mViewAddressSub2.findViewById(R.id.spn_national);
        Spinner spinnerMain = (Spinner) mViewAddressMain.findViewById(R.id.spn_national);
        spinnerSub1.setAdapter(adapter2);
        spinnerSub2.setAdapter(adapter2);
        spinnerMain.setAdapter(adapter2);

        /* mViewAddressSub1 */
        mEdtStreetSub1 = (EditText) mViewAddressSub1.findViewById(R.id.edt_street_sub);
        mEdtHouseNumberSub1 = (EditText) mViewAddressSub1.findViewById(R.id.edt_house_number_sub);
        mEdtPlzSub1Ext = (EditTextPlzExt) mViewAddressSub1.findViewById(R.id.edt_plz_sub);
        mEdtOrtSub1Ext = (EditTextOrtExt) mViewAddressSub1.findViewById(R.id.edt_ort_sub);

        /* mViewAddressSub2 */
        mEdtStreetSub2 = (EditText) mViewAddressSub2.findViewById(R.id.edt_street_sub);
        mEdtHouseNumberSub2 = (EditText) mViewAddressSub2.findViewById(R.id.edt_house_number_sub);
        mEdtPlzSub2Ext = (EditTextPlzExt) mViewAddressSub2.findViewById(R.id.edt_plz_sub);
        mEdtOrtSub2Ext = (EditTextOrtExt) mViewAddressSub2.findViewById(R.id.edt_ort_sub);

        /* mViewAddressMain */
        mEdtStreetMain = (EditText) mViewAddressMain.findViewById(R.id.edt_street_sub);
        mEdtHouseNumberMain = (EditText) mViewAddressMain.findViewById(R.id.edt_house_number_sub);
        mEdtPlzMainExt = (EditTextPlzExt) mViewAddressMain.findViewById(R.id.edt_plz_sub);
        mEdtOrtMainExt = (EditTextOrtExt) mViewAddressMain.findViewById(R.id.edt_ort_sub);
    }

    private void initEvents() {
        if (getIntent().getBooleanExtra(AppConfigs.IS_FIRST_ACTIVATE, false)) {
            mIBtnBack.setVisibility(View.GONE);
            mIBtnMenu.setVisibility(View.GONE);
        } else {
            mIBtnBack.setVisibility(View.VISIBLE);
            mIBtnMenu.setVisibility(View.VISIBLE);

            mIBtnBack.setOnClickListener(this);
            mIBtnMenu.setOnClickListener(this);
        }

        mViewAddressSub1.findViewById(R.id.ibtn_remove).setVisibility(View.GONE);
        mViewAddressMain.findViewById(R.id.ibtn_remove).setVisibility(View.GONE);
        mViewAddressSub2.setVisibility(View.GONE);

        mViewAddressSub2.findViewById(R.id.ibtn_remove).setOnClickListener(this);
        mIBtnAdd.setOnClickListener(this);
        mBtnNext.setOnClickListener(this);

        /* TODO
        mEdtOrtZielExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mEdtOrtZielExt.showOrtListFull();
            }*/

        /* mViewAddressSub1 */
        mEdtStreetSub1.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub1.findViewById(R.id.tv_street_sub)));
        mEdtHouseNumberSub1.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub1.findViewById(R.id.tv_house_number_sub)));
        mEdtPlzSub1Ext.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub1.findViewById(R.id.tv_plz_sub)));
        mEdtOrtSub1Ext.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub1.findViewById(R.id.tv_ort_sub)));
        mEdtPlzSub1Ext.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_1) {
                    onPlzItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_1);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_1;
            }
        });
        mEdtOrtSub1Ext.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_1) {
                    onOrtItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_1);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_1;
            }
        });

        /* mViewAddressSub2 */
        mEdtStreetSub2.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub2.findViewById(R.id.tv_street_sub)));
        mEdtHouseNumberSub2.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub2.findViewById(R.id.tv_house_number_sub)));
        mEdtPlzSub2Ext.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub2.findViewById(R.id.tv_plz_sub)));
        mEdtOrtSub2Ext.addTextChangedListener(new ShowSubTextWatcher(mViewAddressSub2.findViewById(R.id.tv_ort_sub)));
        mEdtPlzSub2Ext.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_2) {
                    onPlzItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_2);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_2;
            }
        });
        mEdtOrtSub2Ext.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_SUB_2) {
                    onOrtItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_2);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_SUB_2;
            }
        });

        /* mViewAddressMain */
        mEdtStreetMain.addTextChangedListener(new ShowSubTextWatcher(mViewAddressMain.findViewById(R.id.tv_street_sub)));
        mEdtHouseNumberMain.addTextChangedListener(new ShowSubTextWatcher(mViewAddressMain.findViewById(R.id.tv_house_number_sub)));
        mEdtPlzMainExt.addTextChangedListener(new ShowSubTextWatcher(mViewAddressMain.findViewById(R.id.tv_plz_sub)));
        mEdtOrtMainExt.addTextChangedListener(new ShowSubTextWatcher(mViewAddressMain.findViewById(R.id.tv_ort_sub)));
        mEdtPlzMainExt.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_MAIN) {
                    onPlzItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_MAIN);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_MAIN;
            }
        });
        mEdtOrtMainExt.setOnItemClickListenerExt(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentType != AUTOCOMPLETETEXTVIEW_ITEM_TYPE.PLZ_MAIN) {
                    onOrtItemClick(AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_MAIN);
                }
                mCurrentType = AUTOCOMPLETETEXTVIEW_ITEM_TYPE.ORT_MAIN;
            }
        });

        mEdtEmail.addTextChangedListener(new ShowSubTextWatcher(mTvEmail));
        mEdtTel.addTextChangedListener(new ShowSubTextWatcher(mTvTel));
    }

    private void initData() {
        mTvTitle.setText(getResources().getString(R.string.personal_title));

        mBtnNext.setText(getResources().getString(R.string.save));
        if (getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false)) {
            mBtnNext.setText(getResources().getString(R.string.next_from_zusam));
        }

        mUser = DBApplication.getServiceFactory().getUserService().getUserInfo();
        if (mUser == null) {
            mUser = UserUtils.convertStringToUser(StorageUtils.getUserInfo(PersonalActivity.this));
        }
        if (mUser != null) {
            mEdtLastName.setText(mUser.getLastName());
            mEdtFirstName.setText(mUser.getFirstName());
            mTvPersonalNo.setText(mUser.getPersonalNo());
            mEdtEmail.setText(mUser.getEmail());
            mEdtTel.setText(mUser.getPhoneNumber());

            if (TextUtils.isEmpty(mUser.getEmployeeStatus())) {
                mSpnEmployeeStatus.setSelection(0);
            } else {
                int i = 0;
                for (String item : getResources().getStringArray(R.array.employee_status_array)) {
                    if (mUser.getEmployeeStatus().equals(item)) {
                        break;
                    }
                    i++;
                }
                if (i < getResources().getStringArray(R.array.employee_status_array).length) {
                    mSpnEmployeeStatus.setSelection(i);
                } else {
                    mSpnEmployeeStatus.setSelection(0);
                }
            }

            /* mViewAddressSub1 */
            if (mUser.getHomeAddressList() != null && mUser.getHomeAddressList().size() > 0) {
                mEdtStreetSub1.setText(mUser.getHomeAddressList().get(0).getStreet());
                mEdtHouseNumberSub1.setText(mUser.getHomeAddressList().get(0).getHomeNumber());
                mEdtPlzSub1Ext.setText(mUser.getHomeAddressList().get(0).getZip());
                mEdtOrtSub1Ext.setText(mUser.getHomeAddressList().get(0).getCity());
            }

            /* mViewAddressSub2 */
            if (mUser.getHomeAddressList() != null && mUser.getHomeAddressList().size() > 1) {
                onAddButtonClick();

                mEdtStreetSub2.setText(mUser.getHomeAddressList().get(1).getStreet());
                mEdtHouseNumberSub2.setText(mUser.getHomeAddressList().get(1).getHomeNumber());
                mEdtPlzSub2Ext.setText(mUser.getHomeAddressList().get(1).getZip());
                mEdtOrtSub2Ext.setText(mUser.getHomeAddressList().get(1).getCity());
            }

            /* mViewAddressMain */
            mEdtStreetMain.setText(mUser.getCompanyStreet());
            mEdtHouseNumberMain.setText(mUser.getCompanyNo());
            mEdtPlzMainExt.setText(mUser.getCompanyZip());
            mEdtOrtMainExt.setText(mUser.getCompanyCity());
        }
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra(AppConfigs.IS_FIRST_ACTIVATE, false)) {
            Intent intent;
            if (getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false)) {
                intent = new Intent(PersonalActivity.this, ZusamActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            } else {
                intent = new Intent(PersonalActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
            }
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibtn_back:
                onBackPressed();
                break;
            case R.id.ibtn_menu:
                showMenuDialog(false, false);
                break;
            case R.id.ibtn_remove:
                onRemoveButtonClick();
                break;
            case R.id.ibtn_add:
                onAddButtonClick();
                break;
            case R.id.btn_next:
                onNextButtonClick();
                break;
            default:
                break;
        }
    }

    private void onRemoveButtonClick() {
        mViewAddressSub2.setVisibility(View.GONE);
        // clear all data in Address Sub2
        mEdtStreetSub2.setText(null);
        mEdtHouseNumberSub2.setText(null);
        mEdtPlzSub2Ext.setText(null);
        mEdtOrtSub2Ext.setText(null);

        mIBtnAdd.setVisibility(View.VISIBLE);
        mTvAddMoreAddress.setVisibility(View.VISIBLE);
    }

    private void onAddButtonClick() {
        mViewAddressSub2.setVisibility(View.VISIBLE);
        // clear all data in Address Sub2
        mIBtnAdd.setVisibility(View.GONE);
        mTvAddMoreAddress.setVisibility(View.GONE);
    }

    private void onNextButtonClick() {
        hideSoftKeyboard();

        if (isValidate()) {
            if (!checkNetworkConnection()) {
                AlertUtils.showMessageAlert(PersonalActivity.this, getString(R.string.notice_no_internet));
            } else {
                boolean hasModify = hasModify();
                if (DBApplication.getServiceFactory().getTravelService().getTravel() != null
                        && !DBApplication.getServiceFactory().getTravelService().getTravel().isModify()
                        && hasModify) {

                    DBApplication.getServiceFactory().getTravelService().getTravel().setModify(true);

                    TravelDbHelper.save(DBApplication.getServiceFactory().getTravelService().getTravel());
                }
                updateUserInfo();
//                new UpdateUserInfoTask().execute();
            }
        }
    }

    private void onPlzItemClick(Enum type) {
        hideSoftKeyboard();

        EditTextOrtExt edtOrtExt;
        EditTextPlzExt edtPlzExt;
        View viewAddress;

        switch ((AUTOCOMPLETETEXTVIEW_ITEM_TYPE) type) {
            case PLZ_MAIN:
                edtOrtExt = mEdtOrtMainExt;
                edtPlzExt = mEdtPlzMainExt;
                viewAddress = mViewAddressMain;
                break;
            case PLZ_SUB_1:
                edtOrtExt = mEdtOrtSub1Ext;
                edtPlzExt = mEdtPlzSub1Ext;
                viewAddress = mViewAddressSub1;
                break;
            case PLZ_SUB_2:
                edtOrtExt = mEdtOrtSub2Ext;
                edtPlzExt = mEdtPlzSub2Ext;
                viewAddress = mViewAddressSub2;
                break;
            default:
                return;
        }

        edtOrtExt.removeTextChangedListener(new ShowSubTextWatcher(viewAddress.findViewById(R.id.tv_ort_sub)));
        edtOrtExt.requestFocus();

        List<CityItem> ortItemsList = TravelDbHelper.getAllCityFromZip(edtPlzExt.getText().toString().trim());
        if (ortItemsList == null || ortItemsList.size() == 0) {
            edtOrtExt.showOrtListFull();
        } else {
            ArrayList<String> newOrtList = new ArrayList<>();
            for (int i = 0; i < ortItemsList.size(); i++) {
                newOrtList.add(ortItemsList.get(i).getCity());
            }

            edtOrtExt.setText(newOrtList.get(0));
            edtOrtExt.showOrtListCustom(newOrtList);
            if (newOrtList.size() == 1) {
                edtOrtExt.dismissDropDown();
            }
        }

        edtOrtExt.setError(null);
        edtPlzExt.addTextChangedListener(new ShowSubTextWatcher(viewAddress.findViewById(R.id.tv_ort_sub)));
    }

    private void onOrtItemClick(Enum type) {
        hideSoftKeyboard();

        EditTextOrtExt edtOrtExt;
        EditTextPlzExt edtPlzExt;
        View viewAddress;

        switch ((AUTOCOMPLETETEXTVIEW_ITEM_TYPE) type) {
            case ORT_MAIN:
                edtOrtExt = mEdtOrtMainExt;
                edtPlzExt = mEdtPlzMainExt;
                viewAddress = mViewAddressMain;
                break;
            case ORT_SUB_1:
                edtOrtExt = mEdtOrtSub1Ext;
                edtPlzExt = mEdtPlzSub1Ext;
                viewAddress = mViewAddressSub1;
                break;
            case ORT_SUB_2:
                edtOrtExt = mEdtOrtSub2Ext;
                edtPlzExt = mEdtPlzSub2Ext;
                viewAddress = mViewAddressSub2;
                break;
            default:
                return;
        }

        edtPlzExt.removeTextChangedListener(new ShowSubTextWatcher(viewAddress.findViewById(R.id.tv_plz_sub)));
        edtPlzExt.requestFocus();

        List<CityItem> plzItemsList = TravelDbHelper.getAllZipFromCity(edtOrtExt.getText().toString().trim());
        if (plzItemsList == null || plzItemsList.size() == 0) {
            edtPlzExt.showPlzListFull();
        } else {
            ArrayList<String> newPlzList = new ArrayList<>();
            for (int i = 0; i < plzItemsList.size(); i++) {
                newPlzList.add(plzItemsList.get(i).getZip());
            }

            edtPlzExt.setText(newPlzList.get(0));
            edtPlzExt.showPlzListCustom(newPlzList);
            if (newPlzList.size() == 1) {
                edtPlzExt.dismissDropDown();
            }
        }

        edtPlzExt.setError(null);
        edtPlzExt.addTextChangedListener(new ShowSubTextWatcher(viewAddress.findViewById(R.id.tv_plz_sub)));
    }

    private boolean isValidate() {
        if (TextUtils.isEmpty(mEdtLastName.getText().toString().trim())) {
            mEdtLastName.setError(getResources().getString(R.string.empty_last_name_msg));
            mEdtLastName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtFirstName.getText().toString().trim())) {
            mEdtFirstName.setError(getResources().getString(R.string.empty_first_name_msg));
            mEdtFirstName.requestFocus();
            return false;
        }
        if (!ValidationUtils.isValidEmail(mEdtEmail.getText().toString().trim())) {
            mEdtEmail.setError(getResources().getString(R.string.empty_email_msg));
            mEdtEmail.requestFocus();
            return false;
        }
        /*if (TextUtils.isEmpty(mEdtTel.getText().toString().trim())) {
            mEdtTel.setError(getResources().getString(R.string.empty_phone_msg));
            mEdtTel.requestFocus();
            return false;
        }*/

        if (mSpnEmployeeStatus.getSelectedItemPosition() == 0) {
            Toast.makeText(PersonalActivity.this, getResources().getString(R.string.empty_employee_status_msg), Toast.LENGTH_LONG).show();
            return false;
        }

        /* mViewAddressSub1 */
        if (TextUtils.isEmpty(mEdtStreetSub1.getText().toString().trim())) {
            mEdtStreetSub1.setError(getResources().getString(R.string.empty_strabe_msg));
            mEdtStreetSub1.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtHouseNumberSub1.getText().toString().trim())) {
            mEdtHouseNumberSub1.setError(getResources().getString(R.string.empty_nr_msg));
            mEdtHouseNumberSub1.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtPlzSub1Ext.getText().toString().trim())) {
            mEdtPlzSub1Ext.setError(getResources().getString(R.string.empty_plz_msg));
            mEdtPlzSub1Ext.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtOrtSub1Ext.getText().toString().trim())) {
            mEdtOrtSub1Ext.setError(getResources().getString(R.string.empty_ort_msg));
            mEdtOrtSub1Ext.requestFocus();
            return false;
        }

        /* mViewAddressSub2 */
        if (mViewAddressSub2.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(mEdtStreetSub2.getText().toString().trim())) {
                mEdtStreetSub2.setError(getResources().getString(R.string.empty_strabe_msg));
                mEdtStreetSub2.requestFocus();
                return false;
            }
            if (TextUtils.isEmpty(mEdtHouseNumberSub2.getText().toString().trim())) {
                mEdtHouseNumberSub2.setError(getResources().getString(R.string.empty_nr_msg));
                mEdtHouseNumberSub2.requestFocus();
                return false;
            }
            if (TextUtils.isEmpty(mEdtPlzSub2Ext.getText().toString().trim())) {
                mEdtPlzSub2Ext.setError(getResources().getString(R.string.empty_plz_msg));
                mEdtPlzSub2Ext.requestFocus();
                return false;
            }
            if (TextUtils.isEmpty(mEdtOrtSub2Ext.getText().toString().trim())) {
                mEdtOrtSub2Ext.setError(getResources().getString(R.string.empty_ort_msg));
                mEdtOrtSub2Ext.requestFocus();
                return false;
            }
        }

        /* mViewAddressMain */
        if (TextUtils.isEmpty(mEdtStreetMain.getText().toString().trim())) {
            mEdtStreetMain.setError(getResources().getString(R.string.empty_strabe_msg));
            mEdtStreetMain.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtHouseNumberMain.getText().toString().trim())) {
            mEdtHouseNumberMain.setError(getResources().getString(R.string.empty_nr_msg));
            mEdtHouseNumberMain.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtPlzMainExt.getText().toString().trim())) {
            mEdtPlzMainExt.setError(getResources().getString(R.string.empty_plz_msg));
            mEdtPlzMainExt.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEdtOrtMainExt.getText().toString().trim())) {
            mEdtOrtMainExt.setError(getResources().getString(R.string.empty_ort_msg));
            mEdtOrtMainExt.requestFocus();
            return false;
        }

        return true;
    }

    private boolean hasModify() {
        if (mUser == null) {
            return true;
        }
        if (!TextUtils.equals(mUser.getLastName(), mEdtLastName.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getFirstName(), mEdtFirstName.getText().toString().trim())) {
            return true;
        }
//        if (!TextUtils.equals(mUser.getPersonalNo(), mTvPersonalNo.getText().toString().trim())) {
//            return true;
//        }
        if (!TextUtils.equals(mUser.getEmail(), mEdtEmail.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getPhoneNumber(), mEdtTel.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getEmployeeStatus(), mSpnEmployeeStatus.getSelectedItem().toString())) {
            return true;
        }

        if (mUser.getHomeAddressList() == null || mUser.getHomeAddressList().size() == 0) {
            return true;
        }
        /* mViewAddressSub1 */
        if (!TextUtils.equals(mUser.getHomeAddressList().get(0).getStreet(), mEdtStreetSub1.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getHomeAddressList().get(0).getHomeNumber(), mEdtHouseNumberSub1.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getHomeAddressList().get(0).getZip(), mEdtPlzSub1Ext.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getHomeAddressList().get(0).getCity(), mEdtOrtSub1Ext.getText().toString().trim())) {
            return true;
        }

        /* mViewAddressSub2 */
        if ((mUser.getHomeAddressList().size() > 1 && mViewAddressSub2.getVisibility() == View.GONE)
                || (mUser.getHomeAddressList().size() == 1 && mViewAddressSub2.getVisibility() != View.GONE)) {
            return true;
        } else if (mUser.getHomeAddressList().size() > 1 && mViewAddressSub2.getVisibility() != View.GONE) {
            if (!TextUtils.equals(mUser.getHomeAddressList().get(1).getStreet(), mEdtStreetSub2.getText().toString().trim())) {
                return true;
            }
            if (!TextUtils.equals(mUser.getHomeAddressList().get(1).getHomeNumber(), mEdtHouseNumberSub2.getText().toString().trim())) {
                return true;
            }
            if (!TextUtils.equals(mUser.getHomeAddressList().get(1).getZip(), mEdtPlzSub2Ext.getText().toString().trim())) {
                return true;
            }
            if (!TextUtils.equals(mUser.getHomeAddressList().get(1).getCity(), mEdtOrtSub2Ext.getText().toString().trim())) {
                return true;
            }
        }

        /* mViewAddressMain */
        if (!TextUtils.equals(mUser.getCompanyStreet(), mEdtStreetMain.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getCompanyNo(), mEdtHouseNumberMain.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getCompanyZip(), mEdtPlzMainExt.getText().toString().trim())) {
            return true;
        }
        if (!TextUtils.equals(mUser.getCompanyCity(), mEdtOrtMainExt.getText().toString().trim())) {
            return true;
        }

        return false;
    }

    private void getUserInfo() {
        showLoadingDialog(true);

        updateToken();
        DBApplication.getServiceFactory().getUserService().getUserInfo(new CallBack<User>() {

            @Override
            public void onSuccess(User result) {
                showLoadingDialog(false);

                StorageUtils.storageUserInfo(PersonalActivity.this, UserUtils.convertUserToString());

                initData();
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
//                handleErrorException(t);
                initData();
            }
        });
    }

    private void updateUserInfo() {
        showLoadingDialog(true);

        final User user = new User();
        user.setLastName(mEdtLastName.getText().toString().trim());
        user.setFirstName(mEdtFirstName.getText().toString().trim());
        String personalNo = mTvPersonalNo.getText().toString().trim();
        if (TextUtils.isEmpty(personalNo) || personalNo.equals("00000000")) {
            personalNo = mUser.getPersonalNo();
        }
        user.setPersonalNo(personalNo);
        user.setEmail(mEdtEmail.getText().toString().trim());
        user.setPhoneNumber(mEdtTel.getText().toString().trim());
        user.setEmployeeStatus(mSpnEmployeeStatus.getSelectedItem().toString());

        // For creating PDF file avoid user is lost.
        StorageUtils.storagePersonalNo(PersonalActivity.this, user.getPersonalNo());
        StorageUtils.storageEmployeeStatus(PersonalActivity.this, user.getEmployeeStatus());

        user.setCompanyStreet(mEdtStreetMain.getText().toString().trim());
        user.setCompanyNo(mEdtHouseNumberMain.getText().toString().trim());
        user.setCompanyZip(mEdtPlzMainExt.getText().toString().trim());
        user.setCompanyCity(mEdtOrtMainExt.getText().toString().trim());


        List<HomeAddress> homeAddressList = new ArrayList<>();

        HomeAddress homeAddress1 = new HomeAddress(mEdtStreetSub1.getText().toString().trim(),
                mEdtHouseNumberSub1.getText().toString().trim(),
                mEdtPlzSub1Ext.getText().toString().trim(),
                mEdtOrtSub1Ext.getText().toString().trim(), -1);
        homeAddressList.add(homeAddress1);

        if (mViewAddressSub2.getVisibility() != View.GONE) {
            HomeAddress homeAddress2 = new HomeAddress(mEdtStreetSub2.getText().toString().trim(),
                    mEdtHouseNumberSub2.getText().toString().trim(),
                    mEdtPlzSub2Ext.getText().toString().trim(),
                    mEdtOrtSub2Ext.getText().toString().trim(), -1);
            homeAddressList.add(homeAddress2);
        }

        user.setHomeAddressList(homeAddressList);

        updateToken();
        final boolean isFromZusam = getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false);
        DBApplication.getServiceFactory().getUserService().updateUserInfo(user, isFromZusam, new CallBack<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {

                StorageUtils.storageUserInfo(PersonalActivity.this, UserUtils.convertUserToString());

                Intent intent;
                if (isFromZusam) {
                    intent = new Intent(PersonalActivity.this, ZusamActivity.class);
                } else {
                    intent = new Intent(PersonalActivity.this, NoticeSavePersonalInfoSuccessActivity.class);
                    intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, getIntent().getBooleanExtra(AppConfigs.IS_FIRST_ACTIVATE, false));
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, getIntent().getBooleanExtra(AppConfigs.IS_FROM_ZUSAM, false));
                }
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();

                showLoadingDialog(false);
            }

            @Override
            public void onError(Throwable t) {
                showLoadingDialog(false);
                handleErrorException(t);
            }
        });
    }
}
