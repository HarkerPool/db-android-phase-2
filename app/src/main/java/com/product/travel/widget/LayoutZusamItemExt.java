package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.interfaces.ZusamListener;
import com.product.travel.models.ZusamItem;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class LayoutZusamItemExt extends LinearLayout {

    private TextView mTvZusamOverview, mTvZusamContent;
    private ImageView mIvZusamEdit;

    private Context mContext;

    private ZusamItem mZusamItem;
    private ZusamListener mDelegate;

    public LayoutZusamItemExt(Context context) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamItemExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamItemExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamItemExt(Context context, ZusamItem item, ZusamListener listener) {
        super(context);

        mContext = context;
        mZusamItem = item;
        mDelegate = listener;
        initView();
        initObjects();
        initEvents();
        initData();
    }

//    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void initView() {
        ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_zusam_item, this);
    }

    private void initObjects() {
        mTvZusamOverview = (TextView) findViewById(R.id.tv_zusam_overview);
        mTvZusamContent = (TextView) findViewById(R.id.tv_zusam_content);
        mIvZusamEdit = (ImageView) findViewById(R.id.iv_zusam_edit);
    }

    private void initEvents() {
        mIvZusamEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDelegate != null) {
                    mDelegate.onEditListener();
                }
            }
        });
    }

    private void initData() {
        mTvZusamOverview.setText(mZusamItem.getTitle());
        mTvZusamContent.setText(mZusamItem.getValue());
        if (mZusamItem.isHasEdit()) {
            mIvZusamEdit.setVisibility(View.VISIBLE);
        } else {
            mIvZusamEdit.setVisibility(View.GONE);
        }
    }
}
