package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.R;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.CityItem;
import com.product.travel.data.dao.TravelDbHelper;

/**
 * Created by HarkerPool on 5/18/16.
 */
public class EditTextPlzExt extends AutoCompleteTextView {

    Context mContext;
    private ArrayList<String> mPlzList;
    List<CityItem> mZipItemList;

    public EditTextPlzExt(Context context) {
        super(context);
        mContext = context;
        showPlzListFull();
    }

    public EditTextPlzExt(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        showPlzListFull();
    }

    public EditTextPlzExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        showPlzListFull();
    }

    public void showPlzListFull() {
        mPlzList = DBApplication.getServiceFactory().getCityServiceImpl().getAllZipDistinct();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, mPlzList);
        setThreshold(3);
        this.setAdapter(adapter);
    }

    public void showPlzListCustom(ArrayList<String> newList) {
//        mPlzList = newList;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, newList);
        setThreshold(3);
        this.setAdapter(adapter);
    }

    public void setOnItemClickListenerExt(AdapterView.OnItemClickListener listener) {
        setOnItemClickListener(listener);
    }

    private void getAllPlz() {
        mZipItemList = TravelDbHelper.getAllZipDistinct();

        for (int i = 0; i < mZipItemList.size(); i++) {
            mPlzList.add(mZipItemList.get(i).getZip());
        }
    }
}
