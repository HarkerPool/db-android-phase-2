package com.product.travel.widget;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by HarkerPool on 6/6/16.
 */
public class ShowSubTextWatcher implements TextWatcher {

    private View mView;


    public ShowSubTextWatcher(View view) {
        this.mView = view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s)) {
            mView.setVisibility(View.GONE);
        } else {
            mView.setVisibility(View.VISIBLE);
        }
    }
}
