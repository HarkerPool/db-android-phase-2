package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.R;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.CityItem;
import com.product.travel.data.dao.TravelDbHelper;

/**
 * Created by HarkerPool on 5/18/16.
 */
public class EditTextOrtExt extends AutoCompleteTextView {
    Context mContext;
    private ArrayList<String> mOrtList;
    List<CityItem> mCityItemList;

    public EditTextOrtExt(Context context) {
        super(context);
        mContext = context;
        showOrtListFull();
    }

    public EditTextOrtExt(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        showOrtListFull();
    }

    public EditTextOrtExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        showOrtListFull();
    }

    public void showOrtListFull() {
        mOrtList = DBApplication.getServiceFactory().getCityServiceImpl().getAllCityDistinct();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, mOrtList);
        setThreshold(3);
        this.setAdapter(adapter);
    }

    public void showOrtListCustom(ArrayList<String> newList) {
//        mOrtList = newList;
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, newList);
        setThreshold(3);
        this.setAdapter(adapter);
    }

    public void setOnItemClickListenerExt(AdapterView.OnItemClickListener listener) {
        setOnItemClickListener(listener);
    }

    private void getAllOrt() {
        mCityItemList = TravelDbHelper.getAllCityDistinct();

        for (int i = 0; i < mCityItemList.size(); i++) {
            mOrtList.add(mCityItemList.get(i).getCity());
        }
    }
}
