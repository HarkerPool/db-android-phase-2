package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class LayoutZusamSubTitleExt extends LinearLayout {

    private TextView mTvZusamSubTitle;

    private Context mContext;

    private String mTitle;

    public LayoutZusamSubTitleExt(Context context) {
        super(context);

        mContext = context;
        initView();
        initObjects();
    }

    public LayoutZusamSubTitleExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        initView();
        initObjects();
    }

    public LayoutZusamSubTitleExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        initView();
        initObjects();
    }

    public LayoutZusamSubTitleExt(Context context, String title) {
        super(context);

        mContext = context;
        mTitle = title;
        initView();
        initObjects();
        initData();
    }

//    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void initView() {
        ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_zusam_subtitle, this);
    }

    private void initObjects() {
        mTvZusamSubTitle = (TextView) findViewById(R.id.tv_zusam_subtitle);
    }

    private void initData() {
        mTvZusamSubTitle.setText(mTitle);
    }
}
