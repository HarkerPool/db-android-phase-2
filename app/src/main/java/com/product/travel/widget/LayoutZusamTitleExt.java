package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.interfaces.ZusamListener;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class LayoutZusamTitleExt extends LinearLayout {

    private TextView mTvZusamTitle;
    private ImageView mIvZusamEdit;

    private Context mContext;

    private String mTitle;
    private ZusamListener mDelegate;

    public LayoutZusamTitleExt(Context context) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamTitleExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamTitleExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZusamTitleExt(Context context, String title, ZusamListener listener) {
        super(context);

        mContext = context;
        mTitle = title;
        mDelegate = listener;
        initView();
        initObjects();
        initEvents();
        initData();
    }

//    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void initView() {
        ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_zusam_title, this);
    }

    private void initObjects() {
        mTvZusamTitle = (TextView) findViewById(R.id.tv_zusam_title);
        mIvZusamEdit = (ImageView) findViewById(R.id.iv_zusam_edit);
    }

    private void initEvents() {
        mIvZusamEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDelegate != null) {
                    mDelegate.onEditListener();
                }
            }
        });
    }

    private void initData() {
        mTvZusamTitle.setText(mTitle);
    }
}
