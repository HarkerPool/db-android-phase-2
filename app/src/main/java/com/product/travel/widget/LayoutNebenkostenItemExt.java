package com.product.travel.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.NebenkostenItemListener;
import com.product.travel.models.NebenkostenItem;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class LayoutNebenkostenItemExt extends LinearLayout {

    private ImageButton mIBtnRemove, mIBtnCamera;
    private EditText mEdtBezeichnung, mEdtBetrag;
    private TextView mTvCameraSuggestion;

    private Context mContext;
    private int mCurrentIndex;
    private double mBetragValue;

    private NebenkostenItem mNebenkostenItem;
    private NebenkostenItemListener mDelegate;

    public LayoutNebenkostenItemExt(Context context) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutNebenkostenItemExt(Context context, NebenkostenItem item, int index, NebenkostenItemListener listener) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();

        mNebenkostenItem = item;
        mDelegate = listener;

        mCurrentIndex = index;
        initData();
    }

//    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void initView() {
        ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_nebenkosten_item, this);
    }

    private void initObjects() {
        mIBtnRemove = (ImageButton) findViewById(R.id.ibtn_remove);
        mIBtnCamera = (ImageButton) findViewById(R.id.ibtn_camera);
        mEdtBezeichnung = (EditText) findViewById(R.id.edt_bezeichnung);
        mEdtBetrag = (EditText) findViewById(R.id.edt_betrag);
        mTvCameraSuggestion = (TextView) findViewById(R.id.tv_camera_suggestion);
    }

    private void initEvents() {
        mIBtnRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDelegate.onRemoveListener(mCurrentIndex);
            }
        });

        mIBtnCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNebenkostenItem == null || TextUtils.isEmpty(mNebenkostenItem.getImage())) {
                    mDelegate.onCaptureListener(mCurrentIndex);
                } else {
                    mDelegate.onReviewListener(mCurrentIndex);
                }
            }
        });

        mEdtBezeichnung.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mDelegate.onBezeichnungChangeListener(mCurrentIndex, mEdtBezeichnung.getText().toString().trim());
            }
        });

        mEdtBetrag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String oriText = s.toString();
                String comma = "";
                if (!TextUtils.isEmpty(oriText)) {
                    if (oriText.charAt(oriText.length() - 1) == ',') {
                        comma = ",";
                    } else if (oriText.length() >= 3
                            && oriText.charAt(oriText.length() - 2) == ','
                            && oriText.charAt(oriText.length() - 1) == '0') {
                        comma = ",0";
                    } else if (oriText.length() >= 4
                            && oriText.charAt(oriText.length() - 3) == ','
                            && oriText.charAt(oriText.length() - 1) == '0') {

                        if (oriText.charAt(oriText.length() - 2) == '0') {
                            comma = ",00";
                        } else {
                            comma = "0";
                        }
                    }
                }
                String text = getCommaSeparatedString(s);
                mEdtBetrag.removeTextChangedListener(this);
                s.clear();
                s.append(text);
                s.append(comma);
                mEdtBetrag.addTextChangedListener(this);
                mDelegate.onBetragChangeListener(mCurrentIndex, mBetragValue);
            }
        });
    }

    private void initData() {
        mIBtnRemove.setVisibility(View.VISIBLE);
        if (mCurrentIndex == 0) {
            mIBtnRemove.setVisibility(View.GONE);
        }

        mIBtnCamera.setImageResource(R.drawable.camera);

        if (mNebenkostenItem != null) {
            mEdtBezeichnung.setText(mNebenkostenItem.getBezeichnung());
            if (mNebenkostenItem.getBetrag() > 0) {
                mEdtBetrag.setText(String.format(Locale.GERMAN, "%.2f", mNebenkostenItem.getBetrag()));
            }

            if (!TextUtils.isEmpty(mNebenkostenItem.getImage())) {
                try {
                    mTvCameraSuggestion.setVisibility(GONE);

                    Bitmap bitmap = DBApplication.getBitmapCache().getBitmap(mNebenkostenItem.getImage());
                    if (bitmap != null) {
                        mIBtnCamera.setImageBitmap(bitmap);
                    } else {
                        Picasso.with(mContext)
                                .load(AppConfigs.SERVER_URL + mNebenkostenItem.getImage())
                                .placeholder(R.drawable.loading_icon)
                                .error(R.drawable.error)
                                .resize(200, 150)
                                .centerCrop()
                                .into(mIBtnCamera);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private String getCommaSeparatedString(Editable editable) {

        String newText = editable.toString();
        int iComTmp = newText.indexOf(",");
        String decimal = "";

        // integer
        String integer = newText.substring(0, iComTmp > 0 ? iComTmp : newText.length());
        integer = integer.replace(".", "");

        // decimal
        if (iComTmp > 0) {
            if (iComTmp != newText.length() - 1) {
                decimal = newText.substring(iComTmp, iComTmp + 3 > newText
                        .length() ? newText.length() : iComTmp + 3);
            } else {
                decimal = "";
            }
            decimal = decimal.replace(",", ".");
        }

        String format = newText;
        if (!TextUtils.isEmpty(format)) {
            decimal = decimal.replace("..", "");
            if ((integer.equals("") || integer.equals(",")) && decimal.equals("")) {
                integer = "0";
            }
            if (decimal.length() > 0 && decimal.charAt(decimal.length() - 1) == '.') {
                decimal = decimal.substring(0, decimal.length() - 1);
            }
            Locale locale = Locale.GERMAN;
            NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
            numberFormat.setMaximumFractionDigits(2);
            numberFormat.setMaximumIntegerDigits(9);

            mBetragValue = Double.parseDouble(integer + decimal);
            format = numberFormat.format(mBetragValue);
        } else {
            mBetragValue = 0;
        }

        return format;
    }
}
