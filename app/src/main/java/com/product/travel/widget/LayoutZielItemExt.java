package com.product.travel.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.product.travel.R;
import com.product.travel.interfaces.ZielItemListener;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class LayoutZielItemExt extends LinearLayout {

    private ImageButton mIBtnRemove, mIBtnOrtStarZiel, mIBtnStreetStarZiel;
    private EditTextOrtExt mEdtOrtZielExt;
    private EditText mEdtStreetZiel;
    private TextView mTvOrtZiel, mTvStreetZiel;
    private Spinner mSpnOrtStar, mSpnStreetStar;

    private Context mContext;
    private int mCurrentIndex;

    private ZielItemListener mDelegate;
    private boolean mIsOrtStarClick, mIsOrtStreetClick;

    public LayoutZielItemExt(Context context) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZielItemExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZielItemExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        initView();
        initObjects();
        initEvents();
    }

    public LayoutZielItemExt(Context context, String city, String street, int index, ZielItemListener listener) {
        super(context);

        mContext = context;
        initView();
        initObjects();
        initEvents();
        initStars();

        mCurrentIndex = index;
        mDelegate = listener;
        mEdtOrtZielExt.setText(city);
        mEdtStreetZiel.setText(street);

        initData();
    }

//    public LayoutNebenkostenItemExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

    private void initView() {
        ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_ziel_item, this);
    }

    private void initObjects() {
        mIBtnRemove = (ImageButton) findViewById(R.id.ibtn_remove);
        mIBtnOrtStarZiel = (ImageButton) findViewById(R.id.ibtn_star_ort_ziel);
        mIBtnStreetStarZiel = (ImageButton) findViewById(R.id.ibtn_star_street_ziel);
        mEdtOrtZielExt = (EditTextOrtExt) findViewById(R.id.edt_ort_ziel);
        mEdtStreetZiel = (EditText) findViewById(R.id.edt_street_ziel);
        mTvOrtZiel = (TextView) findViewById(R.id.tv_ort_ziel);
        mTvStreetZiel = (TextView) findViewById(R.id.tv_street_ziel);
        mSpnOrtStar = (Spinner) findViewById(R.id.spn_ort_star);
        mSpnStreetStar = (Spinner) findViewById(R.id.spn_street_star);
    }

    private void initEvents() {
        mIBtnRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDelegate.onRemoveListener(mCurrentIndex);
            }
        });

        mEdtOrtZielExt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mEdtOrtZielExt.showOrtListFull();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    mTvOrtZiel.setVisibility(View.GONE);
                } else {
                    mTvOrtZiel.setVisibility(View.VISIBLE);
                }

                if (mDelegate != null) {
                    mDelegate.onTextChangeListener(mCurrentIndex, mEdtOrtZielExt.getText().toString().trim(), mEdtStreetZiel.getText().toString().trim());
                }
            }
        });

        mEdtStreetZiel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    mTvStreetZiel.setVisibility(View.GONE);
                } else {
                    mTvStreetZiel.setVisibility(View.VISIBLE);
                }

                if (mDelegate != null) {
                    mDelegate.onTextChangeListener(mCurrentIndex, mEdtOrtZielExt.getText().toString().trim(), mEdtStreetZiel.getText().toString().trim());
                }
            }
        });

        mIBtnOrtStarZiel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpnOrtStar.performClick();
            }
        });

        mIBtnStreetStarZiel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpnStreetStar.performClick();
            }
        });

        mSpnOrtStar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsOrtStarClick) {
                    mEdtOrtZielExt.setText(mSpnOrtStar.getSelectedItem().toString());
                } else {
                    mIsOrtStarClick = true;
                }

                if (TextUtils.isEmpty(mEdtOrtZielExt.getText().toString())) {
                    mTvOrtZiel.setVisibility(View.GONE);
                } else {
                    mTvOrtZiel.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpnStreetStar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mIsOrtStreetClick) {
                    mEdtStreetZiel.setText(mSpnStreetStar.getSelectedItem().toString());
                } else {
                    mIsOrtStreetClick = true;
                }

                if (TextUtils.isEmpty(mEdtStreetZiel.getText().toString())) {
                    mTvStreetZiel.setVisibility(View.GONE);
                } else {
                    mTvStreetZiel.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initStars() {
        String city = StorageUtils.getZielCityStarsValue(mContext);
        List<String> cityList = new ArrayList<>(Arrays.asList(city.split(";;")));
        if (cityList.size() != 0 && cityList.get(0) != "") {
            cityList.add(0, "");
            ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(mContext, R.layout.item_spinner, cityList);
            cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpnOrtStar.setAdapter(cityAdapter);
        }

        String street = StorageUtils.getZielStreetStarsValue(mContext);
        List<String> streetList = new ArrayList<>(Arrays.asList(street.split(";;")));
        if (streetList.size() != 0 && streetList.get(0) != "") {
            streetList.add(0, "");
            ArrayAdapter<String> streetAdapter = new ArrayAdapter<>(mContext, R.layout.item_spinner, streetList);
            streetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpnStreetStar.setAdapter(streetAdapter);
        }
    }

    private void initData() {
        mIBtnRemove.setVisibility(View.VISIBLE);
        if (mCurrentIndex == 0) {
            mIBtnRemove.setVisibility(View.GONE);
        }

        mIsOrtStarClick = false;
        mIsOrtStreetClick = false;

        mSpnOrtStar.setSelection(0);
        mSpnStreetStar.setSelection(0);
    }
}
