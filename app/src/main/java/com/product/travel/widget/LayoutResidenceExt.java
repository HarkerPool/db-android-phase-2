package com.product.travel.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.product.travel.R;

/**
 * Created by HarkerPool on 6/6/16.
 */
public class LayoutResidenceExt extends LinearLayout {

    private View mView;
    private Context mContext;
    TextView mTvStreetSub, mTvHouseNumberSub, mTvPlzSub, mTvOrtSub;
    EditText mEdtStreetSub, mEdtHouseNumberSub;
    EditTextPlzExt mEdtPlzSubExt;
    EditTextOrtExt mEdtOrtSubExt;

    public LayoutResidenceExt(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.layout_residence, this);
        mContext = context;
//        inflateLayout();
        initObjects();
        initEvents();
    }

    public LayoutResidenceExt(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.layout_residence, this);
//        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mView = layoutInflater.inflate(R.layout.layout_residence, this, true);
//        inflateLayout();
        initObjects();
        initEvents();
    }

    public LayoutResidenceExt(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        LayoutInflater.from(context).inflate(R.layout.layout_residence, this);
//        inflateLayout();
        initObjects();
        initEvents();
    }

//    public LayoutResidenceExt(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

//    @Override
//    public void onFinishInflate() {
//        super.onFinishInflate();
//        addView(mView, 0);
//    }

    private void inflateLayout() {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = layoutInflater.inflate(R.layout.layout_residence, this, true);
    }

    private void initObjects() {
        mTvStreetSub = (TextView) findViewById(R.id.tv_street_sub);
        mTvHouseNumberSub = (TextView) findViewById(R.id.tv_house_number_sub);
        mTvPlzSub = (TextView) findViewById(R.id.tv_plz_sub);
        mTvOrtSub = (TextView) findViewById(R.id.tv_ort_sub);
        mEdtStreetSub = (EditText) findViewById(R.id.edt_street_sub);
        mEdtHouseNumberSub = (EditText) findViewById(R.id.edt_house_number_sub);
        mEdtPlzSubExt = (EditTextPlzExt) findViewById(R.id.edt_plz_sub);
        mEdtOrtSubExt = (EditTextOrtExt) findViewById(R.id.edt_ort_sub);
    }

    private void initEvents() {
        mEdtStreetSub.addTextChangedListener(new ShowSubTextWatcher(mTvStreetSub));
        mEdtHouseNumberSub.addTextChangedListener(new ShowSubTextWatcher(mTvHouseNumberSub));
        mEdtPlzSubExt.addTextChangedListener(new ShowSubTextWatcher(mTvPlzSub));
        mEdtOrtSubExt.addTextChangedListener(new ShowSubTextWatcher(mTvOrtSub));
    }
}
