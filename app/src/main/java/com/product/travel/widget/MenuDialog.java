package com.product.travel.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.activities.EinstellungenActivity;
import com.product.travel.activities.HifleActivity;
import com.product.travel.activities.HomeActivity;
import com.product.travel.activities.ImpressumActivity;
import com.product.travel.activities.NoticeSaveTravelWithDraftActivity;
import com.product.travel.activities.OpenInternalWebViewActivity;
import com.product.travel.activities.PersonalActivity;
import com.product.travel.activities.WelcomeActivity;
import com.product.travel.commons.AppConfigs;

/**
 * Created by HarkerPool on 6/17/16.
 */
public class MenuDialog extends Dialog implements View.OnClickListener {

    private Context mContext;
    private Button mBtnHome, mBtnPersonal, mBtnHilfe, mBtnMaps, mBtnReiseauskunft, mBtnEinstellungen, mBtnImpressum, mBtnAbbruchBeenden;
    private boolean mIsNeedToSave, mIsFromEditScreen;

    public MenuDialog(Context context, boolean isNeedToSave, boolean isFromEditScreen) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);

        mContext = context;
        mIsNeedToSave = isNeedToSave;
        mIsFromEditScreen = isFromEditScreen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_menu);

        final Window window = getWindow();
        window.setTitle(null);
        window.getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        initObjects();
        initEvents();
        initData();
    }

    private void initObjects() {
        mBtnHome = (Button) findViewById(R.id.btn_home);
        mBtnPersonal = (Button) findViewById(R.id.btn_personal);
        mBtnHilfe = (Button) findViewById(R.id.btn_hilfe);
        mBtnMaps = (Button) findViewById(R.id.btn_maps);
        mBtnReiseauskunft = (Button) findViewById(R.id.btn_reiseauskunft);
        mBtnEinstellungen = (Button) findViewById(R.id.btn_einstellungen);
//        mBtnEpostselect = (Button) findViewById(R.id.btn_epostselect);
        mBtnImpressum = (Button) findViewById(R.id.btn_impressum);
        mBtnAbbruchBeenden = (Button) findViewById(R.id.btn_abbruch_beenden);
    }

    private void initEvents() {
        mBtnHome.setOnClickListener(this);
        mBtnPersonal.setOnClickListener(this);
        mBtnHilfe.setOnClickListener(this);
        mBtnMaps.setOnClickListener(this);
        mBtnReiseauskunft.setOnClickListener(this);
        mBtnEinstellungen.setOnClickListener(this);
//        mBtnEpostselect.setOnClickListener(this);
        mBtnImpressum.setOnClickListener(this);
        mBtnAbbruchBeenden.setOnClickListener(this);
    }

    private void initData() {
        if (mIsFromEditScreen) {
            mBtnAbbruchBeenden.setText(mContext.getResources().getString(R.string.abbruch));
        } else {
            mBtnAbbruchBeenden.setText(mContext.getResources().getString(R.string.beenden));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        dismiss();
        return super.onTouchEvent(event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home:
                onButtonClick(AppConfigs.MENU_ITEM.HOME);
                break;
            case R.id.btn_personal:
                onButtonClick(AppConfigs.MENU_ITEM.PERSONAL);
                break;
            case R.id.btn_hilfe:
                onButtonClick(AppConfigs.MENU_ITEM.HILFE);
                break;
            case R.id.btn_maps:
                onButtonClick(AppConfigs.MENU_ITEM.MAPS);
                break;
            case R.id.btn_reiseauskunft:
                onButtonClick(AppConfigs.MENU_ITEM.REISEAUSKUNFT);
                break;
            case R.id.btn_einstellungen:
                onButtonClick(AppConfigs.MENU_ITEM.EINSTELLUNGEN);
                break;
            /*case R.id.btn_epostselect:
                onButtonClick(AppConfigs.MENU_ITEM.EPOSTSELECT);
                break;*/
            case R.id.btn_impressum:
                onButtonClick(AppConfigs.MENU_ITEM.IMPRESSUM);
                break;
            case R.id.btn_abbruch_beenden:
                onButtonClick(AppConfigs.MENU_ITEM.ABBRUCH_BEENDEN);
                break;
            default:
                break;
        }
    }

    private void onButtonClick(Enum menu_item) {

        hide(); // Should be hide before do action.
        
        Intent intent;
        
        switch ((AppConfigs.MENU_ITEM) menu_item) {
            case HOME:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HOME);
                } else {
                    intent = new Intent(mContext, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
                break;
            case PERSONAL:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.PERSONAL);
                } else {
                    intent = new Intent(mContext, PersonalActivity.class);
                    intent.putExtra(AppConfigs.IS_FIRST_ACTIVATE, false);
                    intent.putExtra(AppConfigs.IS_FROM_ZUSAM, false);
                }
                break;
            case HILFE:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.HILFE);
                } else {
                    intent = new Intent(mContext, HifleActivity.class);
                }
                break;
            case MAPS:
                Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                intent.setPackage("com.google.android.apps.maps");
                if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                    mContext.startActivity(intent);
                }
                return;
            case REISEAUSKUNFT:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.REISEAUSKUNFT);
                } else {
                    intent = new Intent(mContext, OpenInternalWebViewActivity.class);
                    intent.putExtra(AppConfigs.TITLE_URL, mContext.getResources().getString(R.string.db_reiseauskunft));
                    intent.putExtra(AppConfigs.INTERNAL_URL_KEY, AppConfigs.REISEAUSKUNFT_URL);
                }
                break;
            case EINSTELLUNGEN:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.EINSTELLUNGEN);
                } else {
                    intent = new Intent(mContext, EinstellungenActivity.class);
                }
                break;
            /*case EPOSTSELECT:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.EPOSTSELECT);
                } else {
                    intent = new Intent(mContext, OpenInternalWebViewActivity.class);
                    intent.putExtra(AppConfigs.TITLE_URL, mContext.getResources().getString(R.string.activation_epostselect));
                    intent.putExtra(AppConfigs.INTERNAL_URL_KEY, AppConfigs.E_POST_SELECT_URL);
                }
                break;*/
            case IMPRESSUM:
                if (mIsNeedToSave) {
                    intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
                    intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.IMPRESSUM);
                } else {
                    intent = new Intent(mContext, ImpressumActivity.class);
                }
                break;
            case ABBRUCH_BEENDEN:
                onCancelButtonClick();
                return;
            default:
                return;
        }

        mContext.startActivity(intent);
        ((Activity)mContext).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        ((Activity)mContext).finish();
    }

    private void onCancelButtonClick() {
        Intent intent;
        if (mIsNeedToSave) {
            intent = new Intent(mContext, NoticeSaveTravelWithDraftActivity.class);
            intent.putExtra(String.valueOf(AppConfigs.MENU_ITEM.MENU_ITEM_KEY), AppConfigs.MENU_ITEM.ABBRUCH_BEENDEN);
            mContext.startActivity(intent);
            ((Activity)mContext).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            ((Activity)mContext).finish();
        } else if (mIsFromEditScreen) {
            intent = new Intent(mContext, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mContext.startActivity(intent);
            ((Activity)mContext).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            ((Activity)mContext).finish();
        } else {
            intent = new Intent(mContext, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra(AppConfigs.EXIT_APP, true);
            mContext.startActivity(intent);
            ((Activity)mContext).overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.fade_out);
            ((Activity)mContext).finish();
        }
    }
}
