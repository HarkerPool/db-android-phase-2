package com.product.travel.services;

import com.product.travel.data.api.TravelRepositoryImpl;
import com.product.travel.data.api.UserRepositoryImpl;
import com.product.travel.interfaces.ServiceFactory;
import com.product.travel.interfaces.TravelService;
import com.product.travel.interfaces.UserService;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class ServiceFactoryImpl implements ServiceFactory {

    private UserService mUserService;
    private TravelService mTravelService;
    private CitiesServiceImpl mCityServiceImpl;

    public ServiceFactoryImpl() {
        mUserService = new UserServiceImpl(new UserRepositoryImpl());
        mTravelService = new TravelServiceImpl(new TravelRepositoryImpl());
        mCityServiceImpl = new CitiesServiceImpl();
    }

    @Override
    public UserService getUserService() {
        return mUserService;
    }

    @Override
    public TravelService getTravelService() {
        return mTravelService;
    }

    @Override
    public CitiesServiceImpl getCityServiceImpl() {
        return mCityServiceImpl;
    }
}
