package com.product.travel.services;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.data.dao.NachrichtenDbHelper;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.CallBack;
import com.product.travel.interfaces.TravelRepository;
import com.product.travel.interfaces.TravelService;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.models.Travel;
import com.product.travel.models.TravelStatus;
import com.product.travel.utils.StorageUtils;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class TravelServiceImpl implements TravelService {

    private Travel mTravel;
    private TravelRepository mTravelRepository;

    public TravelServiceImpl(TravelRepository travelRepository) {
        mTravelRepository = travelRepository;
    }

    @Override
    public void setTravel(Travel travel) {
        if (mTravel == null) {
            mTravel = new Travel();
        }
        mTravel.updateTravel(travel);
    }

    @Override
    public void setTravel(TravelDbItem travelDbItem) {
        if (mTravel == null) {
            mTravel = new Travel();
        }
        mTravel.setTravel(travelDbItem);
    }

    @Override
    public Travel getTravel() {
        return mTravel;
    }

    @Override
    public Travel restoreTravel(Context context) {
        String travelId = StorageUtils.getTravelId(context);
        mTravel = null;

        TravelDbItem travelDbItem = TravelDbHelper.getTravelById(travelId);
        if (travelDbItem != null) {
            travelDbItem.setTravel(true);
            travelDbItem.setDraft(true);
        }
        setTravel(travelDbItem);

        return mTravel;
    }

    @Override
    public void resetTravel() {
        mTravel = null;
    }

    @Override
    public void createTravel(final Travel travel, final CallBack<Travel> callBack) {
        if (travel == null) {
            callBack.onError(new Exception("Travel is null."));
            return;
        }
        /*if (travel.isCreatePdf() && TextUtils.isEmpty(travel.getPdfName())) {
            callBack.onError(new Exception("Pdf name is null."));
            return;
        }*/

        try {
            mTravelRepository.createTravel(travel, new CallBack<Travel>() {
                @Override
                public void onSuccess(Travel result) {
                    if (mTravel == null) {
                        mTravel = new Travel();
                        mTravel = travel;
                    }

//                    mTravel.setDraft(false); // TODO - must be set in other place when is_uploading = false
//                    mTravel.setFavorite(false); // can not set it, if set at here, Vorlagen will never be sent to srv.
                    mTravel.setModify(false);
                    mTravel.setUploaded(true);

                    TravelDbHelper.save(mTravel);

                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void uploadTravelImage(String imageName, String travelId, boolean isLast, final CallBack<Boolean> callBack) {
        if (TextUtils.isEmpty(imageName) || TextUtils.isEmpty(travelId)) {
            callBack.onError(new Exception("Image name or travel_id is null."));
            return;
        }
        try {
            mTravelRepository.uploadTravelImage(imageName, travelId, isLast, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void updateTravelStatus(final CallBack<List<TravelStatus>> callBack) {
        try {
            mTravelRepository.updateTravelStatus(new CallBack<List<TravelStatus>>() {
                @Override
                public void onSuccess(List<TravelStatus> result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void getTravelById(String travelId, final CallBack<Travel> callBack) {
        if (TextUtils.isEmpty(travelId)) {
            callBack.onError(new Exception("travel_id is null."));
            return;
        }
        try {
            mTravelRepository.getTravelById(travelId, new CallBack<Travel>() {
                @Override
                public void onSuccess(Travel result) {
                    TravelDbHelper.save(result);

                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void deleteTravel(String travelId, final CallBack<Boolean> callBack) {
        if (TextUtils.isEmpty(travelId)) {
            callBack.onError(new Exception("travel_id is null."));
            return;
        }
        try {
            mTravelRepository.deleteTravel(travelId, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void deleteImage(String imageName, final CallBack<Boolean> callBack) {
        if (TextUtils.isEmpty(imageName)) {
            callBack.onError(new Exception("image_name is null."));
            return;
        }

        try {
            mTravelRepository.deleteImage(imageName, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void getEmail(final CallBack<ArrayList<NachrichtenItem>> callBack) {
        try {
            mTravelRepository.getEmail(new CallBack<ArrayList<NachrichtenItem>>() {
                @Override
                public void onSuccess(ArrayList<NachrichtenItem> result) {
                    if (result != null) {
                        for (int i = 0; i < result.size(); i++) {
                            NachrichtenDbHelper.save(result.get(i));
                        }
                    }
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void getEmailByTravel(String travelId, final CallBack<NachrichtenItem> callBack) {
        try {
            mTravelRepository.getEmailByTravel(travelId, new CallBack<NachrichtenItem>() {
                @Override
                public void onSuccess(NachrichtenItem result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void deleteEmail(ArrayList<Integer> email_id, final CallBack<Boolean> callBack) {
        // if email_id list = null --> delete all email

        try {
            mTravelRepository.deleteEmail(email_id, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void updateEmail(int email_id, final CallBack<Boolean> callBack) {
        try {
            mTravelRepository.updateEmail(email_id, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }
}
