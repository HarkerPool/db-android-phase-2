package com.product.travel.services;

import android.text.TextUtils;

import com.product.travel.interfaces.UserRepository;
import com.product.travel.params.ActivationParams;
import com.product.travel.params.ChangePasswordParams;
import com.product.travel.interfaces.CallBack;
import com.product.travel.interfaces.UserService;
import com.product.travel.models.User;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class UserServiceImpl implements UserService {

    private User mUser;
    private UserRepository mUserRepository;

    public UserServiceImpl(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public void updateUserInfo(User user) {
        if (mUser == null) {
            mUser = new User();
        }
        mUser.updateUserInfo(user);
    }

    @Override
    public User getUserInfo() {
        return mUser;
    }

    @Override
    public void activation(ActivationParams params, final CallBack<User> callBack) {
        if (params == null) {
            callBack.onError(new Exception("params is null."));
            return;
        }

        try {
            mUserRepository.activation(params, new CallBack<User>() {
                @Override
                public void onSuccess(User result) {
                    updateUserInfo(result);
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void getUserInfo(final CallBack<User> callBack) {

        try {
            mUserRepository.getUserInfo(new CallBack<User>() {
                @Override
                public void onSuccess(User result) {
                    updateUserInfo(result);
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void updateUserInfo(User user, final CallBack<Boolean> callBack) {
        updateUserInfo(user, false, callBack);
    }

    @Override
    public void updateUserInfo(final User user, boolean isFromZusam, final CallBack<Boolean> callBack) {
        if (user == null) {
            callBack.onError(new Exception("user is null."));
            return;
        }

        if (TextUtils.isEmpty(user.getPersonalNo()) || user.getPersonalNo().equals("00000000")) {
            callBack.onError(new Exception("Personal No. is null."));
            return;
        }

        try {
            mUserRepository.updateUserInfo(user, isFromZusam, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    updateUserInfo(user);
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void createPassword(String password, final CallBack<Boolean> callBack) {
        if (TextUtils.isEmpty(password)) {
            callBack.onError(new Exception("Password is null."));
            return;
        }

        try {
            mUserRepository.createPassword(password, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void resetPassword(String code, final CallBack<String> callBack) {
        /*if (TextUtils.isEmpty(code)) {
            callBack.onError(new Exception("Registration Code is null."));
            return;
        }*/

        try {
            mUserRepository.resetPassword(code, new CallBack<String>() {
                @Override
                public void onSuccess(String result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void changePassword(ChangePasswordParams params, final CallBack<Boolean> callBack) {
        if (params == null || TextUtils.isEmpty(params.getOldPassword()) || TextUtils.isEmpty(params.getNewPassword())) {
            callBack.onError(new Exception("params are null."));
            return;
        }

        try {
            mUserRepository.changePassword(params, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void deletePassword(String password, final CallBack<Boolean> callBack) {
        if (TextUtils.isEmpty(password)) {
            callBack.onError(new Exception("Password is null."));
            return;
        }

        try {
            mUserRepository.deletePassword(password, new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void deleteUser(final CallBack<Boolean> callBack) {
        try {
            mUserRepository.deleteUser(new CallBack<Boolean>() {
                @Override
                public void onSuccess(Boolean result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }
    }

    @Override
    public void needToUpdateNewBuild(final CallBack<String> callBack) {
        try {
            mUserRepository.needToUpdateNewBuild(new CallBack<String>() {
                @Override
                public void onSuccess(String result) {
                    callBack.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

            callBack.onError(e);
        }
    }
}
