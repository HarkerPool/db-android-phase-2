package com.product.travel.services;

import java.util.ArrayList;
import java.util.List;

import com.product.travel.data.dao.CityItem;
import com.product.travel.data.dao.TravelDbHelper;

/**
 * Created by HarkerPool on 6/27/16.
 */
public class CitiesServiceImpl {

    private List<CityItem> mCityItemList, mZipItemList;
    private ArrayList<String> mOrtList, mPlzList;

    public CitiesServiceImpl() {
        getAllCityAndZipDistinct();

        /*Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                if (mOrtList.size() == 0) {
                    mCityItemList = TravelDbHelper.getAllCityDistinct();

                    for (int i = 0; i < mCityItemList.size(); i++) {
                        mOrtList.add(mCityItemList.get(i).getCity());
                    }
                }
                if (mPlzList.size() == 0) {
                    mZipItemList = TravelDbHelper.getAllZipDistinct();

                    for (int i = 0; i < mZipItemList.size(); i++) {
                        mPlzList.add(mZipItemList.get(i).getZip());
                    }
                }

                return null;
            }
        });*/
    }

    private void getAllCityAndZipDistinct() {
        if (mOrtList == null) {
            mOrtList = new ArrayList<>();
        }
        if (mPlzList == null) {
            mPlzList = new ArrayList<>();
        }

        if (mOrtList.size() == 0) {
            mCityItemList = TravelDbHelper.getAllCityDistinct();

            for (int i = 0; i < mCityItemList.size(); i++) {
                mOrtList.add(mCityItemList.get(i).getCity());
            }
        }
        if (mPlzList.size() == 0) {
            mZipItemList = TravelDbHelper.getAllZipDistinct();

            for (int i = 0; i < mZipItemList.size(); i++) {
                mPlzList.add(mZipItemList.get(i).getZip());
            }
        }
    }

    public ArrayList<String> getAllCityDistinct() {
        if (mOrtList == null || mOrtList.size() == 0) {
            getAllCityAndZipDistinct();
        }

        return mOrtList;
    }

    public ArrayList<String> getAllZipDistinct() {
        if (mPlzList == null || mPlzList.size() == 0) {
            getAllCityAndZipDistinct();
        }

        return mPlzList;
    }
}
