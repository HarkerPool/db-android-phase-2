package com.product.travel.data.api;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.product.travel.commons.AppConfigs;
import com.product.travel.interfaces.UserRepository;
import com.product.travel.models.HomeAddress;
import com.product.travel.params.ActivationParams;
import com.product.travel.params.ChangePasswordParams;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.User;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class UserRepositoryImpl extends BaseRepositoryImpl implements UserRepository {

    private static final String TAG = UserRepositoryImpl.class.getSimpleName();

    @Override
    public void activation(ActivationParams params, final CallBack<User> callBack) throws Exception {

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("code", params.getActivationCode());
            jsonRequest.put("device_token", params.getDeviceToken());
            jsonRequest.put("device_type", 1);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.ACTIVATION_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "Activation response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            JSONObject data = response.getJSONObject("data");

                            String token = getStringValue(data, "token");
                            JSONArray jArrAllCountry = null;
                            if (data.has("all_country")) {
                                jArrAllCountry = data.getJSONArray("all_country");
                            }
                            LinkedHashMap<Integer, String> hashAllCountry = new LinkedHashMap<>();
                            int key;
                            String value;
                            JSONObject jsonObject;

                            if (jArrAllCountry != null) {
                                for (int i = 0; i < jArrAllCountry.length(); i++) {
                                    jsonObject = jArrAllCountry.getJSONObject(i);

                                    key = getIntValue(jsonObject, "country_id");
                                    value = getStringValue(jsonObject, "country_name");

                                    hashAllCountry.put(key, value);
                                }
                            }

                            User user = new User(token, hashAllCountry);
                            callBack.onSuccess(user);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        });

//        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void getUserInfo(final CallBack<User> callBack) throws Exception {

        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.GET_USER_INFO_URL, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "getUserInfo response message:\n" + response.toString());
                        try {

                            boolean result = getBooleanValue(response, "result");

                            if (!result) {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            List<HomeAddress> homeAddressList = new ArrayList<>();

                            JSONObject data = response.getJSONObject("data");
                            if (data.has("home_address")) {
                                JSONArray jArrHomeAddress = data.getJSONArray("home_address");

                                JSONObject jsonObject;
                                for (int i = 0; i < jArrHomeAddress.length(); i++) {
                                    jsonObject = jArrHomeAddress.getJSONObject(i);

                                    HomeAddress element = new HomeAddress(getStringValue(jsonObject, "street"), getStringValue(jsonObject, "no"),
                                            getStringValue(jsonObject, "zip"), getStringValue(jsonObject, "city"), getIntValue(jsonObject, "country_id"));

                                    homeAddressList.add(element);
                                }
                            }

                            User user = new User();

                            user.setId(getIntValue(data, "id"));
                            user.setLastName(getStringValue(data, "last_name"));
                            user.setFirstName(getStringValue(data, "first_name"));
                            user.setPersonalNo(getStringValue(data, "personal_no"));
                            user.setEmail(getStringValue(data, "email"));
                            user.setPhoneNumber(getStringValue(data, "phone"));
                            user.setEmployeeStatus(getStringValue(data, "employee_status"));
                            user.setCompanyStreet(getStringValue(data, "company_street"));
                            user.setCompanyNo(getStringValue(data, "company_no"));
                            user.setCompanyZip(getStringValue(data, "company_zip"));
                            user.setCompanyCity(getStringValue(data, "company_city"));
//                            user.setCountryId(getIntValue(data, "country_id")); // TODO
                            user.setCountryId(-1);
                            user.setHomeAddressList(homeAddressList);

                            callBack.onSuccess(user);

                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void updateUserInfo(User user, final CallBack<Boolean> callBack) throws Exception {
        updateUserInfo(user, false, callBack);
    }

    @Override
    public void updateUserInfo(User user, boolean isFromZusam, final CallBack<Boolean> callBack) throws Exception {

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("last_name", user.getLastName());
            jsonRequest.put("first_name", user.getFirstName());
            jsonRequest.put("personal_no", user.getPersonalNo());
            jsonRequest.put("email", user.getEmail());
            jsonRequest.put("phone", user.getPhoneNumber() == null ? "" : user.getPhoneNumber());
            jsonRequest.put("employee_status", user.getEmployeeStatus());
            jsonRequest.put("company_street", user.getCompanyStreet());
            jsonRequest.put("company_no", user.getCompanyNo());
            jsonRequest.put("company_zip", user.getCompanyZip());
            jsonRequest.put("company_city", user.getCompanyCity());
            jsonRequest.put("country_id", -1);
            jsonRequest.put("is_from_zusam", isFromZusam);
            jsonRequest.put("is_delete", false);

            JSONArray jArrHomeAddress = new JSONArray();
            JSONObject jHomeAddress1 = new JSONObject();
            jHomeAddress1.put("street", user.getHomeAddressList().get(0).getStreet());
            jHomeAddress1.put("no", user.getHomeAddressList().get(0).getHomeNumber());
            jHomeAddress1.put("zip", user.getHomeAddressList().get(0).getZip());
            jHomeAddress1.put("city", user.getHomeAddressList().get(0).getCity());
            jHomeAddress1.put("country_id", user.getHomeAddressList().get(0).getCountryId());
            jArrHomeAddress.put(jHomeAddress1);

            if (user.getHomeAddressList().size() > 1) {
                JSONObject jHomeAddress2 = new JSONObject();
                jHomeAddress2.put("street", user.getHomeAddressList().get(1).getStreet());
                jHomeAddress2.put("no", user.getHomeAddressList().get(1).getHomeNumber());
                jHomeAddress2.put("zip", user.getHomeAddressList().get(1).getZip());
                jHomeAddress2.put("city", user.getHomeAddressList().get(1).getCity());
                jHomeAddress2.put("country_id", user.getHomeAddressList().get(1).getCountryId());
                jArrHomeAddress.put(jHomeAddress2);
            }

            jsonRequest.put("home_address", jArrHomeAddress);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.UPDATE_USER_INFO_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "updateUserInfo response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }
                            callBack.onSuccess(true);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(new Exception("Error when updating personal info."));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void createPassword(String password, final CallBack<Boolean> callBack) throws Exception {

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("password", password);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.CREATE_PASSWORD_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "createPassword response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }
                            callBack.onSuccess(true);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void resetPassword(final String code, final CallBack<String> callBack) throws Exception {
        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.RESET_PASSWORD_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "resetPassword response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            String newPassword = getStringValue(response, "data");
                            if (!result || TextUtils.isEmpty(newPassword)) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            callBack.onSuccess(newPassword);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader(code, true);
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void changePassword(ChangePasswordParams params, final CallBack<Boolean> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("password_old", params.getOldPassword());
            jsonRequest.put("password_new", params.getNewPassword());
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.CHANGE_PASSWORD_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "changePassword response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }
                            callBack.onSuccess(true);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void deletePassword(String password, final CallBack<Boolean> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("password_old", password);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.DELETE_PASSWORD_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "deletePassword response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }
                            callBack.onSuccess(true);
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void deleteUser(final CallBack<Boolean> callBack) throws Exception {
        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.DELETE_USER_URL, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "deleteUser response message:\n" + response.toString());
                        try {

                            boolean result = getBooleanValue(response, "result");

                            if (result) {
                                callBack.onSuccess(true);
                            } else {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                            }
                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void needToUpdateNewBuild(final CallBack<String> callBack) throws Exception {
        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.GET_VERSION_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "needToUpdateNewBuild response message:\n" + response.toString());
                try {
                    boolean result = getBooleanValue(response, "result");
                    if (!result) {
                        String message = response.getString("message");
                        callBack.onError(new Exception(message));
                        return;
                    }

                    JSONObject data = response.getJSONObject("data");
                    if (data.has("version")) {
                        String version = getStringValue(data, "version");

                        callBack.onSuccess(version);
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    callBack.onError(e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();

                headers.put("deviceType", "1");

                return headers;
            }
        };

        VolleyManager.getInstance().addToRequestQueue(jr);
    }
}
