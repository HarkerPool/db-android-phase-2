package com.product.travel.data.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Task;
import com.product.travel.R;
import com.product.travel.activities.ActivationActivity;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.CallBack;
import com.product.travel.models.Travel;
import com.product.travel.models.TravelStatus;
import com.product.travel.models.User;
import com.product.travel.params.ActivationParams;
import com.product.travel.utils.DownloaderUtils;
import com.product.travel.utils.ImageUtils;
import com.product.travel.utils.StorageUtils;
import com.product.travel.utils.UserUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HarkerPool on 6/3/16.
 */
public class ThreadInBackground {

    public static void activation(final Context context) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                String activationCode = StorageUtils.getActivationCode(context);
                if (TextUtils.isEmpty(activationCode)) {
                    Intent intent = new Intent(context, ActivationActivity.class);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    ((Activity) context).finish();
                } else {
                    String deviceToken;
                    try {
                        InstanceID instanceID = InstanceID.getInstance(context);
                        deviceToken = instanceID.getToken(context.getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        deviceToken = "CouldNotGetAToken";
                    }
                    Log.i("Activation", "Device token:" + deviceToken);

                    final ActivationParams params = new ActivationParams(activationCode, deviceToken);

                    DBApplication.getServiceFactory().getUserService().activation(params, new CallBack<User>() {
                        @Override
                        public void onSuccess(User result) {
                            StorageUtils.storageToken(context, result.getToken());

                            getUserInfo(context);
                            updateTravelStatus(); // Synchronize data when opening app.
                            uploadAllVorlagen(); // re-send vorlagen
                        }

                        @Override
                        public void onError(Throwable t) {
                            if (t != null && !TextUtils.isEmpty(t.getMessage()) && (t.getMessage().contains("Session time out") || t.getMessage().contains("Missing token") || t.getMessage().contains("Missing token in headers"))) {
                                activation(context);
                            }
                        }
                    });
                }

                return null;
            }
        });
    }

    public static void getUserInfo(final Context context) {
        DBApplication.getServiceFactory().getUserService().getUserInfo(new CallBack<User>() {
            @Override
            public void onSuccess(User result) {
                StorageUtils.storageUserInfo(context, UserUtils.convertUserToString());

                // For creating PDF file avoid user is lost.
                StorageUtils.storagePersonalNo(context, result.getPersonalNo());
                StorageUtils.storageEmployeeStatus(context, result.getEmployeeStatus());
            }

            @Override
            public void onError(Throwable t) {
                if (t != null && !TextUtils.isEmpty(t.getMessage()) && (t.getMessage().contains("Session time out") || t.getMessage().contains("Missing token") || t.getMessage().contains("Missing token in headers"))) {
                    activation(context);
                }
            }
        });
    }

    private static void createVorlagen(TravelDbItem travelDbItem) {
        final Travel travelFavorite = new Travel();

        try {
            Thread.sleep(2000);

            if (travelDbItem != null) {
                travelFavorite.setTravelId(travelDbItem.getTravelId());
                travelFavorite.setTravel(travelDbItem.isTravel());
                travelFavorite.setReason(travelDbItem.getReason());
                travelFavorite.setZiel(travelDbItem.getZiel());
                travelFavorite.setBeginCity(travelDbItem.getBeginCity());
                travelFavorite.setVorlagen(true); // set to vorlagen
                travelFavorite.setFirstPdfDate("");

                Thread.sleep(1000);

                Task.callInBackground(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        DBApplication.getServiceFactory().getTravelService().createTravel(travelFavorite, new CallBack<Travel>() {
                            @Override
                            public void onSuccess(Travel result) {
                                Log.i("createVorlagen", result == null ? "" : result.toString());
                            }

                            @Override
                            public void onError(Throwable t) {
                                Log.i("createVorlagen", t == null ? "Error!" : t.getMessage());
                            }
                        });

                        return null;
                    }
                });
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void uploadAllVorlagen() {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                List<TravelDbItem> vorlagenItemList = TravelDbHelper.getAllVorlagenNotSend();

                for (int i = 0; i < vorlagenItemList.size(); i++) {
                    createVorlagen(vorlagenItemList.get(i));
                }

                return null;
            }
        });
    }

    private static void handleTravelStatus(List<TravelStatus> travelStatusList) {
        if (travelStatusList == null || travelStatusList.size() == 0) {
            TravelDbHelper.deleteAllTravelHadSent();
            return;
        }

        List<TravelDbItem> travelInfoList = TravelDbHelper.getAllTravelInfo();
        if (travelInfoList.size() == 0) {
            for (int i = 0; i < travelStatusList.size(); i++) {
                getTravelById(travelStatusList.get(i).getTravelId());
            }
        } else {
            int i, j;
            for (i = 0; i < travelStatusList.size(); i++) {
                for (j = 0; j < travelInfoList.size(); j++) {
                    if (travelStatusList.get(i).getTravelId().equals(travelInfoList.get(j).getTravelId())) {
                        if (!travelInfoList.get(j).isDraft()) {
                            TravelDbHelper.deleteTravelById(travelStatusList.get(i).getTravelId());
                            getTravelById(travelStatusList.get(i).getTravelId());
                        }
                        break;
                    }
                }
                if (j == travelInfoList.size()) {
                    getTravelById(travelStatusList.get(i).getTravelId());
                }
            }

            for (i = 0; i < travelInfoList.size(); i++) {
                for (j = 0; j < travelStatusList.size(); j++) {
                    if (travelInfoList.get(i).getTravelId().equals(travelStatusList.get(j).getTravelId())) {
                        break;
                    }
                }
                if (j == travelStatusList.size() && !travelInfoList.get(i).isDraft() && travelInfoList.get(i).isUploaded() && !travelInfoList.get(i).isUploading()) {
                    // deleteTravelById
                    TravelDbHelper.deleteTravelById(travelInfoList.get(i).getTravelId());
                }
            }
        }
    }

    public static void updateTravelStatus() {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DBApplication.getServiceFactory().getTravelService().updateTravelStatus(new CallBack<List<TravelStatus>>() {
                    @Override
                    public void onSuccess(List<TravelStatus> result) {
                        Log.i("updateTravelStatus", result == null ? "" : result.toString());

                        handleTravelStatus(result);
                    }

                    @Override
                    public void onError(Throwable t) {
//                        Log.e("updateTravelStatus", (t == null && t.getMessage() == null) ? "" : t.getMessage());
                    }
                });

                return null;
            }
        });
    }

    private static void getTravelById(final String travelId) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DBApplication.getServiceFactory().getTravelService().getTravelById(travelId, new CallBack<Travel>() {
                    @Override
                    public void onSuccess(final Travel result) {
                        Log.i("getTravelById", "travel_id " + (result == null ? "" : result.getTravelId()));
                        if (result != null) {
                            Task.callInBackground(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    DownloaderUtils.downloadPdf(result.getTravelId(), result.getPdfName(), result.getPdfMd5());

                                    return null;
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e("getTravelById", t == null ? "" : t.toString());
                    }
                });

                return null;
            }
        });
    }

    public static void deleteTravel(final Context context, final String travelId) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DBApplication.getServiceFactory().getTravelService().deleteTravel(travelId, new CallBack<Boolean>() {
                    @Override
                    public void onSuccess(Boolean result) {
                        Log.i("deleteTravel", "successful!");

                        StorageUtils.storageDeletedTravelIdList(context, travelId, true);
                        TravelDbHelper.deleteTravelById(travelId);
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e("deleteTravel", "Error " + (t == null ? "" : t.getMessage()));

                        StorageUtils.storageDeletedTravelIdList(context, travelId, false);
                        TravelDbHelper.deleteTravelById(travelId);
                    }
                });

                return null;
            }
        });
    }

    public static void syncDeleteTravels(Context context) {
        String value = StorageUtils.getDeletedTravelIdList(context);
        if (!TextUtils.isEmpty(value)) {
            ArrayList<String> deletedTravelIdList = new ArrayList<>(Arrays.asList(value.split(";;")));

            for (int i = 0; i < deletedTravelIdList.size(); i++) {
                deleteTravel(context, deletedTravelIdList.get(i));
            }
        }
    }

    public static void updateEmail(final int email_id) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                DBApplication.getServiceFactory().getTravelService().updateEmail(email_id, new CallBack<Boolean>() {
                    @Override
                    public void onSuccess(Boolean result) {
                        Log.i("updateEmail", "id "+ email_id + " successful!");
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.e("updateEmail", "id "+ email_id + " with error: " + (t == null ? "" : t.getMessage()));
                    }
                });

                return null;
            }
        });
    }

    public static void downloadImagesHandler(final Context context) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                List<TravelDbItem> travelInfoList = TravelDbHelper.getAllTravelInfo();

                if (travelInfoList != null && travelInfoList.size() > 0) {
                    for (int i = 0; i < travelInfoList.size(); i++) {
                        downloadImages(context, travelInfoList.get(i));
                    }
                }

                return null;
            }
        });
    }

    private static void downloadImages(Context context, TravelDbItem travel) {
        String imageName;
        String url = AppConfigs.PDF_URL + travel.getTravelId() + "/";
        ArrayList<String> imagesList = getImagesList(context, travel);

        for (int i = 0; i < imagesList.size(); i++) {
            imageName = imagesList.get(i);

            String path = context.getApplicationInfo().dataDir;
            File folder = new File(path, "images");
            folder.mkdirs();

            File imageFile = new File(folder, imageName);

            DownloaderUtils.downloadFile(url + imageName, imageFile);
        }
    }

    private static ArrayList<String> getImagesList(Context context, TravelDbItem travel) {
        ArrayList<String> imagesList = new ArrayList<>();

        if (!TextUtils.isEmpty(travel.getUberImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getUberImages());
                JSONArray jsonArray = jsonObject.getJSONArray("uber_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!TextUtils.isEmpty(travel.getTaxiImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getTaxiImages());
                JSONArray jsonArray = jsonObject.getJSONArray("taxi_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getFahrtkostenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getFahrtkostenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("fahrtkosten_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getMietwagenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getMietwagenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("mietwagen_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getKraftstoffImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getKraftstoffImages());
                JSONArray jsonArray = jsonObject.getJSONArray("kraftstoff_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getParkenImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getParkenImages());
                JSONArray jsonArray = jsonObject.getJSONArray("parken_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getTagungsImages())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getTagungsImages());
                JSONArray jsonArray = jsonObject.getJSONArray("tagungs_images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.getString(i).contains("uploads")) {
                        String[] parts = jsonArray.getString(i).split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!TextUtils.isEmpty(travel.getNeben())) {
            try {
                JSONObject jsonObject = new JSONObject(travel.getNeben());
                JSONArray jsonArray = jsonObject.getJSONArray("neben");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItem = jsonArray.getJSONObject(i);

                    if (jsonItem.getString("image").contains("uploads")) {
                        String[] parts = jsonItem.getString("image").split("/");
                        if (!ImageUtils.hasImageExist(context, parts[parts.length - 1])) {
                            imagesList.add(parts[parts.length - 1]);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return imagesList;
    }
}
