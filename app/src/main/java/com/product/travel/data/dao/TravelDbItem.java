package com.product.travel.data.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

/**
 * Created by HarkerPool on 6/3/16.
 */
@Table(name = "Travels")
public class TravelDbItem extends Model {

    @Column(name = "travel_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String travelId;

    @Column(name = "is_travel")
    private boolean isTravel;

    @Column(name = "reason")
    private String reason;

    @Column(name = "is_beamter")
    private boolean isBeamter;

    @Column(name = "ziel")
    private String ziel; // jsonObject
    /* ""ziel"": [
              {
                      ""city"": String,
                      ""street"": String
            },
            ...
        ]*/

    @Column(name = "create_time")
    private String createTime;

    @Column(name = "first_pdf_date")
    private String firstPdfDate;

    @Column(name = "begin_city")
    private String beginCity;

    @Column(name = "end_city")
    private String endCity;

    @Column(name = "begin_date")
    private Date beginDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "advance_money")
    private double advanceMoney;

    /* uber */
    @Column(name = "radio_uber")
    private Boolean radioUber;

    @Column(name = "radio_verp")
    private Boolean radioVerp;

    @Column(name = "radio_beleg")
    private Boolean radioBeleg;

    @Column(name = "inklusive_fruhstuck")
    private int inklusiveFruhstuck;

    @Column(name = "ohne_fruhstuck")
    private int ohneFruhstuck;

    @Column(name = "ohne_rechnung")
    private int ohneRechnung;

    @Column(name = "unentgeltlich")
    private int unentgeltlich;

    @Column(name = "images")
    private String uberImages; // jsonObject

    @Column(name = "verpflegung")
    private String verpflegung; // jsonObject
     /* ""verpflegung"": [
              {
                    ""date"": ""DD.MM.YYYY"",
                    ""is_breakfast"": boolean,
                    ""is_lunch"": boolean,
                    ""is_dinner"": boolean
              }
              ...
         ], */

    /* Beleg */
    @Column(name = "privater")
    private int privater;

    @Column(name = "taxi")
    private String taxiImages; // jsonObject

    @Column(name = "fahrtkosten")
    private String fahrtkostenImages; // jsonObject

    @Column(name = "mietwagen")
    private String mietwagenImages; // jsonObject

    @Column(name = "kraftstoff")
    private String kraftstoffImages; // jsonObject

    @Column(name = "parken")
    private String parkenImages; // jsonObject

    @Column(name = "tagungs")
    private String tagungsImages; // jsonObject

    @Column(name = "neben")
    private String neben; // jsonObject
    /*""neben"": [
    {
        ""bezeichnung"": String,
            ""betrag"": float,
        ""image"": String // null-able
    },
    ...
    ] */

    @Column(name = "comment")
    private String comment;

    @Column(name = "is_favorite")
    private boolean isFavorite;

    @Column(name = "is_vorlagen")
    private boolean isVorlagen;

    @Column(name = "pdf_name")
    private String pdfName;

    @Column(name = "is_draft")
    private boolean isDraft; // true - Entwurfe, false - Antrage

    @Column(name = "is_uploading")
    private boolean isUploading; // true - sending images or trying to send travel but not success, false - sent all success

    @Column(name = "is_modify")
    private boolean isModify; // edit from Antrage - need to create pdf.

    @Column(name = "status")
    private int status; // status of travel

    @Column(name = "message")
    private String message; // default message or comment from Archive server

    @Column(name = "is_edit")
    private boolean isEdit; // using locally. Use it when press back button or go to menu or checking before send a travel to the server.

    @Column(name = "is_uploaded")
    private boolean isUploaded; // true - sent successful to server, false - sent failed to server, retry when re-open app.

    @Column(name = "travel_from")
    private int travelFrom; // 1 - Entwurfe, 2 - Reisen in/from Antrage, 3 - Vorschuss in/from Antrage

    @Column(name = "pdf_md5")
    private String pdfMd5;

    @Column(name = "body_1")
    private String body1;

    @Column(name = "body_2")
    private String body2;

    @Column(name = "missing_title_1")
    private String missingTitle1;

    @Column(name = "missing_title_2")
    private String missingTitle2;

    @Column(name = "missing_title_3")
    private String missingTitle3;

    @Column(name = "missing_detail_1")
    private String missingDetail1;

    @Column(name = "missing_detail_2")
    private String missingDetail2;

    @Column(name = "missing_detail_3")
    private String missingDetail3;

    /* Constructor */
    public TravelDbItem() {
        super();
    }

    /* Getter & Setter */
    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public boolean isTravel() {
        return isTravel;
    }

    public void setTravel(boolean travel) {
        isTravel = travel;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isBeamter() {
        return isBeamter;
    }

    public void setBeamter(boolean beamter) {
        isBeamter = beamter;
    }

    public String getZiel() {
        return ziel;
    }

    public void setZiel(String ziel) {
        this.ziel = ziel;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getFirstPdfDate() {
        return firstPdfDate;
    }

    public void setFirstPdfDate(String firstPdfDate) {
        this.firstPdfDate = firstPdfDate;
    }

    public String getBeginCity() {
        return beginCity;
    }

    public void setBeginCity(String beginCity) {
        this.beginCity = beginCity;
    }

    public String getEndCity() {
        return endCity;
    }

    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getAdvanceMoney() {
        return advanceMoney;
    }

    public void setAdvanceMoney(double advanceMoney) {
        this.advanceMoney = advanceMoney;
    }

    public Boolean getRadioUber() {
        return radioUber;
    }

    public void setRadioUber(Boolean radioUber) {
        this.radioUber = radioUber;
    }

    public Boolean getRadioVerp() {
        return radioVerp;
    }

    public void setRadioVerp(Boolean radioVerp) {
        this.radioVerp = radioVerp;
    }

    public Boolean getRadioBeleg() {
        return radioBeleg;
    }

    public void setRadioBeleg(Boolean radioBeleg) {
        this.radioBeleg = radioBeleg;
    }

    public int getInklusiveFruhstuck() {
        return inklusiveFruhstuck;
    }

    public void setInklusiveFruhstuck(int inklusiveFruhstuck) {
        this.inklusiveFruhstuck = inklusiveFruhstuck;
    }

    public int getOhneFruhstuck() {
        return ohneFruhstuck;
    }

    public void setOhneFruhstuck(int ohneFruhstuck) {
        this.ohneFruhstuck = ohneFruhstuck;
    }

    public int getOhneRechnung() {
        return ohneRechnung;
    }

    public void setOhneRechnung(int ohneRechnung) {
        this.ohneRechnung = ohneRechnung;
    }

    public int getUnentgeltlich() {
        return unentgeltlich;
    }

    public void setUnentgeltlich(int unentgeltlich) {
        this.unentgeltlich = unentgeltlich;
    }

    public String getUberImages() {
        return uberImages;
    }

    public void setUberImages(String uberImages) {
        this.uberImages = uberImages;
    }

    public String getVerpflegung() {
        return verpflegung;
    }

    public void setVerpflegung(String verpflegung) {
        this.verpflegung = verpflegung;
    }

    public int getPrivater() {
        return privater;
    }

    public void setPrivater(int privater) {
        this.privater = privater;
    }

    public String getTaxiImages() {
        return taxiImages;
    }

    public void setTaxiImages(String taxiImages) {
        this.taxiImages = taxiImages;
    }

    public String getFahrtkostenImages() {
        return fahrtkostenImages;
    }

    public void setFahrtkostenImages(String fahrtkostenImages) {
        this.fahrtkostenImages = fahrtkostenImages;
    }

    public String getMietwagenImages() {
        return mietwagenImages;
    }

    public void setMietwagenImages(String mietwagenImages) {
        this.mietwagenImages = mietwagenImages;
    }

    public String getKraftstoffImages() {
        return kraftstoffImages;
    }

    public void setKraftstoffImages(String kraftstoffImages) {
        this.kraftstoffImages = kraftstoffImages;
    }

    public String getParkenImages() {
        return parkenImages;
    }

    public void setParkenImages(String parkenImages) {
        this.parkenImages = parkenImages;
    }

    public String getTagungsImages() {
        return tagungsImages;
    }

    public void setTagungsImages(String tagungsImages) {
        this.tagungsImages = tagungsImages;
    }

    public String getNeben() {
        return neben;
    }

    public void setNeben(String neben) {
        this.neben = neben;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isVorlagen() {
        return isVorlagen;
    }

    public void setVorlagen(boolean vorlagen) {
        isVorlagen = vorlagen;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public boolean isDraft() {
        return isDraft;
    }

    public void setDraft(boolean draft) {
        isDraft = draft;
    }

    public boolean isUploading() {
        return isUploading;
    }

    public void setUploading(boolean uploading) {
        isUploading = uploading;
    }

    public boolean isModify() {
        return isModify;
    }

    public void setModify(boolean modify) {
        isModify = modify;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public int getTravelFrom() {
        return travelFrom;
    }

    public void setTravelFrom(int travelFrom) {
        this.travelFrom = travelFrom;
    }

    public String getPdfMd5() {
        return pdfMd5;
    }

    public void setPdfMd5(String pdfMd5) {
        this.pdfMd5 = pdfMd5;
    }

    public String getBody1() {
        return body1;
    }

    public void setBody1(String body1) {
        this.body1 = body1;
    }

    public String getBody2() {
        return body2;
    }

    public void setBody2(String body2) {
        this.body2 = body2;
    }

    public String getMissingTitle1() {
        return missingTitle1;
    }

    public void setMissingTitle1(String missingTitle1) {
        this.missingTitle1 = missingTitle1;
    }

    public String getMissingTitle2() {
        return missingTitle2;
    }

    public void setMissingTitle2(String missingTitle2) {
        this.missingTitle2 = missingTitle2;
    }

    public String getMissingTitle3() {
        return missingTitle3;
    }

    public void setMissingTitle3(String missingTitle3) {
        this.missingTitle3 = missingTitle3;
    }

    public String getMissingDetail1() {
        return missingDetail1;
    }

    public void setMissingDetail1(String missingDetail1) {
        this.missingDetail1 = missingDetail1;
    }

    public String getMissingDetail2() {
        return missingDetail2;
    }

    public void setMissingDetail2(String missingDetail2) {
        this.missingDetail2 = missingDetail2;
    }

    public String getMissingDetail3() {
        return missingDetail3;
    }

    public void setMissingDetail3(String missingDetail3) {
        this.missingDetail3 = missingDetail3;
    }
}
