package com.product.travel.data.api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by HarkerPool on 6/5/16.
 */
public class MultipartRequest extends Request {
    private static final String TAG = MultipartRequest.class.getName();

    private Map<String, String> mHeaders;
    private Response.Listener mListener;
    private HttpEntity mHttpEntity;

    public MultipartRequest(int method, String url, File file, String mimeType, Map<String, String> headers,
                            Response.Listener listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);

        mHeaders = headers;
        mListener = listener;

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("image", file);
//        builder.addBinaryBody("image", file, ContentType.create(mimeType), file.getName());
//        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//        builder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));

        mHttpEntity = builder.build();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders == null ? super.getHeaders() : mHeaders;
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException ex) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }

        return bos.toByteArray();
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        String json;

        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, String.format("Encoding problem parsing API response. NetworkResponse:%s", response.toString()), e);

            return Response.error(new ParseError(e));
        }

        try {
            return Response.success((Object) json, HttpHeaderParser.parseCacheHeaders(response));
        } catch (JsonSyntaxException e) {
            Log.e(TAG, String.format("Couldn't API parse JSON response. NetworkResponse:%s", response.toString()), e);
            Log.e(TAG, String.format("Couldn't API parse JSON response. Json dump: %s", json));

            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Object response) {
        mListener.onResponse(response);
    }
}
