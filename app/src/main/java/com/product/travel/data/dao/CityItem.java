package com.product.travel.data.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by HarkerPool on 6/20/16.
 */
@Table(name = "Cities")
public class CityItem extends Model {

    @Column(name = "city")
    private String city;

    @Column(name = "zip")
    private String zip;

    public CityItem() {
        super();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
