package com.product.travel.data.dao;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import com.product.travel.models.Travel;

import bolts.Task;

/**
 * Created by HarkerPool on 6/9/16.
 */
public class TravelDbHelper {

    /* Travel Item */

    public static void save(final Travel travel) {
        if (travel == null || TextUtils.isEmpty(travel.getTravelId())) {
            return;
        }

        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                try {
                    ActiveAndroid.beginTransaction();

                    TravelDbItem dbItem = new TravelDbItem();

                    dbItem.setTravelId(travel.getTravelId());
                    dbItem.setTravel(travel.isTravel());
                    if (!TextUtils.isEmpty(travel.getReason())) {
                        dbItem.setReason(travel.getReason());
                    }
                    dbItem.setBeamter(travel.isBeamter());
                    if (!TextUtils.isEmpty(travel.getZiel())) {
                        dbItem.setZiel(travel.getZiel());
                    }
                    if (!TextUtils.isEmpty(travel.getCreateTime())) {
                        dbItem.setCreateTime(travel.getCreateTime());
                    }
                    if (travel.getFirstPdfDate() != null) {
                        dbItem.setFirstPdfDate(travel.getFirstPdfDate());
                    }
                    if (!TextUtils.isEmpty(travel.getBeginCity())) {
                        dbItem.setBeginCity(travel.getBeginCity());
                    }
                    /*if (!TextUtils.isEmpty(travel.getEndCity())) {
                        dbItem.setEndCity(travel.getEndCity());
                    }*/
                    if (travel.getBeginDate() != null) {
                        dbItem.setBeginDate(travel.getBeginDate());
                    }
                    if (travel.getEndDate() != null) {
                        dbItem.setEndDate(travel.getEndDate());
                    }
                    if (travel.getAdvanceMoney() > 0) {
                        dbItem.setAdvanceMoney(travel.getAdvanceMoney());
                    }
                    dbItem.setRadioUber(travel.getRadioUber());
                    dbItem.setRadioBeleg(travel.getRadioBeleg());
                    dbItem.setRadioVerp(travel.getRadioVerp());
                    if (travel.getInklusiveFruhstuck() > 0) {
                        dbItem.setInklusiveFruhstuck(travel.getInklusiveFruhstuck());
                    }
                    if (travel.getOhneFruhstuck() > 0) {
                        dbItem.setOhneFruhstuck(travel.getOhneFruhstuck());
                    }
                    if (travel.getOhneRechnung() > 0) {
                        dbItem.setOhneRechnung(travel.getOhneRechnung());
                    }
                    if (travel.getUnentgeltlich() > 0) {
                        dbItem.setUnentgeltlich(travel.getUnentgeltlich());
                    }
                    if (!TextUtils.isEmpty(travel.getUberImages())) {
                        dbItem.setUberImages(travel.getUberImages());
                    }
                    if (!TextUtils.isEmpty(travel.getVerpflegung())) {
                        dbItem.setVerpflegung(travel.getVerpflegung());
                    }
                    if (travel.getPrivater() > 0) {
                        dbItem.setPrivater(travel.getPrivater());
                    }
                    if (!TextUtils.isEmpty(travel.getTaxiImages())) {
                        dbItem.setTaxiImages(travel.getTaxiImages());
                    }
                    if (!TextUtils.isEmpty(travel.getFahrtkostenImages())) {
                        dbItem.setFahrtkostenImages(travel.getFahrtkostenImages());
                    }
                    if (!TextUtils.isEmpty(travel.getMietwagenImages())) {
                        dbItem.setMietwagenImages(travel.getMietwagenImages());
                    }
                    if (!TextUtils.isEmpty(travel.getKraftstoffImages())) {
                        dbItem.setKraftstoffImages(travel.getKraftstoffImages());
                    }
                    if (!TextUtils.isEmpty(travel.getParkenImages())) {
                        dbItem.setParkenImages(travel.getParkenImages());
                    }
                    if (!TextUtils.isEmpty(travel.getTagungsImages())) {
                        dbItem.setTagungsImages(travel.getTagungsImages());
                    }
                    if (!TextUtils.isEmpty(travel.getNeben())) {
                        dbItem.setNeben(travel.getNeben());
                    }
                    if (!TextUtils.isEmpty(travel.getComment())) {
                        dbItem.setComment(travel.getComment());
                    }
                    dbItem.setFavorite(travel.isFavorite());
                    dbItem.setVorlagen(travel.isVorlagen());
                    if (!TextUtils.isEmpty(travel.getPdfName())) {
                        dbItem.setPdfName(travel.getPdfName());
                    }
                    dbItem.setDraft(travel.isDraft());
                    dbItem.setUploading(travel.isUploading());
                    dbItem.setModify(travel.isModify());
                    dbItem.setStatus(travel.getStatus());
                    dbItem.setMessage(travel.getMessage());
                    dbItem.setEdit(travel.isEdit());
                    dbItem.setUploaded(travel.isUploaded());
                    dbItem.setTravelFrom(travel.getTravelFrom());
                    dbItem.setPdfMd5(travel.getPdfMd5());
                    if (!TextUtils.isEmpty(travel.getBody1())) {
                        dbItem.setBody1(travel.getBody1());
                    }
                    if (!TextUtils.isEmpty(travel.getBody2())) {
                        dbItem.setBody2(travel.getBody2());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingTitle1())) {
                        dbItem.setMissingTitle1(travel.getMissingTitle1());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingTitle2())) {
                        dbItem.setMissingTitle2(travel.getMissingTitle2());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingTitle3())) {
                        dbItem.setMissingTitle3(travel.getMissingTitle3());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingDetail1())) {
                        dbItem.setMissingDetail1(travel.getMissingDetail1());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingDetail2())) {
                        dbItem.setMissingDetail2(travel.getMissingDetail2());
                    }
                    if (!TextUtils.isEmpty(travel.getMissingDetail3())) {
                        dbItem.setMissingDetail3(travel.getMissingDetail3());
                    }

                    dbItem.save();
                    ActiveAndroid.setTransactionSuccessful();

                    Log.i(TravelDbHelper.class.getSimpleName(), "Saved travel " + travel.getTravelId() + " into database successful.");
                } finally {
                    ActiveAndroid.endTransaction();
                }

                return null;
            }
        });
    }

    public static void updateTravelStatus(final String travelId, final int status, final String message) {
        if (TextUtils.isEmpty(travelId)) {
            return;
        }

        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                TravelDbItem itemId = new Select(new String[]{"Id"}).from(TravelDbItem.class).where("travel_id = ?", travelId).executeSingle();
                TravelDbItem item = TravelDbItem.load(TravelDbItem.class, itemId.getId());
                item.setStatus(status);
                item.setMessage(message);
                item.save();
                /*new Update(TravelDbItem.class)
                        .set("status = ", status)
                        .set("message = ", message)
                        .where("travel_id = ?", travelId)
                        .execute();*/

                return null;
            }
        });
    }

    public static void deleteTravelById(final String travelId) {
        if (TextUtils.isEmpty(travelId)) {
            return;
        }
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                new Delete().from(TravelDbItem.class).where("travel_id = ?", travelId).execute();

                return null;
            }
        });
    }

    public static TravelDbItem getTravelById(String travelId) {
        if (TextUtils.isEmpty(travelId)) {
            return null;
        }
        return new Select()
                .from(TravelDbItem.class)
                .where("travel_id = ?", travelId)
//                .orderBy("begin_date ASC")
                .executeSingle();
    }

    public static List<TravelDbItem> getAllVorlagenNotSend() {
        try {
            ActiveAndroid.beginTransaction();

            List<TravelDbItem> travelIdList = new Select()
                    .from(TravelDbItem.class)
                    .where("is_vorlagen = ?", true)
                    .where("is_uploaded = ?", false)
                    .execute();

            ActiveAndroid.setTransactionSuccessful();

            return travelIdList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<TravelDbItem> getAllReiseCanSend() {
        try {
            ActiveAndroid.beginTransaction();

            Date currentDate = new Date();

            List<TravelDbItem> travelIdList = new Select()
                    .from(TravelDbItem.class)
                    .where("is_draft = ?", true)
                    .where("is_travel = ?", true)
                    .where("end_date <= ?", currentDate.getTime())
                    .execute();

            ActiveAndroid.setTransactionSuccessful();

            return travelIdList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<TravelDbItem> getAllTravelInfo() {
        try {
            ActiveAndroid.beginTransaction();
//            List<TravelDbItem> travelIdList = new Select(new String[]{"Id, travel_id, status, create_time"})
            List<TravelDbItem> travelIdList = new Select()
                    .from(TravelDbItem.class)
//                    .where("is_draft = ?", false)
//                    .where("is_vorlagen = ?", false)
                    .execute();
            ActiveAndroid.setTransactionSuccessful();

            return travelIdList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void deleteAllTravelHadSent() {
        new Delete().from(TravelDbItem.class).where("is_draft = ?", false).or("is_vorlagen = ?", true).execute();
    }

    public static List<TravelDbItem> getAntrage(int status, boolean isTravel) {
        switch (status) {
            case -1:
                return new Select()
                        .from(TravelDbItem.class)
                        .where("is_draft = ?", false)
                        .where("is_vorlagen = ?", false)
                        .where("is_travel = ?", isTravel)
                        .orderBy("begin_date DESC")
                        .execute();
            case 0:
                return new Select()
                        .from(TravelDbItem.class)

                        .where("status = ?", 0)
                        .where("is_draft = ?", false)
                        .where("is_vorlagen = ?", false)
                        .where("is_travel = ?", isTravel)

                        .or("status = ?", 1)
                        .where("is_draft = ?", false)
                        .where("is_vorlagen = ?", false)
                        .where("is_travel = ?", isTravel)

                        .orderBy("begin_date DESC")
                        .execute();
            case 1:
            case 2:
            case 3:
            case 4:
                return new Select()
                        .from(TravelDbItem.class)
                        .where("is_draft = ?", false)
                        .where("is_vorlagen = ?", false)
                        .where("is_travel = ?", isTravel)
                        .where("status = ?", status)
                        .orderBy("begin_date DESC")
                        .execute();
            default:
                return null;
        }
    }

    public static List<TravelDbItem> getEntwurfe(boolean isTravel) {
        return new Select()
                .from(TravelDbItem.class)
                .where("is_draft = ?", true)
//                .where("is_vorlagen = ?", false)
                .where("is_travel = ?", isTravel)
                .orderBy("begin_date DESC")
                .execute();
    }

    public static List<TravelDbItem> getVorlagen(boolean isTravel) {
        return new Select()
                .from(TravelDbItem.class)
                .where("is_vorlagen = ?", true)
                .where("is_travel = ?", isTravel)
                .orderBy("begin_date DESC")
                .execute();
    }

    public static int countVorlagen() {
        return new Select()
                .from(TravelDbItem.class)
                .where("is_vorlagen = ?", true)
                .execute()
                .size();
    }

    public static boolean hasOverlapDate(Date beginDate, Date endDate, String travelId) {
        int size = new Select().from(TravelDbItem.class)
                .where("begin_date <= ?", endDate.getTime())
                .where("end_date >= ?", beginDate.getTime())
                .where("is_vorlagen = ?", false)
                .where("travel_id != ?", travelId)
                .execute().size();

        return size > 0;
    }



    /* City Item */

    public static void saveCities(final Context context) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                boolean exist = new Select().from(CityItem.class).exists();
                if (!exist) {
                    try {
                        InputStream is = context.getResources().getAssets().open("data/OrtPLZ.csv");
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        String line;
                        ArrayList<ArrayList<String>> cityList = new ArrayList<>();

                        while ((line = br.readLine()) != null) {
                            ArrayList<String> valueList = new ArrayList<>(Arrays.asList(line.split(";")));
                            cityList.add(valueList);
                        }

                        br.close();
                        is.close();

                        ActiveAndroid.beginTransaction();
                        try {
                            for (int i = 0; i < cityList.size(); i++) {
                                CityItem item = new CityItem();
                                item.setZip(cityList.get(i).get(0));
                                item.setCity(cityList.get(i).get(1));
                                item.save();
                            }
                            ActiveAndroid.setTransactionSuccessful();
                        } finally {
                            ActiveAndroid.endTransaction();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                return null;
            }
        });
    }

    public static List<CityItem> getAllCityDistinct() {
        try {
            ActiveAndroid.beginTransaction();
            List<CityItem> cityItemList = new Select(new String[]{"Id, city"}).distinct().from(CityItem.class).orderBy("city ASC").groupBy("city").execute();
            ActiveAndroid.setTransactionSuccessful();

            return cityItemList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<CityItem> getAllZipDistinct() {
        try {
            ActiveAndroid.beginTransaction();
            List<CityItem> cityItemList = new Select(new String[]{"Id, zip"}).distinct().from(CityItem.class).orderBy("zip ASC").groupBy("zip").execute();
            ActiveAndroid.setTransactionSuccessful();

            return cityItemList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<CityItem> getAllCityFromZip(String zip) {
        try {
            ActiveAndroid.beginTransaction();
            List<CityItem> cityItemList = new Select(new String[]{"Id, city"}).distinct().from(CityItem.class).where("zip = ?", zip).orderBy("city ASC").execute();
            ActiveAndroid.setTransactionSuccessful();

            return cityItemList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<CityItem> getAllZipFromCity(String city) {
        try {
            ActiveAndroid.beginTransaction();
            List<CityItem> cityItemList = new Select(new String[]{"Id, zip"}).distinct().from(CityItem.class).where("city = ?", city).orderBy("zip ASC").execute();
            ActiveAndroid.setTransactionSuccessful();

            return cityItemList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }
}
