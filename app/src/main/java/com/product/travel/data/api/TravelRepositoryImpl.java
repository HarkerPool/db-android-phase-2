package com.product.travel.data.api;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.product.travel.interfaces.CallBack;
import com.product.travel.interfaces.TravelRepository;
import com.product.travel.models.NachrichtenItem;
import com.product.travel.models.Travel;
import com.product.travel.models.TravelStatus;
import com.product.travel.models.User;
import com.product.travel.utils.DateUtils;
import com.product.travel.utils.SecurityUtils;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class TravelRepositoryImpl extends BaseRepositoryImpl implements TravelRepository {

    private static final String TAG = TravelRepositoryImpl.class.getSimpleName();

    @Override
    public void createTravel(final Travel travel, final CallBack<Travel> callBack) throws Exception {

        JSONObject jsonRequest = new JSONObject();
        JSONObject jsonObject;
        JSONArray jsonArray;
        try {
            jsonRequest.put("travel_id", travel.getTravelId() == null ? "" : travel.getTravelId());
            jsonRequest.put("is_travel", travel.isTravel());
            jsonRequest.put("reason", travel.getReason() == null ? "" : travel.getReason());
            jsonRequest.put("is_beamter", travel.isBeamter());

            if (TextUtils.isEmpty(travel.getZiel())) {
                jsonRequest.put("ziel", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getZiel());
                jsonArray = getJsonArrayValue(jsonObject, "ziel");
                jsonRequest.put("ziel", jsonArray);
            }

            jsonRequest.put("begin_city", travel.getBeginCity());
//            jsonRequest.put("end_city", travel.getEndCity());
            jsonRequest.put("end_city", "");
            jsonRequest.put("first_pdf_date", travel.getFirstPdfDate());
            jsonRequest.put("begin_date", DateUtils.convertDateToString(travel.getBeginDate()));
            jsonRequest.put("end_date", DateUtils.convertDateToString(travel.getEndDate()));
            if (travel.isTravel()) {
                Calendar calendar = Calendar.getInstance();
                if (travel.getBeginDate() == null) {
                    jsonRequest.put("begin_time", "00:00");
                } else {
                    calendar.setTime(travel.getBeginDate());
                    jsonRequest.put("begin_time", String.format(Locale.GERMAN, "%d:%d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
                }
                if (travel.getEndDate() == null) {
                    jsonRequest.put("end_time", "00:00");
                } else {
                    calendar.setTime(travel.getEndDate());
                    jsonRequest.put("end_time", String.format(Locale.GERMAN, "%d:%d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
                }
            } else {
                jsonRequest.put("begin_time", "00:00");
                jsonRequest.put("end_time", "00:00");
            }
            jsonRequest.put("advance_money", travel.getAdvanceMoney());

            // Ubernachtung
            JSONObject jUber = new JSONObject();
            jUber.put("inklusive_fruhstuck", travel.getInklusiveFruhstuck());
            jUber.put("ohne_fruhstuck", travel.getOhneFruhstuck());
            jUber.put("ohne_rechnung", travel.getOhneRechnung());
            jUber.put("unentgeltlich", travel.getUnentgeltlich());
            if (TextUtils.isEmpty(travel.getUberImages())) {
                jUber.put("images", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getUberImages());
                jsonArray = getJsonArrayValue(jsonObject, "uber_images");
                jUber.put("images", jsonArray);
            }
            jsonRequest.put("uber", jUber);

            // Verpflegung
            if (TextUtils.isEmpty(travel.getVerpflegung())) {
                jsonRequest.put("verpflegung", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getVerpflegung());
                jsonArray = getJsonArrayValue(jsonObject, "verpflegung");
                jsonRequest.put("verpflegung", jsonArray);
            }

            // Beleg
            JSONObject jBeleg = new JSONObject();

            // Privater
            jBeleg.put("privater", travel.getPrivater());
            
            // Taxi
            if (TextUtils.isEmpty(travel.getTaxiImages())) {
                jBeleg.put("taxi", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getTaxiImages());
                jsonArray = getJsonArrayValue(jsonObject, "taxi_images");
                jBeleg.put("taxi", jsonArray);
            }
            
            // Fahrtkosten
            if (TextUtils.isEmpty(travel.getFahrtkostenImages())) {
                jBeleg.put("fahrtkosten", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getFahrtkostenImages());
                jsonArray = getJsonArrayValue(jsonObject, "fahrtkosten_images");
                jBeleg.put("fahrtkosten", jsonArray);
            }

            // Mietwagen
            if (TextUtils.isEmpty(travel.getMietwagenImages())) {
                jBeleg.put("mietwagen", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getMietwagenImages());
                jsonArray = getJsonArrayValue(jsonObject, "mietwagen_images");
                jBeleg.put("mietwagen", jsonArray);
            }
            
            // Kraftstoff
            if (TextUtils.isEmpty(travel.getKraftstoffImages())) {
                jBeleg.put("kraftstoff", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getKraftstoffImages());
                jsonArray = getJsonArrayValue(jsonObject, "kraftstoff_images");
                jBeleg.put("kraftstoff", jsonArray);
            }
            
            // Parken
            if (TextUtils.isEmpty(travel.getParkenImages())) {
                jBeleg.put("parken", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getParkenImages());
                jsonArray = getJsonArrayValue(jsonObject, "parken_images");
                jBeleg.put("parken", jsonArray);
            }
            
            // Tagungs
            if (TextUtils.isEmpty(travel.getTagungsImages())) {
                jBeleg.put("tagungs", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getTagungsImages());
                jsonArray = getJsonArrayValue(jsonObject, "tagungs_images");
                jBeleg.put("tagungs", jsonArray);
            }

            // Neben
            if (TextUtils.isEmpty(travel.getNeben())) {
                jBeleg.put("neben", new JSONArray());
            } else {
                jsonObject = new JSONObject(travel.getNeben());
                jsonArray = getJsonArrayValue(jsonObject, "neben");
                jBeleg.put("neben", jsonArray);
            }

            jsonRequest.put("beleg", jBeleg);

            jsonRequest.put("comment", travel.getComment() == null ? "" : travel.getComment());
            jsonRequest.put("is_favorite", travel.isFavorite());
            jsonRequest.put("pdf_name", travel.getPdfName() == null ? "" : travel.getPdfName());
            jsonRequest.put("is_create_pdf", travel.isCreatePdf());
            jsonRequest.put("is_modify", travel.isModify());
            jsonRequest.put("is_uploading", travel.isUploading());
            jsonRequest.put("is_vorlagen", travel.isVorlagen());
            jsonRequest.put("status", travel.getStatus()); // it always must be '0'

            User user = DBApplication.getServiceFactory().getUserService().getUserInfo();
            if (user != null) {
                JSONObject jUser = new JSONObject();
                try {
                    jUser.put("last_name", user.getLastName());
                    jUser.put("first_name", user.getFirstName());
                    jUser.put("personal_no", user.getPersonalNo());
                    jUser.put("email", user.getEmail());
                    jUser.put("phone", user.getPhoneNumber() == null ? "" : user.getPhoneNumber());
                    jUser.put("employee_status", user.getEmployeeStatus());
                    jUser.put("company_street", user.getCompanyStreet());
                    jUser.put("company_no", user.getCompanyNo());
                    jUser.put("company_zip", user.getCompanyZip());
                    jUser.put("company_city", user.getCompanyCity());
                    jUser.put("country_id", -1);

                    JSONArray jArrHomeAddress = new JSONArray();
                    JSONObject jHomeAddress1 = new JSONObject();
                    jHomeAddress1.put("street", user.getHomeAddressList().get(0).getStreet());
                    jHomeAddress1.put("no", user.getHomeAddressList().get(0).getHomeNumber());
                    jHomeAddress1.put("zip", user.getHomeAddressList().get(0).getZip());
                    jHomeAddress1.put("city", user.getHomeAddressList().get(0).getCity());
                    jHomeAddress1.put("country_id", user.getHomeAddressList().get(0).getCountryId());
                    jArrHomeAddress.put(jHomeAddress1);

                    if (user.getHomeAddressList().size() > 1) {
                        JSONObject jHomeAddress2 = new JSONObject();
                        jHomeAddress2.put("street", user.getHomeAddressList().get(1).getStreet());
                        jHomeAddress2.put("no", user.getHomeAddressList().get(1).getHomeNumber());
                        jHomeAddress2.put("zip", user.getHomeAddressList().get(1).getZip());
                        jHomeAddress2.put("city", user.getHomeAddressList().get(1).getCity());
                        jHomeAddress2.put("country_id", user.getHomeAddressList().get(1).getCountryId());
                        jArrHomeAddress.put(jHomeAddress2);
                    }

                    jUser.put("home_address", jArrHomeAddress);
                } catch (Exception ex) {
                    ex.printStackTrace();

                    callBack.onError(ex);
                }
                jsonRequest.put("user_info", jUser);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        Log.i("createTravel - json", jsonRequest.toString());

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.CREATE_TRAVEL_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "createTravel response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (!result) {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            JSONObject data = response.getJSONObject("data");

                            String createTime = getStringValue(data, "create_time"); // TODO - no need because of sync data when go home.
                            String pdfLink = getStringValue(data, "pdf_link");

                            /*Travel travel1 = DBApplication.getServiceFactory().getTravelService().getTravel();
                            if (travel1 != null) { // It's Vorlagen
                                travel1.setCreateTime(createTime);
//                                travel1.setPdfName(pdfLink); // TODO
                                travel1.setDraft(false);
                                travel1.setUploaded(true);
                            }*/
                            callBack.onSuccess(null); // TODO
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(new Exception("Error when saving/updating travel into database."));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void uploadTravelImage(final String imageName, final String travelId, final boolean isLast, final CallBack<Boolean> callBack) throws Exception {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    final File file;
                    String md5;

                    try {
                        String imagePath;
                        if (imageName.contains("uploads")) {
                            return;
                        }

                        imagePath = "/data/data/com.product.travel/images/" + imageName;
                        file = new File(imagePath);
                        if (!file.exists() || file.length() == 0) {
                            String error = "Image '" + imageName + "' does not exists.";
                            Log.i("uploadTravelImage", error);
                            callBack.onError(new Exception(error));
                            return;
                        }

                        FileInputStream fis = new FileInputStream(imagePath);
                        md5 = SecurityUtils.getMd5(fis);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        return;
                    }

                    MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
                    OkHttpClient client = new OkHttpClient.Builder()
                            .connectTimeout(50, TimeUnit.SECONDS)
                            .writeTimeout(50, TimeUnit.SECONDS)
                            .readTimeout(50, TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)
                            .build();

                    String token = "";
                    if (DBApplication.getServiceFactory().getUserService().getUserInfo() != null) {
                        token = DBApplication.getServiceFactory().getUserService().getUserInfo().getToken();
                    }

                    Log.i("uploadTravelImage", "md5: " + md5);

                    okhttp3.Request request = new okhttp3.Request.Builder()
                            .url(AppConfigs.UPLOAD_IMAGE_URL)
                            .header("token", token == null ? "" : token)
                            .addHeader("image", imageName)
                            .addHeader("travelId", travelId)
//                    .addHeader("isLast", isLast ? "true" : "false")
                            .addHeader("md5", md5)
                            .post(RequestBody.create(MEDIA_TYPE_JPEG, file))
                            .build();

                    okhttp3.Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        String sResult = response.body().string().trim();
                        Log.i("uploadTravelImage", "result:\n" + sResult);

                        if (!TextUtils.isEmpty(sResult) && (sResult.contains("\"result\":true") || sResult.contains("\"result\":false"))) {
                            callBack.onSuccess(true);
                        } else {
                            callBack.onError(new Exception("Response is empty."));
                        }

                /*try {
                    if (TextUtils.isEmpty(sResult)) {
                        callBack.onError(new Exception("Response is empty."));
                    } else {
                        Object json = new JSONTokener(sResult).nextValue();
                        if (json instanceof JSONObject) {
                            callBack.onSuccess(true);
                        }
//                        JSONObject jResult = new JSONObject(sResult);
//                        boolean result = getBooleanValue(jResult, "result");
//                        if (result) {
//                            callBack.onSuccess(true);
//                        } else {
//                            String message = getStringValue(jResult, "message");
//                            callBack.onError(new Exception(message));
//                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                    if (!TextUtils.isEmpty(sResult) && sResult.contains("\"result\":true")) {
                        callBack.onSuccess(true);
                    } else {
                        callBack.onError(ex);
                    }
                }*/
                    } else {
                        callBack.onError(new IOException("Unexpected code: " + response));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();

                    callBack.onError(ex);
                }
            }
        }).start();
    }

    @Override
    public void updateTravelStatus(final CallBack<List<TravelStatus>> callBack) throws Exception {

        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.UPDATE_STATUS_URL, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "updateTravelStatus response message:\n" + response.toString());
                        try {
                            boolean result = getBooleanValue(response, "result");

                            if (!result) {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            List<TravelStatus> travelStatusList = new ArrayList<>();

                            JSONObject data = response.getJSONObject("data");

                            if (data.has("list_travel_id")) {
                                JSONArray jArrListTravelId = getJsonArrayValue(data, "list_travel_id");


                                JSONObject jsonObject;
                                for (int i = 0; i < jArrListTravelId.length(); i++) {
                                    jsonObject = jArrListTravelId.getJSONObject(i);

                                    TravelStatus element = new TravelStatus();
                                    element.setTravelId(getStringValue(jsonObject, "travel_id"));
                                    element.setStatus(getIntValue(jsonObject, "status"));
                                    element.setMessage(getStringValue(jsonObject, "message"));
                                    element.setCreateTime(getStringValue(jsonObject, "create_time"));

                                    travelStatusList.add(element);
                                }
                            }

                            callBack.onSuccess(travelStatusList);

                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void getTravelById(final String travelId, final CallBack<Travel> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("travel_id", travelId);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.GET_TRAVEL_BY_ID_URL, jsonRequest, /* TODO using for v3 */
//        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.GET_TRAVEL_BY_ID_URL, null, /* TODO using for phase 2 */
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "getTravelById response message:\n" + response.toString());
                        try {
                            boolean result = getBooleanValue(response, "result");

                            if (!result) {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            Travel travel = new Travel();

                            JSONObject data = response.getJSONObject("data");

                            travel.setTravelId(getStringValue(data, "travel_id"));
                            travel.setTravel(getBooleanValue(data, "is_travel"));
                            travel.setVorlagen(getBooleanValue(data, "is_vorlagen"));
                            travel.setReason(getStringValue(data, "reason"));
                            travel.setBeamter(getBooleanValue(data, "is_beamter"));

                            JSONObject jZiel = new JSONObject();
                            jZiel.put("ziel", getJsonArrayValue(data, "ziel"));
                            travel.setZiel(jZiel.toString());

                            travel.setBeginCity(getStringValue(data, "begin_city"));
//                            travel.setEndCity(getStringValue(data, "end_city"));

                            if (travel.isVorlagen()) {
                                travel.setFirstPdfDate(null);
                                travel.setBeginDate(null);
                                travel.setEndDate(null);
                            } else {
                                travel.setFirstPdfDate(getStringValue(data, "first_pdf_date"));
                                travel.setBeginDate(DateUtils.convertStringToDate(getStringValue(data, "begin_date") + " " + getStringValue(data, "begin_time")));
                                travel.setEndDate(DateUtils.convertStringToDate(getStringValue(data, "end_date") + " " + getStringValue(data, "end_time")));
                            }

                            travel.setAdvanceMoney(Double.parseDouble(getStringValue(data, "advance_money")));
                            travel.setCreateTime(getStringValue(data, "create_time"));

                            /* Ubernachtung */
                            JSONObject uber = data.getJSONObject("uber");
                            travel.setInklusiveFruhstuck(getIntValue(uber, "inklusive_fruhstuck"));
                            travel.setOhneFruhstuck(getIntValue(uber, "ohne_fruhstuck"));
                            travel.setOhneRechnung(getIntValue(uber, "ohne_rechnung"));
                            travel.setUnentgeltlich(getIntValue(uber, "unentgeltlich"));
                            if (travel.isTravel() && !travel.isVorlagen()) {
                                travel.setRadioUber(false);
                                if (travel.getInklusiveFruhstuck() > 0
                                        || travel.getOhneRechnung() > 0
                                        || travel.getOhneFruhstuck() > 0
                                        || travel.getUnentgeltlich() > 0) {
                                    travel.setRadioUber(true);
                                }
                            }

                            JSONObject jUber = new JSONObject();
                            jUber.put("uber_images", getJsonArrayValue(uber, "images"));
                            travel.setUberImages(jUber.toString());

                            /* Verpflegung */
                            JSONObject jVerp = new JSONObject();
                            jVerp.put("verpflegung", getJsonArrayValue(data, "verpflegung"));
                            travel.setVerpflegung(jVerp.toString());
                            if (travel.isTravel() && !travel.isVorlagen()) {
                                travel.setRadioVerp(false);
                                if (getJsonArrayValue(data, "verpflegung").length() > 0) {
                                    for (int i = 0; i < getJsonArrayValue(data, "verpflegung").length(); i++) {
                                        JSONObject jsonItem = getJsonArrayValue(data, "verpflegung").getJSONObject(i);

                                        if (getBooleanValue(jsonItem, "is_breakfast")
                                                || getBooleanValue(jsonItem, "is_lunch")
                                                || getBooleanValue(jsonItem, "is_dinner")) {

                                            travel.setRadioVerp(true);
                                            break;
                                        }
                                    }
                                }
                            }

                            /* Beleg */
                            JSONObject beleg = data.getJSONObject("beleg");
                            travel.setPrivater(getIntValue(beleg, "privater"));

                            JSONObject jTaxi = new JSONObject();
                            jTaxi.put("taxi_images", getJsonArrayValue(beleg, "taxi"));
                            travel.setTaxiImages(jTaxi.toString());

                            JSONObject jFahr = new JSONObject();
                            jFahr.put("fahrtkosten_images", getJsonArrayValue(beleg, "fahrtkosten"));
                            travel.setFahrtkostenImages(jFahr.toString());

                            JSONObject jMiet = new JSONObject();
                            jMiet.put("mietwagen_images", getJsonArrayValue(beleg, "mietwagen"));
                            travel.setMietwagenImages(jMiet.toString());

                            JSONObject jKraf = new JSONObject();
                            jKraf.put("kraftstoff_images", getJsonArrayValue(beleg, "kraftstoff"));
                            travel.setKraftstoffImages(jKraf.toString());

                            JSONObject jParken = new JSONObject();
                            jParken.put("parken_images", getJsonArrayValue(beleg, "parken"));
                            travel.setParkenImages(jParken.toString());

                            JSONObject jTagungs = new JSONObject();
                            jTagungs.put("tagungs_images", getJsonArrayValue(beleg, "tagungs"));
                            travel.setTagungsImages(jTagungs.toString());

                            JSONObject jNeben = new JSONObject();
                            jNeben.put("neben", getJsonArrayValue(beleg, "neben"));
                            travel.setNeben(jNeben.toString());

                            if (travel.isTravel() && !travel.isVorlagen()) {
                                travel.setRadioBeleg(false);
                                if (travel.getPrivater() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "taxi").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "fahrtkosten").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "mietwagen").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "kraftstoff").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "parken").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "tagungs").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                                if (getJsonArrayValue(beleg, "neben").length() > 0) {
                                    travel.setRadioBeleg(true);
                                }
                            }

                            travel.setComment(getStringValue(data, "comment"));
                            travel.setFavorite(getBooleanValue(data, "is_favorite"));
                            travel.setPdfName(getStringValue(data, "pdf_name"));
                            travel.setPdfMd5(getStringValue(data, "pdf_md5"));
                            travel.setModify(false);
                            travel.setUploading(getBooleanValue(data, "is_uploading"));
                            travel.setStatus(getIntValue(data, "status"));
                            travel.setMessage(getStringValue(data, "message"));
                            travel.setUploaded(true);

                            if (travel.isTravel()) {
                                travel.setEdit(true);
                                travel.setTravelFrom(2);
                            } else {
                                travel.setEdit(false);
                                travel.setTravelFrom(3);
                            }

                            if (!travel.isVorlagen() && (travel.isUploading() || travel.getStatus() == 0)) {
                                travel.setDraft(true);
                                travel.setTravelFrom(1);
//                            } else {
//                                travel.setDraft(false);
                            }

                            travel.setBody1(getStringValue(data, "body_1"));
                            travel.setBody2(getStringValue(data, "body_2"));
                            travel.setMissingTitle1(getStringValue(data, "missing_title_1"));
                            travel.setMissingTitle2(getStringValue(data, "missing_title_2"));
                            travel.setMissingTitle3(getStringValue(data, "missing_title_3"));
                            travel.setMissingDetail1(getStringValue(data, "missing_detail_1"));
                            travel.setMissingDetail2(getStringValue(data, "missing_detail_2"));
                            travel.setMissingDetail3(getStringValue(data, "missing_detail_3"));

                            callBack.onSuccess(travel);

                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
//                return createAuthorizationHeader(travelId); // TODO using for phase 2
                return createAuthorizationHeader(); // TODO using for v3
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 5, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void deleteTravel(String travelId, final CallBack<Boolean> callBack) throws Exception {

        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("travel_id", travelId);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.DELETE_TRAVEL_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "deleteTravel response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (result) {
                                callBack.onSuccess(true);
                            } else {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void deleteImage(String imageName, final CallBack<Boolean> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("image_name", imageName);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.DELETE_IMAGE_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "deleteImage response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (result) {
                                callBack.onSuccess(true);
                            } else {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void getEmail(final CallBack<ArrayList<NachrichtenItem>> callBack) throws Exception {

        JsonObjectRequest jr = new JsonObjectRequest(AppConfigs.GET_ALL_EMAIL_URL, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "getEmail response message:\n" + response.toString());
                        try {
                            boolean result = getBooleanValue(response, "result");

                            if (!result) {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            ArrayList<NachrichtenItem> nachrichtenItemList = new ArrayList<>();

                            JSONObject data = response.getJSONObject("data");

                            if (data.has("list_email")) {
                                JSONArray jArrListTravelId = getJsonArrayValue(data, "list_email");

                                JSONObject jsonObject;
                                for (int i = 0; i < jArrListTravelId.length(); i++) {
                                    jsonObject = jArrListTravelId.getJSONObject(i);

                                    NachrichtenItem item = new NachrichtenItem();
                                    item.setId(getIntValue(jsonObject, "id"));
                                    item.setSubject(getStringValue(jsonObject, "subject"));
                                    item.setBody1(getStringValue(jsonObject, "body_1"));
                                    item.setBody2(getStringValue(jsonObject, "body_2"));
                                    item.setCreateDate(getStringValue(jsonObject, "create_date"));
                                    item.setTravelId(getStringValue(jsonObject, "travel_id"));
                                    item.setMissingTitle1(getStringValue(jsonObject, "missing_title_1"));
                                    item.setMissingTitle2(getStringValue(jsonObject, "missing_title_2"));
                                    item.setMissingTitle3(getStringValue(jsonObject, "missing_title_3"));
                                    item.setMissingDetail1(getStringValue(jsonObject, "missing_detail_1"));
                                    item.setMissingDetail2(getStringValue(jsonObject, "missing_detail_2"));
                                    item.setMissingDetail3(getStringValue(jsonObject, "missing_detail_3"));
                                    item.setRead(getBooleanValue(jsonObject, "is_read"));
//                                    item.setDelete(false);

                                    nachrichtenItemList.add(item);
                                }
                            }

                            callBack.onSuccess(nachrichtenItemList);

                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void getEmailByTravel(String travelId, final CallBack<NachrichtenItem> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("travel_id", travelId);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.GET_EMAIL_BY_TRAVEL_URL, jsonRequest,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i(TAG, "getEmailByTravel response message:\n" + response.toString());
                        try {
                            boolean result = getBooleanValue(response, "result");

                            if (!result) {
                                String message = response.getString("message");
                                callBack.onError(new Exception(message));
                                return;
                            }

                            // get data
                            NachrichtenItem nachrichtenItem = new NachrichtenItem();

                            JSONObject data = response.getJSONObject("data");

                            if (data.has("email")) {
                                JSONObject jsonObject = data.getJSONObject("email");

                                nachrichtenItem.setId(getIntValue(jsonObject, "id"));
                                nachrichtenItem.setSubject(getStringValue(jsonObject, "subject"));
                                nachrichtenItem.setBody1(getStringValue(jsonObject, "body_1"));
                                nachrichtenItem.setBody2(getStringValue(jsonObject, "body_2"));
                                nachrichtenItem.setCreateDate(getStringValue(jsonObject, "create_date"));
                                nachrichtenItem.setTravelId(getStringValue(jsonObject, "travel_id"));
                                nachrichtenItem.setMissingTitle1(getStringValue(jsonObject, "missing_title_1"));
                                nachrichtenItem.setMissingTitle2(getStringValue(jsonObject, "missing_title_2"));
                                nachrichtenItem.setMissingTitle3(getStringValue(jsonObject, "missing_title_3"));
                                nachrichtenItem.setMissingDetail1(getStringValue(jsonObject, "missing_detail_1"));
                                nachrichtenItem.setMissingDetail2(getStringValue(jsonObject, "missing_detail_2"));
                                nachrichtenItem.setMissingDetail3(getStringValue(jsonObject, "missing_detail_3"));
                                nachrichtenItem.setRead(getBooleanValue(jsonObject, "is_read"));
                            }

                            callBack.onSuccess(nachrichtenItem);

                        } catch (Exception ex) {
                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void deleteEmail(ArrayList<Integer> email_id, final CallBack<Boolean> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();

        if (email_id != null && email_id.size() > 0) { // if email_id list = null --> delete all email
            try {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < email_id.size(); i++) {
                    jsonArray.put(email_id.get(i));
                }

                jsonRequest.put("list_id", jsonArray);

            } catch (Exception ex) {
                ex.printStackTrace();

                callBack.onError(ex);
            }
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.DELETE_EMAIL_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "deleteEmail response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (result) {
                                callBack.onSuccess(true);
                            } else {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }

    @Override
    public void updateEmail(int email_id, final CallBack<Boolean> callBack) throws Exception {
        JSONObject jsonRequest = new JSONObject();
        try {
            jsonRequest.put("email_id", email_id);
        } catch (Exception ex) {
            ex.printStackTrace();

            callBack.onError(ex);
        }

        JsonObjectRequest jr = new JsonObjectRequest(Request.Method.POST, AppConfigs.UPDATE_EMAIL_URL, jsonRequest,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, "updateEmail response message: " + response.toString());

                        try {
                            boolean result = getBooleanValue(response, "result");
                            if (result) {
                                callBack.onSuccess(true);
                            } else {
                                String message = getStringValue(response, "message");
                                callBack.onError(new Exception(message));
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();

                            callBack.onError(ex);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                callBack.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return createAuthorizationHeader();
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(10000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jr.setRetryPolicy(policy);
        VolleyManager.getInstance().addToRequestQueue(jr);
    }
}
