package com.product.travel.data.dao;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by HarkerPool on 11/22/16.
 */

@Table(name = "NachrichtenItems")
public class NachrichtenDbItem extends Model {

    @Column(name = "nachrichten_id")
    private int nachrichtenId;

    @Column(name = "subject")
    private String subject;

    @Column(name = "body1")
    private String body1;

    @Column(name = "body2")
    private String body2;

    @Column(name = "create_date")
    private String createDate;

    @Column(name = "travel_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String travelId;

    @Column(name = "missing_title1")
    private String missingTitle1;

    @Column(name = "missing_title2")
    private String missingTitle2;

    @Column(name = "missing_title3")
    private String missingTitle3;

    @Column(name = "missing_detail1")
    private String missingDetail1;

    @Column(name = "missing_detail2")
    private String missingDetail2;

    @Column(name = "missing_detail3")
    private String missingDetail3;

    @Column(name = "is_read")
    private boolean isRead;

    @Column(name = "is_delete")
    private boolean isDelete;

    /* Constructor */

    public NachrichtenDbItem() {
        super();
    }

    /* Getter & Setter */

    public int getNachrichtenId() {
        return nachrichtenId;
    }

    public void setNachrichtenId(int nachrichtenId) {
        this.nachrichtenId = nachrichtenId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody1() {
        return body1;
    }

    public void setBody1(String body1) {
        this.body1 = body1;
    }

    public String getBody2() {
        return body2;
    }

    public void setBody2(String body2) {
        this.body2 = body2;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public String getMissingTitle1() {
        return missingTitle1;
    }

    public void setMissingTitle1(String missingTitle1) {
        this.missingTitle1 = missingTitle1;
    }

    public String getMissingTitle2() {
        return missingTitle2;
    }

    public void setMissingTitle2(String missingTitle2) {
        this.missingTitle2 = missingTitle2;
    }

    public String getMissingTitle3() {
        return missingTitle3;
    }

    public void setMissingTitle3(String missingTitle3) {
        this.missingTitle3 = missingTitle3;
    }

    public String getMissingDetail1() {
        return missingDetail1;
    }

    public void setMissingDetail1(String missingDetail1) {
        this.missingDetail1 = missingDetail1;
    }

    public String getMissingDetail2() {
        return missingDetail2;
    }

    public void setMissingDetail2(String missingDetail2) {
        this.missingDetail2 = missingDetail2;
    }

    public String getMissingDetail3() {
        return missingDetail3;
    }

    public void setMissingDetail3(String missingDetail3) {
        this.missingDetail3 = missingDetail3;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }
}