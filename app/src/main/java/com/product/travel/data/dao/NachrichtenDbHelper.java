package com.product.travel.data.dao;

import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.product.travel.models.NachrichtenItem;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.Task;

/**
 * Created by HarkerPool on 11/22/16.
 */

public class NachrichtenDbHelper {

    public static void save(final NachrichtenItem item) {
        if (item == null) {
            return;
        }

        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                try {
                    ActiveAndroid.beginTransaction();

                    NachrichtenDbItem dbItem = new NachrichtenDbItem();

                    dbItem.setNachrichtenId(item.getId());
                    dbItem.setTravelId(item.getTravelId());
                    dbItem.setSubject(item.getSubject());
                    dbItem.setBody1(item.getBody1());
                    dbItem.setBody2(item.getBody2());
                    dbItem.setCreateDate(item.getCreateDate());
                    dbItem.setMissingTitle1(item.getMissingTitle1());
                    dbItem.setMissingTitle2(item.getMissingTitle2());
                    dbItem.setMissingTitle3(item.getMissingTitle3());
                    dbItem.setMissingDetail1(item.getMissingDetail1());
                    dbItem.setMissingDetail2(item.getMissingDetail2());
                    dbItem.setMissingDetail3(item.getMissingDetail3());
                    dbItem.setRead(item.isRead());
                    dbItem.setDelete(item.isDelete());

                    dbItem.save();
                    ActiveAndroid.setTransactionSuccessful();

                    Log.i(TravelDbHelper.class.getSimpleName(), "Saved Nachrichten item " + item.getTravelId() + " into database successful.");
                } finally {
                    ActiveAndroid.endTransaction();
                }

                return null;
            }
        });
    }

    public static List<NachrichtenDbItem> getAllNachrichten(boolean isDelete) {
        try {
            ActiveAndroid.beginTransaction();

            List<NachrichtenDbItem> nachrichtenList = new Select()
                    .from(NachrichtenDbItem.class)
                    .where("is_delete = ?", isDelete)
                    .execute();

            ActiveAndroid.setTransactionSuccessful();

            return nachrichtenList;
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static NachrichtenDbItem getNachrichtenByTravelId(String travelId) {
        if (TextUtils.isEmpty(travelId)) {
            return null;
        }
        return new Select()
                .from(NachrichtenDbItem.class)
                .where("travel_id = ?", travelId)
                .executeSingle();
    }

    public static void setIsRead(final boolean isRead, final int nachrichtenId) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                new Update(NachrichtenDbItem.class)
                        .set("is_read = ?", isRead ? 1 : 0)
                        .where("nachrichten_id = ?", nachrichtenId)
                        .execute();

                return null;
            }
        });
    }

    public static void setIsDelete(final boolean isDelete, final int nachrichtenId) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                new Update(NachrichtenDbItem.class)
                        .set("is_delete = ?", isDelete ? 1 : 0)
                        .where("nachrichten_id = ?", nachrichtenId)
                        .execute();

                return null;
            }
        });
    }

    public static void deleteNachrichtenById(final int nachrichtenId) {
        Task.callInBackground(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                new Delete().from(NachrichtenDbItem.class).where("nachrichten_id = ?", nachrichtenId).execute();

                return null;
            }
        });
    }
}
