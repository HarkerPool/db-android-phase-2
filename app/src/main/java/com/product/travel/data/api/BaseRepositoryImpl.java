package com.product.travel.data.api;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import com.product.travel.commons.DBApplication;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class BaseRepositoryImpl {

    protected boolean getBooleanValue(JSONObject jsonObj, String keyName) throws Exception {
        if (jsonObj == null) {
            return false;
        }
        boolean result = false;

        if (jsonObj.has(keyName) && !jsonObj.getString(keyName).equals("null")) {
            result = jsonObj.getBoolean(keyName);
        }

        return result;
    }

    protected String getStringValue(JSONObject jsonObject, String keyName) throws Exception {
        if (jsonObject == null) {
            return "";
        }
        String result = "";

        if (jsonObject.has(keyName) && !jsonObject.getString(keyName).equals("null")) {
            result = jsonObject.getString(keyName);
        }

        return result;
    }

    protected int getIntValue(JSONObject jsonObject, String keyName) throws Exception {
        if (jsonObject == null) {
            return 0;
        }
        int result = 0;

        if (jsonObject.has(keyName) && !jsonObject.getString(keyName).equals("null")) {
            result = jsonObject.getInt(keyName);
        }

        return result;
    }

    protected JSONArray getJsonArrayValue(JSONObject jsonObject, String keyName) throws Exception {
        if (jsonObject == null) {
            return new JSONArray();
        }
        JSONArray result = new JSONArray();

        if (jsonObject.has(keyName) && !jsonObject.getString(keyName).equals("null")) {
            result = jsonObject.getJSONArray(keyName);
        }

        return result;
    }

    protected HashMap<String, String> createAuthorizationHeader() {
        return createAuthorizationHeader(null);
    }

    protected HashMap<String, String> createAuthorizationHeader(String travelId) {
        return createAuthorizationHeader(travelId, false);
    }

    protected HashMap<String, String> createAuthorizationHeader(String travelId, boolean isResetPassword) {
        String token = "";
        HashMap<String, String> headers = new HashMap<String, String>();

        if (isResetPassword) {
            headers.put("code", travelId == null ? "" : travelId);
        } else {
            try {
                if (DBApplication.getServiceFactory().getUserService().getUserInfo() != null) {
                    token = DBApplication.getServiceFactory().getUserService().getUserInfo().getToken();
                }
                if (TextUtils.isEmpty(token)) {
                    token = "";
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            Log.i("Token", token);
            headers.put("token", token);

            if (!TextUtils.isEmpty(travelId)) {
                headers.put("travelId", travelId);
            }
        }

        return headers;
    }
}
