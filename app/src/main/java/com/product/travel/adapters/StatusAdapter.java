package com.product.travel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.product.travel.R;
import com.product.travel.models.StatusModel;

import java.util.ArrayList;

/**
 * Created by HarkerPool on 7/21/16.
 */
public class StatusAdapter extends ArrayAdapter<StatusModel> {

    private ArrayList<StatusModel> mArrayList;
    private LayoutInflater mInflater;

    public StatusAdapter(Context context, int textViewResourceId, ArrayList<StatusModel> objects) {
        super(context, textViewResourceId, objects);

        mArrayList = objects;

        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mArrayList == null ? 0 : mArrayList.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = mInflater.inflate(R.layout.spinner_image_item, parent, false);

        TextView name = (TextView) row.findViewById(R.id.tv_item_name);
        ImageView icon = (ImageView) row.findViewById(R.id.iv_icon);

        if (getCount() > 0) {
            StatusModel item = mArrayList.get(position);

            name.setText(item.getName());
            if (item.getIcon() != -1) {
                icon.setVisibility(View.VISIBLE);
                icon.setImageResource(item.getIcon());
            } else {
                icon.setVisibility(View.GONE);
            }
        }

        return row;
    }
}
