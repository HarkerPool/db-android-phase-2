package com.product.travel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.interfaces.BelegItemChangeListener;
import com.product.travel.models.BelegItem;

/**
 * Created by HarkerPool on 6/13/16.
 */
public class BelegAdapter extends BaseAdapter {

    ArrayList<BelegItem> mItemList;
    BelegItemChangeListener mDelegate;

    public BelegAdapter(ArrayList<BelegItem> itemList, BelegItemChangeListener listener) {
        mItemList = itemList;
        mDelegate = listener;
    }

    @Override
    public int getCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_beleg_item, null);

            holder.mIvIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
            holder.mCbName = (CheckBox) convertView.findViewById(R.id.cb_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (getCount() > position) {
            BelegItem item = (BelegItem) getItem(position);

            if (item != null) {
                holder.mIvIcon.setBackgroundResource(item.getIcon());
                holder.mCbName.setText(item.getName());
                holder.mCbName.setChecked(item.isChecked());
            }

            holder.mCbName.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    holder.mCbName.setChecked(!holder.mCbName.isChecked());
                    mDelegate.onCheckboxClickListener(position);
                }
            });
        }

        return convertView;
    }

    static class ViewHolder {
        ImageView mIvIcon;
        CheckBox mCbName;
    }
}
