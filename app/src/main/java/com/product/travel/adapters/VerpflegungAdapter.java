package com.product.travel.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.interfaces.VerpflegungItemChangeListener;
import com.product.travel.models.VerpflegungItem;
import com.product.travel.utils.DateUtils;

/**
 * Created by HarkerPool on 6/11/16.
 */
public class VerpflegungAdapter extends RecyclerView.Adapter<VerpflegungAdapter.ViewHolder> {

    private ArrayList<VerpflegungItem> mList;
    VerpflegungItemChangeListener mDelegate;

    public VerpflegungAdapter(ArrayList<VerpflegungItem> list, VerpflegungItemChangeListener listener) {
        mList = list;
        mDelegate = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_verpflegung_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        VerpflegungItem item = mList.get(position);
        if (item != null) {
            holder.mTvDate.setText(DateUtils.convertDateToString(item.getDate()));
            holder.mCbBreakfast.setChecked(item.isHasBreakfast());
            holder.mCbLunch.setChecked(item.isHasLunch());
            holder.mCbDinner.setChecked(item.isHasDinner());
        }

        holder.mCbBreakfast.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mDelegate != null) {
                    mDelegate.onItemChangeListener(AppConfigs.MEAL.BREAKFAST, isChecked, holder.getAdapterPosition());
                }
            }
        });

        holder.mCbLunch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mDelegate != null) {
                    mDelegate.onItemChangeListener(AppConfigs.MEAL.LUNCH, isChecked, holder.getAdapterPosition());
                }
            }
        });

        holder.mCbDinner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mDelegate != null) {
                    mDelegate.onItemChangeListener(AppConfigs.MEAL.DINNER, isChecked, holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvDate;
        private final CheckBox mCbBreakfast, mCbLunch, mCbDinner;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvDate = (TextView) itemView.findViewById(R.id.tv_date);
            mCbBreakfast = (CheckBox) itemView.findViewById(R.id.cb_breakfast);
            mCbLunch = (CheckBox) itemView.findViewById(R.id.cb_lunch);
            mCbDinner = (CheckBox) itemView.findViewById(R.id.cb_dinner);
        }
    }
}