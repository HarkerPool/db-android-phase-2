package com.product.travel.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import com.product.travel.R;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.AntrageItemListener;
import com.product.travel.utils.DateUtils;

/**
 * Created by HarkerPool on 6/11/16.
 */
public class AntrageAdapter extends RecyclerView.Adapter<AntrageAdapter.ViewHolder> {

    private boolean mIsFromAntrage;
    private List<TravelDbItem> mList;
    private AntrageItemListener mDelegate;

    public AntrageAdapter(boolean isFromAntrage, List<TravelDbItem> list, AntrageItemListener listener) {
        mIsFromAntrage = isFromAntrage;
        mList = list;
        mDelegate = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_antrage_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TravelDbItem item = mList.get(position);
        if (item != null) {
            holder.mTvReisezweck.setText(item.getReason());
            holder.mTvDatumBegin.setText(DateUtils.convertDateToString(item.getBeginDate()));
            holder.mTvDatumEnd.setText(DateUtils.convertDateToString(item.getEndDate()));
            holder.mTvCurrentDateTime.setText(item.getCreateTime());

            if (!TextUtils.isEmpty(item.getZiel())) {
                try {
                    JSONObject jsonObject = new JSONObject(item.getZiel());
                    JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                    if (jsonArray != null && jsonArray.length() > 0) {
                        JSONObject jsonItem = jsonArray.getJSONObject(0);

                        holder.mTvReisezielOrt.setText(jsonItem.getString("city"));
                        holder.mTvReisezielStrabe.setText(jsonItem.getString("street"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            holder.mIvEdit.setVisibility(View.VISIBLE);
            if (mIsFromAntrage && !item.isTravel()) {
                holder.mIvEdit.setVisibility(View.GONE);
            }

            holder.mIvPdf.setVisibility(View.VISIBLE);
            holder.mIvStatus.setVisibility(View.VISIBLE);
            holder.mIvDelete.setVisibility(View.VISIBLE);
            if (item.getStatus() == 0) {
                holder.mIvEdit.setVisibility(View.GONE);
                holder.mIvPdf.setVisibility(View.GONE);
                holder.mIvStatus.setImageResource(R.drawable.status1);
            } else if (item.getStatus() == 1) {
                holder.mIvDelete.setVisibility(View.GONE);
                holder.mIvStatus.setImageResource(R.drawable.status1);
            } else if (item.getStatus() == 2) {
                holder.mIvStatus.setImageResource(R.drawable.status2);
            } else if (item.getStatus() == 3) {
                holder.mIvStatus.setImageResource(R.drawable.status3);
            } else if (item.getStatus() == 4) {
                holder.mIvStatus.setImageResource(R.drawable.status4);
            } else if (item.getStatus() == 5) {
                holder.mIvStatus.setImageResource(R.drawable.status5);
            } else {
                holder.mIvStatus.setVisibility(View.GONE);
            }
        }

        holder.mIvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseViewInfoListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussViewInfoListener(holder.getAdapterPosition());
                    }
                }
            }
        });

        holder.mIvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseEditListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussEditListener(holder.getAdapterPosition());
                    }
                }
            }
        });

        holder.mIvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseDeleteListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussDeleteListener(holder.getAdapterPosition());
                    }
                }
            }
        });

        // PDF
        holder.mIvPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseOpenPdfListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussOpenPdfListener(holder.getAdapterPosition());
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvReisezweck, mTvDatumBegin, mTvDatumEnd, mTvReisezielOrt, mTvReisezielStrabe, mTvCurrentDateTime;
        private final ImageView mIvStatus, mIvInfo, mIvEdit, mIvDelete, mIvPdf;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvReisezweck = (TextView) itemView.findViewById(R.id.tv_reisezweck);
            mTvDatumBegin = (TextView) itemView.findViewById(R.id.tv_datum_begin);
            mTvDatumEnd = (TextView) itemView.findViewById(R.id.tv_datum_end);
            mTvReisezielOrt = (TextView) itemView.findViewById(R.id.tv_reiseziel_ort);
            mTvReisezielStrabe = (TextView) itemView.findViewById(R.id.tv_reiseziel_strabe);
            mTvCurrentDateTime = (TextView) itemView.findViewById(R.id.tv_current_date_time);
            mIvStatus = (ImageView) itemView.findViewById(R.id.iv_status);
            mIvInfo = (ImageView) itemView.findViewById(R.id.iv_info);
            mIvEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            mIvDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
            mIvPdf = (ImageView) itemView.findViewById(R.id.iv_pdf);
        }
    }
}