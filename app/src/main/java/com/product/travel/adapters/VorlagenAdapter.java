package com.product.travel.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import com.product.travel.R;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.EntwurfeItemListener;

/**
 * Created by HarkerPool on 6/11/16.
 */
public class VorlagenAdapter extends RecyclerView.Adapter<VorlagenAdapter.ViewHolder> {

    private List<TravelDbItem> mList;
    private EntwurfeItemListener mDelegate;
    private boolean mIsEditable;

    public VorlagenAdapter(List<TravelDbItem> list, boolean isEditable, EntwurfeItemListener listener) {
        mList = list;
        mIsEditable = isEditable;
        mDelegate = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_vorlagen_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (mIsEditable) {
            holder.mIvEdit.setVisibility(View.VISIBLE);
        } else {
            holder.mIvEdit.setVisibility(View.GONE);
        }

        final TravelDbItem item = mList.get(position);
        if (item != null) {
            holder.mTvReisezweck.setText(item.getReason());
            holder.mTvOrtBegin.setText(item.getBeginCity());
//            holder.mTvOrtEnd.setText(item.getEndCity());

            if (!TextUtils.isEmpty(item.getZiel())) {
                try {
                    JSONObject jsonObject = new JSONObject(item.getZiel());
                    if (jsonObject.has("ziel")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                        if (jsonArray.length() > 0) {
                            JSONObject jsonItem = jsonArray.getJSONObject(0);

                            holder.mTvZielOrt.setText(jsonItem.getString("city"));
                            holder.mTvZielStrabe.setText(jsonItem.getString("street"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        holder.mIvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseEditListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussEditListener(holder.getAdapterPosition());
                    }
                }
            }
        });

        holder.mIvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseDeleteListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussDeleteListener(holder.getAdapterPosition());
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvReisezweck, mTvOrtBegin, mTvZielOrt, mTvZielStrabe;
        private final ImageView mIvEdit, mIvDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvReisezweck = (TextView) itemView.findViewById(R.id.tv_reisezweck);
            mTvOrtBegin = (TextView) itemView.findViewById(R.id.tv_ort_begin);
//            mTvOrtEnd = (TextView) itemView.findViewById(R.id.tv_ort_end);
            mTvZielOrt = (TextView) itemView.findViewById(R.id.tv_ziel_ort);
            mTvZielStrabe = (TextView) itemView.findViewById(R.id.tv_ziel_strabe);
            mIvEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            mIvDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }
}