package com.product.travel.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.commons.AppConfigs;
import com.product.travel.commons.DBApplication;
import com.squareup.picasso.Picasso;

/**
 * Created by HarkerPool on 6/9/16.
 */
public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> mImageList;

    public ImageAdapter(Context context, ArrayList<String> imageList) {
        mContext = context;
        mImageList = imageList;
    }

    @Override
    public int getCount() {
        return mImageList != null ? mImageList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {

            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_image_item, null);

            holder.mIvImage = (ImageView) convertView.findViewById(R.id.iv_image_item);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (getCount() > position) {
            String imageName = (String) getItem(position);

            Bitmap bitmap = DBApplication.getBitmapCache().getBitmap(imageName);
            if (!TextUtils.isEmpty(imageName) && bitmap != null) {
                holder.mIvImage.setImageBitmap(bitmap);
            } else {
                Picasso.with(mContext)
                        .load(AppConfigs.SERVER_URL + imageName)
                        .placeholder(R.drawable.loading_icon)
                        .error(R.drawable.error)
                        .into(holder.mIvImage);
            }
        }

        return convertView;
    }

    static class ViewHolder {
        ImageView mIvImage;
    }
}
