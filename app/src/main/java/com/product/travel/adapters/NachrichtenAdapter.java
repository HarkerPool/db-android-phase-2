package com.product.travel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.product.travel.R;
import com.product.travel.interfaces.NachrichtenItemListener;
import com.product.travel.models.NachrichtenItem;

/**
 * Created by HarkerPool on 6/13/16.
 */
public class NachrichtenAdapter extends ArrayAdapter<NachrichtenItem> {

    private ArrayList<NachrichtenItem> mItemList;
    private NachrichtenItemListener mDelegate;

    public NachrichtenAdapter(Context context, int resource, ArrayList<NachrichtenItem> list, NachrichtenItemListener mDelegate) {
        super(context, resource, list);
        this.mItemList = list;
        this.mDelegate = mDelegate;
    }

    @Override
    public int getCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null || !(convertView.getTag() instanceof ViewHolder)) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_nachrichten_item, null);

            holder.mLinNachrichtenItem = (LinearLayout) convertView.findViewById(R.id.lin_nachrichten_item);
            holder.mTvTime = (TextView) convertView.findViewById(R.id.tv_nachrichten_time);
            holder.mTvSubject = (TextView) convertView.findViewById(R.id.tv_nachrichten_subject);
            holder.mTvContent = (TextView) convertView.findViewById(R.id.tv_nachrichten_content);
            holder.mCbSelect = (CheckBox) convertView.findViewById(R.id.cb_nachrichten_content);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (getCount() > position) {
            NachrichtenItem item = mItemList.get(position);
            if (item != null) {
                if (item.isRead()) {
                    holder.mLinNachrichtenItem.setBackgroundResource(R.color.white);
                } else {
                    holder.mLinNachrichtenItem.setBackgroundResource(R.color.bg_nachrichten_item);
                }

                holder.mTvSubject.setText(item.getSubject());

                holder.mCbSelect.setVisibility(View.GONE);
                holder.mTvTime.setText(item.getCreateDate());
                holder.mTvContent.setText(item.getBody1().replace("<p>", "").replace("</p>", "\n").replace("<br>", "\n"));

                if (item.getId() != -1) {
                    holder.mCbSelect.setVisibility(View.VISIBLE);
                    holder.mCbSelect.setChecked(item.isDelete());
                }
            }

            holder.mCbSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDelegate != null) {
                        mDelegate.onNachrichtenItemSelectListener(holder.mCbSelect.isChecked(), position);
                    }
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDelegate != null) {
                        mDelegate.onNachrichtenItemClickListener(position);
                    }
                }
            });
        }

        return convertView;
    }

    static class ViewHolder {
        private LinearLayout mLinNachrichtenItem;
        private TextView mTvTime, mTvSubject, mTvContent;
        private CheckBox mCbSelect;
    }
}
