package com.product.travel.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import com.product.travel.R;
import com.product.travel.data.dao.TravelDbItem;
import com.product.travel.interfaces.EntwurfeItemListener;
import com.product.travel.utils.DateUtils;

/**
 * Created by HarkerPool on 6/11/16.
 */
public class EntwurfeAdapter extends RecyclerView.Adapter<EntwurfeAdapter.ViewHolder> {

    private List<TravelDbItem> mList;
    private EntwurfeItemListener mDelegate;

    public EntwurfeAdapter(List<TravelDbItem> list, EntwurfeItemListener listener) {
        mList = list;
        mDelegate = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_entwurfe_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TravelDbItem item = mList.get(position);
        if (item != null) {
            holder.mTvReisezweck.setText(item.getReason());
            holder.mTvDatumBegin.setText(DateUtils.convertDateToString(item.getBeginDate()));
            holder.mTvDatumEnd.setText(DateUtils.convertDateToString(item.getEndDate()));
            holder.mTvCurrentDateTime.setText(item.getCreateTime());

            if (!TextUtils.isEmpty(item.getZiel())) {
                try {
                    JSONObject jsonObject = new JSONObject(item.getZiel());
                    JSONArray jsonArray = jsonObject.getJSONArray("ziel");

                    JSONObject jsonItem = jsonArray.getJSONObject(0);

                    holder.mTvReisezielOrt.setText(jsonItem.getString("city"));
                    holder.mTvReisezielStrabe.setText(jsonItem.getString("street"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        holder.mIvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseEditListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussEditListener(holder.getAdapterPosition());
                    }
                }
            }
        });

        holder.mIvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    if (item.isTravel()) {
                        mDelegate.onReiseDeleteListener(holder.getAdapterPosition());
                    } else {
                        mDelegate.onVorschussDeleteListener(holder.getAdapterPosition());
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTvReisezweck, mTvDatumBegin, mTvDatumEnd, mTvReisezielOrt, mTvReisezielStrabe, mTvCurrentDateTime;
        private final ImageView mIvEdit, mIvDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            mTvReisezweck = (TextView) itemView.findViewById(R.id.tv_reisezweck);
            mTvDatumBegin = (TextView) itemView.findViewById(R.id.tv_datum_begin);
            mTvDatumEnd = (TextView) itemView.findViewById(R.id.tv_datum_end);
            mTvReisezielOrt = (TextView) itemView.findViewById(R.id.tv_reiseziel_ort);
            mTvReisezielStrabe = (TextView) itemView.findViewById(R.id.tv_reiseziel_strabe);
            mTvCurrentDateTime = (TextView) itemView.findViewById(R.id.tv_current_date_time);
            mIvEdit = (ImageView) itemView.findViewById(R.id.iv_edit);
            mIvDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }
}