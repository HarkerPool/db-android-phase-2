package com.product.travel.models;

import java.util.Date;

/**
 * Created by HarkerPool on 6/11/16.
 */
public class VerpflegungItem {

    private Date date;
    private boolean hasBreakfast;
    private boolean hasLunch;
    private boolean hasDinner;

    public VerpflegungItem() {

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isHasBreakfast() {
        return hasBreakfast;
    }

    public void setHasBreakfast(boolean hasBreakfast) {
        this.hasBreakfast = hasBreakfast;
    }

    public boolean isHasLunch() {
        return hasLunch;
    }

    public void setHasLunch(boolean hasLunch) {
        this.hasLunch = hasLunch;
    }

    public boolean isHasDinner() {
        return hasDinner;
    }

    public void setHasDinner(boolean hasDinner) {
        this.hasDinner = hasDinner;
    }
}
