package com.product.travel.models;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class TravelStatus {
    private String travelId;
    private int status; // 0 - 4
    private String message;
    private String createTime;

    public TravelStatus() {

    }

    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
