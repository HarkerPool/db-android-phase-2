package com.product.travel.models;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class NebenkostenItem {

    private String bezeichnung;
    private double betrag;
    private String image;

    public NebenkostenItem() {

    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
