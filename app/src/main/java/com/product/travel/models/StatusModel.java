package com.product.travel.models;

/**
 * Created by HarkerPool on 7/21/16.
 */
public class StatusModel {

    String name;
    int icon;

    public StatusModel(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
