package com.product.travel.models;

import android.text.TextUtils;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class User {
    private int id;
    private String lastName;
    private String firstName;
    private String personalNo;
    private String email;
    private String phoneNumber;
    private String employeeStatus;
    private String companyStreet;
    private String companyNo;
    private String companyZip;
    private String companyCity;
    private Integer countryId;
    private LinkedHashMap<Integer, String> hashAllCountry;
    private List<HomeAddress> homeAddressList;
    private String token;

    public User() {
        countryId = -1;
    }

    public User(String token, LinkedHashMap<Integer, String> hashAllCountry) {
        countryId = -1;

        this.token = token;
        this.hashAllCountry = hashAllCountry;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPersonalNo() {
        return personalNo;
    }

    public void setPersonalNo(String personalNo) {
        this.personalNo = personalNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public String getCompanyStreet() {
        return companyStreet;
    }

    public void setCompanyStreet(String companyStreet) {
        this.companyStreet = companyStreet;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getCompanyZip() {
        return companyZip;
    }

    public void setCompanyZip(String companyZip) {
        this.companyZip = companyZip;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public LinkedHashMap<Integer, String> getHashAllCountry() {
        return hashAllCountry;
    }

    public void setHashAllCountry(LinkedHashMap<Integer, String> hashAllCountry) {
        this.hashAllCountry = hashAllCountry;
    }

    public List<HomeAddress> getHomeAddressList() {
        return homeAddressList;
    }

    public void setHomeAddressList(List<HomeAddress> homeAddressList) {
        this.homeAddressList = homeAddressList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void updateUserInfo(User user) {
        if (user == null) {
            return;
        }
        if (!TextUtils.isEmpty(user.getLastName())) {
            this.lastName = user.getLastName();
        }
        if (!TextUtils.isEmpty(user.getFirstName())) {
            this.firstName = user.getFirstName();
        }
        if (!TextUtils.isEmpty(user.getPersonalNo())) {
            this.personalNo = user.getPersonalNo();
        }
        if (!TextUtils.isEmpty(user.getEmail())) {
            this.email = user.getEmail();
        }
        if (!TextUtils.isEmpty(user.getPhoneNumber())) {
            this.phoneNumber = user.getPhoneNumber();
        }
        if (!TextUtils.isEmpty(user.getEmployeeStatus())) {
            this.employeeStatus = user.getEmployeeStatus();
        }
        if (!TextUtils.isEmpty(user.getCompanyStreet())) {
            this.companyStreet = user.getCompanyStreet();
        }
        if (!TextUtils.isEmpty(user.getCompanyNo())) {
            this.companyNo = user.getCompanyNo();
        }
        if (!TextUtils.isEmpty(user.getCompanyZip())) {
            this.companyZip = user.getCompanyZip();
        }
        if (!TextUtils.isEmpty(user.getCompanyCity())) {
            this.companyCity = user.getCompanyCity();
        }
        if (user.getCountryId() != -1) {
            this.countryId = user.getCountryId();
        }
        if (user.getHashAllCountry() != null && user.getHashAllCountry().size() > 0) {
            this.hashAllCountry = user.getHashAllCountry();
        }
        if (user.getHomeAddressList() != null && user.getHomeAddressList().size() > 0) {
            this.homeAddressList = user.getHomeAddressList();
        }
        if (!TextUtils.isEmpty(user.getToken())) {
            this.token = user.getToken();
        }
    }
}
