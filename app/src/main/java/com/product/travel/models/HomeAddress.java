package com.product.travel.models;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class HomeAddress {
    private String street;
    private String homeNumber;
    private String zip;
    private String city;
    private Integer countryId;

    public HomeAddress(String street, String homeNumber, String zip, String city, Integer countryId) {
        this.street = street;
        this.homeNumber = homeNumber;
        this.zip = zip;
        this.city = city;
        this.countryId = countryId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }
}
