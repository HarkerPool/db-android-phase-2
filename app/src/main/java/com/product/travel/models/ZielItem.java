package com.product.travel.models;

/**
 * Created by HarkerPool on 6/14/16.
 */
public class ZielItem {

    private String city;
    private String street;

    public ZielItem() {

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
