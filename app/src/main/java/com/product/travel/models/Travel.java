package com.product.travel.models;

import android.text.TextUtils;

import java.util.Date;

import com.product.travel.data.dao.TravelDbItem;

/**
 * Created by HarkerPool on 6/2/16.
 */
public class Travel {
    private String travelId; // Travel - R-timestamp, Vorschuss - VS-timestamp, Vorlagen - RF/VF-timestamp
    private boolean isTravel;
    private boolean isCreatePdf; // true - Vorschuss or Travel with has images, false - travel without images.
    private String reason;
    private boolean isBeamter;
    /* Ziel(e) */
    private String ziel; // jsonObject
    /* ""ziel"": [
              {
                      ""city"": String,
                      ""street"": String
            },
            ...
        ]*/
    private String createTime;
    private String firstPdfDate; // 1st startDate
    private String beginCity;
//    private String endCity;
    private Date beginDate;
    private Date endDate;
    private double advanceMoney;

    /* Uber */
    private Boolean radioUber;
    private Boolean radioVerp;
    private Boolean radioBeleg;
    private int inklusiveFruhstuck;
    private int ohneFruhstuck;
    private int ohneRechnung;
    private int unentgeltlich;
    private String uberImages; // jsonObject

    /* Verpflegung */
    private String verpflegung; // jsonObject
     /* ""verpflegung"": [
              {
                    ""date"": ""DD.MM.YYYY"",
                    ""is_breakfast"": boolean,
                    ""is_lunch"": boolean,
                    ""is_dinner"": boolean
              }
              ...
         ], */

    /* Beleg */
    private int privater;
    private String taxiImages; // jsonObject
    private String fahrtkostenImages; // jsonObject
    private String mietwagenImages; // jsonObject
    private String kraftstoffImages; // jsonObject
    private String parkenImages; // jsonObject
    private String tagungsImages; // jsonObject

    /* Neben */
    private String neben; // jsonObject
    /*""neben"": [
    {
        ""bezeichnung"": String,
            ""betrag"": float,
        ""image"": String // null-able
    },
    ...
    ] */

    private String comment;
    private boolean isFavorite;
    private boolean isVorlagen;
    private String pdfName; // create pdf from client then send it to server
    private boolean isDraft; // true - Entwurfe, false - Antrage
    private boolean isUploading; // true - sending images or trying to send travel but not success, false - sent all success
    private boolean isModify; // edit from Antrage - need to create pdf.
    private int status; // status of travel
    private String message; // default message or comment from Archive server
    private boolean isEdit; // using locally. Use it when press back button or go to menu or checking before send a travel to the server.
    private boolean isUploaded;
    private int travelFrom; // 1 - Entwurfe, 2 - Reisen in/from Antrage, 3 - Vorschuss in/from Antrage
    private String pdfMd5;
    private String body1;
    private String body2;
    private String missingTitle1;
    private String missingTitle2;
    private String missingTitle3;
    private String missingDetail1;
    private String missingDetail2;
    private String missingDetail3;

    /* Constructor */
    public Travel() {
        super(); // TODO
    }

    /* Getter & Setter */
    public String getTravelId() {
        return travelId;
    }

    public void setTravelId(String travelId) {
        this.travelId = travelId;
    }

    public boolean isTravel() {
        return isTravel;
    }

    public void setTravel(boolean travel) {
        isTravel = travel;
    }

    public boolean isCreatePdf() {
        return isCreatePdf;
    }

    public void setCreatePdf(boolean createPdf) {
        isCreatePdf = createPdf;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isBeamter() {
        return isBeamter;
    }

    public void setBeamter(boolean beamter) {
        isBeamter = beamter;
    }

    public String getZiel() {
        return ziel;
    }

    public void setZiel(String ziel) {
        this.ziel = ziel;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getFirstPdfDate() {
        return firstPdfDate;
    }

    public void setFirstPdfDate(String firstPdfDate) {
        this.firstPdfDate = firstPdfDate;
    }

    public String getBeginCity() {
        return beginCity;
    }

    public void setBeginCity(String beginCity) {
        this.beginCity = beginCity;
    }

    /*public String getEndCity() {
        return endCity;
    }

    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }*/

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getAdvanceMoney() {
        return advanceMoney;
    }

    public void setAdvanceMoney(double advanceMoney) {
        this.advanceMoney = advanceMoney;
    }

    public Boolean getRadioUber() {
        return radioUber;
    }

    public void setRadioUber(Boolean radioUber) {
        this.radioUber = radioUber;
    }

    public Boolean getRadioVerp() {
        return radioVerp;
    }

    public void setRadioVerp(Boolean radioVerp) {
        this.radioVerp = radioVerp;
    }

    public Boolean getRadioBeleg() {
        return radioBeleg;
    }

    public void setRadioBeleg(Boolean radioBeleg) {
        this.radioBeleg = radioBeleg;
    }

    public int getInklusiveFruhstuck() {
        return inklusiveFruhstuck;
    }

    public void setInklusiveFruhstuck(int inklusiveFruhstuck) {
        this.inklusiveFruhstuck = inklusiveFruhstuck;
    }

    public int getOhneFruhstuck() {
        return ohneFruhstuck;
    }

    public void setOhneFruhstuck(int ohneFruhstuck) {
        this.ohneFruhstuck = ohneFruhstuck;
    }

    public int getOhneRechnung() {
        return ohneRechnung;
    }

    public void setOhneRechnung(int ohneRechnung) {
        this.ohneRechnung = ohneRechnung;
    }

    public int getUnentgeltlich() {
        return unentgeltlich;
    }

    public void setUnentgeltlich(int unentgeltlich) {
        this.unentgeltlich = unentgeltlich;
    }

    public String getUberImages() {
        return uberImages;
    }

    public void setUberImages(String uberImages) {
        this.uberImages = uberImages;
    }

    public String getVerpflegung() {
        return verpflegung;
    }

    public void setVerpflegung(String verpflegung) {
        this.verpflegung = verpflegung;
    }

    public int getPrivater() {
        return privater;
    }

    public void setPrivater(int privater) {
        this.privater = privater;
    }

    public String getTaxiImages() {
        return taxiImages;
    }

    public void setTaxiImages(String taxiImages) {
        this.taxiImages = taxiImages;
    }

    public String getFahrtkostenImages() {
        return fahrtkostenImages;
    }

    public void setFahrtkostenImages(String fahrtkostenImages) {
        this.fahrtkostenImages = fahrtkostenImages;
    }

    public String getMietwagenImages() {
        return mietwagenImages;
    }

    public void setMietwagenImages(String mietwagenImages) {
        this.mietwagenImages = mietwagenImages;
    }

    public String getKraftstoffImages() {
        return kraftstoffImages;
    }

    public void setKraftstoffImages(String kraftstoffImages) {
        this.kraftstoffImages = kraftstoffImages;
    }

    public String getParkenImages() {
        return parkenImages;
    }

    public void setParkenImages(String parkenImages) {
        this.parkenImages = parkenImages;
    }

    public String getTagungsImages() {
        return tagungsImages;
    }

    public void setTagungsImages(String tagungsImages) {
        this.tagungsImages = tagungsImages;
    }

    public String getNeben() {
        return neben;
    }

    public void setNeben(String neben) {
        this.neben = neben;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isVorlagen() {
        return isVorlagen;
    }

    public void setVorlagen(boolean vorlagen) {
        isVorlagen = vorlagen;
    }

    public String getPdfName() {
        return pdfName;
    }

    public void setPdfName(String pdfName) {
        this.pdfName = pdfName;
    }

    public boolean isDraft() {
        return isDraft;
    }

    public void setDraft(boolean draft) {
        isDraft = draft;
    }

    public boolean isUploading() {
        return isUploading;
    }

    public void setUploading(boolean uploading) {
        isUploading = uploading;
    }

    public boolean isModify() {
        return isModify;
    }

    public void setModify(boolean modify) {
        isModify = modify;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean edit) {
        isEdit = edit;
    }

    public boolean isUploaded() {
        return isUploaded;
    }

    public void setUploaded(boolean uploaded) {
        isUploaded = uploaded;
    }

    public int getTravelFrom() {
        return travelFrom;
    }

    public void setTravelFrom(int travelFrom) {
        this.travelFrom = travelFrom;
    }

    public String getPdfMd5() {
        return pdfMd5;
    }

    public void setPdfMd5(String pdfMd5) {
        this.pdfMd5 = pdfMd5;
    }

    public String getBody1() {
        return body1;
    }

    public void setBody1(String body1) {
        this.body1 = body1;
    }

    public String getBody2() {
        return body2;
    }

    public void setBody2(String body2) {
        this.body2 = body2;
    }

    public String getMissingTitle1() {
        return missingTitle1;
    }

    public void setMissingTitle1(String missingTitle1) {
        this.missingTitle1 = missingTitle1;
    }

    public String getMissingTitle2() {
        return missingTitle2;
    }

    public void setMissingTitle2(String missingTitle2) {
        this.missingTitle2 = missingTitle2;
    }

    public String getMissingTitle3() {
        return missingTitle3;
    }

    public void setMissingTitle3(String missingTitle3) {
        this.missingTitle3 = missingTitle3;
    }

    public String getMissingDetail1() {
        return missingDetail1;
    }

    public void setMissingDetail1(String missingDetail1) {
        this.missingDetail1 = missingDetail1;
    }

    public String getMissingDetail2() {
        return missingDetail2;
    }

    public void setMissingDetail2(String missingDetail2) {
        this.missingDetail2 = missingDetail2;
    }

    public String getMissingDetail3() {
        return missingDetail3;
    }

    public void setMissingDetail3(String missingDetail3) {
        this.missingDetail3 = missingDetail3;
    }

    public void updateTravel(Travel travel) {
        if (travel == null) {
            return;
        }

        if (!TextUtils.isEmpty(travel.getTravelId())) {
            setTravelId(travel.getTravelId());
        }
        setTravel(travel.isTravel());
        setCreatePdf(travel.isCreatePdf());
        if (!TextUtils.isEmpty(travel.getReason())) {
            setReason(travel.getReason());
        }
        setBeamter(travel.isBeamter());
        if (!TextUtils.isEmpty(travel.getZiel())) {
            setZiel(travel.getZiel());
        }
        if (travel.getCreateTime() != null) {
            setCreateTime(travel.getCreateTime());
        }
        if (travel.getFirstPdfDate() != null) {
            setFirstPdfDate(travel.getFirstPdfDate());
        }
        if (!TextUtils.isEmpty(travel.getBeginCity())) {
            setBeginCity(travel.getBeginCity());
        }
        /*if (!TextUtils.isEmpty(travel.getEndCity())) {
            setEndCity(travel.getEndCity());
        }*/
        if (travel.getBeginDate() != null) {
            setBeginDate(travel.getBeginDate());
        }
        if (travel.getEndDate() != null) {
            setEndDate(travel.getEndDate());
        }
        if (travel.getAdvanceMoney() > 0) {
            setAdvanceMoney(travel.getAdvanceMoney());
        }
        setRadioUber(travel.getRadioUber());
        setRadioVerp(travel.getRadioVerp());
        setRadioBeleg(travel.getRadioBeleg());
        if (travel.getInklusiveFruhstuck() > 0) {
            setInklusiveFruhstuck(travel.getInklusiveFruhstuck());
        }
        if (travel.getOhneFruhstuck() > 0) {
            setOhneFruhstuck(travel.getOhneFruhstuck());
        }
        if (travel.getOhneRechnung() > 0) {
            setOhneRechnung(travel.getOhneRechnung());
        }
        if (travel.getUnentgeltlich() > 0) {
            setUnentgeltlich(travel.getUnentgeltlich());
        }
        if (!TextUtils.isEmpty(travel.getUberImages())) {
            setUberImages(travel.getUberImages());
        }
        if (!TextUtils.isEmpty(travel.getVerpflegung())) {
            setVerpflegung(travel.getVerpflegung());
        }
        if (travel.getPrivater() > 0) {
            setPrivater(travel.getPrivater());
        }
        if (!TextUtils.isEmpty(travel.getTaxiImages())) {
            setTaxiImages(travel.getTaxiImages());
        }
        if (!TextUtils.isEmpty(travel.getFahrtkostenImages())) {
            setFahrtkostenImages(travel.getFahrtkostenImages());
        }
        if (!TextUtils.isEmpty(travel.getMietwagenImages())) {
            setMietwagenImages(travel.getMietwagenImages());
        }
        if (!TextUtils.isEmpty(travel.getKraftstoffImages())) {
            setKraftstoffImages(travel.getKraftstoffImages());
        }
        if (!TextUtils.isEmpty(travel.getParkenImages())) {
            setParkenImages(travel.getParkenImages());
        }
        if (!TextUtils.isEmpty(travel.getTagungsImages())) {
            setTagungsImages(travel.getTagungsImages());
        }
        if (!TextUtils.isEmpty(travel.getNeben())) {
            setNeben(travel.getNeben());
        }
        if (!TextUtils.isEmpty(travel.getComment())) {
            setComment(travel.getComment());
        }
        setFavorite(travel.isFavorite());
        setVorlagen(travel.isVorlagen());
        if (!TextUtils.isEmpty(travel.getPdfName())) {
            setPdfName(travel.getPdfName());
        }
        setDraft(travel.isDraft());
        setUploading(travel.isUploading());
        setModify(travel.isModify());
        setStatus(travel.getStatus());
        if (!TextUtils.isEmpty(travel.getMessage())) {
            setMessage(travel.getMessage());
        }
        setEdit(travel.isEdit());
        setUploaded(travel.isUploaded());
        setTravelFrom(travel.getTravelFrom());
        if (!TextUtils.isEmpty(travel.getPdfMd5())) {
            setPdfMd5(travel.getPdfMd5());
        }
        if (!TextUtils.isEmpty(travel.getBody1())) {
            setBody1(travel.getBody1());
        }
        if (!TextUtils.isEmpty(travel.getBody2())) {
            setBody2(travel.getBody2());
        }
        if (!TextUtils.isEmpty(travel.getMissingTitle1())) {
            setMissingTitle1(travel.getMissingTitle1());
        }
        if (!TextUtils.isEmpty(travel.getMissingTitle2())) {
            setMissingTitle2(travel.getMissingTitle2());
        }
        if (!TextUtils.isEmpty(travel.getMissingTitle3())) {
            setMissingTitle3(travel.getMissingTitle3());
        }
        if (!TextUtils.isEmpty(travel.getMissingDetail1())) {
            setMissingDetail1(travel.getMissingDetail1());
        }
        if (!TextUtils.isEmpty(travel.getMissingDetail2())) {
            setMissingDetail2(travel.getMissingDetail2());
        }
        if (!TextUtils.isEmpty(travel.getMissingDetail3())) {
            setMissingDetail3(travel.getMissingDetail3());
        }
    }

    public void setTravel(TravelDbItem travelDbItem) {
        if (travelDbItem == null) {
            return;
        }

        setTravelId(travelDbItem.getTravelId());
        setTravel(travelDbItem.isTravel());
//        setCreatePdf(true);
        setReason(travelDbItem.getReason());
        setBeamter(travelDbItem.isBeamter());
        setZiel(travelDbItem.getZiel());
        setCreateTime(travelDbItem.getCreateTime());
        setFirstPdfDate(travelDbItem.getFirstPdfDate());
        setBeginCity(travelDbItem.getBeginCity());
//        setEndCity(travelDbItem.getEndCity());
        setBeginDate(travelDbItem.getBeginDate());
        setEndDate(travelDbItem.getEndDate());
        setAdvanceMoney(travelDbItem.getAdvanceMoney());
        setRadioUber(travelDbItem.getRadioUber());
        setRadioVerp(travelDbItem.getRadioVerp());
        setRadioBeleg(travelDbItem.getRadioBeleg());
        setInklusiveFruhstuck(travelDbItem.getInklusiveFruhstuck());
        setOhneFruhstuck(travelDbItem.getOhneFruhstuck());
        setOhneRechnung(travelDbItem.getOhneRechnung());
        setUnentgeltlich(travelDbItem.getUnentgeltlich());
        setUberImages(travelDbItem.getUberImages());
        setVerpflegung(travelDbItem.getVerpflegung());
        setPrivater(travelDbItem.getPrivater());
        setTaxiImages(travelDbItem.getTaxiImages());
        setFahrtkostenImages(travelDbItem.getFahrtkostenImages());
        setMietwagenImages(travelDbItem.getMietwagenImages());
        setKraftstoffImages(travelDbItem.getKraftstoffImages());
        setParkenImages(travelDbItem.getParkenImages());
        setTagungsImages(travelDbItem.getTagungsImages());
        setNeben(travelDbItem.getNeben());
        setComment(travelDbItem.getComment());
        setFavorite(travelDbItem.isFavorite());
        setVorlagen(travelDbItem.isVorlagen());
        setPdfName(travelDbItem.getPdfName());
        setDraft(travelDbItem.isDraft());
        setUploading(travelDbItem.isUploading());
        setModify(travelDbItem.isModify());
        setStatus(travelDbItem.getStatus());
        setMessage(travelDbItem.getMessage());
        setEdit(travelDbItem.isEdit());
        setUploaded(travelDbItem.isUploaded());
        setTravelFrom(travelDbItem.getTravelFrom());
        if (!TextUtils.isEmpty(travelDbItem.getPdfMd5())) {
            setPdfMd5(travelDbItem.getPdfMd5());
        }
        setBody1(travelDbItem.getBody1());
        setBody2(travelDbItem.getBody2());
        setMissingTitle1(travelDbItem.getMissingTitle1());
        setMissingTitle2(travelDbItem.getMissingTitle2());
        setMissingTitle3(travelDbItem.getMissingTitle3());
        setMissingDetail1(travelDbItem.getMissingDetail1());
        setMissingDetail2(travelDbItem.getMissingDetail2());
        setMissingDetail3(travelDbItem.getMissingDetail3());
    }
}
