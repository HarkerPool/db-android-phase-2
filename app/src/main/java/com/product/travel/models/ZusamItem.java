package com.product.travel.models;

/**
 * Created by HarkerPool on 6/16/16.
 */
public class ZusamItem {
    private String title;
    private String value;
    private boolean hasEdit;

    public ZusamItem() {

    }

    public ZusamItem(String title, String value, boolean hasEdit) {
        this.title = title;
        this.value = value;
        this.hasEdit = hasEdit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isHasEdit() {
        return hasEdit;
    }

    public void setHasEdit(boolean hasEdit) {
        this.hasEdit = hasEdit;
    }
}
