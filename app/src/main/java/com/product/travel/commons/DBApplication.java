package com.product.travel.commons;

import com.activeandroid.ActiveAndroid;

import com.product.travel.data.dao.TravelDbHelper;
import com.product.travel.interfaces.ServiceFactory;
import com.product.travel.services.ServiceFactoryImpl;
import com.product.travel.utils.LruBitmapCache;

/**
 * Created by HarkerPool on 6/3/16.
 */
public class DBApplication extends com.activeandroid.app.Application {

    private static DBApplication mInstance;

    private ServiceFactory mServiceFactory;

    private LruBitmapCache mLruBitmapCache;

    @Override
    public void onCreate() {
        try {
            super.onCreate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        ActiveAndroid.initialize(this);
        TravelDbHelper.saveCities(this);

        mInstance = this;

        mServiceFactory = new ServiceFactoryImpl();

        if (mLruBitmapCache == null) {
            mLruBitmapCache = new LruBitmapCache();
        }
    }

    public static DBApplication getInstance() {
        return mInstance;
    }

    public static ServiceFactory getServiceFactory() {
        return mInstance.mServiceFactory;
    }

    public static LruBitmapCache getBitmapCache() {
        return mInstance.mLruBitmapCache;
    }
}
