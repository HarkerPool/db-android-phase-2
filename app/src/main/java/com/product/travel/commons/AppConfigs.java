package com.product.travel.commons;

/**
 * Created by HarkerPool on 5/17/16.
 */
public class AppConfigs {

//    public static final String SERVER_URL = "https://dbrk-smart-integ.sps.de.swisspost.com";
//    private static final String APPLICATION_URL = SERVER_URL + "/index.cfm/api4/";
//    public static final String SERVER_URL = "http://dbrk-smart.sps.de.swisspost.com";
    public static final String SERVER_URL = "http://dbrk-temp.sps-delivery.de";
//    public static final String SERVER_URL = "http://172.16.0.96:50870";/
//    public static final String SERVER_URL = "http://dbtravel-server.rasi/a.wiki";
    private static final String APPLICATION_URL = SERVER_URL + "/index.cfm/api/";

    public static final String PDF_URL = SERVER_URL + "/uploads/";
    public static final String ACTIVATION_URL = APPLICATION_URL + "activate";
    public static final String ACTIVATION_LOG_URL = APPLICATION_URL + "activateLog";
    public static final String GET_USER_INFO_URL = APPLICATION_URL + "getUserInfo";
    public static final String UPDATE_USER_INFO_URL = APPLICATION_URL + "updateUserInfo";
    public static final String CREATE_TRAVEL_URL = APPLICATION_URL + "updateTravel";
    public static final String UPLOAD_IMAGE_URL = APPLICATION_URL + "uploadFile"; // TODO using for phase 3
//    public static final String UPLOAD_IMAGE_URL = APPLICATION_URL + "uploadFile1"; // TODO using for phase 2
    public static final String CREATE_PDF_URL = APPLICATION_URL + "createPdfByHand";
    public static final String SESSION_HAS_EXPIRED_URL = APPLICATION_URL + "session";
    public static final String UPDATE_STATUS_URL = APPLICATION_URL + "updateStatus";
    public static final String GET_TRAVEL_BY_ID_URL = APPLICATION_URL + "getTravel";
    public static final String GET_ALL_EMAIL_URL = APPLICATION_URL + "getEmail";
    public static final String GET_EMAIL_BY_TRAVEL_URL = APPLICATION_URL + "getEmailByTravel";
    public static final String UPDATE_EMAIL_URL = APPLICATION_URL + "updateEmail";
    public static final String DELETE_EMAIL_URL = APPLICATION_URL + "deleteEmail";
    public static final String DELETE_TRAVEL_URL = APPLICATION_URL + "deleteTravel";
    public static final String DELETE_IMAGE_URL = APPLICATION_URL + "deleteFile";
    public static final String CREATE_PASSWORD_URL = APPLICATION_URL + "passCreate";
    public static final String RESET_PASSWORD_URL = APPLICATION_URL + "passRecovery";
    public static final String CHANGE_PASSWORD_URL = APPLICATION_URL + "passChange";
    public static final String DELETE_PASSWORD_URL = APPLICATION_URL + "passDelete";
    public static final String CLEAR_UPLOADING_URL = APPLICATION_URL + "clearUploading";
    public static final String DELETE_USER_URL = APPLICATION_URL + "deleteUser";
    public static final String GET_VERSION_URL = APPLICATION_URL + "getVersion";

    public static final String TITLE_URL = "TITLE_URL";
//    public static final String E_POST_SELECT_URL = "https://apps.post.ch/epostselect/main.seam?cid=536";
    public static final String FAQ_URL = "http://www1.deutschebahn.com/db-personalservice/aktuelles/rahmenrichtlinie.html";
    public static final String REISEAUSKUNFT_URL = "http://www.bahn.de/m/view/de/index.shtml";
    public static final String APP_STORE_URL = "https://play.google.com/store/apps/details?id=";
    public static final String INTERNAL_URL_KEY = "INTERNAL_URL_KEY";
    public static final String IS_FIRST_ACTIVATE = "IS_FIRST_ACTIVATE";
    public static final String IS_FROM_ZUSAM = "IS_FROM_ZUSAM";
    public static final String IS_SENT_REISE = "IS_SENT_REISE";
    public static final String HAS_DATE_CHANGED = "HAS_DATE_CHANGED";
    public static final String HAS_IMAGE = "HAS_IMAGE";
    public static final String HAS_CANCELED = "HAS_CANCELED";
    public static final String NACHRICHTEN_DETAIL_BODY = "NACHRICHTEN_DETAIL_BODY";
    public static final String NACHRICHTEN_DETAIL_TRAVEL_ID = "NACHRICHTEN_DETAIL_TRAVEL_ID";

    public static final String TRAVEL_ID_KEY = "TRAVEL_ID_KEY";
    public static final String SCANNED_RESULT = "SCANNED_RESULT";

    public static final String EXIT_APP = "EXIT_APP";

    public static final int REQUEST_CAMERA_CAPTURE = 6;
    public static final int IMAGE_RESULT = 7;

    public enum MENU_ITEM {
        MENU_ITEM_KEY, HOME, PERSONAL, HILFE, MAPS, REISEAUSKUNFT, EINSTELLUNGEN, EPOSTSELECT, IMPRESSUM, ABBRUCH_BEENDEN
    }

    public enum MEAL {
        BREAKFAST, LUNCH, DINNER
    }

    public enum BELEG_ITEM {
        TITLE, TAXI, FAHRTKOSTEN, MIETWAGEN, KRAFTSTOFF, PARKEN, TAGUNGSPAUSCHALE
    }

    public enum PASSWORD_HANDLER {
        KEY, CREATE, CHANGE, DELETE
    }
}
