ALTER TABLE Travels ADD COLUMN body_1 TEXT;
ALTER TABLE Travels ADD COLUMN body_2 TEXT;
ALTER TABLE Travels ADD COLUMN missing_title_1 TEXT;
ALTER TABLE Travels ADD COLUMN missing_title_2 TEXT;
ALTER TABLE Travels ADD COLUMN missing_title_3 TEXT;
ALTER TABLE Travels ADD COLUMN missing_detail_1 TEXT;
ALTER TABLE Travels ADD COLUMN missing_detail_2 TEXT;
ALTER TABLE Travels ADD COLUMN missing_detail_3 TEXT;